﻿using System;
using System.Web;
using VSW.Core.Global;

namespace VSW.Lib.Global
{
    public class Cookies
    {
        private static string SiteId => Core.Web.Setting.Sys_SiteID + "_";

        public static bool Exists(string key)
        {
            key = SiteId + key;

            return HttpContext.Current.Request.Cookies[key] != null;
        }

        public static void SetValue(string key, string value, int minutes, bool secure)
        {
            key = SiteId + key;

            var IP = HttpContext.Current.Request.UserHostAddress;

            var current = HttpContext.Current;

            if (IP != "127.0.0.1" && Core.Web.Setting.Mod_DomainCookies != string.Empty)
                current.Response.Cookies[key].Domain = Core.Web.Setting.Mod_DomainCookies;

            current.Response.Cookies[key].Value = secure ? CryptoString.Encrypt(HttpUtility.UrlEncode(IP + "_VSW_" + key + value)) : HttpUtility.UrlEncode(value);

            if (minutes > 0)
                current.Response.Cookies[key].Expires = DateTime.Now.AddMinutes(minutes);
        }

        public static void SetValue(string key, string value, int minutes)
        {
            SetValue(key, value, minutes, false);
        }

        public static void SetValue(string key, string value, bool secure)
        {
            SetValue(key, value, 0, secure);
        }

        public static void SetValue(string key, string value)
        {
            SetValue(key, value, 0, false);
        }

        public static string GetValue(string key, bool secure)
        {
            if (!Exists(key)) return string.Empty;

            var current = HttpContext.Current;

            var keyCookie = SiteId + key;

            if (!secure)
                return HttpUtility.UrlDecode(current.Request.Cookies[keyCookie].Value);

            var IP = HttpContext.Current.Request.UserHostAddress;

            var s = HttpUtility.UrlDecode(CryptoString.Decrypt(current.Request.Cookies[keyCookie].Value)).Replace(IP + "_VSW_" + keyCookie, string.Empty);

            if (s.IndexOf("_VSW_" + keyCookie, StringComparison.Ordinal) <= -1)
                return HttpUtility.UrlDecode(s);

            Remove(key);

            return string.Empty;
        }

        public static string GetValue(string key)
        {
            return GetValue(key, false);
        }

        public static void Remove(string key)
        {
            if (!Exists(key)) return;

            key = SiteId + key;

            var current = HttpContext.Current;

            //ip
            var IP = HttpContext.Current.Request.UserHostAddress;

            //local
            if (IP != "127.0.0.1" && Core.Web.Setting.Mod_DomainCookies != string.Empty)
                current.Response.Cookies[key].Domain = Core.Web.Setting.Mod_DomainCookies;

            //remove
            current.Response.Cookies[key].Value = string.Empty;
            current.Response.Cookies[key].Expires = DateTime.Now.AddDays(-1);
        }
    }
}