﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Aspose.Cells;
using VSW.Lib.Models;
using System.IO;

namespace VSW.Lib.Global
{
    public class Excel
    {
        public static void Export(List<List<object>> list, int start_row, string sourceFile, string exportFile)
        {
            if (list == null) return;

            Workbook workbook = new Workbook();
            workbook.Open(sourceFile);
            Cells cells = workbook.Worksheets[0].Cells;

            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    cells[start_row + i, j].PutValue(list[i][j]);
                }
            }

            workbook.Save(exportFile);
        }

        public static void ImportCity(string file, int langID)
        {
            if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

            FileStream fstream = new FileStream(file, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

            for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
            {
                string _Name = worksheet.Cells[i, 0].StringValue.Trim();
                string _Code = Data.GetCode(_Name);

                var _item = WebMenuService.Instance.CreateQuery()
                                                .Where(o => o.Activity == true && o.Type == "City" && o.Code == _Code)
                                                .ToSingle_Cache();
                if (_item == null)
                {
                    _item = new WebMenuEntity();

                    _item.Name = _Name;
                    _item.Code = _Code;
                    _item.Type = "City";
                    _item.CityCode = worksheet.Cells[i, 1].StringValue.Trim();

                    _item.ParentID = 8;
                    _item.LangID = langID;

                    _item.Order = GetMaxOrder(langID, 8);
                    _item.Activity = true;

                    WebMenuService.Instance.Save(_item);
                }
            }

            fstream.Close();
            fstream.Dispose();
        }

        public static void ImportDistrict(string file, int langID)
        {
            if (!file.EndsWith(".xls") && !file.EndsWith(".xlsx")) return;

            FileStream fstream = new FileStream(file, FileMode.Open);
            Workbook workbook = new Workbook();
            workbook.Open(fstream);
            Worksheet worksheet = workbook.Worksheets[0];

            if (worksheet == null || worksheet.Cells.MaxRow < 1) return;

            for (int i = 1; i <= worksheet.Cells.MaxRow; i++)
            {
                string _Name = worksheet.Cells[i, 2].StringValue.Trim();
                string _Code = Data.GetCode(_Name);
                string _CityCode = worksheet.Cells[i, 1].StringValue.Trim();

                var _item = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.Type == "City" && o.Code == _Code)
                                    .ToSingle_Cache();

                var parent = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.Type == "City" && o.CityCode == _CityCode)
                                    .ToSingle_Cache();

                if (_item == null)
                {
                    _item = new WebMenuEntity();

                    _item.Name = _Name;
                    _item.Code = _Code;
                    _item.Type = "City";

                    _item.ParentID = parent != null ? parent.ID : 5;
                    _item.LangID = langID;

                    _item.Order = GetMaxOrder(langID, _item.ParentID);
                    _item.Activity = true;

                    WebMenuService.Instance.Save(_item);
                }
            }

            fstream.Close();
            fstream.Dispose();
        }

        private static int GetMaxOrder(int langID, int parentID)
        {
            return WebMenuService.Instance.CreateQuery()
                            .Where(o => o.LangID == langID && o.ParentID == parentID)
                            .Max(o => o.Order)
                            .ToValue().ToInt(0) + 1;
        }
    }
}
