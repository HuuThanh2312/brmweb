﻿using System;

namespace VSW.Lib.Global
{
    public class Application
    {
        public static void OnError()
        {
            Exception ex;
            Core.Web.HttpRequest.OnError(out ex);

            if (ex.InnerException != null)
            {
                Error.Write(ex.InnerException.InnerException ?? ex.InnerException);
            }
            else
                Error.Write(ex);
        }
    }
}