﻿using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Version = Lucene.Net.Util.Version;

namespace VSW.Lib.Global
{
    public static class SearchIndex
    {
        #region properties
        public static string _luceneDir = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "~/Data/Search/lucene.index");
        private static FSDirectory _directoryTemp;
        private static FSDirectory _directory
        {
            get
            {
                if (_directoryTemp == null) _directoryTemp = FSDirectory.Open(new DirectoryInfo(_luceneDir));
                if (IndexWriter.IsLocked(_directoryTemp)) IndexWriter.Unlock(_directoryTemp);
                var lockFilePath = Path.Combine(_luceneDir, "~/Data/Search/write.lock");
                if (File.Exists(lockFilePath)) File.Delete(lockFilePath);
                return _directoryTemp;
            }
        }
        #endregion

        #region search methods
        public static List<SearchData> GetAll()
        {
            //if(System.IO.Directory.GetFiles(_luceneDir) == null) return new List<SearchData>();

            if (!System.IO.Directory.EnumerateFiles(_luceneDir).Any()) return new List<SearchData>();
            
            var searcher = new IndexSearcher(_directory, false);
            var reader = IndexReader.Open(_directory, false);
            var docs = new List<Document>();
            var term = reader.TermDocs();

            while (term.Next()) docs.Add(searcher.Doc(term.Doc));
            reader.Dispose();
            searcher.Dispose();
            return _mapLuceneToDataList(docs);
        }

        public static List<SearchData> Search(string input, string fieldName = "")
        {
            if (string.IsNullOrEmpty(input)) return new List<SearchData>();

            var terms = input.Trim().Replace("-", " ").Split(' ').Where(o => !string.IsNullOrEmpty(o)).Select(o => o.Trim() + "*");

            input = string.Join(" ", terms);

            return _search(input, fieldName);
        }

        public static List<SearchData> SearchDefault(string input, string fieldName = "")
        {
            return string.IsNullOrEmpty(input) ? new List<SearchData>() : _search(input, fieldName);
        }
        #endregion

        #region private
        // main search method
        private static List<SearchData> _search(string searchQuery, string searchField = "")
        {
            // validation
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return new List<SearchData>();

            // set up lucene searcher
            using (var searcher = new IndexSearcher(_directory, false))
            {
                var hits_limit = 1000;
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);

                // search by single field
                if (!string.IsNullOrEmpty(searchField))
                {
                    var parser = new QueryParser(Version.LUCENE_30, searchField, analyzer);
                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, hits_limit).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
                // search by multiple fields (ordered by RELEVANCE)
                else
                {
                    var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "Id", "Name", "Description" }, analyzer);

                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, null, hits_limit, Sort.INDEXORDER).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
            }
        }

        private static Query parseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }
        
        // map Lucene search index to data
        private static List<SearchData> _mapLuceneToDataList(List<Document> hits)
        {
            return hits.Select(_mapLuceneDocumentToData).ToList();
        }

        private static List<SearchData> _mapLuceneToDataList(List<ScoreDoc> hits, IndexSearcher searcher)
        {
            // v 2.9.4: use 'hit.doc'
            // v 3.0.3: use 'hit.Doc'
            return hits.Select(o => _mapLuceneDocumentToData(searcher.Doc(o.Doc))).ToList();
        }

        private static SearchData _mapLuceneDocumentToData(Document doc)
        {
            return new SearchData
            {
                Id = Convert.ToInt32(doc.Get("Id")),
                Name = doc.Get("Name"),
                Description = doc.Get("Description")
            };
        }

        private static void _addToLuceneIndex(SearchData item, IndexWriter writer)
        {
            // remove older index entry
            var searchQuery = new TermQuery(new Term("ID", item.ID.ToString()));
            writer.DeleteDocuments(searchQuery);

            // add new index entry
            var doc = new Document();

            // add lucene fields mapped to db fields
            doc.Add(new Field("ID", item.ID.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Type", item.Type, Field.Store.YES, Field.Index.ANALYZED));

            doc.Add(new Field("Name", item.Name, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("URL", item.URL, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("File", item.File, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("Summary", item.Summary, Field.Store.YES, Field.Index.ANALYZED));

            doc.Add(new Field("DisplayName", item.DisplayName, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("DisplayURL", item.DisplayURL, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("DisplayFile", item.DisplayFile, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("DisplaySummary", item.DisplaySummary, Field.Store.YES, Field.Index.ANALYZED));

            // add entry to index
            writer.AddDocument(doc);
        }

        #endregion

        #region search data
        public static void InsertOrUpdate(SearchData item)
        {
            InsertOrUpdate(new List<SearchData> { item });
        }

        public static void InsertOrUpdate(List<SearchData> listItem)
        {
            // init lucene
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                // add data to lucene search index (replaces older entries if any)
                foreach (var item in listItem) _addToLuceneIndex(item, writer);

                // close handles
                analyzer.Close();
                writer.Dispose();
            }
        }

        public static void Remove(int record_id)
        {
            // init lucene
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                // remove older index entry
                var searchQuery = new TermQuery(new Term("ID", record_id.ToString()));
                writer.DeleteDocuments(searchQuery);

                // close handles
                analyzer.Close();
                writer.Dispose();
            }
        }

        public static bool Remove()
        {
            try
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(_directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    // remove older index entries
                    writer.DeleteAll();

                    // close handles
                    analyzer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public static void Optimize()
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
        }

        #endregion

        public class SearchData
        {
            public int ID { get; set; }
            public string Type { get; set; }

            public string Name { get; set; }
            public string URL { get; set; }
            public string File { get; set; }
            public string Summary { get; set; }

            public string DisplayName { get; set; }
            public string DisplayURL { get; set; }
            public string DisplayFile { get; set; }
            public string DisplaySummary { get; set; }
        }
    }
}