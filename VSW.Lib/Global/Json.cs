﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace VSW.Lib.Global
{
    public class Json
    {
        public class JsonModel
        {
            public string Html { get; set; }
            public string Params { get; set; }
            public string Js { get; set; }
        }

        public static readonly JsonModel Instance = new JsonModel { Html = string.Empty, Params = string.Empty, Js = string.Empty };

        public static string JsonSerializer<T>(T t)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            var memoryStream = new MemoryStream();
            serializer.WriteObject(memoryStream, t);
            var jsonString = Encoding.UTF8.GetString(memoryStream.ToArray());
            memoryStream.Close();

            return jsonString;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            var obj = (T)serializer.ReadObject(memoryStream);

            return obj;
        }

        public static void Create()
        {
            var json = JsonSerializer(Instance);

            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.ContentType = "application/json; charset=utf-8";
            System.Web.HttpContext.Current.Response.Write(json);
            System.Web.HttpContext.Current.Response.End();
        }

        public static string GetResponse(string url)
        {
            var uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Get;

            var response = (HttpWebResponse)request.GetResponse();
            var responseStream = response.GetResponseStream();
            if (responseStream == null) return string.Empty;

            var reader = new StreamReader(responseStream);
            var output = reader.ReadToEnd();

            response.Close();

            return output;
        }
    }
}