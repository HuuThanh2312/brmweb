﻿using VSW.Core.Global;

namespace VSW.Lib.Global
{
    public class Setting : Core.Web.Setting
    {
        //facebook
        public static string FacebookClientId = Config.GetValue("Facebook.ClientID").ToString();

        public static string FacebookClientSecret = Config.GetValue("Facebook.ClientSecret").ToString();
        public static string FacebookRedirectUri = Config.GetValue("Facebook.RedirectUri").ToString();
        
        //google
        public static string GoogleClientId = Config.GetValue("Google.ClientID").ToString();

        public static string GoogleClientSecret = Config.GetValue("Google.ClientSecret").ToString();
        public static string GoogleRedirectUri = Config.GetValue("Google.RedirectUri").ToString();

        //SAPrice
        public static string BerichSAPrice = Config.GetValue("Berich.SAPrice").ToString();
    }
}