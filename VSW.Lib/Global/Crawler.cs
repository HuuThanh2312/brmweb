﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using VSW.Lib.Global;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Data;
using System.Xml;
using System.Data.SqlClient;
using VSW.Lib.Models;
using System.Collections.Specialized;
using HtmlAgilityPack;
using System.Xml.Xsl;

namespace VSW.Lib.Global
{
    public class Crawler
    {
        #region insert database
        private static int GetMaxOrder()
        {
            return ModNewsService.Instance.CreateQuery()
                            .Max(o => o.Order)
                            .ToValue().ToInt(0) + 1;
        }

        private static bool Exists(string code)
        {
            return ModNewsService.Instance.CreateQuery().Where(o => o.Code == code).ToSingle() != null;
        }

        int Insert(
            int menuID,
            int langID,
            string name,
            string file,
            string summary,
            string content,
            SqlConnection conn
        )
        {
            //#region insert sql

            //SqlCommand cmd = new SqlCommand("INSERT INTO Mod_News(MenuID,Name,Code,State,Files,Summary,Content,Tags,NewsCode,AuthorID,TranslatorID,PublishersID,SizeNews,Weight,Publish,SellPrice,OriginPrice,Views,Created,Orders,Activity) VALUES(@MenuID,@Name,@Code,@State,@Files,@Summary,@Content,@Tags,@NewsCode,@AuthorID,@TranslatorID,@PublishersID,@SizeNews,@Weight,@Publish,@SellPrice,@OriginPrice,@Views,@Created,@Orders,@Activity)", conn);

            //cmd.Parameters.Clear();

            //int x=0;

            //cmd.Parameters.Add(new SqlParameter("@MenuID", menuId));
            //cmd.Parameters.Add(new SqlParameter("@Name", name));
            //cmd.Parameters.Add(new SqlParameter("@Code", Data.GetCode(name)));
            //cmd.Parameters.Add(new SqlParameter("@State", x));
            //cmd.Parameters.Add(new SqlParameter("@Files", file));
            //cmd.Parameters.Add(new SqlParameter("@Summary", ""));
            //cmd.Parameters.Add(new SqlParameter("@Content", content));
            //cmd.Parameters.Add(new SqlParameter("@Tags", ""));
            //cmd.Parameters.Add(new SqlParameter("@NewsCode", bookCode));
            //cmd.Parameters.Add(new SqlParameter("@AuthorID", authorId));
            //cmd.Parameters.Add(new SqlParameter("@TranslatorID", x));
            //cmd.Parameters.Add(new SqlParameter("@PublishersID", publisherId));
            //cmd.Parameters.Add(new SqlParameter("@SizeNews", sizeNews));
            //cmd.Parameters.Add(new SqlParameter("@Weight", weight));
            //cmd.Parameters.Add(new SqlParameter("@Publish", publish));
            //cmd.Parameters.Add(new SqlParameter("@SellPrice", price));
            //cmd.Parameters.Add(new SqlParameter("@OriginPrice", price));
            //cmd.Parameters.Add(new SqlParameter("@Views", x));
            //cmd.Parameters.Add(new SqlParameter("@Created", DateTime.Now));
            //cmd.Parameters.Add(new SqlParameter("@Orders", GetMaxOrder()));
            //cmd.Parameters.Add(new SqlParameter("@Activity", true));

            //cmd.ExecuteNonQuery();

            //#endregion

            #region insert C#
            int newsID = ModNewsService.Instance.Save(new ModNewsEntity()
            {
                MenuID = menuID,
                Name = name,
                Code = Data.GetCode(name),
                File = file,
                Summary = summary,
                Content = content,
                Published = DateTime.Now,
                Order = GetMaxOrder(),
                Activity = true
            });

            //update url
            ModCleanURLService.Instance.InsertOrUpdate(Data.GetCode(name), "News", newsID, menuID, langID);

            #endregion

            return newsID;
        }
        #endregion

        #region with ajax (not change url)
        HtmlDocument SubmitFormValues(NameValueCollection fv, string url)
        {
            // Attach a temporary delegate to handle attaching
            // the post back data
            HtmlWeb.PreRequestHandler handler = delegate (HttpWebRequest request)
            {
                var payload = AssemblePostPayload(fv);
                var buff = Encoding.ASCII.GetBytes(payload.ToCharArray());

                request.ContentLength = buff.Length;
                request.ContentType = "application/x-www-form-urlencoded";

                var reqStream = request.GetRequestStream();
                reqStream.Write(buff, 0, buff.Length);

                return true;
            };

            var web = new HtmlWeb();
            web.PreRequest += handler;
            var doc = web.Load(url, "POST");
            web.PreRequest -= handler;

            return doc;
        }
        
        private static string AssemblePostPayload(NameValueCollection fv)
        {
            var sb = new StringBuilder();
            foreach (var key in fv.AllKeys)
            {
                sb.Append("/" + key + "/" + fv.Get(key));
            }
            return sb.ToString().Substring(1);
        }

        public class GZipWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri address)
            {
                var request = (HttpWebRequest)base.GetWebRequest(address);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                return request;
            }
        }

        #endregion

        #region base
        private static HtmlDocument StripHtmlTags(string str)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(str);

            return doc;
        }
        #endregion

        #region gzip
        //gzip
        //public class GZipWebClient : WebClient
        //{
        //    protected override WebRequest GetWebRequest(Uri address)
        //    {
        //        HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
        //        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        //        return request;
        //    }
        //}

        //method with gzip
        //public int InsertLangVanHoa(string url, int menuId, int langID, int countPage, string dataUrl, string desUrl)
        //{
        //    int page = 0;
        //    int Count = 0;

        //    //khoi tao + mo ket noi database
        //    SqlConnection conn = new SqlConnection(VSW.Core.Data.ConnectionString.Default);
        //    conn.Open();

        //    HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
        //    HtmlAgilityPack.HtmlDocument doc = new HtmlDocument();

        //    using (var wc = new GZipWebClient())
        //    {
        //        wc.Encoding = Encoding.UTF8;
        //        string html = wc.DownloadString(url);
        //        doc.LoadHtml(html);
        //    }

        //    while (page < countPage)
        //    {
        //        //xu ly phan trang
        //        page++;
        //        //string urlPage = url.Replace(".html", "") + "/page/" + page + ".html";

        //        if (doc != null)
        //        {
        //            //tach 1 tin
        //            var listNode = doc.DocumentNode.SelectNodes("//ul[@class=\"layout-news\"]/li");

        //            for (int i = 0; listNode != null && i < listNode.Count; i++)
        //            {
        //                string title = string.Empty, summary = string.Empty, file = string.Empty, content = string.Empty, detailUrl = string.Empty;

        //                HtmlDocument temp = StripHTMLTags(listNode[i].InnerHtml);

        //                //get url
        //                var _NodeTitle = temp.DocumentNode.SelectNodes("//span[@class=\"head\"]/strong/a");
        //                if (_NodeTitle == null) continue;

        //                detailUrl = "http://langvanhoa.vn" + _NodeTitle[0].Attributes["href"].Value.Trim();
        //                title = _NodeTitle[0].InnerText.Trim();

        //                HtmlAgilityPack.HtmlDocument _DocDetail = new HtmlDocument();
        //                using (var wc = new GZipWebClient())
        //                {
        //                    wc.Encoding = Encoding.UTF8;
        //                    string html = wc.DownloadString(detailUrl);
        //                    _DocDetail.LoadHtml(html);
        //                }

        //                if (_DocDetail == null) continue;

        //                //tach thanh phan tin
        //                var _NodeSummary = temp.DocumentNode.SelectNodes("//span[@class=\"head\"]/p");
        //                if (_NodeSummary != null) summary = _NodeSummary[0].InnerText.Trim();

        //                var _NodeFile = temp.DocumentNode.SelectNodes("//img");
        //                if (_NodeFile != null) file = "http://langvanhoa.vn" + _NodeFile[0].Attributes["src"].Value.Trim().Replace("@", "");

        //                string[] arrFile = !string.IsNullOrEmpty(file) ? file.Split('/') : null;
        //                string fileData = !string.IsNullOrEmpty(file) ? dataUrl + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
        //                string fileDes = !string.IsNullOrEmpty(file) ? desUrl + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
        //                if (!string.IsNullOrEmpty(file)) Global.DownloadImage.SaveImage(file, fileDes);

        //                //get content
        //                var _NodeDetail = _DocDetail.DocumentNode.SelectNodes("//p[@class=\"kBody\"]");
        //                if (_NodeDetail != null) content = _NodeDetail[0].InnerHtml.Trim();

        //                //insert database
        //                int value = Insert(menuId, langID, title, fileData, summary, content, conn);
        //                if (value > 0) Count++;
        //            }
        //        }
        //    }

        //    //dong ket noi database
        //    conn.Close();
        //    return Count;
        //}
        #endregion

        #region mediamart
        public static string GetMediaMart(string url)
        {
            HtmlWeb htmlWeb = new HtmlWeb();
            HtmlDocument htmlDocument = htmlWeb.Load(url);

            if (htmlDocument != null)
            {
                var nodeHtmlPrice = htmlDocument.DocumentNode.SelectNodes("//div[@class=\"draw-price-content\"]");
                if (nodeHtmlPrice != null)
                {
                    string result = string.Empty;

                    var htmlPrice = nodeHtmlPrice[0].InnerHtml;
                    var ArrHtml = Regex.Split(htmlPrice, "</span>");
                    for (int i = 0; ArrHtml != null && i < ArrHtml.Length; i++)
                    {
                        if (ArrHtml[i].Contains("-dot") || ArrHtml[i].Contains("-dash")) continue;
                        string html = ArrHtml[i].Replace("<span class=\"", "").Replace("\">", "").Replace("\n","").Replace("\r", "").Trim();
                        if (!string.IsNullOrEmpty(html)) result += html.Substring(html.Length - 1);
                    }

                    return string.Format("{0:#,##0}", VSW.Core.Global.Convert.ToLong(result));
                }
            }

            return "Không lấy được giá. Nhập lại URL";
        }
        #endregion

        #region lazada
        public static string GetLazada(string url)
        {
            HtmlWeb htmlWeb = new HtmlWeb();
            HtmlDocument htmlDocument = htmlWeb.Load(url);

            if (htmlDocument != null)
            {
                var nodeHtmlPrice = htmlDocument.DocumentNode.SelectNodes("//span[@id=\"special_price_box\"]");
                if (nodeHtmlPrice != null)
                {
                    var htmlPrice = nodeHtmlPrice[0].InnerHtml;

                    return htmlPrice;
                }
            }

            return "Không lấy được giá. Nhập lại URL";
        }
        #endregion

        #region 2sao
        public int Insert2sao(string url, int menuID, int langID, int totalPage, string dataURL, string desURL)
        {
            int page = 0;
            int count = 0;

            //khoi tao + mo ket noi database
            var conn = new SqlConnection(VSW.Core.Data.ConnectionString.Default);
            conn.Open();

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();

            while (page < totalPage)
            {
                //xu ly phan trang
                page++;
                string urlPage = url.Replace(".vnn", "") + "/trang-" + page + ".vnn";
                doc = web.Load(urlPage);

                if (doc != null)
                {
                    //tach 1 tin
                    var listNode = doc.DocumentNode.SelectNodes("//div[@id=\"listNews\"]/div/div");

                    for (int i = 0; listNode != null && i < listNode.Count; i++)
                    {
                        string title = string.Empty, summary = string.Empty, file = string.Empty, content = string.Empty, detailUrl = string.Empty;

                        HtmlDocument temp = StripHtmlTags(listNode[i].InnerHtml);

                        //get url
                        var _NodeTitle = temp.DocumentNode.SelectNodes("//h2/a");
                        if (_NodeTitle == null) continue;

                        detailUrl = "http://2sao.vn" + _NodeTitle[0].Attributes["href"].Value.Trim();
                        title = _NodeTitle[0].InnerText.Trim();

                        var detail = new HtmlAgilityPack.HtmlWeb();
                        var _DocDetail = detail.Load(detailUrl);

                        if (_DocDetail == null) continue;

                        //tach thanh phan tin
                        var _NodeSummary = temp.DocumentNode.SelectNodes("//p");
                        if (_NodeSummary != null) summary = _NodeSummary[0].InnerText.Trim().Replace("(2Sao) - ", "");

                        var _NodeFile = temp.DocumentNode.SelectNodes("//img");
                        if (_NodeFile != null) file = _NodeFile[0].Attributes["src"].Value.Trim();

                        string[] arrFile = !string.IsNullOrEmpty(file) ? file.Split('/') : null;
                        string fileData = !string.IsNullOrEmpty(file) ? dataURL + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
                        string fileDes = !string.IsNullOrEmpty(file) ? desURL + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
                        if (!string.IsNullOrEmpty(file)) Global.DownloadImage.SaveImage(file, fileDes);

                        //get content
                        var _NodeDetail = _DocDetail.DocumentNode.SelectNodes("//span[@id=\"advenueINTEXT\"]");
                        if (_NodeDetail != null) content = _NodeDetail[0].InnerHtml.Trim();

                        //insert database
                        int value = Insert(menuID, langID, title, fileData, summary, content, conn);
                        if (value > 0) count++;
                    }
                }
            }

            //dong ket noi database
            conn.Close();
            return count;
        }
        #endregion

        #region ngoisao
        public int InsertNgoisao(string url, int menuID, int langID, int totalPage, string dataURL, string desURL)
        {
            int page = 0;
            int count = 0;

            //khoi tao + mo ket noi database
            var conn = new SqlConnection(VSW.Core.Data.ConnectionString.Default);
            conn.Open();

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();

            while (page < totalPage)
            {
                //xu ly phan trang
                page++;
                string urlPage = url + "trang-" + page;
                doc = web.Load(urlPage);

                if (doc != null)
                {
                    //tach 1 tin
                    var listNode = doc.DocumentNode.SelectNodes("//div[@id='cate-items']/div[contains(@class, 'cate-item')]");

                    for (int i = 0; listNode != null && i < listNode.Count; i++)
                    {
                        string title = string.Empty, summary = string.Empty, file = string.Empty, content = string.Empty, detailUrl = string.Empty;

                        HtmlDocument temp = StripHtmlTags(listNode[i].InnerHtml);

                        //get url
                        var _NodeTitle = temp.DocumentNode.SelectNodes("//h3/a");
                        if (_NodeTitle == null) continue;

                        detailUrl = "http://ngoisao.vn/" + _NodeTitle[0].Attributes["href"].Value.Trim();
                        title = _NodeTitle[0].InnerText.Trim();

                        var detail = new HtmlAgilityPack.HtmlWeb();
                        var _DocDetail = detail.Load(detailUrl);

                        if (_DocDetail == null) continue;

                        //tach thanh phan tin
                        var _NodeSummary = temp.DocumentNode.SelectNodes("//div[@class='sapo']");
                        if (_NodeSummary != null) summary = _NodeSummary[0].InnerText.Replace("(Ngoisao.vn) -", "").Trim();

                        var _NodeFile = temp.DocumentNode.SelectNodes("//a/img");
                        if (_NodeFile != null) file = _NodeFile[0].Attributes["src"].Value.Trim().Replace("resize_210x126/", "");

                        string[] arrFile = !string.IsNullOrEmpty(file) ? file.Split('/') : null;
                        string fileData = !string.IsNullOrEmpty(file) ? dataURL + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
                        string fileDes = !string.IsNullOrEmpty(file) ? desURL + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
                        if (!string.IsNullOrEmpty(file)) Global.DownloadImage.SaveImage(file, fileDes);

                        //get content
                        var _NodeDetail = _DocDetail.DocumentNode.SelectNodes("//div[@class='detail-content']");
                        if (_NodeDetail != null) content = _NodeDetail[0].InnerHtml.Trim();

                        //insert database
                        int value = Insert(menuID, langID, title, fileData, summary, content, conn);
                        if (value > 0) count++;
                    }
                }
            }

            //dong ket noi database
            conn.Close();
            return count;
        }
        #endregion

        #region vietgiaitri
        public int InsertVietgiaitri(string url, int menuID, int langID, int totalPage, string dataURL, string desURL)
        {
            int page = 0;
            int count = 0;

            //khoi tao + mo ket noi database
            var conn = new SqlConnection(VSW.Core.Data.ConnectionString.Default);
            conn.Open();

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();

            while (page < totalPage)
            {
                //xu ly phan trang
                page++;
                string urlPage = url + "page/" + page;
                doc = web.Load(urlPage);

                if (doc != null)
                {
                    //tach 1 tin
                    var listNode = doc.DocumentNode.SelectNodes("//div[contains(@class, 'content-block')]/div[contains(@class, 'post-content-archive')]");

                    for (int i = 0; listNode != null && i < listNode.Count; i++)
                    {
                        string title = string.Empty, summary = string.Empty, file = string.Empty, content = string.Empty, detailUrl = string.Empty;

                        HtmlDocument temp = StripHtmlTags(listNode[i].InnerHtml);

                        //get url
                        var _NodeTitle = temp.DocumentNode.SelectNodes("//h3/a");
                        if (_NodeTitle == null) continue;

                        detailUrl = "http://www.vietgiaitri.com" + _NodeTitle[0].Attributes["href"].Value.Trim();
                        title = _NodeTitle[0].InnerText.Trim();

                        var detail = new HtmlAgilityPack.HtmlWeb();
                        var _DocDetail = detail.Load(detailUrl);

                        if (_DocDetail == null) continue;

                        //tach thanh phan tin
                        summary = listNode[i].InnerText;

                        var _NodeFile = temp.DocumentNode.SelectNodes("//img[@class='post-thumbnail']");
                        if (_NodeFile != null) file = _NodeFile[0].Attributes["src"].Value.Trim().Replace("-127x91", "");

                        string[] arrFile = !string.IsNullOrEmpty(file) ? file.Split('/') : null;
                        string fileData = !string.IsNullOrEmpty(file) ? dataURL + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
                        string fileDes = !string.IsNullOrEmpty(file) ? desURL + (arrFile != null ? arrFile[arrFile.Length - 1] : "") : string.Empty;
                        if (!string.IsNullOrEmpty(file)) Global.DownloadImage.SaveImage(file, fileDes);

                        //get content
                        var _NodeDetail = _DocDetail.DocumentNode.SelectNodes("//div[@itemprop='articleBody']");
                        if (_NodeDetail != null) content = _NodeDetail[0].InnerHtml.Trim();

                        //bo mo ta
                        var _NodeSummaryContent = _DocDetail.DocumentNode.SelectNodes("//div[@itemprop='articleBody']/p[1]");
                        if (_NodeSummaryContent != null) content = content.Replace(_NodeSummaryContent[0].InnerHtml, "");

                        //if (!string.IsNullOrEmpty(summary)) content = content.Replace(summary, "");

                        //bo iframe
                        var _NodeIFrame = _DocDetail.DocumentNode.SelectNodes("//div[@itemprop='articleBody']/div[@id='abd_vidinpage']");
                        if (_NodeIFrame != null) content = content.Replace(_NodeIFrame[0].InnerHtml, "");

                        //bo tin lien quan
                        var _NodeRelative = _DocDetail.DocumentNode.SelectNodes("//div[@itemprop='articleBody']/div[contains(@class, 'more-post')]");
                        if (_NodeRelative != null) content = content.Replace(_NodeRelative[0].InnerHtml, "");

                        //insert database
                        int value = Insert(menuID, langID, title, fileData, summary, content, conn);
                        if (value > 0) count++;
                    }
                }
            }

            //dong ket noi database
            conn.Close();
            return count;
        }
        #endregion

        #region eva
        public int InsertEva(string url, int menuID, int langID, int totalPage, string dataURL, string desURL)
        {
            return 0;
        }
        #endregion

        #region vietgiaitri
        public string InsertDemo(string url)
        {
            //khoi tao + mo ket noi database
            //var conn = new SqlConnection(VSW.Core.Data.ConnectionString.Default);
            //conn.Open();

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();

            doc = web.Load(url);

            if (doc != null)
            {
                //tach 1 tin
                var listNode = doc.DocumentNode.SelectNodes("//section[@id='introduction']");

                HtmlDocument temp = StripHtmlTags(listNode[0].InnerHtml);

                var node1 = temp.DocumentNode.SelectNodes("//ol[@class='breadcrumb']").ToList();
                if (node1 != null) node1.RemoveAt(0);

                string s = listNode[0].InnerHtml;

                return s;
            }

            //dong ket noi database
            //conn.Close();

            return string.Empty;
        }
        #endregion

        #region utilities
        public static string GetGold()
        {
            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://hcm.24h.com.vn/ttcb/giavang/giavang.php");

            if (doc == null) return string.Empty;

            var _ResultNode = doc.DocumentNode.SelectNodes("//table[@class=\"tb-giaVang\"]");
            if (_ResultNode == null) return string.Empty;

            return "<table class=\"tb-giaVang\" width=\"100%\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\">" + _ResultNode[0].InnerHtml + "</table>";
        }

        public static string GetKQXS()
        {
            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://ketquaxoso.24h.com.vn/");

            if (doc == null) return string.Empty;

            var _ResultNode = doc.DocumentNode.SelectNodes("//div[@id=\"content\"]");
            if (_ResultNode == null) return string.Empty;

            HtmlDocument temp = StripHtmlTags(_ResultNode[0].InnerHtml);

            _ResultNode = temp.DocumentNode.SelectNodes("//table[@style=\"margin-top:6px;\"]");

            return "<table class=\"tb-giaVang\" width=\"100%\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\">" + _ResultNode[0].InnerHtml + "</table>";
        }

        public static string GetWeather()
        {
            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://hn.24h.com.vn/ttcb/thoitiet/thoitiet.php");

            if (doc == null) return string.Empty;

            var _ResultNode = doc.DocumentNode.SelectNodes("//div[@class=\"thoiTietBox\"]");
            if (_ResultNode == null) return string.Empty;

            HtmlDocument temp = StripHtmlTags(_ResultNode[0].InnerHtml);

            _ResultNode = temp.DocumentNode.SelectNodes("//table[@width=\"100%\"]");

            return "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" + _ResultNode[0].InnerHtml + "</table>";
        }

        public static string GetRate()
        {
            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://hcm.24h.com.vn/ttcb/tygia/tygia.php");

            if (doc == null) return string.Empty;

            var _ResultNode = doc.DocumentNode.SelectNodes("//table[@class=\"tb-giaVang\"]");
            if (_ResultNode == null) return string.Empty;

            string result = string.Empty;
            if (!string.IsNullOrEmpty(_ResultNode[0].InnerHtml)) result = _ResultNode[0].InnerHtml.Replace("images/btdown.gif", "/Content/utils/html/images/btdown.gif").Replace("images/btup.gif", "/Content/utils/html/images/btup.gif");

            return "<table class=\"tb-giaVang\" width=\"100%\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\">" + result + "</table>";
        }

        public static string GetStock()
        {
            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://chungkhoan.24h.com.vn/");

            if (doc == null) return string.Empty;

            var _ResultNode = doc.DocumentNode.SelectNodes("//table[@id=\"tableContainer\"]");
            if (_ResultNode == null) return string.Empty;

            string result = string.Empty;
            if (!string.IsNullOrEmpty(_ResultNode[0].InnerHtml)) result = _ResultNode[0].InnerHtml.Replace("/images/btdown.gif", "/Content/utils/html/images/btdown.gif").Replace("/images/btup.gif", "/Content/utils/html/images/btup.gif").Replace("/images/btnochange.jpg", "/Content/utils/html/images/btnochange.jpg");

            return "<table class=\"tb-giaVang\" width=\"100%\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\">" + result + "</table>";
        }
        #endregion
    }
}
