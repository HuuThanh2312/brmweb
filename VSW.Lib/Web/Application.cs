﻿using System;
using System.Collections.Generic;

//using System.Data.SqlClient;
using System.Reflection;
using System.Web;
using VSW.Lib.MVC;

namespace VSW.Lib.Web
{
    //public class SubdomainModule : IHttpModule
    //{
    //   public void Init(HttpApplication app)
    //   {
    //       //register for pipeline events
    //       app.BeginRequest += new EventHandler(this.OnBeginRequest);
    //   }
    //   public void Dispose() {}
    //   public void OnBeginRequest(object o, EventArgs args)
    //   {
    //       //get access to app and context
    //       HttpApplication app = (HttpApplication)o;
    //       HttpContext ctx = app.Context;
    //       if (!(app.Context.Request.Url.Host.StartsWith("localhost") || app.Context.Request.Url.Host.StartsWith("www")))
    //       {
    //           if (ctx.Request.Path.ToUpper() == "/Default.aspx")
    //           {
    //               string[] domainParts = app.Context.Request.Url.Host.Split(".".ToCharArray());
    //               if (domainParts.Length > 2)
    //                   ctx.Server.Transfer("/explore.aspx?q=" + domainParts[0]);
    //           }
    //       }
    //   }
    //}

    public class Application : HttpApplication
    {

        #region private

        //private void Redirection()
        //{
        //    var absoluteUri = Request.Url.AbsoluteUri.ToLower();
        //    if (absoluteUri.Length >= 260) Response.Redirect("http://" + HttpContext.Current.Request.Url.Host.ToLower() + "/");

        //    var listRedirection = Models.WebRedirectionService.Instance.CreateQuery().ToList_Cache();

        //    if (listRedirection == null) return;
        //    int index = listRedirection.FindIndex(o => o.Url == absoluteUri);
        //    if (index <= -1 || string.IsNullOrEmpty(listRedirection[index].Redirect)) return;

        //    Response.Clear();
        //    Response.Status = "301 Moved Permanently";
        //    Response.AddHeader("Location", listRedirection[index].Redirect);
        //    Response.End();

        //    //HttpContext.Current.Response.Redirect(listRedirection[_Index].Redirect);
        //}

        #endregion private

        public static List<CPModuleInfo> CPModules { get; set; }

        public new static List<ModuleInfo> Modules { get; set; }

        protected void Application_Start(object sender, EventArgs e)
        {
            //signalR
            //RouteTable.Routes.MapHubs();

            //license excel
            //string licenseFile = HttpContext.Current.Server.MapPath("~/bin" + "/Aspose.Cells.lic");
            //if (System.IO.File.Exists(licenseFile))
            //{
            //    Aspose.Cells.License license = new Aspose.Cells.License();
            //    license.SetLicense(licenseFile);
            //}

            if (CPModules == null)
            {
                CPModules = new List<CPModuleInfo>();
                Modules = new List<ModuleInfo>();

                var types = Assembly.GetExecutingAssembly().GetTypes();
                foreach (var type in types)
                {
                    var attributes = type.GetCustomAttributes(typeof(CPModuleInfo), true);
                    if (attributes.GetLength(0) == 0)
                    {
                        attributes = type.GetCustomAttributes(typeof(ModuleInfo), true);
                        if (attributes.GetLength(0) == 0)
                            continue;

                        if (attributes[0] is ModuleInfo moduleInfo)
                        {
                            if (Modules.Find(o => o.Code == moduleInfo.Code) == null)
                            {
                                moduleInfo.ModuleType = type;

                                Modules.Add(moduleInfo);
                            }
                        }

                        continue;
                    }

                    {
                        var moduleInfo = attributes[0] as CPModuleInfo;
                        if (moduleInfo == null) continue;
                        if (CPModules.Find(o => o.Code == moduleInfo.Code) == null)
                            CPModules.Add(moduleInfo);
                    }
                }
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //Redirection();
            Core.Web.Application.BeginRequest();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Global.Application.OnError();
        }

        //protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        //{
        //    Core.Web.Application.PreRequestHandlerExecute(sender as HttpApplication);
        //}
    }
}