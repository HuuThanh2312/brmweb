﻿
using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Doanh thu", Code = "MBusinessRevenue", Order = 6)]
    public class MBusinessRevenueController : Controller
    {

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;

        public void ActionIndex(MBusinessRevenueModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser.GetShopNotActive() != null)
            {
                ViewPage.Confirm("Bạn đã là đối tác của Berich. <br> Nhưng chưa được Berich xét duyệt.", "/");
                return;
            }
            else if (_WebUser.GetShop() == null)
            {
                ViewPage.Confirm("Bạn chưa phải là đối tác của Berich. <br> Bạn có muốn đăng ký là đối tác của Berich không?.", ViewPage.RegisterBuninessUrl);
                return;
            }


            int _shopID = _WebUser.GetShop().ID;
            var dbQuery = ModBusinessRevenueService.Instance.CreateQuery()
                                .Where(o => o.ShopID == _shopID)
                                .Where(model.StartDate > DateTime.MinValue, o => o.Published >= model.StartDate)
                                .Where(model.EndDate > DateTime.MinValue, o => o.Published <= model.EndDate)
                                .Take(model.PageSize)
                                .OrderByDesc(o => new { o.Published })
                                .Skip(model.page * PageSize);

            ViewBag.User = _WebUser.GetShop();
            ViewBag.Data = dbQuery.ToList_Cache();

            var dbQuery2 = ModBusinessRevenueService.Instance.CreateQuery().Select(o => o.Revenue).Where(o => o.ShopID == _shopID).ToList_Cache();
            double _Total =0;
            for (int i = 0; dbQuery2 != null && i < dbQuery2.Count; i++)
            {
                _Total += dbQuery2[i].Revenue;
            }
            ViewBag.Total =_Total;
            ViewBag.Model = model;
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);

            //SEO

        }



    }

    public class MBusinessRevenueModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int id { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
        public int State { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }
    }
}