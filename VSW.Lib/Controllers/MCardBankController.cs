﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Quản lý thông tin ngân hàng", Code = "MCardBank", Order = 6)]
    public class MCardBankController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID;

        public void ActionIndex(MCardBankModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            //if (model.ID > 0) { RedirectToAction("Detail"); return; }


            var dbQuery = ModBankingUserService.Instance.GetListByUserID(_WebUser.ID);
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.User = _WebUser;
            ViewBag.Data = dbQuery;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionDetail(MCardBankModel model)
        {


            var Card = ModBankingUserService.Instance.GetByID(model.ID);
            if (Card == null)
                ViewPage.Error404();


            ViewBag.Model = model;

            ViewBag.Data = Card;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

    

        public void ActionBankingPOST(ModBankingUserEntity item, MCardBankModel model)
        {
            if (model.ID > 0)
            {
                item = ModBankingUserService.Instance.GetByID(model.ID);
            }
            else item = new ModBankingUserEntity();
            var _WebUser = WebLogin.CurrentUser;
            TryUpdateModel(item);

            item.ID = model.ID;
            ViewBag.Banking = item;
            ViewBag.Model = model;

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (item.NameBank.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập. Tên ngân hàng");

            if (item.AccountCode.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số tài khoản.");

            if (item.CardCode.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: số thẻ ATM.");

            if (item.AddressBank.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Chi nhánh ngân hàng.");

            //if (item.CityID < 1)
            //    ViewPage.Message.ListMessage.Add("Chọn: Tỉnh / thành phố.");

            //if (item.CommuneID < 1)
            //    ViewPage.Message.ListMessage.Add("Chọn: Quận / huyện.");

            var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            var sValidCode = model.ValidCode.Trim();

            if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                var message = ViewPage.Message.ListMessage.Aggregate(string.Empty, (current, t) => current + ("- " + t + "<br />"));

                ViewPage.Alert(message);
            }
            else
            {


                if (model.ID > 0)
                {


                    item.Update = DateTime.Now;
                    if (item.Activity)
                    {
                        var _list = ModBankingUserService.Instance.GetListByUserID(item.UserID);

                        for (int i = 0; _list != null && i < _list.Count; i++)
                        {
                            _list[i].Activity = false;
                            ModBankingUserService.Instance.Save(_list[i], o => new { o.Activity });
                        }
                    }
                    ModBankingUserService.Instance.Save(item, o => new { o.AddressBank, o.Name, o.CardCode, o.NameBank, o.Activity, o.Update });

                    ViewPage.AlertThenRedirect("Cập nhật tài khoản thành công", ViewPage.BankUserCPUrl);

                    VSW.Core.Web.Cache.Clear(ModBankingUserService.Instance);

                }
                else
                {
                    if(item.Activity)
                    {
                        var listAcc = ModBankingUserService.Instance.GetListByUserID(_WebUser.ID);

                        for (int i = 0; listAcc != null && i < listAcc.Count; i++)
                        {
                            listAcc[i].Activity = false;
                            ModBankingUserService.Instance.Save(listAcc[i], o => new { o.Activity });
                        }
                    }

                    item.UserID = _WebUser.ID;
                    item.Created = DateTime.Now;
                    item.Update = DateTime.Now;
                    

                    ModBankingUserService.Instance.Save(item);
                    ViewPage.AlertThenRedirect("Thêm mới tài khoản thành công", ViewPage.BankUserCPUrl);
                    VSW.Core.Web.Cache.Clear(ModBankingUserService.Instance);
                }


            }

            ViewBag.Data = item;
            ViewBag.Model = model;

        }
    }

    public class MCardBankModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int ID { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
}