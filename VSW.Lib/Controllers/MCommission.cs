﻿
using System;
using System.Collections.Generic;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Doanh thu hoa hồng", Code = "MCommission", Order = 6)]
    public class MCommissionController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;
        public void ActionIndex(MCommissionModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            //if (model.ID > 0) { RedirectToAction("Detail"); return; }


            var dbQuery = ModWebUserService.Instance.CreateQuery()
                                                .Where(o => o.UserParentIDs.StartsWith(_WebUser.ID.ToString()) || o.UserParentIDs.Contains("," + _WebUser.ID.ToString() + ",") || o.UserParentIDs.EndsWith("," + _WebUser.ID.ToString()))
                                                .OrderByAsc(o => o.Created)
                                                .ToList_Cache();

            var root = new Level();
            var listMember = root.Data;
            //List<Level> listRoot = new List<Level>();
            //for (int i = 0; dbQuery != null && i < dbQuery.Count; i++)
            //{


            //}


            ViewBag.User = _WebUser;
            ViewBag.Data = listMember;
            ViewBag.Model = model;
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }



    }

    public class MCommissionModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
    public class MemberCommiss
    {
        public int UserID { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }
        public int Level2 { get; set; }
    }
    public class Level
    {
        public int _level { get; set; }
        public List<MemberCommiss> Data { get; set; }
    }
}

