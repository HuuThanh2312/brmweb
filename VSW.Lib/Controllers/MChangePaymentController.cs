﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Thay đổi hạn mức chuyển khoản", Code = "MChangePayment", Order = 6)]
    public class MChangePaymentController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;

        public void ActionIndex(MChangePaymentModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            //if (model.ID > 0) { RedirectToAction("Detail"); return; }


            //var dbQuery = ModBankingUserService.Instance.GetListByUserID(_WebUser.ID);
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.User = _WebUser;
            //ViewBag.Data = dbQuery;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }




        //public void ActionBankingPOST(ModBankingUserEntity item, MChangePaymentModel model)
        //{
        //    TryUpdateModel(item);


        //    ViewBag.Banking = item;
        //    ViewBag.Model = model;

        //    if (item.TypeID < 1)
        //        ViewPage.Message.ListMessage.Add("Chọn: Loại dịch vụ.");

        //    if (item.BrandID < 1)
        //        ViewPage.Message.ListMessage.Add("Chọn. Nhà cung cấp");

        //    if (item.PriceID < 1)
        //        ViewPage.Message.ListMessage.Add("Chọn: Mệnh giá.");

        //    if (item.PaymentID < 1)
        //        ViewPage.Message.ListMessage.Add("Chọn: Hình thức thanh toán.");

        //    if (item.Quantity < 1)
        //        ViewPage.Message.ListMessage.Add("Nhập: Số lượng thẻ.");

        //    var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
        //    var sValidCode = model.ValidCode.Trim();

        //    if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
        //        ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");



        //    if (ViewPage.Message.ListMessage.Count > 0)
        //    {
        //        var message = ViewPage.Message.ListMessage.Aggregate(string.Empty, (current, t) => current + ("- " + t + "<br />"));

        //        ViewPage.Alert(message);
        //    }
        //    else
        //    {
        //        ModChangePaymentService.Instance.Save(item);

        //    }

        //    ViewBag.Data = item;
        //    ViewBag.Model = model;

        //}
    }

    public class MChangePaymentModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
}