﻿using System;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Sản phẩm", Code = "CProduct", IsControl = true, Order = 3)]
    public class CProductController : Controller
    {
        [Core.MVC.PropertyInfo("Default[MenuID-true|PageID-true|PageSize-true|FlastSale-false],FlastSale[MenuID-true|PageID-true|PageSize-true|State-true|FlastSale-true]")]
        public string LayoutDefine;

        [Core.MVC.PropertyInfo("Chuyên mục", "Type|Product")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Trang")]
        public int PageID;

        [Core.MVC.PropertyInfo("Vị trí", "ConfigKey|Mod.ProductState")]
        public int State;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 10;

        [Core.MVC.PropertyInfo("Thời gian Giảm giá")]
        public DateTime FlastSale = DateTime.MinValue;

        public override void OnLoad()
        {
            var webUser = ViewPage.ViewBag.Shop as ModWebUserEntity;

            if (ViewLayout == "FlastSale")
            {
                var dbQuery = ModProductService.Instance.CreateQuery()
                                   .Where(o => o.Activity == true && o.ActivityPrice == true)
                                   .Where(State > 0, o => (o.State & State) == State)
                                   .Where(FlastSale > DateTime.MinValue, o => o.FlastSale <= FlastSale)
                                   .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                   .OrderByDesc(o => new { o.Order, o.ID })
                                   .Take(PageSize);
                ViewBag.Data = dbQuery.ToList_Cache();
                ViewBag.Sale = FlastSale;
            }
            else
            {
                var dbQuery = ModProductService.Instance.CreateQuery()
                                        .Where(o => o.Activity == true && o.ActivityPrice == true)
                                        .Where(State > 0, o => (o.State & State) == State)
                                        .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                        .OrderByDesc(o => new { o.Order, o.ID })
                                        .Take(PageSize);
                ViewBag.Data = dbQuery.ToList_Cache();
            }

            ViewBag.Page = SysPageService.Instance.GetByID_Cache(PageID);
        }
    }
}