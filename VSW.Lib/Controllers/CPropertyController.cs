﻿using System;
using System.Linq;
using VSW.Lib.MVC;
using VSW.Lib.Models;
using System.Collections.Generic;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Thuộc tính sản phẩm", Code = "CProperty", IsControl = true, Order = 2)]
    public class CPropertyController : Controller
    {
        Dictionary<WebPropertyEntity, List<WebPropertyEntity>> listItem;
        public override void OnLoad()
        {
            if (ViewPage.CurrentModule.Code != "MProduct") return;

            var menu = WebMenuService.Instance.GetByID_Cache(ViewPage.CurrentPage.MenuID);
            if (menu == null || menu.PropertyID < 1) return;

            var atr = ViewPage.PageViewState.GetValue("atr").ToString().Trim();
            var ArrPropertyID = !string.IsNullOrEmpty(atr) ? Core.Global.Array.ToInts(atr.Split('-')) : null;

            ViewBag.ArrPropertyID = ArrPropertyID;

            var listProperty = WebPropertyService.Instance.CreateQuery()
                                        .Select(o => new { o.Name, o.ID })
                                        .Where(o => o.Activity == true && o.ParentID == menu.PropertyID)
                                        .OrderByAsc(o => o.Order)
                                        .ToList_Cache();

            List<ModPropertyEntity> listResult = null;
            var listItemTemp = new Dictionary<WebPropertyEntity, List<WebPropertyEntity>>();

            listItem = new Dictionary<WebPropertyEntity, List<WebPropertyEntity>>();

            var listPropertyID = string.Empty;
            for (var i = 0; listProperty != null && i < listProperty.Count; i++)
                listPropertyID += (listPropertyID == string.Empty ? string.Empty : ",") + listProperty[i].ID;

            if (listPropertyID != string.Empty)
            {
                var dbQuery = ModPropertyService.Instance.CreateQuery()
                                        .Select(o => o.PropertyValueID)
                                        .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", ViewPage.CurrentPage.MenuID, ViewPage.CurrentLang.ID))
                                        .WhereIn(o => o.PropertyID, listPropertyID);

                for (var k = 0; ArrPropertyID != null && k < ArrPropertyID.Length; k++)
                {
                    var propertySelectID = ArrPropertyID[k];

                    dbQuery.WhereIn(o => o.ProductID, ModPropertyService.Instance.CreateQuery()
                                                                .Select(o => o.ProductID)
                                                                .Where(o => o.PropertyValueID == propertySelectID)
                                   );
                }

                listResult = dbQuery.ToList();
            }

            for (var i = 0; listResult != null && listProperty != null && i < listProperty.Count; i++)
            {
                var parentID = listProperty[i].ID;
                var list = WebPropertyService.Instance.CreateQuery()
                                    .Select(o => new { o.Name, o.ID })
                                    .Where(o => o.Activity == true && o.ParentID == parentID)
                                    .OrderByAsc(o => o.Order)
                                    .ToList();

                if (list == null) continue;
                var selectMode = false;
                var selectID = 0;

                for (var j = list.Count - 1; j > -1; j--)
                {
                    if (ArrPropertyID == null || Array.IndexOf(ArrPropertyID, list[j].ID) <= -1) continue;
                    selectID = list[j].ID;
                    selectMode = true;
                    break;
                }

                for (var j = list.Count - 1; j > -1; j--)
                {
                    if (selectMode && selectID != list[j].ID)
                    {
                        list.RemoveAt(j);
                        continue;
                    }
                    var listResultSelected = listResult.FindAll(o => o.PropertyValueID == list[j].ID);
                    var count = listResultSelected.Count;

                    if (count > 0)
                        list[j].Count = count;
                    else
                        list.RemoveAt(j);
                }

                listProperty[i].Selected = selectMode;

                if (list.Count != 0)
                    listItemTemp[listProperty[i]] = list;
            }

            foreach (var item in listItemTemp.Keys)
            {
                if (item.Selected)
                    listItem[item] = listItemTemp[item];
            }

            foreach (var item in listItemTemp.Keys.Where(item => !item.Selected))
            {
                listItem[item] = listItemTemp[item];
            }

            ViewBag.Data = listItem;
        }
    }
}
