﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Quản lý thông tin thẻ BCoinCard", Code = "MAccBcoinCard", Order = 6)]
    public class MAccBcoinCardController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID;

        public void ActionIndex(MAccBcoinCardModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            if (model.ID > 0) { RedirectToAction("Detail"); return; }


            var dbQuery = ModAccBcoinCardService.Instance.GetByUserID_Default(_WebUser.ID);
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.User = _WebUser;
            ViewBag.Data = dbQuery;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionDetail(MAccBcoinCardModel model)
        {


            var AccBcoinCard = ModBankingUserService.Instance.GetByID(model.ID);
            if (AccBcoinCard == null)
                ViewPage.Error404();


            ViewBag.Model = model;

            ViewBag.Data = AccBcoinCard;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        private ModAccBcoinCardEntity _banking;
        public void ActionAddBanking(MAccBcoinCardModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (model.ID > 0)
            {
                _banking = ModAccBcoinCardService.Instance.GetByID(model.ID);
            }
            else _banking = new ModAccBcoinCardEntity();

            TryUpdateModel(_banking);

            //chong hack
            _banking.ID = model.ID;

            ViewBag.Banking = _banking;
            ViewBag.Model = model;


            if (model.ID < 1) ViewPage.CurrentPage.PageTitle = "Quản lý tài khoản - Tạo một thẻ mới.";
            else ViewPage.CurrentPage.PageTitle = "Quản lý tài khoản - Cập nhật lại thẻ.";
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionBankingPOST(ModAccBcoinCardEntity item, MAccBcoinCardModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            TryUpdateModel(item);

            item.ID = model.ID;
            ViewBag.Banking = item;
            ViewBag.Model = model;

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");


            if (item.AccountBcoin.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số tài khoản.");

            var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            var sValidCode = model.ValidCode.Trim();

            if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                var message = ViewPage.Message.ListMessage.Aggregate(string.Empty, (current, t) => current + ("- " + t + "<br />"));

                ViewPage.Alert(message);
            }
            else
            {


                if (model.ID > 0)
                {
                    item.Update = DateTime.Now;


                    ModAccBcoinCardService.Instance.Save(item, o => new { o.Name, o.CardCode, o.Password, o.Summary,o.Update });

                    ViewPage.AlertThenRedirect("Cập nhật tài khoản thành công", ViewPage.LogoutUrl);

                }
                else
                {


                    item.UserID = _WebUser.ID;
                    item.Created = DateTime.Now;
                    item.Update = DateTime.Now;
                    item.Activity = true;

                    ModAccBcoinCardService.Instance.Save(item);
                    ViewPage.AlertThenRedirect("Thêm mới tài khoản thẻ thành công", ViewPage.LogoutUrl);
                }


            }

            ViewBag.Data = item;
            ViewBag.Model = model;

        }
    }

    public class MAccBcoinCardModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int ID { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
}