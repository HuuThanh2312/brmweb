﻿using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Thông tin tài khoản", Code = "MUserCP", Order = 50)]
    public class MUserCPController : Controller
    {
        #region INDEX

        public void ActionIndex(MUserCPModel model)
        {
            if (!WebLogin.IsLogin()) ViewPage.Response.Redirect(ViewPage.LoginUrl);

            var item = WebLogin.CurrentUser;

            ViewBag.Data = item;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        #endregion INDEX

        #region POST

        public void ActionInfoPOST(MUserCPModel model)
        {
            var item = WebLogin.CurrentUser;

            TryUpdateModel(item);

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");

            //var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            //var sValidCode = model.ValidCode.Trim();

            //if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
            //    ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                ModWebUserService.Instance.Save(item);

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionPasswordPOST(MUserCPModel model)
        {
            if (model.Password.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu hiện tại.");
            else if (Security.Md5(model.Password.Trim()) != WebLogin.CurrentUser.Password)
                ViewPage.Message.ListMessage.Add("Mật khẩu hiện tại không đúng.");

            if (model.Password1.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu mới.");
            else if (model.Password1.Trim().Length < 6 || model.Password1.Trim().Length > 12)
                ViewPage.Message.ListMessage.Add("Mật khẩu phải từ 6-12 ký tự.");
            else if (model.Password1.Trim() != model.Password2.Trim())
                ViewPage.Alert("Mật khẩu và mật khẩu nhập lại không giống nhau.");

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                //save
                WebLogin.CurrentUser.Password = Security.Md5(model.Password1.Trim());
                ModWebUserService.Instance.Save(WebLogin.CurrentUser, o => new { o.Password });

                ViewPage.Alert("Bạn đã đổi mật khẩu thành công.");
                ViewPage.Navigate("/");
            }

            ViewBag.Model = model;
        }

        public void ActionLogout()
        {
            WebLogin.Logout();
            ViewPage.Response.Redirect("~/");
        }

        #endregion POST
    }

    public class MUserCPModel
    {
        public string Password { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }

        public string ValidCode { get; set; }
    }
}