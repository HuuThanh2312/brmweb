﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Đánh giá", Code = "MBusinessReview", Order = 6)]
    public class MBusinessReviewController : Controller
    {

        [Core.MVC.PropertyInfo("Chuyên mục", "Type|News")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;

        public void ActionIndex(MBusinessReviewModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);


            if (model.id > 0)
            {
                var _news = ModReviewService.Instance.GetByID(model.id);
                if (_news == null) ViewPage.Response.Redirect(ViewPage.BuninessReviewCpUrl);
                else
                {
                    RenderView("Detail");
                    ViewBag.Data = _news;

                    ViewPage.CurrentPage.PageTitle = "Chi tiết đánh giá của " + _news.Name;
                    ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                    ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
                    return;
                }
            }
            else
            {

                var dbQuery = ModReviewService.Instance.CreateQuery()
                                .Where(o => o.ShopID == _WebUser.GetShop().ID)
                                .Take(model.PageSize)
                                .OrderByDesc(o => new { o.Created })
                                .Skip(model.page * PageSize);

                ViewBag.User = _WebUser;
                ViewBag.Data = dbQuery.ToList_Cache();
                ViewBag.Model = model;
                ViewPage.CurrentPage.PageTitle = "Quản lý đánh giá";
                ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
            }
            //SEO

        }



    }

    public class MBusinessReviewModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int id { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
    }
}