﻿using System;

using VSW.Lib.MVC;
using VSW.Lib.Models;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Ý kiến khách hàng", Code = "CComment", IsControl = true, Order = 4)]
    public class CCommentController : Controller
    {
        [VSW.Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 10;

        //[VSW.Core.MVC.PropertyInfo("Tiêu đề")]
        //public string Title = string.Empty;

        public override void OnLoad()
        {
            ViewBag.Data = ModCommentService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .OrderByDesc(o => o.Order)
                                    .Take(PageSize)
                                    .ToList_Cache();
            //ViewBag.Title = Title;
        }
    }
}
