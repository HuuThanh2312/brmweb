﻿
using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Sản phẩm Từ Berich", Code = "MBusinessProduct", Order = 6)]
    public class MBusinessProductController : Controller
    {
        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;
        public void ActionIndex(MBusinessProductModel model)
        {

            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser.GetShopNotActive() != null)
            {
                ViewPage.Confirm("Bạn đã là đối tác của Berich. <br> Nhưng chưa được Berich xét duyệt.", "/");
                return;
            }
            else if (_WebUser.GetShop() == null)
            {
                ViewPage.Confirm("Bạn chưa phải là đối tác của Berich. <br> Bạn có muốn đăng ký là đối tác của Berich không?.", ViewPage.RegisterBuninessUrl);
                return;
            }





            var dbQuery = ModBerichProductService.Instance.CreateQuery()
                                                    .Where(!string.IsNullOrEmpty(model.CategoryID), o => o.CategoryID == model.CategoryID)
                                                    .Take(PageSize)
                                                    .Skip(PageSize * model.page)
                                                    .OrderByAsc(o => new { o.ProductName, o.ProductCode });

            var dbCategory = ModBerichCategoryService.Instance.CreateQuery()
                                                    .Select(o => new { o.CategoryName, o.CategoryID, o.ParentID, o.IsParent })
                                                    .OrderByAsc(o => new { o.CategoryName, o.CategoryID })
                                                    .ToList_Cache();
            ViewBag.User = _WebUser.GetShop();
            ViewBag.Data = dbQuery.ToList_Cache();
            ViewBag.Category = dbCategory;
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);


        }

        public void ActionAddProductPOST(MBusinessProductModel model)
        {
            var _UserShop = WebLogin.CurrentUser.GetShop();
            if (_UserShop == null) { ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL); return; }
            var _valueProduct = Cookies.GetValue("save_product_selected");

            RenderView("Index");

            if (_valueProduct.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Bạn chưa chọn sản phẩm nào");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                int _Count = 0;
                string[] _productID = _valueProduct.Split(',');
                for (int i = 0; _productID.Length > 1 && i < _productID.Length - 1; i++)
                {
                    var _ProductBerich = ModBerichProductService.Instance.GetByProductID_Cache(new Guid(_productID[i]));
                    if (_ProductBerich != null)
                    {
                        var _webmenu = WebMenuService.Instance.GetByCategoryID_Cache(_ProductBerich.CategoryID);

                        var _checkIsStoreShop = ModProductService.Instance.GetByShopID_Cache(_ProductBerich.ProductID.ToString("D"), _UserShop.ID);
                        if (_checkIsStoreShop == null)
                        {
                            var _productFileD = _ProductBerich.GetFileProduct();
                            var listFile = _productFileD.GetFiles();
                            var _barCode = _ProductBerich.GetBarCode();
                            var _proDuct = new ModProductEntity();

                            _proDuct.File = _productFileD.File;
                            _proDuct.Content = _productFileD.Content;


                            //_proDuct.Code = Data.GetCode(_ProductBerich.ProductName) + "-" + _UserShop.ID;
                            _proDuct.BerichProductID = _ProductBerich.ProductID.ToString("D");
                            _proDuct.Activity = false;
                            _proDuct.BerichCategoryID = _ProductBerich.CategoryID;
                            _proDuct.Name = _ProductBerich.ProductName;
                            _proDuct.ShopID = _UserShop.ID;
                            _proDuct.Published = DateTime.Now;
                            _proDuct.MenuID = _webmenu != null ? _webmenu.ID : 0;
                            _proDuct.Model = _ProductBerich.ProductCode;
                            _proDuct.Barcode = _barCode != null ? _barCode.BarcodeCode : string.Empty;
                            _proDuct.CategoryName = _ProductBerich.GetCategory() != null ? _ProductBerich.GetCategory().CategoryName : string.Empty;
                            _proDuct.CostPrice = _ProductBerich.GetCostPrice() != null ? _ProductBerich.GetCostPrice().SalePrice : 0;

                            ModProductService.Instance.Save(_proDuct);

                            //update URl
                            _proDuct.Code = Data.GetCode(_proDuct.Name) + "-" + _UserShop.ID + "-" + _proDuct.ID;
                            ModProductService.Instance.Save(_proDuct, o => new { o.Code });
                            ModCleanURLService.Instance.InsertOrUpdate(_proDuct.Code, "Product", _proDuct.ID, _proDuct.MenuID, ViewPage.CurrentLang.ID);


                            //get ảnh
                            ModProductFileService.Instance.Delete(o => o.ProductID == _proDuct.ID);
                            for (int z = 0; listFile != null && z < listFile.Count; z++)
                            {
                                ModProductFileService.Instance.Save(new ModProductFileEntity()
                                {
                                    ID = 0,
                                    ProductID = _proDuct.ID,
                                    File = listFile[z],
                                    Order = GetMaxProductFileOrder(_proDuct.ID),
                                    Activity = true
                                });
                            }

                            _Count += 1;
                            bool _check = ModMenuShopService.Instance.Exists(_UserShop.ID, _proDuct.MenuID);
                            if (!_check)
                            {
                                ModMenuShopService.Instance.Save(new ModMenuShopEntity
                                {
                                    MenuID = _proDuct.MenuID,
                                    ParentID = _webmenu.ParentID,
                                    Name = _webmenu.Name,
                                    PropertyID = _webmenu.PropertyID,
                                    ShopID = _UserShop.ID
                                });
                            }
                        }
                    }
                }
                Cookies.Remove("save_product_selected");
                VSW.Core.Web.Cache.Clear(ModProductService.Instance);
                VSW.Core.Web.Cache.Clear(ModMenuShopService.Instance);
                VSW.Core.Web.Cache.Clear(ModProductFileService.Instance);
                if (_Count < 1)
                {
                    ViewPage.Confirm("Có thể các sản phẩm bạn đã lấy rồi.<br>Nhấn \"Yes\" vào trang quản lý sản phẩm, để kiểm tra", ViewPage.BuninessMNGUrl);
                }
                else
                {
                    ViewPage.Confirm("Bạn đã nhập " + _Count + " sản phẩm.<br>Nhấn \"Yes\" vào trang quản lý sản phẩm, để hoàn thành việc nhập hàng", ViewPage.BuninessMNGUrl);
                }
            }
            ViewBag.User = _UserShop;

            ViewBag.Model = model;
        }


        private static int GetMaxProductFileOrder(int productID)
        {
            return ModProductFileService.Instance.CreateQuery()
                    .Where(o => o.ProductID == productID)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }



    }

    public class MBusinessProductModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
        public string CategoryID { get; set; }

    }
}