﻿using System.Drawing;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Đổi sđt OTP", Code = "MWebUserCPOTP", Order = 50)]
    public class MWebUserCPOTPController : Controller
    {

        [Core.MVC.PropertyInfo("Trang")]
        public int PageID =125;
        public void ActionIndex(MWebUserCPOTPModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.Data = _WebUser;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }
        

        public void ActionInfoPOST(MWebUserCPOTPModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");

            var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            var sValidCode = model.ValidCode.Trim();

            if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                ModWebUserService.Instance.Save(item, o=>new { o.Phone });

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

    }

    public class MWebUserCPOTPModel
    {
        public string Password { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }

        public string ValidCode { get; set; }
    }
}