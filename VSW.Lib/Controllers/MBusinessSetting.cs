﻿
using System;
using System.Collections.Generic;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Cài đặt shop", Code = "MBusinessSetting", Order = 6)]
    public class MBusinessSettingController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID =125;


        public void ActionIndex(MBusinessSettingModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser.GetShopNotActive() != null)
            {
                ViewPage.Confirm("Bạn đã là đối tác của Berich. <br> Nhưng chưa được Berich xét duyệt.", "/");
                return;
            }
            else if (_WebUser.GetShop() == null)
            {
                ViewPage.Confirm("Bạn chưa phải là đối tác của Berich. <br> Bạn có muốn đăng ký là đối tác của Berich không?.", ViewPage.RegisterBuninessUrl);
                return;
            }
            //if (model.ID > 0) { RedirectToAction("Detail"); return; }

        
            

            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.User = _WebUser.GetShop();
           
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }



    }

    public class MBusinessSettingModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }

  
}