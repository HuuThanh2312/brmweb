﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Đơn hàng", Code = "MBusinessOrder", Order = 6)]
    public class MBusinessOrderController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;


        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;
        public void ActionIndex(MBusinessOrderModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser.GetShopNotActive() != null)
            {
                ViewPage.Confirm("Bạn đã là đối tác của Berich. <br> Nhưng chưa được Berich xét duyệt.", "/");
                return;
            }
            else if (_WebUser.GetShop() == null)
            {
                ViewPage.Confirm("Bạn chưa phải là đối tác của Berich. <br> Bạn có muốn đăng ký là đối tác của Berich không?.", ViewPage.RegisterBuninessUrl);
                return;
            }
            //ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);

            if (model.id > 0)
            {
                var _orderDetail = ModOrderDetailService.Instance.GetByID(model.id);
                if (_orderDetail == null) ViewPage.Response.Redirect(ViewPage.BuninessOrderCpUrl);
                else
                {
                    RenderView("Detail");
                    ViewBag.Order = _orderDetail;

                    ViewPage.CurrentPage.PageTitle = "Chi tiết đơn hàng #" + _orderDetail.GetOrder().Code;
                    ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                    ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);

                }
            }
            else
            {

                int _shopID = _WebUser.GetShop().ID;
                var dbQuery = ModOrderService.Instance.CreateQuery()
                                                        .Where(o => o.ShopID.StartsWith(_shopID + ",") || o.ShopID.EndsWith("," + _shopID) || o.ShopID.Contains("," + _shopID + ","))
                                                        .Where(model.start > DateTime.MinValue, o => o.Created >= model.start)
                                                        .Where(model.end > DateTime.MinValue, o => o.Created <= model.end)
                                                        //.Where(model.StatusID > 0, o => o.StatusID == model.StatusID)
                                                        .Take(PageSize)
                                                        .Skip(PageSize * model.page).ToList_Cache();
                ViewBag.User = _WebUser.GetShop();
                ViewBag.Data = dbQuery;
                ViewBag.Model = model;

                //SEO
                 ViewPage.CurrentPage.PageTitle = "Quản lý đơn hàng";
                ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
            }
        }



    }

    public class MBusinessOrderModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public int id { get; set; }

        public int StatusID { get; set; }
        public string ValidCode { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
}