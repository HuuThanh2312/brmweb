﻿using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO: Tin tức", Code = "MNews", Order = 6)]
    public class MNewsController : Controller
    {
        [Core.MVC.PropertyInfo("Chuyên mục", "Type|News")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 10;
        
        public void ActionIndex(MNewsModel model)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            string _Keyword = !string.IsNullOrEmpty(model.news) ? Data.GetCode(model.news) : "";

            var dbQuery = ModNewsService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(_Keyword != "", o => o.Code.Contains(_Keyword))
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize)
                                    .OrderByDesc(o => new { o.Order, o.ID });

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;
            
            var item = ModNewsService.Instance.CreateQuery()
                                .Where(o => o.Activity == true && o.Code == endCode)
                                .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                                .ToSingle();

            if (item != null)
            {
                //up view
                item.UpView();

                //ViewBag.Other = ModNewsService.Instance.CreateQuery()
                //                            .Where(o => o.Activity == true && o.ID != item.ID)
                //                            .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("News", MenuID, ViewPage.CurrentLang.ID))
                //                            .OrderByDesc(o => new { o.Order, o.ID })
                //                            .Take(PageSize)
                //                            .ToList_Cache();

                ViewPage.ViewBag.Data = ViewBag.Data = item;

                //SEO
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Code);
                ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(item.File, 200, 200);
            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MNewsModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }

        public string news { get; set; }
    }
}