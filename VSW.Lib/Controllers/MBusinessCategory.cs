﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Danh mục", Code = "MBusinessCategory", Order = 6)]
    public class MBusinessCategoryController : Controller
    {


        public void ActionIndex(MBusinessCategoryModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);


            var dbQuery = WebMenuService.Instance.CreateQuery()
                                                .Where(o => o.Activity == true && o.Type == "Product" && o.ParentID > 0)
                                                .OrderByAsc(o => o.Name)
                                                .ToList_Cache();

            ViewBag.Data = dbQuery;
            ViewBag.User = _WebUser;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }



    }

    public class MBusinessCategoryModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
}