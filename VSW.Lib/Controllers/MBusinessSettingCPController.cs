﻿using System;
using System.Drawing;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Hồ sơ Shop", Code = "MBusinessSettingCP", Order = 2)]
    public class MBusinessSettingCPController : Controller
    {


        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;
        public void ActionIndex(MBuninessSettingCPModel model)
        {

            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser.GetShopNotActive() != null)
            {
                ViewPage.Confirm("Bạn đã là đối tác của Berich. <br> Nhưng chưa được Berich xét duyệt.", "/");
                return;
            }
            else if (_WebUser.GetShop() == null)
            {
                ViewPage.Confirm("Bạn chưa phải là đối tác của Berich. <br> Bạn có muốn đăng ký là đối tác của Berich không?.", ViewPage.RegisterBuninessUrl);
                return;
            }

            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.User = _WebUser.GetShop();

            ViewBag.Model = model;


            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);


        }

        public void ActionShopInfoPOST(MBuninessSettingCPModel model, ModShopEntity item)
        {
            item = ViewBag.User;
            if (item == null) { ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL); return; }

            TryUpdateModel(item);

            //RenderView("Index");
            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Tên shop");

            if (item.Email.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Email liên hệ");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại");

            string sVY = Core.Global.CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {

                ModShopService.Instance.Save(item, o => new { o.Name, o.Phone, o.Email, o.Summary });
                ViewPage.Alert("Cập nhật thành công");
                VSW.Core.Web.Cache.Clear(ModShopService.Instance);
            }
            ViewBag.User = item;

            ViewBag.Model = model;
        }
        public void ActionImagePost(MBuninessSettingCPModel model, ModShopEntity item)
        {
            item = ViewBag.User;

            TryUpdateModel(item);

            //logo, banner
            #region Images
            for (int i = 0; i < ViewPage.Request.Files.Count; i++)
            {
                var _File = ViewPage.Request.Files[i];
                if (_File == null) continue;

                string _FileName = _File.FileName;
                if (_FileName.Length < 1) continue;

                if (_File.ContentLength > 5 * 1024 * 1024)
                {
                    ViewPage.Message.ListMessage.Add("Dung lượng File " + (i + 1) + " không được lớn hơn 5Mb.");
                    continue;
                }

                try
                {
                    new Bitmap(_File.InputStream);

                    string _Path = ViewPage.Server.MapPath("~/Data/upload/images/user/" + item.GetWebUser().LoginName + "/shop/logo-shop-" + item.Url + "/");

                    if (!System.IO.Directory.Exists(_Path))
                        System.IO.Directory.CreateDirectory(_Path);

                    string _DBFile = "~/Data/upload/images/user/" + item.GetWebUser().LoginName + "/shop/logo-shop-" + item.Url + "/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(_FileName)) + System.IO.Path.GetExtension(_FileName);
                    string _PhysFile = ViewPage.Server.MapPath(_DBFile);

                    _File.SaveAs(_PhysFile);

                    item.Logo = _DBFile;
                }
                catch
                {
                    ViewPage.Message.ListMessage.Add("File tải lên không phải ảnh. Chọn file khác.");
                    continue;
                }
            }
            #endregion

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {

                ModShopService.Instance.Save(item, o => new { o.Logo });

                ViewPage.Alert("Đã cập nhật ảnh logo.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }
        public void ActionImageBannerPost(MBuninessSettingCPModel model, ModShopEntity item)
        {
            item = ViewBag.User;

            TryUpdateModel(item);

            //logo, banner
            #region Images
            for (int i = 0; i < ViewPage.Request.Files.Count; i++)
            {
                var _File = ViewPage.Request.Files[i];
                if (_File == null) continue;

                string _FileName = _File.FileName;
                if (_FileName.Length < 1) continue;

                if (_File.ContentLength > 5 * 1024 * 1024)
                {
                    ViewPage.Message.ListMessage.Add("Dung lượng File " + (i + 1) + " không được lớn hơn 5Mb.");
                    continue;
                }

                try
                {
                    new Bitmap(_File.InputStream);

                    string _Path = ViewPage.Server.MapPath("~/Data/upload/images/user/" + item.GetWebUser().LoginName + "/shop/banner-shop-" + item.Url + "/");

                    if (!System.IO.Directory.Exists(_Path))
                        System.IO.Directory.CreateDirectory(_Path);

                    string _DBFile = "~/Data/upload/images/user/" + item.GetWebUser().LoginName + "/shop/banner-shop-" + item.Url + "/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(_FileName)) + System.IO.Path.GetExtension(_FileName);
                    string _PhysFile = ViewPage.Server.MapPath(_DBFile);

                    _File.SaveAs(_PhysFile);

                    item.Banner = _DBFile;
                }
                catch
                {
                    ViewPage.Message.ListMessage.Add("File tải lên không phải ảnh. Chọn file khác.");
                    continue;
                }
            }
            #endregion

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {

                ModShopService.Instance.Save(item, o=>new { o.Banner});

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

    }

    public class MBuninessSettingCPModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }
        public string ValidCode { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
    }
}