﻿using Newtonsoft.Json;
using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Chuyển khoản thành công", Code = "MATMComplete", Order = 99)]
    public class MATMCompleteController : Controller
    {
        public void ActionIndex(ModATMPayEntity item)
        {
          
            string Token = ViewPage.Request["token"];
            RequestCheckOrder info = new RequestCheckOrder();

            info.Merchant_id = VSW.Core.Global.Config.GetValue("NganLuong.API.ID").ToString();
            info.Merchant_password = VSW.Core.Global.Config.GetValue("NganLuong.API.Password").ToString();

            info.Token = Token;

            NganLuong objNLChecout = new NganLuong();
            ResponseCheckOrder result = objNLChecout.GetTransactionDetail(info);

            //luu thong tin chuyen khoan
            item.ID = 0;
            item.WebUserID = WebLogin.WebUserID;

            item.Name = result.payerName;
            item.Email = result.payerEmail;
            item.Phone = result.payerMobile;

            item.BankCode = result.paymentMethod;
            item.TotalAmount = VSW.Core.Global.Convert.ToLong(result.paymentAmount);
            item.Content = result.description;

            item.Created = DateTime.Now;
            item.IP = VSW.Core.Web.HttpRequest.IP;

            ModATMPayService.Instance.Save(item);

            ////luu tong tien thanh vien
            //var _WebUser = WebLogin.CurrentUser;

            //_WebUser.Total += Utils.GetPrice(item.TotalAmount);
            //ModWebUserService.Instance.Save(_WebUser, o => o.Total);

            ObjectCookies<ModATMPayEntity>.SetValue("ATMComplate", item);

            ViewPage.AlertThenRedirect("Bạn đã chuyển khoản thành công số tiền " + result.paymentAmount + " VND", ViewPage.CompleteUrl);

            ViewBag.Data = item;
            
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        

        #region private func

        private static string GetOrder()
        {
            int maxId = ModATMPayService.Instance.CreateQuery()
                                .Max(o => o.ID)
                                .ToValue()
                                .ToInt();

            maxId++;

            string result = string.Empty;
            for (int i = 1; i <= (10 - maxId.ToString().Length); i++)
            {
                result += "0";
            }

            return result + (maxId + 1).ToString();
        }

        #endregion private func
    }
}