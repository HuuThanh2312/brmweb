﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Trình quản lý", Code = "MWebUserCPUpdate", Order = 50)]
    public class MWebUserCPUpdateController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;
        public void ActionIndex(MWebUserCPUpdateModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.Data = _WebUser;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        #region avarta post old
        //public void ActionImagePost(MWebUserCPUpdateModel model)
        //{
        //    var item = ViewBag.Data as ModWebUserEntity;

        //    TryUpdateModel(item);

        //    //logo, banner
        //    #region Images
        //    for (int i = 0; i < ViewPage.Request.Files.Count; i++)
        //    {
        //        var _File = ViewPage.Request.Files[i];
        //        if (_File == null) continue;

        //        string _FileName = _File.FileName;
        //        if (_FileName.Length < 1) continue;

        //        if (_File.ContentLength > 5 * 1024 * 1024)
        //        {
        //            ViewPage.Message.ListMessage.Add("Dung lượng File " + (i + 1) + " không được lớn hơn 5Mb.");
        //            continue;
        //        }

        //        try
        //        {
        //            new Bitmap(_File.InputStream);

        //            string _Path = ViewPage.Server.MapPath("~/Data/upload/images/user/" + item.LoginName + "/avartar-" + item.LoginName + "/");

        //            if (!System.IO.Directory.Exists(_Path))
        //                System.IO.Directory.CreateDirectory(_Path);

        //            string _DBFile = "~/Data/upload/images/user/" + item.LoginName + "/avartar-" + item.LoginName + "/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(_FileName)) + System.IO.Path.GetExtension(_FileName);
        //            string _PhysFile = ViewPage.Server.MapPath(_DBFile);

        //            _File.SaveAs(_PhysFile);

        //            item.File = _DBFile;
        //        }
        //        catch
        //        {
        //            ViewPage.Message.ListMessage.Add("File tải lên không phải ảnh. Chọn file khác.");
        //            continue;
        //        }
        //    }
        //    #endregion

        //    if (ViewPage.Message.ListMessage.Count > 0)
        //    {
        //        string message = string.Empty;
        //        for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
        //            message += ViewPage.Message.ListMessage[i] + "<br />";

        //        ViewPage.Alert(message);
        //    }
        //    else
        //    {


        //        ModWebUserService.Instance.Save(item, o => new { o.File });

        //        ViewPage.Alert("Đã cập nhật ảnh đại diện.");
        //    }

        //    ViewBag.Data = item;
        //    ViewBag.Model = model;
        //}

        #endregion

        public void ActionInfoPOST(MWebUserCPUpdateModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (item.Address.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ cụ thể.");

            if (item.CityID < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Tỉnh/Thành phố.");

            if (item.DistrictID < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Quận/Huyện.");

            //if (item.PhuongID < 1)
            //    ViewPage.Message.ListMessage.Add("Chọn: Xã/Phường.");

            if (item.BirthDay <= DateTime.MinValue)
                ViewPage.Message.ListMessage.Add("Chọn: Ngày sinh.");

            //var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            //var sValidCode = model.ValidCode.Trim();

            //if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
            //    ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");


            #region Images
            for (int i = 0; i < ViewPage.Request.Files.Count; i++)
            {
                var _File = ViewPage.Request.Files[i];
                if (_File == null) continue;

                string _FileName = _File.FileName;
                if (_FileName.Length < 1) continue;

                if (_File.ContentLength > 5 * 1024 * 1024)
                {
                    ViewPage.Message.ListMessage.Add("Dung lượng File " + (i + 1) + " không được lớn hơn 5Mb.");
                    continue;
                }

                try
                {
                    new Bitmap(_File.InputStream);

                    string _Path = ViewPage.Server.MapPath("~/Data/upload/images/user/" + item.LoginName + "/avartar-" + item.LoginName + "/");

                    if (!System.IO.Directory.Exists(_Path))
                        System.IO.Directory.CreateDirectory(_Path);

                    string _DBFile = "~/Data/upload/images/user/" + item.LoginName + "/avartar-" + item.LoginName + "/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(_FileName)) + System.IO.Path.GetExtension(_FileName);
                    string _PhysFile = ViewPage.Server.MapPath(_DBFile);

                    _File.SaveAs(_PhysFile);

                    item.File = _DBFile;
                }
                catch
                {
                    ViewPage.Message.ListMessage.Add("File tải lên không phải ảnh. Chọn file khác.");
                    continue;
                }
            }
            #endregion


            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                ModWebUserService.Instance.Save(item);

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionConfirmPOST(MWebUserCPUpdateModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);
            var hasPhone = new Regex(@"^[0-9]{3}.[0-9]{3}.[0-9]{4}$");
            if (item.CodeCMND.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số chứng minh nhân dân.");

            if (item.AddressNo.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ theo chứng minh nhân dân");

            if (item.CityCMND < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Tỉnh/Thành phố.");

            if (item.DistrictCMND < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Quận/Huyện.");

            if (item.CityCACMND < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Nơi cấp.");

            if (item.DateCMND <= DateTime.MinValue)
                ViewPage.Message.ListMessage.Add("Chọn: Ngày cấp.");

            if (item.Phone.Trim()==string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");
            else if (!hasPhone.IsMatch(item.Phone) || item.Phone.Length != 12)
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng số điện thoại");


            if (item.Email.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Email.");
            else if (!VSW.Lib.Global.Utils.IsEmailAddress(item.Email))
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng Email");

            var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            var sValidCode = model.ValidCode.Trim();

            if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");
            


            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                item.Phone = item.Phone.Replace(".", "");
                ModWebUserService.Instance.Save(item);

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionPasswordPOST(MWebUserCPUpdateModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);

            if (item.OldPassword == Security.Md5(model.Password1.Trim()))
                ViewPage.Message.ListMessage.Add("Không được đặt mật khẩu giống 5 lần gần nhất.");

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");

            var isValidated = hasNumber.IsMatch(model.Password1) && hasUpperChar.IsMatch(model.Password1) && hasMinimum8Chars.IsMatch(model.Password1);

            if (model.Password.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu hiện tại.");
            else if (item.Password != Security.Md5(model.Password.Trim()))
                ViewPage.Message.ListMessage.Add("Mật khẩu hiện tại không đúng.");

            if (model.Password1.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu mới.");

            else if (isValidated)
                ViewPage.Message.ListMessage.Add("Mật khẩu có độ bảo mật thấp.");

            else if (model.Password1.Trim().Length < 6 || model.Password1.Trim().Length > 15)
                ViewPage.Message.ListMessage.Add("Mật khẩu phải từ 8-15 ký tự.");

            else if (model.Password1.Trim() != model.Password2.Trim())
                ViewPage.Alert("Mật khẩu và mật khẩu nhập lại không giống nhau.");

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                item.Password = Security.Md5(model.Password1.Trim());

                ModWebUserService.Instance.Save(item, o => o.Password);

                ViewPage.Alert("Mật khẩu đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionLogout()
        {
            WebLogin.Logout();
            ViewPage.Response.Redirect("~/");
        }
    }

    public class MWebUserCPUpdateModel
    {
        public string Password { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }

        public string ValidCode { get; set; }
    }
}