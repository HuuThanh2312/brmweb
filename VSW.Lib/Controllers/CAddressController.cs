﻿using VSW.Lib.MVC;
using VSW.Lib.Models;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Showroom", Code = "CAddress", IsControl = true, Order = 2)]
    public class CAddressController : Controller
    {
        [Core.MVC.PropertyInfo("Chuyên mục", "Type|Address")]
        public int MenuID;

        public override void OnLoad()
        {
            ViewBag.Data = WebMenuService.Instance.GetByParentID_Cache(MenuID);
        }
    }
}