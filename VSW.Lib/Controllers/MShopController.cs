﻿using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Trang kênh người bán", Code = "MShop", Order = 2)]
    public class MShopController : Controller
    {
        [Core.MVC.PropertyInfo("Chuyên mục", "Type|Shop")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 5;

        
        public void ActionIndex(MShopModel model)
        {

            //model.page = ViewPage.ViewBag.IndexPage > 0 ? ViewPage.ViewBag.IndexPage: 1;
            var _Shop = ViewPage.ViewBag.Shop as ModShopEntity;
            if (_Shop != null)
            {

                var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.ShopID == _Shop.ID)
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize)
                                    .OrderByDesc(o => new { o.Order, o.ID });

                ViewBag.Data = dbQuery.ToList_Cache();
                model.TotalRecord = dbQuery.TotalRecord;
                model.PageSize = PageSize;
                ViewBag.Model = model;

                //SEO
                ViewPage.CurrentPage.PageTitle = _Shop.Name;
                ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
            }
            else
            {
                ViewPage.Error404();
            }

        }
    }

    public class MShopModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
    }
}