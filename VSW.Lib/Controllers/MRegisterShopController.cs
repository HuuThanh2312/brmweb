﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đăng ký là đối tác", Code = "MRegisterShop", Order = 6)]
    public class MRegisterShopController : Controller
    {
        public void ActionIndex(ModShopEntity item, MRegisterShopModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser != null && (_WebUser.GetShop() != null || _WebUser.GetShopNotActive() != null))
            {
                ViewPage.AlertThenRedirect("Bạn đã là đối tác của Berich.", ViewPage.BuninesskUrl);
                return;
            }



            ViewBag.User = _WebUser;
            ViewBag.Model = model;
            ViewBag.Data = item;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);


        }

        public void ActionRegisterShopPOST(MRegisterShopModel model, ModShopEntity item)
        {
            var _webUser = WebLogin.CurrentUser;
            if (_webUser == null) { ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL); return; }

            TryUpdateModel(model);

            //RenderView("Index");
            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Tên shop");

            if (item.Email.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Email liên hệ");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại");

            if (item.Address.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ");

            if (item.CityID < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Tỉnh/thành");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                item.Published = DateTime.Now;
                item.UserID = _webUser.ID;
                item.Activity = false;
                item.VrCode = Guid.NewGuid().ToString();
                item.Vr = false;

                ModShopService.Instance.Save(item);


                if (item.ID > 0)
                {
                    string sURL = "http://" + ViewPage.Request.Url.Host + "/confirm.html?code=" + item.VrCode + "&uid=" + item.ID;

                    string sHtml = VSW.Lib.Global.Template.GetHtml("EmailActive",
                        "URL", sURL,
                        "Email", item.Email,
                        "Pass", ""
                        );

                    Global.Mail.SendMail(
                        item.Email,
                        "noreply@gmail.com",
                        "http://" + ViewPage.Request.Url.Host + "/",
                        "http://" + ViewPage.Request.Url.Host + "/" + "- Kích hoạt tài khoản đối tác - Đăng ký ngày " + string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now),
                        sHtml
                    );
                }

                item = new ModShopEntity();
                _webUser.IsShop = true;
                ModWebUserService.Instance.Save(_webUser, o => new { o.IsShop });
                ViewPage.AlertThenRedirect("Bạn đã đăng ký là đối tác của Berich thành công. <br> Vui lòng đợi Berich xét duyệt để trở thành đối tác chính thức.", ViewPage.WebUserCPUrl);
            }
            ViewBag.User = _webUser;

            ViewBag.Model = model;
        }



    }

    public class MRegisterShopModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
        public string CategoryID { get; set; }

    }
}