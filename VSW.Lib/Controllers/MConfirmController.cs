﻿using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Xác nhận tài khoản", Code = "MConfirm", Order = 50)]
    public class MConfirmController : Controller
    {
        public void ActionIndex(MConfirmModel model)
        {
            if (model == null || model.uid < 1 || string.IsNullOrEmpty(model.code)) ViewPage.Response.Redirect("/");

            string code = Data.FormatRemoveSql(model.code);

            var item = ModWebUserService.Instance.GetByID(model.uid);
            var item2 = ModShopService.Instance.GetByID(model.uid);

            if (item != null)
            {
                item.VrCode = string.Empty;
                item.Vr = true;
                item.Activity = true;

                ModWebUserService.Instance.Save(item, o => new { o.VrCode, o.Vr, o.Activity });

                VSW.Lib.Global.WebLogin.SetLogin(item.ID, true);

                ViewPage.AlertThenRedirect("Thông báo !", "Bạn đã kích hoạt tài khoản thành công.", "/");
            }
            else if (item2 != null)
            {
                item2.VrCode = string.Empty;
                item2.Vr = true;
                item2.Activity = true;

                ModShopService.Instance.Save(item2, o => new { o.VrCode, o.Vr, o.Activity });
                ViewPage.AlertThenRedirect("Thông báo !", "Bạn đã kích hoạt tài khoản shop thành công.", "/");

            }
            else
            {
                ViewPage.AlertThenRedirect("Thông báo !", "Tài khoản không tồn tại hoặc sai mã kích hoạt.", "/");
            }
        }
    }

    public class MConfirmModel
    {
        public string Email { get; set; }

        public int uid { get; set; }
        public string code { get; set; }
    }
}