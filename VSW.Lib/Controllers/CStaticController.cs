﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Web;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Static", Code = "CStatic", IsControl = true, Order = 4)]
    public class CStaticController : Controller
    {
        //[Core.MVC.PropertyInfo("Default[MenuID-false|MenuID2-false],City[MenuID-true|MenuID2-false],SearchBox[MenuID-false|MenuID2-true]")]
        //public string LayoutDefine;

        public override void OnLoad()
        {
            if (ViewLayout == "Shop")
            {
                var item = ViewPage.ViewBag.Shop as ModShopEntity;
                ViewBag.Data = item;
            }
            if (ViewLayout == "SearchBox")
            {
                var listKeyword = ModKeywordService.Instance.CreateQuery()
                                                         .Where(o => o.Activity == true)
                                                         .OrderByAsc(o => o.Click)
                                                         .Take(12)
                                                         .ToList();
                List<ModKeywordEntity> listRandom = new List<ModKeywordEntity>();
                int randomIndex = 0;
                System.Random rand = new System.Random();
                while (listKeyword != null && listKeyword.Count > 0)
                {
                    randomIndex = rand.Next(0, listKeyword.Count); //Choose a random object in the list
                    listRandom.Add(listKeyword[randomIndex]); //add it to the new, random list
                    listKeyword.RemoveAt(randomIndex);
                }
                ViewBag.Keyword = listRandom;
            }
        }

        public void ActionLoginPOST(CStaticModel model)
        {
            ViewBag.Model = model;
            int _count = 0;

            var _WebUser = ModWebUserService.Instance.GetForLoginName(model.LoginName, Security.Md5(model.Password), model.Phone);

            if (_WebUser == null)
            {
                ViewPage.Alert("Sai tài khoản hoặc mật khẩu.");
                _count++;
                return;
            }
            if (_count > 5)
            {
                ViewPage.Alert("Bạn chưa đăng nhập được.<br/> Tài khoản bạn đã bị khóa do đăng nhập sai quá nhiều lần.");
                return;
            }
            WebLogin.SetLogin(_WebUser.ID, true);
            if (_WebUser.FirstLogin < 1)
            {
                ViewPage.Response.Redirect(!string.IsNullOrEmpty(model.returnpath) ? model.returnpath : ViewPage.WebUserCPUpdateUrl); _WebUser.FirstLogin += 1; ModWebUserService.Instance.Save(_WebUser, o => new { o.FirstLogin });
            }
            else
            {
                ViewPage.Response.Redirect(!string.IsNullOrEmpty(model.returnpath) ? model.returnpath : "/");
                var _shop = _WebUser.GetShop();
                if (_shop != null) { _shop.LogsLogin = DateTime.Now; ModShopService.Instance.Save(_shop, o => new { o.LogsLogin }); }
            }
        }


        public void ActionRegisterPOST(ModWebUserEntity item, CStaticModel model)
        {
            var hasPhone = new Regex(@"[0-9]+");

            if (item.LoginName.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Tên tài khoản.");
            else if (!Global.Utils.IsLoginName(item.LoginName.Trim()))
                ViewPage.Message.ListMessage.Add("Nhập: Tên tài khoản không bao gồm các ký tự đặc biệt.");
            else if (ModWebUserService.Instance.GetByLogiName(item.LoginName.Trim()) != null)
                ViewPage.Message.ListMessage.Add("Nhập: Tên tài khoản đã tồn tại.");

            if (!Global.Utils.IsEmailAddress(item.Email.Trim()))
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng emal.");
            else if (ModWebUserService.Instance.GetByEmail(item.Email.Trim()) != null)
                ViewPage.Message.ListMessage.Add("Email đã tồn tại. Hãy chọn email khác.");

            if (item.Password.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu.");
            else if (item.Password.Trim().Length < 6 || item.Password.Trim().Length > 12)
                ViewPage.Message.ListMessage.Add("Mật khẩu phải từ 6-12 ký tự.");
            else if (item.Password.Trim() != model.Password2)
                ViewPage.Message.ListMessage.Add("Mật khẩu không đồng nhất.");

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (!hasPhone.IsMatch(item.Phone) || item.Phone.Length != 10)
                ViewPage.Message.ListMessage.Add("Nhập: Đúng định dạng số điện thoại .");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");

            if (item.BirthDay <= DateTime.MinValue)
                ViewPage.Message.ListMessage.Add("Chọn: Ngày sinh.");

            string sVY = Core.Global.CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");

            //hien thi thong bao loi
            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                string pass = item.Password;

                item.ID = 0;
                item.Activity = false;
                item.FirstLogin = 1;
                item.Password = Security.Md5(item.Password.Trim());
                item.OldPassword = item.Password;
                item.IP = HttpRequest.IP;
                item.Created = DateTime.Now;
                item.VrCode = Guid.NewGuid().ToString();
                item.Vr = false;
                item.LoginName = Data.GetLoginName(item.LoginName);
                item.Phone = item.Phone.Replace(".", "");

                if (model.uid > 0)
                {
                    var _user = ModWebUserService.Instance.GetByID_Cache(model.uid);
                    if (_user.CountUser() < 3)
                    {
                        ModTreeUserService.Instance.Save(new ModTreeUserEntity
                        {
                            UserID = item.ID,
                            ParentUserID = model.uid,
                            Published = DateTime.Now,
                            Level = _user.Level + 1
                        });
                        VSW.Core.Web.Cache.Clear(ModTreeUserService.Instance);
                        item.ParentID = model.uid;
                        item.Level = _user.Level + 1;
                        item.UserParentIDs = !string.IsNullOrEmpty(_user.UserParentIDs) ? _user.UserParentIDs + "," + _user.ID : _user.ID.ToString();
                    }
                }
                ModWebUserService.Instance.Save(item);

                if (item.ID > 0)
                {
                    string sURL = "http://" + ViewPage.Request.Url.Host + "/confirm.html?code=" + item.VrCode + "&uid=" + item.ID;

                    string sHtml = VSW.Lib.Global.Template.GetHtml("EmailActive",
                        "URL", sURL,
                        "Email", item.Email,
                        "Pass", pass
                        );

                    Global.Mail.SendMail(
                        item.Email,
                        "noreply@gmail.com",
                        "http://" + ViewPage.Request.Url.Host + "/",
                        "http://" + ViewPage.Request.Url.Host + "/" + "- Kích hoạt tài khoản - Đăng ký ngày " + string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now),
                        sHtml
                    );
                }

                // WebLogin.SetLogin(item.ID, true);

                item = new ModWebUserEntity();
                //ViewPage.Confirm("Bạn có muốn đăng nhập luôn không?", "");
                ViewPage.AlertThenRedirect("Bạn đã đăng ký thành công.<br/> Vui lòng kiểm tra Email để xác thực tài khoản", "/");


            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }
    }

    public class CStaticModel
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public string ValidCode { get; set; }
        public string Password2 { get; set; }
        public string LoginName { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime BirthDay { get; set; }
        public int uid { get; set; }

        public string returnpath { get; set; }
    }
}
