﻿
using System;
using System.Collections.Generic;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: DS Thành viên giới thiệu", Code = "MListMembers", Order = 6)]
    public class MListMembersController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;

        public void ActionIndex(MListMembersModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            //if (model.ID > 0) { RedirectToAction("Detail"); return; }


            var dbQuery = ModWebUserService.Instance.GetByParentID_Cache(_WebUser.ID);
            List<Member> listMember = new List<Member>();
            for (int i = 0; dbQuery != null && i < dbQuery.Count; i++)
            {
                var _member = new Member();
                _member.Name = dbQuery[i].Name;
                _member.UserID = dbQuery[i].ID;
                _member.Created = dbQuery[i].Created;
                _member.Level = dbQuery[i].Level;
                _member.Address = dbQuery[i].Address;
                listMember.Add(_member);

                var child = ModWebUserService.Instance.GetByParentID_Cache(dbQuery[i].ID);

                for (int j = 0; child != null && j < child.Count; j++)
                {
                    var _member2 = new Member();
                    _member2.Name = child[j].Name;
                    _member2.UserID = child[j].ID;
                    _member2.Created = child[j].Created;
                    _member2.Level = child[j].Level;
                    _member2.Address = child[j].Address;
                    listMember.Add(_member2);

                    var child2 = ModWebUserService.Instance.GetByParentID_Cache(child[j].ID);
                    for (int z = 0; child2 != null && z < child2.Count; z++)
                    {
                        var _member3 = new Member();
                        _member3.Name = child2[z].Name;
                        _member3.UserID = child2[z].ID;
                        _member3.Created = child2[z].Created;
                        _member3.Level = child2[z].Level;
                        _member3.Address = child2[z].Address;
                        listMember.Add(_member3);
                    }
                }

            }
            ViewBag.User = _WebUser;
            ViewBag.Data = listMember;
            ViewBag.Model = model;
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }



    }

    public class MListMembersModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }

    public class Member
    {
        public int UserID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
        public DateTime Created { get; set; }
        public int Level { get; set; }
    }
}