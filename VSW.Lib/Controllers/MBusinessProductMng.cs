﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Quản lý Sản phẩm", Code = "MBusinessProductMng", Order = 6)]
    public class MBusinessProductMngController : Controller
    {
        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;
        public void ActionIndex(MBusinessProductMngModel model)
        {
            var _WebUser = WebLogin.CurrentUser.GetShop();
            if (_WebUser == null) ViewPage.AlertThenRedirect("Bạn chưa phải là đối tác của Berich", "/");

            if (model.idprd > 0)
            {
                ViewBag.Product = ModProductService.Instance.GetByID_Cache(model.idprd);
                RedirectToAction("Detail");
                return;
            }



            var dbQuery = ModProductService.Instance.CreateQuery()
                                                    .Where(o => o.ShopID == _WebUser.ID)
                                                    .WhereIn(model.CategoryID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", model.CategoryID, ViewPage.CurrentLang.ID))
                                                    .Take(PageSize)
                                                    .Skip(PageSize * model.page)
                                                    .OrderByAsc(o => new { o.Name, o.ID });

            var dbCategory = ModMenuShopService.Instance.CreateQuery()
                                                    .Where(o => o.ShopID == _WebUser.ID)
                                                    .OrderByAsc(o => new { o.ID, o.Name })
                                                    .ToList_Cache();
            ViewBag.User = _WebUser;
            ViewBag.Data = dbQuery.ToList_Cache();
            ViewBag.Category = dbCategory;
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);


        }

        public void ActionDetail(MBusinessProductMngModel model)
        {

            var item = ViewBag.Product as ModProductEntity;

            if (item != null)
            {

                var _WebUser = WebLogin.CurrentUser.GetShop();
                ViewBag.User = _WebUser;
                ViewBag.Product = item;

                //SEO
                ViewPage.CurrentPage.PageTitle = "Sản phẩm - " + item.Name;
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Code);
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(item.File, 200, 200);
            }
            else
            {
                ViewPage.Error404();
            }
        }

        public void ActionProductMngPOST(MBusinessProductMngModel model, ModProductEntity item)
        {
            item = ViewBag.Product;
            TryUpdateModel(item);


            if (item.Price < 1)
                ViewPage.Message.ListMessage.Add("Nhập: Giá bán");

            if (item.Price2 < 1)
                ViewPage.Message.ListMessage.Add("Nhập: Giá gốc.");

            if (item.Inventory < 1)
                ViewPage.Message.ListMessage.Add("Nhập: Số lượng tồn kho.");

            if (item.MenuID < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Chuyên mục sản phẩm.");

            if (item.FromCityID < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Kho hàng.");


            if (item.Content.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mô tả chi tiết sản phẩm.");

            string sVY = Core.Global.CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                //cap nhat thuoc tinh
                ModPropertyService.Instance.Delete(o => o.ProductID == item.ID);

                var _AllKey = ViewPage.PageViewState.AllKeys;
                for (var i = 0; i < _AllKey.Length; i++)
                {
                    if (!_AllKey[i].StartsWith("Property_")) continue;

                    string[] arrProperty = _AllKey[i].Split('_');
                    if (arrProperty == null || arrProperty.Length < 2) continue;

                    int PropertyID = 0, PropertyValueID = 0;

                    if (arrProperty.Length == 2)
                    {
                        PropertyID = Core.Global.Convert.ToInt(_AllKey[i].Replace("Property_", ""));
                        PropertyValueID = Core.Global.Convert.ToInt(ViewPage.PageViewState[_AllKey[i]]);
                    }
                    else if (arrProperty.Length == 3)
                    {
                        PropertyID = Core.Global.Convert.ToInt(arrProperty[1]);
                        PropertyValueID = Core.Global.Convert.ToInt(ViewPage.PageViewState[_AllKey[i]]);
                        //PropertyValueID = Core.Global.Convert.ToInt(arrProperty[2]);
                    }

                    if (PropertyID < 1 || PropertyValueID < 1) continue;

                    var _Property = ModPropertyService.Instance.GetByID(item.ID, item.MenuID, PropertyID, PropertyValueID);
                    if (_Property == null)
                    {
                        _Property = new ModPropertyEntity();
                        _Property.ID = 0;
                        _Property.ProductID = item.ID;
                        _Property.MenuID = item.MenuID;
                        _Property.PropertyID = PropertyID;
                        _Property.PropertyValueID = PropertyValueID;

                        ModPropertyService.Instance.Save(_Property);
                    }
                }

                ModProductService.Instance.Save(item, o => new {o.FromCityID, o.Price, o.Price2, o.Inventory, o.Content, o.FlastSale, o.MenuID });
                VSW.Core.Web.Cache.Clear(ModProductService.Instance);
                ViewPage.Alert("Cập nhật thành công");
            }

            ViewBag.Product = item;
            ViewBag.Model = model;
        }

        public void ActionImagePost(MBuninessSettingCPModel model, ModProductEntity item)
        {
            item = ViewBag.Product;

            TryUpdateModel(item);

            //logo, banner
            #region Images
            for (int i = 0; i < ViewPage.Request.Files.Count; i++)
            {
                var _File = ViewPage.Request.Files[i];
                if (_File == null) continue;

                string _FileName = _File.FileName;
                if (_FileName.Length < 1) continue;

                if (_File.ContentLength > 5 * 1024 * 1024)
                {
                    ViewPage.Message.ListMessage.Add("Dung lượng File " + (i + 1) + " không được lớn hơn 5Mb.");
                    continue;
                }

                try
                {
                    new Bitmap(_File.InputStream);

                    string _Path = ViewPage.Server.MapPath("~/Data/upload/images/user/" + item.GetShop().GetWebUser().LoginName + "/shop/Product/");

                    if (!System.IO.Directory.Exists(_Path))
                        System.IO.Directory.CreateDirectory(_Path);

                    string _DBFile = "~/Data/upload/images/user/" + item.GetShop().GetWebUser().LoginName + "/shop/Product/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(_FileName)) + System.IO.Path.GetExtension(_FileName);
                    string _PhysFile = ViewPage.Server.MapPath(_DBFile);

                    _File.SaveAs(_PhysFile);

                    item.File = _DBFile;
                }
                catch
                {
                    ViewPage.Message.ListMessage.Add("File tải lên không phải ảnh. Chọn file khác.");
                    continue;
                }
            }
            #endregion

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {

                ModProductService.Instance.Save(item, o => new { o.File });

                ViewPage.Alert("Đã cập nhật ảnh logo.");
            }

            ViewBag.Product = item;
            ViewBag.Model = model;
        }

        public void ActionImagesPOST(ModProductEntity item, MBusinessProductMngModel model)
        {
            item = ViewBag.Product;

            TryUpdateModel(item);

            ViewBag.Product = item;
            ViewBag.Model = model;

            #region Images


            var listAllKey = ViewPage.Request.Files.AllKeys;
            var listFileOverViewUpload = new List<HttpPostedFile>();

            for (int i = 0; i < listAllKey.Length; i++)
            {
                if (listAllKey[i].StartsWith("FileOverView"))
                    listFileOverViewUpload.Add(ViewPage.Request.Files[i]);
            }

       
            List<string> listMessage = new List<string>();

            listMessage.AddRange(Upload(listFileOverViewUpload, Data.RemoveVietNamese(item.GetShop().GetWebUser().LoginName), item.GetShop().GetWebUser().LoginName, model));

            for (int i = 0; i < listMessage.Count; i++)
                ViewPage.Message.ListMessage.Add(listMessage[i]);

            var arrFileOverView = model.FileOverView != null ? model.FileOverView : new List<string>();

            if ( model.FileOverView.Count <= 3)
                ViewPage.Message.ListMessage.Add("Up ít nhất 3 ảnh Chi tiết sản phẩm.");


            #endregion

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                try

                {
                    ModProductFileService.Instance.Delete(o => o.ProductID == item.ID);
                    for (int i = 0; model.FileOverView != null && i < model.FileOverView.Count; i++)
                    {
                        ModProductFileService.Instance.Save(new ModProductFileEntity()
                        {
                            ID = 0,
                            ProductID = item.ID,
                            File =  model.FileOverView[i],
                            Order = GetMaxProductFileOrder(item.ID),
                            Activity = true
                        });
                    }

                }
                catch (Exception ex)
                {
                    Error.Write(ex);
                    ViewPage.Alert(ex.Message);
                }
            }
        }
        private static List<string> Upload(List<HttpPostedFile> listItem, string userPath, string imagePath, MBusinessProductMngModel model)
        {
            List<string> listMessage = new List<string>();
            List<string> _ihihi = new List<string>();
            for (int i = 0; i < listItem.Count; i++)
            {
                if (listItem[i] == null) continue;

                string fileName = listItem[i].FileName;
                if (fileName.Length < 1) continue;

                if (listItem[i].ContentLength > 5 * 1024 * 1024)
                {
                    listMessage.Add("Ảnh thứ " + (i + 1) + " dung lượng lớn hơn 5Mb.");
                    continue;
                }

                try
                {
                    new Bitmap(listItem[i].InputStream);

                    string path = HttpContext.Current.Server.MapPath("~/Data/upload/images/user/" + imagePath + "/shop/Product/");

                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    string dbFile = "~/Data/upload/images/user/" + imagePath + "/shop/Product/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(fileName)) + System.IO.Path.GetExtension(fileName);
                    string svFile = HttpContext.Current.Server.MapPath(dbFile);

                    //luu server
                    listItem[i].SaveAs(svFile);

                    //luu model 
                   
                    if (imagePath == userPath)
                    {

                        _ihihi.Add(dbFile);
                    }
                   
                }
                catch
                {
                    listMessage.Add("File thứ " + (i + 1) + " không phải ảnh. Chọn file khác.");
                    continue;
                }
            }
             model.FileOverView = _ihihi;

            return listMessage;
        }
        private static int GetMaxProductFileOrder(int productID)
        {
            return ModProductFileService.Instance.CreateQuery()
                    .Where(o => o.ProductID == productID)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }


    }

    public class MBusinessProductMngModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
        public int CategoryID { get; set; }

        public int idprd { get; set; }
        public List<string> FileOverView { get; set; }

    }
}