﻿using System.Collections.Specialized;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "ĐK : Menu", Code = "CMenu", IsControl = true, Order = 2)]
    public class CMenuController : Controller
    {
        //[Core.MVC.PropertyInfo("Default[PageID-true|MenuIDMF-false|MenuIDCT-false|State-false],Top[PageID-true|MenuIDMF-true|MenuIDCT-true|State-true],MobileHeader[PageID-true|MenuIDMF-true|MenuIDCT-true|State-true]")]
        //public string LayoutDefine;

        [Core.MVC.PropertyInfo("Trang")]
        public int PageID;

        //[Core.MVC.PropertyInfo("Tiêu đề")]
        //public string Title;

        public override void OnLoad()
        {
            var _Page = SysPageService.Instance.GetByID_Cache(PageID);
            if (_Page != null)
            {
                ViewBag.Data = SysPageService.Instance.GetByParent_Cache(_Page.ID);
                ViewBag.Page = _Page;
            }
            else
            {
                ViewBag.Data = SysPageService.Instance.CreateQuery()
                                        .Where(o => o.LangID == ViewPage.CurrentLang.ID && o.Activity == true)
                                        .OrderByAsc(o => new { o.Order, o.ID })
                                        .ToList_Cache();
            }

            //ViewBag.Title = Title;
        }

        public void ActionChangeState()
        {
            string value = Global.Cookies.GetValue("HandheldMode");

            if (string.IsNullOrEmpty(value) || value.ToLower() == "pc") Global.Cookies.SetValue("HandheldMode", "mobile");
            else Global.Cookies.SetValue("HandheldMode", "pc");

            ViewPage.RefreshPage();
        }
    }
}