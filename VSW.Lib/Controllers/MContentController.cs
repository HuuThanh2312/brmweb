﻿using VSW.Lib.Global;
using VSW.Lib.MVC;
using Controller = VSW.Lib.MVC.Controller;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO: Bài viết", Code = "MContent", Order = 11)]
    public class MContentController : Controller
    {
        public void ActionIndex()
        {
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }
    }
}