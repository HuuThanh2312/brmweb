﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Script.Serialization;
using VSW.Core.Global;
using VSW.Core.Web;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : MAjax", Code = "MAjax", Order = 10)]
    public class MAjaxController : Controller
    {
        public void ActionIndex()
        {
        }
        #region api Giao hàng nhanh
        public void ActionGhn(GetDistrictGhnModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var keyCache = Cache.CreateKey("GHN_City" + "." + DateTime.Now.Date);
            var keyCachePrice = Cache.CreateKey("GHN_DisTrict_City" + "." + DateTime.Now.Date);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) { Json.Instance.Html = obj.ToString(); Json.Instance.Js = Cache.GetValue(keyCachePrice).ToString(); }
            else
            {
                string url = Config.GetValue("GHN.GetDistricts").ToString();

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        token = "TokenStaging"
                    });

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var listdata = JsonConvert.DeserializeObject<RootObject>(result);
                    var listDictrict = listdata.Data;

                    var root = new RootObject();

                    var listCity = root.DataCity;


                    for (int i = 0; listDictrict != null && i < listDictrict.Count; i++)
                    {
                        if (listCity == null)
                        {
                            listCity = new List<GetCityGhnModel>();
                            listCity.Add(new GetCityGhnModel { ProvinceID = listDictrict[i].ProvinceID, ProvinceName = listDictrict[i].ProvinceName });
                            Json.Instance.Js += @"<li class=""shopee-address-picker__current-level-list-item districtID_ghn"" data-cityid=""" + listDictrict[i].ProvinceID + @""">" + listDictrict[i].ProvinceName + @"</li>";
                        }
                        else if (listCity != null && listCity.Find(o => o.ProvinceID == listDictrict[i].ProvinceID) == null)
                        {
                            listCity.Add(new GetCityGhnModel { ProvinceID = listDictrict[i].ProvinceID, ProvinceName = listDictrict[i].ProvinceName });
                            Json.Instance.Js += @"<li class=""shopee-address-picker__current-level-list-item districtID_ghn"" data-cityid=""" + listDictrict[i].ProvinceID + @""">" + listDictrict[i].ProvinceName + @"</li>";
                        }
                        Json.Instance.Html += @"<li id=""districtIDghn"" class=""shopee-address-picker__current-level-list-item districtIDghn"" data-districtid=""" + listDictrict[i].DistrictID + @""">" + listDictrict[i].DistrictName + ", " + listDictrict[i].ProvinceName + @"</li>";
                    }
                    Cache.SetValue(keyCache, Json.Instance.Html);
                    Cache.SetValue(keyCachePrice, Json.Instance.Js);

                }
            }
            ViewBag.Model = model;
            Json.Create();
        }

        public void ActionGhnShow(GetDistrictGhnModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var keyCache = Cache.CreateKey("GHN_NameCity_Show" + "." + DateTime.Now.Date);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) { Json.Instance.Html = obj.ToString(); }
            else
            {
                string url = Config.GetValue("GHN.GetDistricts").ToString();


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        token = "TokenStaging"
                    });

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var listdata = JsonConvert.DeserializeObject<RootObject>(result);
                    var listDictrict = listdata.Data;


                    for (int i = 0; listDictrict != null && i < listDictrict.Count; i++)
                    {
                        if (listDictrict[i].DistrictID == model.DistrictID)
                        {
                            Json.Instance.Html = listDictrict[i].ProvinceName + ", " + listDictrict[i].ProvinceName;
                        }
                        else if (listDictrict[i].ProvinceID == model.DistrictID)
                        {
                            Json.Instance.Html = listDictrict[i].ProvinceName;
                        }
                    }

                    Cache.SetValue(keyCache, Json.Instance.Html);

                }
            }
            ViewBag.Model = model;
            Json.Create();
        }

        public void ActionGhn_Fee(GetDistrictGhnModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;


            var keyCache = Cache.CreateKey("GHN_DisTrict_Fee" + "." + model.ToDistrictID);
            var keyCachePrice = Cache.CreateKey("GHN_DisTrict_Price" + "." + model.ToDistrictID);
            var keyCachePrice2 = Cache.CreateKey("GHN_DisTrict_Price2" + "." + model.ToDistrictID);
            var obj = Cache.GetValue(keyCache);
            if (obj != null) { Json.Instance.Html = obj.ToString(); Json.Instance.Js = Cache.GetValue(keyCachePrice).ToString(); Json.Instance.Params = Cache.GetValue(keyCachePrice2).ToString(); }
            else
            {

                string url = Config.GetValue("GHN.FindAvailableServices").ToString();

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        token = "TokenStaging",
                        Weight = 1000,
                        Length = 10,
                        Width = 100,
                        Height = 20,
                        FromDistrictID = model.FromDistrictID,
                        ToDistrictID = model.ToDistrictID
                    });

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    var listdata = JsonConvert.DeserializeObject<RootObject>(result);
                    var listDictrict = listdata.Data;
                    listDictrict.Sort((x, y) => x.ServiceFee.CompareTo(y.ServiceFee));
                    string _price = "";
                    for (int i = 0; listDictrict != null && i < listDictrict.Count; i++)
                    {
                        var _ngaygiao = Math.Round((listDictrict[i].ExpectedDeliveryTime - DateTime.Now).TotalDays);
                        Json.Instance.Html += @"<div class=""giao1 mb10"">
                                <p class=""all"">
                                    <span class=""pull-left"">" + listDictrict[i].Name + @"</span>
                                    <span class=""pull-right"">" + string.Format("{0:#,##0}", listDictrict[i].ServiceFee) + @"đ</span>
                                </p>
                                <p>
                                    <span style = ""font-size: 12px;"" >Giao hàng từ " + _ngaygiao + " - " + (_ngaygiao + 1) + @" ngày</span>
                                </p>
                                <div class=""_1FtLoT"">
                                    Miễn Phí Vận Chuyển for orders from<span class=""text-color"">₫99.000</span> (up to <span class=""text-color"">₫40.000</span>)<br>
                                    Miễn Phí Vận Chuyển for orders from<span class=""text-color"">₫200.000</span> (up to <span class=""text-color"">₫40.000</span>)
                                </div>
                            </div>";
                        _price += string.Format("{0:#,##0}", listDictrict[i].ServiceFee) + "đ - ";
                        Json.Instance.Js = !string.IsNullOrEmpty(_price) ? _price.Remove(_price.Length - 2) : "";
                        Json.Instance.Params = listDictrict[0].ServiceFee.ToString();
                    }
                    Cache.SetValue(keyCache, Json.Instance.Html);
                    Cache.SetValue(keyCachePrice2, Json.Instance.Params);
                    Cache.SetValue(keyCachePrice, Json.Instance.Js);

                }
            }
            ViewBag.Model = model;
            Json.Create();
        }
        #endregion

        public void ActionSecurity(SecurityModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            Captcha.CaseSensitive = false;
            var ci = new Captcha(175, 35);

            ViewPage.Response.Clear();
            ViewPage.Response.ContentType = "image/jpeg";

            ci.Image.Save(ViewPage.Response.OutputStream, ImageFormat.Jpeg);
            ci.Dispose();

            ViewBag.Model = model;

            ViewPage.Response.End();
        }

        public void ActionGetChild(GetChildModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var listItem = WebMenuService.Instance.CreateQuery()
                                    .Select(o => new { o.ID, o.Name })
                                    .Where(o => o.Activity == true && o.Type == "City" && o.ParentID == model.ParentID)
                                    .OrderByAsc(o => new { o.Order, o.ID })
                                    .ToList_Cache();

            Json.Instance.Html += @"<option value=""0"" selected=""selected"">- chọn quận / huyện -</option>";
            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<option value=""" + listItem[i].ID + @""" " + (listItem[i].ID == model.SelectedID ? @"selected=""selected""" : @"") + @">" + listItem[i].Name + @"</option>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        #region property
        public void ActionGetProperties2(GetPropertiesModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var _Menu = WebMenuService.Instance.GetByID(model.MenuID);
            if (_Menu == null) Json.Create();

            var _Property = WebPropertyService.Instance.GetByID(_Menu.PropertyID);
            if (_Property == null) Json.Create();

            var listProperty = WebPropertyService.Instance.CreateQuery()
                                        .Select(o => new { o.ID, o.Name, o.Code, o.Multiple })
                                        .Where(o => o.Activity == true && o.ParentID == _Property.ID)
                                        .OrderByAsc(o => o.Order)
                                        .ToList_Cache();

            for (var i = 0; listProperty != null && i < listProperty.Count; i++)
            {
                var listPropertyValue = WebPropertyService.Instance.CreateQuery()
                                                    .Select(o => new { o.ID, o.Name })
                                                    .Where(o => o.ParentID == listProperty[i].ID)
                                                    .OrderByAsc(o => o.Order)
                                                    .ToList_Cache();

                if (listProperty[i].Multiple)
                {
                    Json.Instance.Html += @"<div class=""form-group row"">
                                                <label class=""col-md-3 col-form-label text-right"">" + listProperty[i].Name + @":</label>
                                                <div class=""col-md-9"">
                                                    <div class=""checkbox-list"">";

                    for (var j = 0; listPropertyValue != null && j < listPropertyValue.Count; j++)
                    {
                        var exists = model.ProductID > 0 ? ModPropertyService.Instance.GetByID(model.ProductID, model.MenuID, listProperty[i].ID, listPropertyValue[j].ID) != null : false;
                        Json.Instance.Html += @"        <label class=""itemCheckBox itemCheckBox-sm"">
                                                            <input type=""checkbox"" " + (exists ? "checked" : "") + @" name=""Property_" + listProperty[i].ID + @"_" + listPropertyValue[j].ID + @""" id=""Property_" + listProperty[i].ID + @"_" + listPropertyValue[j].ID + @""" value=""" + listPropertyValue[j].ID + @""" />
                                                            <i class=""check-box""></i>
                                                            <span>" + listPropertyValue[j].Name + @"</span>
                                                        </label>";
                    }

                    Json.Instance.Html += @"        </div>
                                                </div>
                                            </div>";
                }
                else
                {
                    Json.Instance.Html += @" <div class=""form-group row"">
                                                <label class=""col-md-3 col-form-label text-right"">" + listProperty[i].Name + @":</label>
                                                <div class=""col-md-9"">
                                                    <select class=""form-control"" name=""Property_" + listProperty[i].ID + @""" id=""Property_" + listProperty[i].ID + @""">
                                                        <option value=""0"">Root</option>";

                    for (var j = 0; listPropertyValue != null && j < listPropertyValue.Count; j++)
                    {
                        var exists = model.ProductID > 0 ? ModPropertyService.Instance.GetByID(model.ProductID, model.MenuID, listProperty[i].ID, listPropertyValue[j].ID) != null : false;
                        Json.Instance.Html += @"        <option value=""" + listPropertyValue[j].ID + @""" " + (exists ? "selected" : "") + @">" + listPropertyValue[j].Name + @"</option>";
                    }

                    Json.Instance.Html += @"        </select>
                                                </div>
                                            </div>";
                }
            }

            Json.Create();
        }

        public void ActionGetProperties(GetPropertiesModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var _Menu = WebMenuService.Instance.GetByID(model.MenuID);
            if (_Menu == null) Json.Create();

            var _Property = WebPropertyService.Instance.GetByID(_Menu.PropertyID);
            if (_Property == null) Json.Create();

            var listProperty = WebPropertyService.Instance.CreateQuery()
                                        .Select(o => new { o.ID, o.Name, o.Code, o.Multiple })
                                        .Where(o => o.Activity == true && o.ParentID == _Property.ID)
                                        .OrderByAsc(o => o.Order)
                                        .ToList_Cache();

            for (var i = 0; listProperty != null && i < listProperty.Count; i++)
            {
                var listPropertyValue = WebPropertyService.Instance.CreateQuery()
                                                    .Select(o => new { o.ID, o.Name })
                                                    .Where(o => o.ParentID == listProperty[i].ID)
                                                    .OrderByAsc(o => o.Order)
                                                    .ToList_Cache();

                Json.Instance.Html += @"<div class=""row"">
                                            <div class=""form-group clear"">
                                                <div class=""text-left col-md-4"">
                                                    <label>" + listProperty[i].Name + @"</label>
                                                </div>
                                                <div class=""text-left col-md-8"">";

                if (listProperty[i].Multiple)
                {
                    for (var j = 0; listPropertyValue != null && j < listPropertyValue.Count; j++)
                    {
                        var _Exists = model.ProductID > 0 ? ModPropertyService.Instance.GetByID(model.ProductID, model.MenuID, listProperty[i].ID, listPropertyValue[j].ID) != null : false;
                        Json.Instance.Html += @"    <label class=""itemCheckBox"">
                                                        <input type=""checkbox"" name=""Property_" + listProperty[i].ID + "_" + listPropertyValue[j].ID + @""" id=""Property_" + listProperty[i].ID + "_" + listPropertyValue[j].ID + @""" value=""" + listPropertyValue[j].ID + @""" " + (_Exists ? "checked" : "") + @" /><i class=""check-box""></i><span>" + listPropertyValue[j].Name + @"</span>
                                                    </label>";
                    }
                }
                else
                {
                    Json.Instance.Html += @"        <select class=""form-control fl"" name=""Property_" + listProperty[i].ID + @""" id=""Property_" + listProperty[i].ID + @""">
                                                        <option>- " + listProperty[i].Name + @" -</option>";

                    for (var j = 0; listPropertyValue != null && j < listPropertyValue.Count; j++)
                    {
                        var _Exists = model.ProductID > 0 ? ModPropertyService.Instance.GetByID(model.ProductID, model.MenuID, listProperty[i].ID, listPropertyValue[j].ID) != null : false;
                        Json.Instance.Html += @"        <option value=""" + listPropertyValue[j].ID + @""" " + (_Exists ? "selected" : "") + @">" + listPropertyValue[j].Name + @"</option>";
                    }

                    Json.Instance.Html += @"        </select>";
                }

                Json.Instance.Html += @"        </div>
                                            </div>
                                        </div>";
            }

            Json.Create();
        }
        #endregion

        #region account user validate

        public void ActionGetAccount(GetAccountModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;
            if (!string.IsNullOrEmpty(model.LoginName))
            {
                if (model.Type == "LoginName")
                {
                    var _user = ModWebUserService.Instance.GetByLogiName(model.LoginName);

                    if (!VSW.Lib.Global.Utils.IsLoginName(model.LoginName))
                    {
                        Json.Instance.Html = "Tên tài khoản sai định dạng (vd: berichmart)";
                    }
                    else if (_user == null)
                    {
                        Json.Instance.Html = "Bạn có thể dùng tên tài khoản này.";
                    }
                    else Json.Instance.Html = "Tài khoản đã có người sử dụng.";
                }
                else if (model.Type == "email")
                {
                    var _user = ModWebUserService.Instance.GetByEmail(model.LoginName);

                    if (!VSW.Lib.Global.Utils.IsEmailAddress(model.LoginName))
                    {
                        Json.Instance.Html = "Sai định dạng email.";
                    }
                    else if (_user == null)
                    {
                        Json.Instance.Html = "Bạn có thể dùng email này.";
                    }
                    else Json.Instance.Html = "Email đã tồn tại.";
                }
                else if (model.Type == "Shop")
                {
                    var _user = ModShopService.Instance.GetByUrl_Cache(model.LoginName);
                    if (_user == null)
                    {
                        Json.Instance.Html = "Bạn có thể dùng URL này.";
                    }
                    else Json.Instance.Html = "URL đã tồn tại.";
                }
                else if (model.Type == "Phone")
                {
                    var hasPhone = new Regex(@"^[0-9]{3}.[0-9]{3}.[0-9]{4}$");

                    var _user = ModWebUserService.Instance.CheckPhone(model.LoginName.Replace(".",""));
                    if (!hasPhone.IsMatch(model.LoginName) || model.LoginName.Length != 12)
                    {
                        Json.Instance.Html = "Sai định dạng số điện thoại.";
                    }
                    else if (!_user)
                    {
                        Json.Instance.Html = "Bạn có thể dùng số điện thoại này.";
                    }

                    else Json.Instance.Html = "Số điện thoại đã tồn tại.";
                }

                else if (model.Type == "Pass")
                {
                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasMinimum8Chars = new Regex(@".{8,}");

                    var isValidated = hasNumber.IsMatch(model.LoginName) && hasUpperChar.IsMatch(model.LoginName) && hasMinimum8Chars.IsMatch(model.LoginName);
                    if (isValidated) Json.Instance.Html = "Mật khẩu có độ bảo mật tốt.";
                    else Json.Instance.Html = "Thêm ký tự viết hoa và ký tự là số, độ dài hơn 8 ký tự";
                }

            }

            ViewBag.Model = model;

            Json.Create();
        }


        public void ActionGetCardOnline(GetCardOnlineModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;
            if (model.ID > 0)
            {
                var list = WebMenuService.Instance.GetByParentID_Cache(model.ID);
                Json.Instance.Html = @"<option value=""0"">--Chọn--</option>";
                for (int i = 0; list != null && i < list.Count; i++)
                {
                    Json.Instance.Html += @"<option value=""" + list[i].ID + @""">" + list[i].Name + @"</option>";
                }

            }

            ViewBag.Model = model;

            Json.Create();
        }
        public void ActionShowInputBank(GetAccountModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;
            if (!string.IsNullOrEmpty(model.Type))
            {
                if (model.Type == "Add")
                {

                    Json.Instance.Html = @"<form method=""post"" accept-charset=""utf-8"">
                                            <div class=""form-group"">
                                                <input type=""text"" name=""NameBank"" class=""form-control"" value="""" placeholder=""Tên ngân hàng"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type =""text"" name=""AddressBank"" class=""form-control""  value="""" placeholder=""Chi nhánh ngân hàng"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type=""text"" name=""AccountCode"" class=""form-control""  value="""" placeholder=""Tài khoản ngân hàng"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type=""text"" name=""CardCode"" class=""form-control""  value="""" placeholder=""Số thẻ"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type=""text"" name=""Name"" class=""form-control"" value="""" placeholder=""Tên tài khoản"">
                                            </div>
                                            <div class=""form-group"">
                                                  <label class=""radioPure"">
                                                    <input id=""radio3"" type=""radio"" name=""Activity"" value =""1""><span class=""outer""><span class=""inner""></span></span><i>Tài khoản mặc định</i>
                                                  </label>
                                                  <label class=""radioPure"">
                                                    <input id = ""radio4"" type=""radio"" name=""Activity"" checked="""" value=""0""><span class=""outer""><span class=""inner""></span></span><i>Không</i>
                                                  </label>
                                            </div>
                                            <div class=""form-group"">
                                                <div class=""row"">
                                                    <div class=""col-xs-6"">
                                                        <input type=""text"" name=""ValidCode"" id=""ValidCode"" required=""required"" class=""form-control"" placeholder=""Mã bảo mật"">
                                                    </div>
                                                    <div class=""col-xs-6"">
                                                        <img src=""/ajax/Security.html"" style=""height: 35px;"" id=""imgValidCode"" alt=""security code"" />
                                                        <a href=""javascript:void(0)"" onclick=""change_captcha()"" class=""btn btn-icon-only default"">
                                                            <i class=""fa fa-refresh""></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=""form-group text-center mt10"">
                                                <button type=""submit"" name=""_vsw_action[BankingPOST]"" class=""btn blue btn-outline"">Lưu lại</button>
                                                <button type=""button""  onclick=""clearText()"" class=""btn dark btn-outline"">Nhập lại</button>
                                            </div>
                                        </form>";
                }
                else
                {
                    int _ID = VSW.Core.Global.Convert.ToInt(model.Type);
                    var _Bank = ModBankingUserService.Instance.GetByID(_ID);
                    if (_Bank != null)
                    {

                        Json.Instance.Html = @"<form method=""post"" accept-charset=""utf-8"">
                                            <input type=""hidden"" name=""ID"" value=""" + model.Type + @""" class=""form-control"" hidden=""hidden"" />
                                            <div class=""form-group"">
                                                <input type=""text"" name=""NameBank"" class=""form-control"" value=""" + _Bank.NameBank + @""" placeholder=""Tên ngân hàng"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type =""text"" name=""AddressBank"" class=""form-control""  value=""" + _Bank.AddressBank + @""" placeholder=""Chi nhánh ngân hàng"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type=""text"" name=""AccountCode"" class=""form-control""  value=""" + _Bank.AccountCode + @""" placeholder=""Tài khoản ngân hàng"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type=""text"" name=""CardCode"" class=""form-control""  value=""" + _Bank.CardCode + @""" placeholder=""Số thẻ"">
                                            </div>
                                            <div class=""form-group"">
                                                <input type=""text"" name=""Name"" class=""form-control"" value=""" + _Bank.Name + @""" placeholder=""Tên tài khoản"">
                                            </div>
                                            <div class=""form-group"">
                                                  <label class=""radioPure"">
                                                    <input id=""radio3"" type=""radio"" name=""Activity"" " + (_Bank.Activity ? "checked=\"\"" : string.Empty) + @" value =""1""><span class=""outer""><span class=""inner""></span></span><i>Tài khoản mặc định</i>
                                                  </label>
                                                  <label class=""radioPure"">
                                                    <input id = ""radio4"" type=""radio"" name=""Activity"" " + (!_Bank.Activity ? "checked=\"\"" : string.Empty) + @" value=""0""><span class=""outer""><span class=""inner""></span></span><i>Không</i>
                                                  </label>
                                            </div>
                                            <div class=""form-group"">
                                                <div class=""row"">
                                                    <div class=""col-xs-6"">
                                                        <input type=""text"" name=""ValidCode"" id=""ValidCode"" required=""required"" class=""form-control"" placeholder=""Mã bảo mật"">
                                                    </div>
                                                    <div class=""col-xs-6"">
                                                        <img src=""/ajax/Security.html"" style=""height: 35px;"" id=""imgValidCode"" alt=""security code"" />
                                                        <a href=""javascript:void(0)"" onclick=""change_captcha()"" class=""btn btn-icon-only default"">
                                                            <i class=""fa fa-refresh""></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=""form-group text-center mt10"">
                                                <button type=""submit"" name=""_vsw_action[BankingPOST]"" class=""btn blue btn-outline"">Cập nhật</button>
                                                <button type=""button""  onclick=""clearText()"" class=""btn dark btn-outline"">Nhập lại</button>
                                            </div>
                                        </form>";
                    }

                }
            }

            ViewBag.Model = model;

            Json.Create();
        }

        #endregion


        #region order

        public void ActionCancelOrder(GetOrderModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var _order = ModOrderDetailService.Instance.GetByID(model.OrderID);
            if (_order == null) Json.Instance.Params += "Đơn hàng không tồn tại.";
            else if (_order != null && model.StatusID < 1) Json.Instance.Params += "Chọn lý do hủy đơn.";
            else
            {
                _order.CancelID = model.StatusID;
                _order.StatusID = 1233;
                _order.Content = model.Content;
                ModOrderDetailService.Instance.Save(_order, o => new { o.CancelID, o.StatusID, o.Content });

                Json.Instance.Html += "Bạn đã hủy yêu cầu mua sản phẩm " + _order.Name + "<br/> Lý do hủy: " + _order.GetCancel().Name + " thành công.";
                VSW.Core.Web.Cache.Clear(ModOrderDetailService.Instance);
            }


            ViewBag.Model = model;

            Json.Create();
        }


        public void ActionUpdateOrder(GetOrderModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var _order = ModOrderDetailService.Instance.GetByID(model.OrderID);
            if (_order == null) Json.Instance.Params += "Đơn hàng không tồn tại.";
            else if (_order != null && model.StatusID < 1) Json.Instance.Params += "Chọn trạng thái đơn hàng.";
            else
            {
                _order.CancelID = 0;
                _order.StatusID = model.StatusID;
                _order.Content = model.Content;
                ModOrderDetailService.Instance.Save(_order, o => new { o.StatusID, o.Content });

                Json.Instance.Html += "Bạn đã chuyển trạng thái: <p style=\"margin-bottom:2px; margin-top:2px;font-size:16px;color:#34b44a;\">" + _order.GetStatus().Name + "</p> Sản phẩm " + _order.Name + " thành công.";
                if (_order.CancelID < 1 && _order.StatusID == 1231)
                {
                    var _shopRevenue = ModBusinessRevenueService.Instance.GetByShopID(_order.ShopID);
                    if (_shopRevenue != null)
                    {
                        _shopRevenue.Revenue += _order.Price;
                        ModBusinessRevenueService.Instance.Save(_shopRevenue, o => new { o.Revenue });
                    }
                    else
                    {
                        ModBusinessRevenueService.Instance.Save(new ModBusinessRevenueEntity
                        {
                            ProductID = _order.ProductID,
                            OrderDetailID = _order.ID,
                            OrderID = _order.OrderID,
                            Published = DateTime.Now,
                            ShopID = _order.ShopID,
                            Revenue = _order.Price,
                            Name = "Doanh thu Sản phẩm " + _order.Name
                        });
                    }
                    VSW.Core.Web.Cache.Clear(ModBusinessRevenueService.Instance);
                }

                VSW.Core.Web.Cache.Clear(ModOrderDetailService.Instance);

            }
            ViewBag.Model = model;

            Json.Create();
        }

        #endregion

        #region Comment Review Show
        public void ActionCommentPOST(CommentPOSTModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (model.ProductID < 1)
            {
                Json.Instance.Params += "Sản phẩm không tồn tại.";
                Json.Create();
            }

            if (string.IsNullOrEmpty(model.Content))
            {
                Json.Instance.Params += "Nhập nội dung bình luận.";
                Json.Create();
            }

            ModCommentEntity _item = new ModCommentEntity();

            _item.WebUserID = WebLogin.CurrentUser.ID;
            _item.ParentID = model.ParentID;
            _item.ProductID = model.ProductID;
            _item.Name = model.Name;
            _item.Content = model.Content;
            _item.Created = DateTime.Now;
            _item.Activity = true;

            ModCommentService.Instance.Save(_item);

            if (_item.ParentID == 0)
                Json.Instance.Html += @"<li>
                                            <div class=""media"">
                                                <div class=""media-left"" style=""width: 6%"">
                                                    <img src=""" + WebLogin.CurrentUser.File.Replace("~/", "/") + @""" class=""media-object"">
                                                </div>
                                                <div class=""media-body"">
                                                    <h4 class=""media-heading"">" + _item.Name + @"</h4>
                                                    <p>" + _item.Content + @"</p>
                                                    <small class=""gray"">" + _item.PublishedTime + @"</small>
                                                </div>
                                            </div>
                                            <hr>
                                        </li>";


            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionReviewPOST(CommentPOSTModel model)
        {

            var _Product = ModProductService.Instance.GetByID(model.ProductID);
            if (_Product == null)
            {
                Json.Instance.Params += "Sản phẩm không tồn tại.";
                Json.Create();
            }

            if (model.Vote < 1 && model.ParentID == 0)
            {
                Json.Instance.Params += "Quý khách vui lòng đánh giá sản phẩm của chúng tôi";
                Json.Create();
            }

            ModReviewEntity _item = new ModReviewEntity();

            _item.ID = 0;
            _item.WebUserID = WebLogin.WebUserID;
            _item.ProductID = model.ProductID;
            _item.Vote = model.Vote;
            _item.Name = model.Name;
            _item.Content = model.Content;
            _item.Created = DateTime.Now;
            _item.Activity = false;

            ModReviewService.Instance.Save(_item);



            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionGetComment(GetCommentModel model)
        {

            var dbQuery = ModReviewService.Instance.CreateQuery()
                                        .Where(o => o.Activity == true)
                                        .Where(o => o.ProductID == model.ProductID)
                                        .OrderByDesc(o => o.ID)
                                        .Take(model.PageSize)
                                        .Skip(model.PageSize * model.PageComment);

            var listComment = dbQuery.ToList_Cache();

            for (int i = 0; listComment != null && i < listComment.Count; i++)
            {

                if (string.IsNullOrEmpty(listComment[i].Content)) continue;
                Json.Instance.Html += @"<li class=""cmt"">
                                    <span>" + listComment[i].Name + @"</span><span class=""buy_complete"">Đã mua hàng tại đây</span>
                                    <div class=""pop_Info"">
                                        <h4>Umipharmacy.com.vn<span> Xác nhận:</span></h4>
                                        <p><i class=""fa fa-street-view""><span class=""pl5"">Khách hàng: </span></i>" + listComment[i].Name + @"</p>
                                        <p><i class=""fa fa-clock-o""></i><span>" + listComment[i].Time + @"</span></p>
                                    </div>
                                    <p>
                                        <span class=""icon-star-list""><i class=""fa fa-star""></i><i class=""fa fa-star""></i><i class=""fa fa-star""></i></span>
                                        <i>" + listComment[i].Content + @"</i>
                                    </p>
                                    <div class=""ra"">
                                        <a class=""cmtr"">0 thảo luận</a>
                                        <span>• </span>
                                        <a href=""javascprit:void(0)"" class=""cmtl"" data-like=""0""><i class=""iconcom-like""></i>Thích</a>
                                        <span>• </span>
                                        <a href=""javascprit:void(0)"" class=""cmtd"">" + listComment[i].Time + @"</a>
                                    </div>
                                </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        #endregion

        #region search shop con hang

        public void ActionSearchShopProduct(SearchShopProductModel model)
        {
            Thread.Sleep(500);
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var listItem = ModProductService.Instance.CreateQuery()
                                    .Where(o => o.Model == model.Model && o.Activity == true && o.ActivityPrice == true)
                                    .OrderByAsc(o => new { o.Order, o.ID })
                                    .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                var _shop = ModShopService.Instance.CreateQuery()
                                            .Where(o => o.ID == listItem[i].ShopID)
                                            .Where(o => o.CityID == model.AddressID || o.DistrictID == model.AddressID)
                                            .ToSingle();

                if (_shop != null)
                    Json.Instance.Html += @"<li>
                                            <a href=""" + ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code) + @""" class=""click-all""></a>
                                              <p>" + _shop.Address + " - " + _shop.GetDistrict().Name + " - " + _shop.GetCity().Name + @"</p>
                                              <p><span class=""green"">Còn " + listItem[i].Inventory + @" Sản phẩm</span></p>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }


        #endregion



    }



    #region search shop gần


    public class SearchShopProductModel
    {
        public string Model { get; set; }
        public int AddressID { get; set; }
    }

    #endregion
    public class SecurityModel
    {
        public string Code { get; set; }
    }

    public class GetChildModel
    {
        public int ParentID { get; set; }
        public int SelectedID { get; set; }
    }

    #region property
    public class GetPropertiesModel
    {
        public int MenuID { get; set; }
        public int LangID { get; set; }
        public int ProductID { get; set; }
    }
    #endregion

    #region account user
    public class GetAccountModel
    {
        public string LoginName { get; set; }
        public string Type { get; set; }
    }


    public class GetCardOnlineModel
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    #endregion

    #region order
    public class GetOrderModel
    {
        public int StatusID { get; set; }
        public int OrderID { get; set; }
        public string Content { get; set; }
    }

    #endregion

    #region comment



    public class CommentPOSTModel
    {
        public int ParentID { get; set; }
        public int ProductID { get; set; }
        public int Vote { get; set; }
        public int Like { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int ReviewID { get; set; }
    }
    public class GetCommentModel
    {
        public int ProductID { get; set; }
        public int PageComment { get; set; }
        public int PageSize { get; set; }
    }

    #endregion

    #region

    public class GetFeeGhnModel
    {
        public int FromDistrictID { get; set; }
        public int ServiceFee { get; set; }

        public int ToDistrictID { get; set; }
        public int Weight { get; set; }

    }

    public class GetDistrictGhnModel
    {
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }

        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }


        //fee

        public int FromDistrictID { get; set; }
        public int ServiceFee { get; set; }
        public string Name { get; set; }
        public DateTime ExpectedDeliveryTime { get; set; }
        public int ToDistrictID { get; set; }
        public int Weight { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int ServiceID { get; set; }


    }

    public class GetCityGhnModel
    {

        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }

    }

    public class RootObject
    {
        public List<GetDistrictGhnModel> Data { get; set; }
        public List<GetCityGhnModel> DataCity { get; set; }




    }

    #endregion
}