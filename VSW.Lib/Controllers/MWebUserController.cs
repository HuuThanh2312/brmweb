﻿
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO: Gian hàng", Code = "MWebUser", Order = 6)]
    public class MWebUserController : Controller
    {
        [Core.MVC.PropertyInfo("Chuyên mục", "Type|News")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 10;

        public void ActionIndex(MWebUserModel model)
        {
            var dbQuery = ModWebUserService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .OrderByDesc(o => o.ID)
                                    .Take(PageSize)
                                    .Skip(PageSize * model.page);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            var code = ViewPage.CurrentVQS.GetString(1);

            var webUser = ModWebUserService.Instance.GetByCode_Cache(code);
            if (webUser == null)
                ViewPage.Error404();

            if (ViewPage.CurrentVQS.Count > 3)
                ViewPage.Response.Redirect(ViewPage.GetURL("shop", webUser.Code, endCode));

            if (ViewPage.CurrentVQS.Count == 3)
            {
                code = ViewPage.CurrentVQS.GetString(2);

                if (code.ToLower() == "thong-tin-shop")
                    RenderView("Info");
                else
                {
                    var page = SysPageService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.Code == code)
                                    .ToSingle_Cache();

                    if (page == null)
                        ViewPage.Error404();

                    MenuID = page.MenuID;
                }
            }

            var model = new MWebUserModel();
            TryUpdateModel(model);

            var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                    .Take(PageSize)
                                    .Skip(PageSize * model.page)
                                    .OrderByDesc(o => new { o.Order, o.ID });

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            ViewPage.ViewBag.Shop = webUser;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }
    }

    public class MWebUserModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
    }                      
}