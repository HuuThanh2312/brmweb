﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using VSW.Core.Global;
using VSW.Core.Models;
using VSW.Core.MVC;
using VSW.Core.Web;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
using Controller = VSW.Lib.MVC.Controller;
using Cookies = VSW.Lib.Global.Cookies;
using Setting = VSW.Core.Web.Setting;
using Utils = VSW.Lib.Global.Utils;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Hoàn thành", Code = "MComplete", Order = 7)]
    public class MCompleteController : Controller
    {
        [PropertyInfo("Banner")]
        public string File = string.Empty;

        public void ActionIndex(ModOrderEntity item, MCompleteModel model)
        {

            item = ObjectCookies<ModOrderEntity>.GetValue("Orders");
            ModATMPayEntity item2 = ObjectCookies<ModATMPayEntity>.GetValue("ATMComplate");

            if (item == null && item2 == null)
            {
                ViewPage.AlertThenRedirect("Quý khách chưa có đơn hàng nào hoàn thành.", "/");
                return;
            }
            else if (item == null && item2 != null)
            {
                ViewBag.Data = item2;
            }
            else { ViewBag.Data = item; }

            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }


        //public void ActionAddPOST(ModOrderEntity item, MCompleteModel model)
        //{
        //    item = ObjectCookies<ModOrderEntity>.GetValue("Orders");

        //    long _Total = 0;
        //    var _Cart = new Cart();

        //    var html = @"<!DOCTYPE html>
        //                <html lang=""en"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;"">

        //                <body style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: #fff; background-image: none; background-repeat: repeat; background-position: top left; background-attachment: scroll; color: #222; font-size: 13px; font-family: Arial,Helvetica,sans-serif;"">
        //                    <div class=""container"" style=""padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: auto; margin-bottom: auto; margin-right: auto; margin-left: auto; max-width: 650px;"">
        //                        <table class=""table_mail"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; border-spacing: 0; border-collapse: collapse; font-size: 14px; max-width: 600px;"">
        //                            <tbody style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td class="""" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-spacing: 0; border-collapse: collapse; font-size: 14px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #dee2e3; width: 100%!important; background-color: transparent;"">
        //                                            <tbody style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                    <td width=""50%"" height=""49"" valign=""middle"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; width: 50%; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                                        <a href=""http://smartrain.vn/"" target=""_blank"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                            <img src=""cid:logo"" width=""100"" alt="""" class=""CToWUd"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"" />
        //                                                        </a>
        //                                                    </td>
        //                                                </tr>
        //                                            </tbody>
        //                                        </table>
        //                                    </td>
        //                                </tr>
        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td style=""width: 100%; margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <a href=""http://smartrain.vn/"" title="""" target=""_blank"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                            <img src=""cid:banner"" alt="""" class="""" style=""width: 100%; margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                        </a>
        //                                    </td>
        //                                </tr>
        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <div class=""mt20"" style=""margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 20px;""><strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">Kính chào quý khách " + item.Name + @",</strong></div>
        //                                        <div class=""mt10 mb20"" style=""margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 10px; margin-bottom: 20px;"">
        //                                            afc.edu.vn vừa nhận được <strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #f36f21;"">đơn hàng " + item.Code + @" </strong>của quý khách đặt ngày <strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">" + string.Format("{0:F}", item.Created) + @" </strong>với hình thức thanh toán là <strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">" + item.GetPayment().Name + @"</strong>.
        //                                        </div>
        //                                    </td>
        //                                </tr>
        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; background-color: #f2f4f6; border-top-width: 2px; border-top-style: solid; border-top-color: #646464; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-spacing: 0; border-collapse: collapse; font-size: 14px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #dee2e3; width: 100%!important; background-color: transparent;"">
        //                                            <tbody style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                    <td width=""50%"" height=""49"" valign=""middle"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; width: 50%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;"">
        //                                                        <div style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #646464;"">Thời gian giao hàng dự kiến:</div>
        //                                                        <div class=""mt5 mb10"" style=""margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 5px; margin-bottom: 10px;""><strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">Chúng tôi sẽ liên hệ để hẹn ngày giao hàng cho Quý khách</strong></div>
        //                                                    </td>
        //                                                    <td width=""50%"" height=""49"" class="""" valign=""bottom"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; text-align: left; width: 50%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;"">
        //                                                        <div style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #646464;"">Đơn hàng sẽ được giao đến:</div>
        //                                                        <strong class=""mt10"" style=""margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #f36f21; display: block; margin-top: 10px;"">" + item.Name + @"</strong>
        //                                                        <strong class=""mt10 diBlock"" style=""margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 10px; display: block!important;"">Email: " + item.Email + @"
        //                                                            <br style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"" />
        //                                                            Phone: " + item.Phone + @"
        //                                                            <br style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"" />
        //                                                            Địa chỉ: " + item.Address + @"
        //                                                            <br style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"" />
        //                                                            Quận: " + item.GetDistrict().Name + @"
        //                                                            <br style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"" />
        //                                                            Tỉnh/Thành phố: " + item.GetCity().Name + @"
        //                                                            <br style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"" />
        //                                                        </strong>
        //                                                    </td>
        //                                                </tr>
        //                                            </tbody>
        //                                        </table>
        //                                    </td>
        //                                </tr>
        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td align=""left"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <div class=""mt20 mb10"" style=""margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 20px; margin-bottom: 10px;"">
        //                                            <strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">Sau đây là thông tin chi tiết về đơn hàng:</strong>
        //                                        </div>
        //                                    </td>
        //                                </tr>";
        //    var _Orders = string.Empty; ModWebUserEntity _WebUser = null;
        //    if (WebLogin.IsLogin()) _WebUser = ModWebUserService.Instance.GetByID(WebLogin.WebUserId);
        //    else _WebUser = ModWebUserService.Instance.GetByEmail(item.Email);

        //    for (var i = 0; i < _Cart.Count; i++)
        //    {
        //        var _Product = ModProductService.Instance.GetByID(_Cart.Items[i].ProductID);
        //        if (_Product == null) continue;

        //        _Orders += _Product.ID + "," + _Cart.Items[i].Quantity + "," + "|";

        //        _Total += _Product.Price * _Cart.Items[i].Quantity;

        //        if (_WebUser != null)
        //        {
        //            _WebUser.ProductsID += string.IsNullOrEmpty(_WebUser.ProductsID) ?  + _Product.ID + "," : ","+ _Product.ID.ToString() + ",";
        //            ModWebUserService.Instance.Save(_WebUser, o => new { o.ProductsID });
        //        }


        //        html += @"              <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; border-width: 1px; border-style: dashed; border-color: #e7ebed; border-bottom-style: none; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <table style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; border-spacing: 0; background-color: transparent;"">
        //                                            <tbody style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                    <td class="""" valign=""top"" align=""center"" height=""120"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 10px; padding-bottom: 10px; padding-right: 0; padding-left: 0;"">
        //                                                        <a href=""" + ViewPage.GetURL(_Product.MenuID, _Product.Code) + @""" target=""_blank"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                            <img src=""" + "http://smartrain.vn" + _Product.File.Replace("~/", "/") + @""" class=""CToWUd"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width: 120px;"">
        //                                                        </a>
        //                                                    </td>
        //                                                    <td valign=""top"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;"">
        //                                                        <div style=""margin-top: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-bottom: 5px;"">
        //                                                            <a class="""" href="""" target=""_blank"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-decoration: none; color: #292929;"">" + _Product.Name + @"</a>
        //                                                        </div>
        //                                                        <div style=""margin-top: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-bottom: 5px; color: #646464;"">Số lượng: " + _Cart.Items[i].Quantity + @"</div>
        //                                                    </td>
        //                                                    <td valign=""top"" align=""right"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 10px; padding-right: 10px; padding-bottom: 0; padding-left: 0;""><strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">" + string.Format("{0:#,##0}", _Product.Price) + @" VND</strong></td>
        //                                                </tr>
        //                                            </tbody>
        //                                        </table>
        //                                    </td>
        //                                </tr>";
        //    }

        //    html += @"                  <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; border-width: 1px; border-style: dashed; border-color: #e7ebed; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <div style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                            <table border=""0"" width=""100%"" cellpadding=""0"" cellspacing=""0"" align=""left"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-spacing: 0; border-collapse: collapse; font-size: 14px; width: 100%!important; background-color: transparent;"">
        //                                                <tbody style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                    <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                        <td valign=""top"" align=""right"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 10px; padding-right: 10px; color: #646464; width: 80%; padding-bottom: 0; padding-left: 0;"">Tổng cộng:</td>
        //                                                        <td valign=""top"" align=""right"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 10px; padding-right: 10px; color: #292929; min-width: 140px; padding-bottom: 0; padding-left: 0;"">" + string.Format("{0:#,##0}", _Total) + @" VND</td>
        //                                                    </tr>
        //                                                    <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                                        <td valign=""top"" align=""right"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; width: 80%; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;""></td>
        //                                                        <td valign=""top"" align=""right"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 5px; padding-right: 10px; padding-bottom: 10px; font-size: 13px; color: #646464; min-width: 140px; padding-left: 0;""></td>
        //                                                    </tr>
        //                                                </tbody>
        //                                            </table>
        //                                        </div>
        //                                    </td>
        //                                </tr>
        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td valign=""top"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <div style=""margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 20px;"">
        //                                            <strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">Lưu ý:</strong>
        //                                        </div>

        //                                        <div style=""padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 5px; margin-bottom: 20px; margin-right: 0; margin-left: 5px; color: #646464;"">
        //                                            •&nbsp;&nbsp;Quý khách vui lòng chuẩn bị sẵn số tiền mặt tương ứng để thuận tiện cho việc thanh toán.
        //                                            •&nbsp;&nbsp;Trong một số trường hợp, afc.edu.vn sẽ thực hiện cuộc gọi tự động hoặc gửi tin nhắn đến số điện thoại quý khách đã đăng ký để xác nhận đơn hàng. 
        //                                            Để đơn hàng được xử lý nhanh chóng, xin quý khách vui lòng thực hiện theo hướng dẫn của cuộc gọi hoặc nội dung tin nhắn nhận được. 
        //                                            Nếu afc.edu.vn không nhận được phản hồi từ quý khách, đơn hàng sẽ được ngưng thực hiện do xác nhận không thành công.
        //                                        </div>
        //                                    </td>
        //                                </tr>

        //                                <tr style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">
        //                                    <td valign=""bottom"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0;"">
        //                                        <div style=""margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 20px; margin-bottom: 15px;"">
        //                                            <strong style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"">Quý khách cần hỗ trợ thêm?</strong>
        //                                            <div style=""margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-top: 5px;"">
        //                                                Nếu có bất cứ thắc mắc nào, mời quý khách tham khảo trang 
        //                                                <a href=""http://smartrain.vn/huong-dan-mua-hang-giao-hang-thanh-toan.html"" target=""_blank"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-decoration: none; color: #199cb7;"">Hướng dẫn mua hàng, giao hàng, thanh toán</a>. 
        //                                                Để liên hệ với chúng tôi, quý khách vui lòng để lại tin nhắn tại trang
        //                                                <a href=""http://smartrain.vn/lien-he.html"" target=""_blank"" style=""margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0; padding-top: 0; padding-bottom: 0; padding-right: 0; padding-left: 0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-decoration: none; color: #199cb7;"">Liên hệ</a>.
        //                                            </div>
        //                                        </div>
        //                                    </td>

        //                                </tr>
        //                            </tbody>
        //                        </table>
        //                    </div>
        //                </body>
        //                </html>";
        //    var _AvHtml = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);

        //    // Create a LinkedResource object for each embedded image
        //    var _Logo = new LinkedResource(ViewPage.Server.MapPath("/Content/skins/images/logo.png"), MediaTypeNames.Image.Jpeg);
        //    _Logo.ContentId = "logo";
        //    _AvHtml.LinkedResources.Add(_Logo);
        //    string _FileBanner = !string.IsNullOrEmpty(File) ? File : "/Content/skins/images/logo.png";
        //    var _Banner = new LinkedResource(ViewPage.Server.MapPath(_FileBanner), MediaTypeNames.Image.Jpeg);
        //    _Banner.ContentId = "banner";
        //    _AvHtml.LinkedResources.Add(_Banner);

        //    for (var i = 0; i < _Cart.Count; i++)
        //    {
        //        var _Product = ModProductService.Instance.GetByID(_Cart.Items[i].ProductID);
        //        if (_Product == null || string.IsNullOrEmpty(_Product.File) || Data.GetCode(_Product.File) != _Product.File || Data.RemoveVietNamese(_Product.File) != _Product.File) continue;

        //        if (!Global.File.Exists(ViewPage.Server.MapPath(_Product.File))) continue;

        //        var _File = new LinkedResource(ViewPage.Server.MapPath(_Product.File), MediaTypeNames.Image.Jpeg);
        //        _File.ContentId = "product_" + i;
        //        //_AvHtml.LinkedResources.Add(_File);


        //    }
        //    item.UserID = _WebUser != null ? _WebUser.ID : 0;
        //    item.Orders = _Orders;
        //    item.Total = _Total;
        //    var listEmail = item.Email.Trim() + "," + WebResource.GetValue("Web_Email");
        //    ModOrderService.Instance.Save(item);



        //    //gui mail
        //    #region send mail

        //    var domain = ViewPage.Request.Url.Host;


        //    //gui mail cho quan tri va khach hang
        //    Mail.SendMail(
        //        listEmail,
        //                "noreply@gmail.com",
        //                domain,
        //                domain + "- Thông tin đơn hàng - ngày " + string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now),
        //                _AvHtml,
        //                null
        //            );

        //    #endregion send mail

        //    _Cart.RemoveAll();
        //    _Cart.Save();

        //    ObjectCookies<ModOrderEntity>.Remove("Orders");

        //    ViewBag.Model = model;

        //    ViewPage.AlertThenRedirect(WebResource.GetValue("Checkout_Success"), "/");
        //}

        #region private

        private static string GetOrder()
        {
            var maxId = ModOrderService.Instance.CreateQuery()
                                    .Max(o => o.ID)
                                    .ToValue()
                                    .ToInt();

            if (maxId <= 1) return "0000001";

            var result = string.Empty;
            for (var i = 1; i <= (7 - maxId.ToString().Length); i++)
            {
                result += "0";
            }

            return result + (maxId + 1);
        }

        #endregion private
    }

    public class MCompleteModel
    {
        public string ValidCode { get; set; }
    }
}