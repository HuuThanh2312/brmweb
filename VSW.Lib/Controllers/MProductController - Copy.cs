﻿using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Sản phẩm", Code = "MProduct", Order = 3)]
    public class MProductController1 : Controller
    {
        [Core.MVC.PropertyInfo("Chuyên mục", "Type|Product")]
        public int MenuID;

        int BrandID = 0;

        [Core.MVC.PropertyInfo("Vị trí", "ConfigKey|Mod.ProductState")]
        public int State;

        [Core.MVC.PropertyInfo("Bài viết")]
        public int NewsID;

        [Core.MVC.PropertyInfo("Text")]
        public string TextExamp;

        [Core.MVC.PropertyInfo("File")]
        public string File;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 24;

        //[VSW.Core.MVC.PropertyInfo("Cách hiển thị", "Trang chủ Dịch vụ|List,Trang con Dịch vụ|Index")]
        //public string Layout = "Index";

        //[Core.MVC.PropertyInfo("Giao diện", "Chi tiết|19")]
        //public int TemplateDetailID = 19;

        public void ActionIndex(MProductModel model)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            if (ViewPage.ViewBag.BrandID > 0)
                BrandID = ViewPage.ViewBag.BrandID;

            var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(State > 0, o => (o.State & State) == State)
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                    .Where(BrandID > 0, o => o.BrandID == BrandID)
                                    .Take(PageSize)
                                    .Skip(PageSize * model.page);

            string atr = Core.Web.HttpQueryString.GetValue("atr").ToString();
            string sort = Core.Web.HttpQueryString.GetValue("sort").ToString();
            int state = Core.Global.Convert.ToInt(Core.Web.HttpQueryString.GetValue("state"));

            dbQuery.Where(state > 0, o => (o.State & state) == state);

            if (sort == "new_asc") dbQuery.OrderByDesc(o => o.Published);
            else if (sort == "price_asc") dbQuery.OrderByAsc(o => o.Price);
            else if (sort == "price_desc") dbQuery.OrderByDesc(o => o.Price);
            else if (sort == "view_desc") dbQuery.OrderByDesc(o => o.View);
            else dbQuery.OrderByDesc(o => new { o.Order, o.ID });
            
            int[] arrID = Core.Global.Array.ToInts(atr.Split('-'));

            for (int i = 0; i < arrID.Length; i++)
            {
                var pid = arrID[i];

                if (pid < 1) continue;
                dbQuery.WhereIn(o => o.ID, ModPropertyService.Instance.CreateQuery().Select(o => o.ProductID).Distinct().WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID)).Where(o => o.PropertyValueID == pid));
            }

            string html = dbQuery.Html;
            if (!string.IsNullOrEmpty(html))
            {
                ViewBag.Data = html;
            }
            else
            {
                var listItem = dbQuery.ToList_Cache();

                html += @"  <div class=""row"">
                                <ul class=""listpro d-flex-wrap all mt10"">";
                for (var i = 0; listItem != null && i < listItem.Count; i++)
                {
                    string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);

                    html += @"      <li class=""col-lg-3 col-md-6 col-sm-6 col-12"">
                                        <div class=""itempro"">
                                            <div class=""reponsive-img"">
                                                <a href="""+ url + @""">
                                                    <img src="""+ Utils.GetResizeFile(listItem[i].File, 4, 220, 220) + @""" class=""lazy"" alt=""" + listItem[i].Name + @""" />
                                                </a>
                                                <span class=""lablePro sale"">Sale</span>
                                                <span class=""lablePro new"">New</span>
                                            </div>
                                            <div class=""itempro-info"">
                                                <h3 class=""itempro-info-name""><a href=""<%=url %>"">" + listItem[i].Name + @"</a></h3>
                                                <div class=""itempro-info-price"">
                                                    <span class=""special-price"">"+ string.Format("{0:#,##0}", listItem[i].Price) + @" đ</span>
                                                    <span class=""old-price"">"+ string.Format("{0:#,##0}", listItem[i].Price2) + @" đ</span>
                                                </div>
                                                <div class=""social_box"">
                                                    <div class=""s_b""><i class=""fa fa-tags""></i><span class=""luotmua"">0</span> </div>
                                                    <div class=""s_b""><i class=""fa fa-eye""></i><span class=""luotxem"">206</span> </div>
                                                    <div class=""s_b""><i class=""fa fa-comments""></i><span class=""binhluan"">206</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>";
                }

                var pager = new Pager { PageIndex = model.page, PageSize = model.PageSize, TotalRecord = model.TotalRecord };
                pager.Update();

                html += @"      </ul>
                                <ul class=""pagination flex-wrap all justify-content-end"">
                                    "+ pager.html + @"
                                </ul>
                            </div>";

                dbQuery.BuildHtml(html);

                ViewBag.Data = html;
                model.TotalRecord = dbQuery.TotalRecord;
                model.PageSize = PageSize;
            }

            ViewPage.ViewBag.Model = ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            var item = ModProductService.Instance.CreateQuery()
                                .Where(o => o.Activity == true && o.Code == endCode)
                                .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                .ToSingle();

            if (item != null)
            {
                ////neu k phai thiet bi cam thay thi moi doi template
                //if (!ViewPage.MobileMode && !ViewPage.TabletMode)
                //{
                //    var template = SysTemplateService.Instance.GetByID(TemplateDetailID);
                //    if (template != null) ViewPage.ChangeTemplate(template);
                //}

                //up view
                item.UpView();

                ViewBag.Other = ModProductService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ID  != item.ID)
                                            .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                            .OrderByDesc(o => new { o.Order, o.ID })
                                            .Take(PageSize)
                                            .ToList_Cache();

                ViewPage.ViewBag.Data = ViewBag.Data = item;

                //SEO
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Code);
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(item.File, 200, 200);
            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MProductModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public int State { get; set; }
    }
}