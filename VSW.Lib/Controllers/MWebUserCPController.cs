﻿using System.Drawing;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Thông tin tài khoản", Code = "MWebUserCP", Order = 50)]
    public class MWebUserCPController : Controller
    {

        [Core.MVC.PropertyInfo("Trang")]
        public int PageID =125;
        public void ActionIndex(MWebUserCPModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.Data = _WebUser;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionShopPOST(MWebUserCPModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);

            //if (item.Shop.Trim() == string.Empty)
            //    ViewPage.Message.ListMessage.Add("Nhập: Tên gian hàng.");

            if (item.Code.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: URL gian hàng.");
            else if (ModWebUserService.Instance.CheckCode(item.Code.Trim(), item.ID))
                ViewPage.Message.ListMessage.Add("URL đã tồn tại. Hãy chọn URL khác.");

            //logo, banner
            #region Images
            var logo = ViewPage.Request.Files["Logo"];
            var file = ViewPage.Request.Files["File"];

            if (logo != null && logo.FileName.Length > 0)
            {
                if (logo.ContentLength > 5 * 1024 * 1024)
                    ViewPage.Message.ListMessage.Add("Dung lượng Logo không được lớn hơn 5Mb.");
                else
                {
                    try
                    {
                        new Bitmap(logo.InputStream);
                    }
                    catch
                    {
                        ViewPage.Message.ListMessage.Add("File Logo không phải ảnh. Chọn file khác.");
                    }
                }
            }

            if (file != null && file.FileName.Length > 0)
            {
                if (file.ContentLength > 5 * 1024 * 1024)
                    ViewPage.Message.ListMessage.Add("Dung lượng Banner không được lớn hơn 5Mb.");
                else
                {
                    try
                    {
                        new Bitmap(file.InputStream);
                    }
                    catch
                    {
                        ViewPage.Message.ListMessage.Add("File Banner không phải ảnh. Chọn file khác.");
                    }
                }
            }
            #endregion

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                string path = ViewPage.Server.MapPath("~/Data/upload/images/user-" + item.ID + "/");
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);

                //logo
                if (logo != null && logo.FileName.Length > 0)
                {
                    string dbLogo = "~/Data/upload/images/user/user-" + item.LoginName + "/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(logo.FileName)) + System.IO.Path.GetExtension(logo.FileName);
                    string svLogo = ViewPage.Server.MapPath(dbLogo);

                    item.Logo = dbLogo;
                    logo.SaveAs(svLogo);
                }

                //banner
                if (file != null && file.FileName.Length > 0)
                {
                    string dbFile = "~/Data/upload/images/user/user-" + item.LoginName + "/" + Data.GetCode(System.IO.Path.GetFileNameWithoutExtension(file.FileName)) + System.IO.Path.GetExtension(file.FileName);
                    string svFile = ViewPage.Server.MapPath(dbFile);

                    item.File = dbFile;
                    file.SaveAs(svFile);
                }

                item.Activity = true;

                ModWebUserService.Instance.Save(item);

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionInfoPOST(MWebUserCPModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");

            var sVy = CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh") + ".", string.Empty);
            var sValidCode = model.ValidCode.Trim();

            if (sVy == string.Empty || (sVy.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                ModWebUserService.Instance.Save(item);

                ViewPage.Alert("Thông tin đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionPasswordPOST(MWebUserCPModel model)
        {
            var item = ViewBag.Data as ModWebUserEntity;

            TryUpdateModel(item);

            if (model.Password.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu hiện tại.");
            else if (item.Password != Security.Md5(model.Password.Trim()))
                ViewPage.Message.ListMessage.Add("Mật khẩu hiện tại không đúng.");

            if (model.Password1.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Mật khẩu mới.");
            else if (model.Password1.Trim().Length < 6 || model.Password1.Trim().Length > 12)
                ViewPage.Message.ListMessage.Add("Mật khẩu phải từ 6-12 ký tự.");
            else if (model.Password1.Trim() != model.Password2.Trim())
                ViewPage.Alert("Mật khẩu và mật khẩu nhập lại không giống nhau.");

            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {
                item.Password = Security.Md5(model.Password1.Trim());

                ModWebUserService.Instance.Save(item, o => o.Password);

                ViewPage.Alert("Mật khẩu đã được cập nhật.");
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        public void ActionLogout()
        {
            WebLogin.Logout();
            ViewPage.Response.Redirect("~/");
        }
    }

    public class MWebUserCPModel
    {
        public string Password { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }

        public string ValidCode { get; set; }
    }
}