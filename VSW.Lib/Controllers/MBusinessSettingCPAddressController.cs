﻿using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Địa chỉ shop", Code = "MBusinessSettingCPAddress", Order = 2)]
    public class MBusinessSettingCPAddressController : Controller
    {


        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;
        public void ActionIndex(MBusinessSettingCPAddressModel model)
        {

            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);
            else if (_WebUser.GetShopNotActive() != null)
            {
                ViewPage.Confirm("Bạn đã là đối tác của Berich. <br> Nhưng chưa được Berich xét duyệt.", "/");
                return;
            }
            else if (_WebUser.GetShop() == null)
            {
                ViewPage.Confirm("Bạn chưa phải là đối tác của Berich. <br> Bạn có muốn đăng ký là đối tác của Berich không?.", ViewPage.RegisterBuninessUrl);
                return;
            }

            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            ViewBag.User = _WebUser.GetShop();

            ViewBag.Model = model;


            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);


        }

        public void ActionShopAddressPOST(MBusinessSettingCPAddressModel model,ModShopEntity item)
        {
            item = ViewBag.User ;

            TryUpdateModel(item);


            if (item.CityID < 1)
                ViewPage.Message.ListMessage.Add("Chọn: Tỉnh/Thành phố");

            if (item.Address.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ chi tiết.");

            string sVY = Core.Global.CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");



            if (ViewPage.Message.ListMessage.Count > 0)
            {
                string message = string.Empty;
                for (int i = 0; i < ViewPage.Message.ListMessage.Count; i++)
                    message += ViewPage.Message.ListMessage[i] + "<br />";

                ViewPage.Alert(message);
            }
            else
            {

                ModShopService.Instance.Save(item, o => new { o.CityID, o.Address, o.DistrictID});
                VSW.Core.Web.Cache.Clear(ModShopService.Instance);
                ViewPage.Alert("Cập nhật thành công");
            }

            ViewBag.User = item;
            ViewBag.Model = model;
        }

    }

    public class MBusinessSettingCPAddressModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }
        public string ValidCode { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
    }
}