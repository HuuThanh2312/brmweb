﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Global;
using VSW.Core.Models;
using VSW.Core.MVC;
using VSW.Core.Web;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
using Controller = VSW.Lib.MVC.Controller;
using Cookies = VSW.Lib.Global.Cookies;
using Setting = VSW.Core.Web.Setting;
using Utils = VSW.Lib.Global.Utils;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM : Đơn hàng - Quản lý", Code = "MMngOrder", Order = 99)]
    public class MMngOrderController : Controller
    {
        [PropertyInfo("Số lượng")]
        public int PageSize = 20;

        [Core.MVC.PropertyInfo("Giao diện", "Chi tiết|21")]
        public int TemplateDetailID = 21;

        [Core.MVC.PropertyInfo("Trang")]
        public int PageID;
        public void ActionIndex(MMngOrderModel model)
        {

            if (!WebLogin.IsLogin())
            {
                ViewPage.AlertThenRedirect("Bạn phải đăng nhập để sử dụng chức năng này.", "/?returnpath=" + ViewPage.CurrentURL);
                return;
            }

            int _WebUserID = WebLogin.WebUserID;

            if (model.id > 0)
            {
                var _orderDetail = ModOrderDetailService.Instance.GetByID(model.id);
                if (_orderDetail == null) ViewPage.Response.Redirect(ViewPage.MngOrderUrl);
                else
                {
                    RenderView("Detail");
                    ViewBag.Order = _orderDetail;

                    ViewPage.CurrentPage.PageTitle = "Chi tiết đơn hàng #" + _orderDetail.GetOrder().Code;
                    ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                    ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);

                }
            }
            else
            {
                var dbQuery = ModOrderService.Instance.CreateQuery()
                                        .Where(o => o.WebUserID == _WebUserID)
                                        //.Where(model.StatusID > 0, o => o.StatusID == model.StatusID)
                                        .Skip(PageSize * model.page);
                var _date = DateTime.Now.AddDays(-15);
                var _date2 = DateTime.Now.AddDays(-30);
                var _date3 = DateTime.Now.AddMonths(6);
                if (Core.Web.HttpQueryString.Exists("sort"))
                {
                    if (model.sort.Equals("15day", StringComparison.OrdinalIgnoreCase))
                        dbQuery.Where(o => o.Created <= DateTime.Now).Where(o => o.Created >= _date);
                    else if (model.sort.Equals("30day", StringComparison.OrdinalIgnoreCase))
                        dbQuery.Where(o => o.Created <= DateTime.Now).Where(o => o.Created >= _date2);
                    else if (model.sort.Equals("6month", StringComparison.OrdinalIgnoreCase))
                        dbQuery.Where(o => o.Created <= DateTime.Now).Where(o => o.Created >= _date3);
                    else
                        dbQuery.OrderByDesc(o => new { o.ID }).Take(5);
                }
                else
                    dbQuery.OrderByDesc(o => new { o.Created, o.ID }).Take(5);


                model.TotalRecord = dbQuery.TotalRecord;
                ViewBag.Data = dbQuery.ToList_Cache();

                model.PageSize = PageSize;
            }
            ViewBag.Model = model;
            ViewBag.User = WebLogin.CurrentUser;

            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);
            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + VSW.Lib.Global.Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }
        //public void ActionDetail(MMngOrderModel model)
        //{

        //    var item = ModOrderService.Instance.GetByID(model.ID);
        //    var _WebUser = WebLogin.CurrentUser;

        //    if (item != null)
        //    {
        //        //neu k phai thiet bi cam thay thi moi doi template
        //        if (!ViewPage.MobileMode && !ViewPage.TabletMode)
        //        {
        //            var template = SysTemplateService.Instance.GetByID(TemplateDetailID);
        //            if (template != null) ViewPage.ChangeTemplate(template);
        //        }

        //        ViewBag.Data = item;
        //        ViewBag.Model = model;
        //        ViewBag.User = _WebUser;

        //        ViewPage.CurrentPage.PageTitle = "Chi tiết đơn hàng - " + item.Code;
        //    }
        //    else
        //    {
        //        RedirectToAction("Index");
        //        return;
        //    }



        //    ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
        //    ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + VSW.Lib.Global.Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        //}
    }

    public class MMngOrderModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }
        public string sort { get; set; }
        public int id { get; set; }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }

        public int StatusID { get; set; }
    }
}