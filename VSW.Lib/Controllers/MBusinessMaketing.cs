﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_Business: Đối tác - Maketting", Code = "MBusinessMaketing", Order = 6)]
    public class MBusinessMaketingController : Controller
    {

        [Core.MVC.PropertyInfo("Chuyên mục", "Type|News")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;

        public void ActionIndex(MBusinessMaketingModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);


            if (model.id > 0)
            {
                var _news = ModMarketingNewsService.Instance.GetByID(model.id);
                if (_news == null) ViewPage.Response.Redirect(ViewPage.BuninessMarketingCpUrl);
                else
                {
                    RenderView("Detail");
                    ViewBag.Data = _news;

                    ViewPage.CurrentPage.PageTitle = "Chi tiết tin " + _news.Name;
                    ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                    ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);

                }
            }
            else
            {

                var dbQuery = ModMarketingNewsService.Instance.CreateQuery()
                                //.Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(model.SearchText)))
                                .Where(model.State > 0, o => (o.State & model.State) == model.State)
                                .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("MarketingNews", MenuID, ViewPage.CurrentLang.ID))
                                .Where(model.StartDate > DateTime.MinValue, o => o.StartDate >= model.StartDate)
                                .Where(model.EndDate > DateTime.MinValue, o => o.EndDate <= model.EndDate)
                                .Take(model.PageSize)
                                .OrderByDesc(o => new { o.Published, o.Order })
                                .Skip(model.page * PageSize);

                ViewBag.User = _WebUser;
                ViewBag.Data = dbQuery.ToList_Cache();
                ViewBag.Model = model;
                ViewPage.CurrentPage.PageTitle = "Chương trình marketing";
                ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
            }
            //SEO

        }



    }

    public class MBusinessMaketingModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int id { get; set; }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
        public int State { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }
    }
}