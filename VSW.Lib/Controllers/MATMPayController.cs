﻿using Newtonsoft.Json;
using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
using System.Linq;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Chuyển khoản", Code = "MATMPay", Order = 99)]
    public class MATMPayController : Controller
    {
        public void ActionIndex(ModATMPayEntity item, MATMPayModel model)
        {
            ViewBag.Data = item;
            ViewBag.Model = model;

            var order = ObjectCookies<ModOrderEntity>.GetValue("Orders");
            if(order==null)
            {
                ViewPage.AlertThenRedirect("Giỏ hàng không có sản phẩm nào.<br/>Vui lòng chọn mua sản phẩm.", "/");
                return;
            }
            ViewBag.Order = order;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }
        
        public void ActionAddPOST(ModATMPayEntity item, MATMPayModel model)
        {
            TryUpdateModel(model);

            //if (!WebLogin.IsLogin())
            //{
            //    ViewPage.AlertThenRedirect("Bạn phải đăng nhập để sử dụng chức năng này.", ViewPage.LoginUrl);
            //    return;
            //}

            if (string.IsNullOrEmpty(model.option_payment))
                ViewPage.Message.ListMessage.Add("Chọn: Hình thức thanh toán.");
            else if (!string.IsNullOrEmpty(model.option_payment) && model.option_payment.Trim() == "ATM_ONLINE" && string.IsNullOrEmpty(model.bankcode))
                ViewPage.Message.ListMessage.Add("Chọn: Ngân hàng.");

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            if (item.TotalAmount < 0)
                ViewPage.Message.ListMessage.Add("Nhập: Số tiền cần chuyển.");

            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            //string sVY = VSW.Core.Global.CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            //string sValidCode = model.ValidCode.Trim();

            //if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
            //    ViewPage.Message.ListMessage.Add("Nhập mã an toàn.");

            //hien thi thong bao loi
            if (ViewPage.Message.ListMessage.Count > 0)
            {
                var message = ViewPage.Message.ListMessage.Aggregate(string.Empty, (current, t) => current + ("- " + t + "<br />"));
                ViewPage.Alert(message);
            }
            else
            {
                string payment_method = ViewPage.Request.Form["option_payment"];
                string str_bankcode = ViewPage.Request.Form["bankcode"];

                RequestInfo info = new RequestInfo();

                info.Merchant_id = VSW.Core.Global.Config.GetValue("NganLuong.API.ID").ToString();
                info.Merchant_password = VSW.Core.Global.Config.GetValue("NganLuong.API.Password").ToString();
                info.Receiver_email = VSW.Core.Global.Config.GetValue("NganLuong.API.Email").ToString();

                info.cur_code = "vnd";
                info.bank_code = str_bankcode;

                info.Order_code = Guid.NewGuid().ToString();
                info.Total_amount = item.TotalAmount.ToString();
                info.fee_shipping = "0";
                info.Discount_amount = "0";
                info.order_description = item.Content;
                info.return_url = VSW.Core.Global.Config.GetValue("NganLuong.API.Success").ToString();
                info.cancel_url = VSW.Core.Global.Config.GetValue("NganLuong.API.Cancel").ToString();

                info.Buyer_fullname = item.Name;
                info.Buyer_email = item.Email;
                info.Buyer_mobile = item.Phone;

                NganLuong objNLChecout = new NganLuong();
                ResponseInfo result = objNLChecout.GetUrlCheckout(info, payment_method);

                if (result.Error_code == "00") ViewPage.Response.Redirect(result.Checkout_url);
                else ViewPage.AlertThenRedirect(result.Description, info.cancel_url);
            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }
    }

    public class MATMPayModel
    {
        public string option_payment { get; set; }
        public string bankcode { get; set; }

        public string returnpath { get; set; }
    }
}