﻿using VSW.Lib.MVC;
using VSW.Lib.Models;
using VSW.Lib.Global;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Tìm kiếm", Code = "MSearch", Order = 50)]
    public class MSearchController : Controller
    {
        [VSW.Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 30;

        public void ActionIndex(MSearchModel model)
        {
            string _Keyword = !string.IsNullOrEmpty(model.keyword) ? Data.GetCode(model.keyword) : string.Empty;
            var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true)
                                    .Where(!string.IsNullOrEmpty(_Keyword), o => (o.Code.Contains(_Keyword) || o.Model.Contains(_Keyword) || o.Name.Contains(model.keyword)))
                                    .OrderByDesc(o => new { o.Order, o.ID })
                                    .Skip(PageSize * model.page)
                                    .Take(PageSize);

            ViewBag.Data = dbQuery.ToList();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }
    }

    public class MSearchModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public int State { get; set; }

        public string keyword { get; set; }
    }
}
