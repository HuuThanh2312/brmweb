﻿using System;
using System.Linq;
using VSW.Core.Web;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
using Utils = VSW.Lib.Global.Utils;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Giỏ hàng", Code = "MViewCart", Order = 50)]
    public class MViewCartController : Controller
    {
        public void ActionIndex(ModOrderEntity item, MViewCartModel model)
        {
            var cart = new Cart();
            if (cart.Items.Count < 1) ViewPage.AlertThenRedirect("Giỏ hàng của bạn chưa có sản phẩm nào.", "/");

            ViewBag.Data = item;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = "http://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        //them gio hang
        public void ActionAdd(ModOrderEntity item, MViewCartModel model)
        {
            var _Cart = new Cart();
            var _CartShop = new Cart(model.ShopID.ToString());

            var _item = _Cart.Find(new CartItem() { ShopID = model.ShopID, ProductID = model.ProductID });
            if (_item == null)
            {
                _Cart.Add(new CartItem
                {
                    ShopID = model.ShopID,
                    ProductID = model.ProductID,
                    Quantity = model.Quantity,
                    ShippingPrice = model.ShippingPrice
                });

                _CartShop.Add(new CartItem
                {
                    ShopID = model.ShopID,
                    ProductID = model.ProductID,
                    Quantity = model.Quantity
                });
            }
            else _item.Quantity += model.Quantity;

            _Cart.Save();
            _CartShop.Save();

            ViewBag.Data = item;
            ViewBag.Model = model;

            ViewPage.Response.Redirect(ViewPage.ViewCartUrl + "?returnpath=" + model.returnpath);
        }

        public void ActionDelete(ModOrderEntity item, MViewCartModel model)
        {
            var cart = new Cart();

            cart.Remove(cart.Items[model.Index]);
            cart.Save();

            ViewBag.Data = item;
            ViewBag.Model = model;

            ViewPage.Response.Redirect(ViewPage.ViewCartUrl + "?returnpath=" + model.returnpath);
        }

        public void ActionUpdate(ModOrderEntity item, MViewCartModel model)
        {
            var cart = new Cart();

            cart.Items[model.Index].Quantity = model.Quantity;

            cart.Save();

            ViewBag.Data = item;
            ViewBag.Model = model;

            ViewPage.Response.Redirect(ViewPage.ViewCartUrl + "?returnpath=" + model.returnpath);
        }

        public void ActionAddPOST(ModOrderEntity item, MViewCartModel model)
        {
            if (item.Name.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Họ và tên.");

            //if (Utils.GetEmailAddress(item.Email.Trim()) == string.Empty)
            //    ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ Email.");

            if (item.Phone.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Số điện thoại.");

            if (item.Address.Trim() == string.Empty)
                ViewPage.Message.ListMessage.Add("Nhập: Địa chỉ giao hàng.");

            string sVY = VSW.Core.Global.CryptoString.Decrypt(ViewPage.Session["CaptchaANGKORICH"].ToString()).Replace("CaptchaANGKORICH.Secure." + ViewPage.Request.UserHostAddress + "." + string.Format("yyyy.MM.dd.hh", DateTime.Now) + ".", string.Empty);
            string sValidCode = model.ValidCode.Trim();

            if (sVY == string.Empty || (sVY.ToLower() != sValidCode.ToLower()))
                ViewPage.Message.ListMessage.Add("Nhập: Mã bảo mật.");

            //hien thi thong bao loi
            if (ViewPage.Message.ListMessage.Count > 0)
            {
                var message = ViewPage.Message.ListMessage.Aggregate(string.Empty, (current, t) => current + ("- " + t + "<br />"));

                ViewPage.Alert(message);
            }
            else
            {
                //luu don hang
                item.IP = HttpRequest.IP;
                item.Created = DateTime.Now;
                item.WebUserID = WebLogin.CurrentUser != null ? WebLogin.WebUserID : 0;
                item.Code = "DH" + string.Format("{0:ddMMyyyy}", item.Created) + GetOrder();

                ModOrderService.Instance.Save(item);

                //luu chi tiet don hang & send mail
                long total = 0;
                var cart = new Cart();

                var html = @"  <b>Chú ý: Đây là email trả lời tự động. Nếu muốn phản hồi - Quý khách vui lòng gửi email về địa chỉ <span style=""color:#f00"">" + WebResource.GetValue("Web_Email") + @"</span></b><br /><br /><br />
                                    <b>THÔNG TIN ĐƠN HÀNG</b><br /><br />
                                    <b>Mã đơn hàng:</b> " + item.Code + @"<br />
                                    <b>Ngày mua:</b> " + string.Format("{0:dd/MM/yyyy HH:mm}", item.Created) + @"<br />
                                    <b>Địa chỉ IP:</b> " + item.IP + @"<br /><br /><br />
                                    <b>DANH SÁCH SẢN PHẨM</b><br /><br />
                                ";

                for (var i = 0; i < cart.Count; i++)
                {
                    var product = ModProductService.Instance.GetByID(cart.Items[i].ProductID);
                    if (product == null) continue;

                    ModOrderDetailEntity _orderDetail = new ModOrderDetailEntity();
                    var _Shop = ModShopService.Instance.GetByID(cart.Items[i].ShopID);
                    if (_Shop == null) continue;

                    var _CartShop = new Cart(_Shop.ID.ToString());

                    if (_CartShop.Items.Count < 1) continue;

                    ModOrderDetailService.Instance.Save(new ModOrderDetailEntity()
                    {
                        ID = 0,
                        OrderID = item.ID,
                        ProductID = product.ID,
                        Quantity = cart.Items[i].Quantity,
                        Price = product.Price,
                        Name = product.Name,
                        ShopID = _Shop.ID,
                        ShippingPrice = cart.Items[i].ShippingPrice
                    });

                    total += product.Price * cart.Items[i].Quantity;

                    html += @"      <b>Số thứ tự:</b> " + (i + 1) + @"<br />
                                    <b>Sản phẩm:</b> " + product.Name + @"<br />
                                    <b>Shop:</b> " + _Shop.Name + @"<br />
                                    <b>Số lượng:</b> " + cart.Items[i].Quantity + @"<br />
                                    <b>Giá tiền:</b> " + string.Format("{0:#,##0}", product.Price) + @" đ<br />
                                    <b>Thành tiền:</b> " + string.Format("{0:#,##0}", cart.Items[i].Quantity * product.Price) + @" đ<br /><br /><br />
                    ";
                    item.ShopID += _Shop.ID + ",";
                    _CartShop.Remove(_CartShop.Items[i]);
                    _CartShop.Save();
                }

                item.Total = total;

                ModOrderService.Instance.Save(item, o => new { o.Total, o.ShopID });

                html += @"<b>TỔNG TIỀN:</b> " + string.Format("{0:#,##0}", total) + @" đ<br /><br />";

                html += @"<b>THÔNG TIN KHÁCH HÀNG</b><br /><br />";

                if (!string.IsNullOrEmpty(item.Name)) html += "<b>Họ và tên:</b> : " + item.Name + "<br />";
                if (!string.IsNullOrEmpty(item.Email)) html += "<b>Email:</b> : " + item.Email + "<br />";
                if (!string.IsNullOrEmpty(item.Phone)) html += "<b>Điện thoại:</b> : " + item.Phone + "<br />";
                if (!string.IsNullOrEmpty(item.Address)) html += "<b>Địa chỉ:</b> : " + item.Address + "<br />";
                //if (!string.IsNullOrEmpty(item.Title)) _Html += "<b>Tiêu đề:</b> : " + item.Title + "<br />";
                if (!string.IsNullOrEmpty(item.Content)) html += "<b>Nội dung:</b> : " + item.Content.Replace("\n", "<br />") + "<br />";

                //gui mail
                #region send mail

                var domain = ViewPage.Request.Url.Host;
                var listEmail = item.Email.Trim() + "," + WebResource.GetValue("Web_Email");

                //gui mail cho quan tri va khach hang
                Mail.SendMail(
                    listEmail,
                    "noreply@gmail.com",
                    domain,
                    domain + "- Thông tin đơn hàng - ngày " + string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now),
                    html
                );

                #endregion send mail

                cart.RemoveAll();
                cart.Save();

                ViewPage.AlertThenRedirect("Bạn đã mua hàng thành công.<br /> Hãy kiểm tra hộp thư để biết thông tin chi tiết đơn hàng", "/");

            }

            ViewBag.Data = item;
            ViewBag.Model = model;
        }

        #region private

        private static string GetOrder()
        {
            var maxId = ModOrderService.Instance.CreateQuery()
                                    .Max(o => o.ID)
                                    .ToValue()
                                    .ToInt();

            if (maxId <= 1) return "0000001";

            var result = string.Empty;
            for (var i = 1; i <= (7 - maxId.ToString().Length); i++)
            {
                result += "0";
            }

            return result + (maxId + 1);
        }

        #endregion private
    }

    public class MViewCartModel
    {
        public int Index { get; set; }
        public int CityID { get; set; }
        public int ShopID { get; set; }
        public int ProductID { get; set; }

        public int ShippingPrice { get; set; }

        private int _quantity = 1;
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public int[] ArrQuantity { get; set; }
        public string returnpath { get; set; }
        public string ValidCode { get; set; }
    }
}