﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Lịch sử hoa hồng", Code = "MCommissionHistory", Order = 6)]
    public class MCommissionHistoryController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;

        public void ActionIndex(MCommissionHistoryModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            //if (model.ID > 0) { RedirectToAction("Detail"); return; }


            //var dbQuery = ModBankingUserService.Instance.GetListByUserID(_WebUser.ID);

            ViewBag.User = _WebUser;
            //ViewBag.Data = dbQuery;
            ViewBag.Model = model;
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }



    }

    public class MCommissionHistoryModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
}