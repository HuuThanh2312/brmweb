﻿
using System;
using System.Linq;
using VSW.Core.Global;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO_CSM: Cây thành viên", Code = "MTreeMembers", Order = 6)]
    public class MTreeMembersController : Controller
    {
        [Core.MVC.PropertyInfo("Trang")]
        public int PageID = 125;
        public void ActionIndex(MTreeMembersModel model)
        {
            var _WebUser = WebLogin.CurrentUser;
            if (_WebUser == null) ViewPage.Response.Redirect("/?returnpath=" + ViewPage.CurrentURL);

            //if (model.ID > 0) { RedirectToAction("Detail"); return; }


            var dbQuery = ModWebUserService.Instance.CreateQuery()
                                                .Where(o => o.ParentID == _WebUser.ID)
                                                .ToList_Cache();

            ViewBag.User = _WebUser;
            ViewBag.Data = dbQuery;
            ViewBag.Model = model;
            ViewBag.Page = SysPageService.Instance.GetByParent_Cache(PageID);

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }



    }

    public class MTreeMembersModel
    {
        private int _page;
        public int page
        {
            get => _page;
            set => _page = value - 1;
        }
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public string ValidCode { get; set; }
    }
}