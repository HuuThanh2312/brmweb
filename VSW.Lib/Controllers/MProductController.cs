﻿using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
namespace VSW.Lib.Controllers
{
    [ModuleInfo(Name = "MO : Sản phẩm", Code = "MProduct", Order = 3)]
    public class MProductController : Controller
    {
        [Core.MVC.PropertyInfo("Chuyên mục", "Type|Product")]
        public int MenuID;

        [Core.MVC.PropertyInfo("Thương hiệu", "Type|Brand")]
        public int BrandID;

        [Core.MVC.PropertyInfo("Vị trí", "ConfigKey|Mod.ProductState")]
        public int State;

        [Core.MVC.PropertyInfo("Số lượng")]
        public int PageSize = 20;

        //[VSW.Core.MVC.PropertyInfo("Cách hiển thị", "Trang chủ Dịch vụ|List,Trang con Dịch vụ|Index")]
        //public string Layout = "Index";



        [Core.MVC.PropertyInfo("Giao diện chi tiết", "Chi tiết|4")]
        public int TemplateDetailID = 4;

        public void ActionIndex(MProductModel model)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            if (ViewPage.CurrentPage.BrandID > 0)
            {
                BrandID = ViewPage.CurrentPage.BrandID;

                RenderView("Brand");
            }

            var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.ActivityPrice == true)
                                    .Where(State > 0, o => (o.State & State) == State)
                                    .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                    .Where(BrandID > 0, o => o.BrandID == BrandID)
                                    .Take(PageSize)
                                    .Skip(PageSize * model.page);

            string atr = Core.Web.HttpQueryString.GetValue("atr").ToString();
            string sort = Core.Web.HttpQueryString.GetValue("sort").ToString();
            int state = Core.Global.Convert.ToInt(Core.Web.HttpQueryString.GetValue("state"));

            dbQuery.Where(state > 0, o => (o.State & state) == state);

            if (sort == "new_asc") dbQuery.OrderByDesc(o => o.Published);
            else if (sort == "price_asc") dbQuery.OrderByAsc(o => o.Price);
            else if (sort == "price_desc") dbQuery.OrderByDesc(o => o.Price);
            else if (sort == "view_desc") dbQuery.OrderByDesc(o => o.View);
            else dbQuery.OrderByDesc(o => new { o.Order, o.ID });

            int[] arrID = Core.Global.Array.ToInts(atr.Split('-'));

            for (int i = 0; i < arrID.Length; i++)
            {
                var pid = arrID[i];

                if (pid < 1) continue;
                dbQuery.WhereIn(o => o.ID, ModPropertyService.Instance.CreateQuery().Select(o => o.ProductID).Distinct().WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID)).Where(o => o.PropertyValueID == pid));
            }

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            model.PageSize = PageSize;
            ViewBag.Model = model;

            //SEO
            ViewPage.CurrentPage.PageURL = ViewPage.CurrentURL;
            ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(ViewPage.CurrentPage.File, 200, 200);
        }

        public void ActionDetail(string endCode)
        {
            if (ViewPage.CurrentPage.MenuID > 0)
                MenuID = ViewPage.CurrentPage.MenuID;

            var item = ModProductService.Instance.CreateQuery()
                                .Where(o => o.Activity == true && o.Code == endCode)
                                .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                .ToSingle();

            if (item != null)
            {
                //neu k phai thiet bi cam thay thi moi doi template
                if (!ViewPage.MobileMode && !ViewPage.TabletMode)
                {
                    var template = SysTemplateService.Instance.GetByID(TemplateDetailID);
                    if (template != null) ViewPage.ChangeTemplate(template);
                }

                //up view
                item.UpView();

                var listOfShop = ModProductService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ID != item.ID && o.ShopID == item.ShopID && o.ActivityPrice == true)
                                            .WhereIn(MenuID > 0, o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, ViewPage.CurrentLang.ID))
                                            .OrderByDesc(o => new { o.Order, o.ID })
                                            .Take(PageSize)
                                            .ToList_Cache();


                ViewBag.Comment = ModCommentService.Instance.CreateQuery()
                                          .Where(o => o.Activity == true && o.ProductID == item.ID && o.ParentID == 0)
                                          .OrderByDesc(o => o.ID)
                                          .ToList_Cache();
                //ViewBag.Review = ModReviewService.Instance.CreateQuery()
                //                            .Where(o => o.Activity == true && o.ProductID == item.ID && o.ParentID == 0)
                //                            .OrderByDesc(o => o.ID)
                //                            .ToList_Cache();

                //sản phẩm vừa xem
                var _Store = new Cart("Store");
                var _Product = _Store.Find(new CartItem() { ProductID = item.ID });
                if (_Product == null)
                {
                    _Store.Add(new CartItem
                    {
                        ProductID = item.ID
                    });
                }

                _Store.Save();

                if (_Store.Items.Count > 10)
                {
                    _Store.Remove(_Store.Items[_Store.Items.Count - 1]);
                    _Store.Save();
                }

                ViewPage.ViewBag.Data = ViewBag.Data = item;

                ViewPage.ViewBag.ListOfShop = listOfShop;

                //SEO
                ViewPage.CurrentPage.PageURL = ViewPage.GetURL(item.MenuID, item.Code);
                ViewPage.CurrentPage.PageFile = Core.Web.HttpRequest.Scheme + "://" + ViewPage.Request.Url.Host + Utils.GetCropFile(item.File, 200, 200);
            }
            else
            {
                ViewPage.Error404();
            }
        }
    }

    public class MProductModel
    {
        private int _page;
        public int page
        {
            get { return _page; }
            set { _page = value - 1; }
        }

        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public int State { get; set; }
        public int CityID { get; set; }
    }
}