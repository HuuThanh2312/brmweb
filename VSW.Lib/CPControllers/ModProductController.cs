﻿using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Sản phẩm - Đối tác",
        Description = "Quản lý - Sản phẩm đối tác",
        Code = "ModProduct",
        Access = 31,
        Order = 1,
        ShowInMenu = false,
        CssClass = "icon-16-article")]
    public class ModProductController : CPController
    {
        public ModProductController()
        {
            //khoi tao Service
            DataService = ModProductService.Instance;
            DataEntity = new ModProductEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModProductModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            var _keyword = !string.IsNullOrEmpty(model.SearchText) ? Data.GetCode(model.SearchText) : string.Empty;
            //tao danh sach
            var dbQuery = ModProductService.Instance.CreateQuery()
                                    .Where(model.ShopID > 0, o => o.ShopID == model.ShopID)
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Model.Contains(model.SearchText)|| o.Code.Contains(_keyword)))
                                    .Where(model.State > 0, o => (o.State & model.State) == model.State)
                                    .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForCP("Product", model.MenuID, model.LangID))
                                    .Where(model.BrandID > 0, o => o.BrandID == model.BrandID)
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

            //send to client
            //VSW.Core.SignalR.HubData<ModProductEntity>.SendData(ViewBag.Data);
        }

        public void ActionAdd(ModProductModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModProductService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                _item = new ModProductEntity
                {
                    MenuID = model.MenuID,
                    BrandID = model.BrandID,
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = CPViewPage.UserPermissions.Approve
                };

                var json = Global.Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModProductEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModProductModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModProductModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModProductModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
            ModCleanURLService.Instance.Delete("[Value] IN (" + Core.Global.Array.ToString(arrID) + ") AND [Type]='Product'");

            //xoa Product
            DataService.Delete("[ID] IN (" + Core.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModProductEntity _item;
        private bool ValidSave(ModProductModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            if (ModCleanURLService.Instance.CheckCode(_item.Code, "Product", _item.ID, model.LangID))
                CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            if (_item.MenuID < 1)
                CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            //kiem tra chuyen muc
            //if (_item.BrandID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn thương hiệu.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name);

            //cap nhat state
            _item.State = GetState(model.ArrState);

            try
            {
                //save
                ModProductService.Instance.Save(_item);

                //update url
                ModCleanURLService.Instance.InsertOrUpdate(_item.Code, "Product", _item.ID, _item.MenuID, model.LangID);

                //cap nhat thuoc tinh
                ModPropertyService.Instance.Delete(o => o.ProductID == _item.ID);

                var _AllKey = CPViewPage.PageViewState.AllKeys;
                for (var i = 0; i < _AllKey.Length; i++)
                {
                    if (!_AllKey[i].StartsWith("Property_")) continue;

                    string[] arrProperty = _AllKey[i].Split('_');
                    if (arrProperty == null || arrProperty.Length < 2) continue;

                    int PropertyID = 0, PropertyValueID = 0;

                    if (arrProperty.Length == 2)
                    {
                        PropertyID = Core.Global.Convert.ToInt(_AllKey[i].Replace("Property_", ""));
                        PropertyValueID = Core.Global.Convert.ToInt(CPViewPage.PageViewState[_AllKey[i]]);
                    }
                    else if (arrProperty.Length == 3)
                    {
                        PropertyID = Core.Global.Convert.ToInt(arrProperty[1]);
                        PropertyValueID = Core.Global.Convert.ToInt(CPViewPage.PageViewState[_AllKey[i]]);
                        //PropertyValueID = Core.Global.Convert.ToInt(arrProperty[2]);
                    }

                    if (PropertyID < 1 || PropertyValueID < 1) continue;

                    var _Property = ModPropertyService.Instance.GetByID(_item.ID, _item.MenuID, PropertyID, PropertyValueID);
                    if (_Property == null)
                    {
                        _Property = new ModPropertyEntity();
                        _Property.ID = 0;
                        _Property.ProductID = _item.ID;
                        _Property.MenuID = _item.MenuID;
                        _Property.PropertyID = PropertyID;
                        _Property.PropertyValueID = PropertyValueID;

                        ModPropertyService.Instance.Save(_Property);
                    }
                }

                Core.Web.Cache.Clear(ModProductService.Instance);
                Core.Web.Cache.Clear(WebPropertyService.Instance);

                //send to client
                //HubData<ModProductEntity>.SendData(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModProductService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModProductModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }

        public int MenuID { get; set; }
        public int BrandID { get; set; }

        public int ShopID { get; set; }

        public int State { get; set; }
        public List<int> ArrState { get; set; }
    }
}