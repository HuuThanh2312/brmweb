﻿using System;
using System.Configuration;
using System.Web.Configuration;
using VSW.Core.Global;
using VSW.Core.MVC;
using VSW.Lib.Global;
using VSW.Lib.Global.ListItem;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    public class AjaxController : CPController
    {
        public void ActionIndex()
        {
        }

        public void ActionGetChild(GetChildModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var listItem = WebMenuService.Instance.GetByParentID_Cache(model.ParentID);

            Json.Instance.Html = @"<option value=""0"" selected=""selected"">- chọn quận / huyện -</option>";
            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<option value=""" + listItem[i].ID + @""" " + (listItem[i].ID == model.SelectedID ? @"selected=""selected""" : @"") + @">" + listItem[i].Name + @"</option>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionSiteGetPage(SiteGetPageModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var listPage = List.GetList(SysPageService.Instance, model.LangID);

            for (var i = 0; listPage != null && i < listPage.Count; i++)
            {
                Json.Instance.Params += "<option " + (model.PageID.ToString() == listPage[i].Value ? "selected" : "") + " value='" + listPage[i].Value + "'>&nbsp; " + listPage[i].Name + "</option>";
            }

            Json.Create();
        }

        public void ActionTemplateGetCustom(int templateID)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            Json.Instance.Html = SysTemplateService.Instance.GetByID(templateID).Custom;

            Json.Create();
        }

        public void ActionPageGetCustom(int pageID)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            Json.Instance.Html = SysPageService.Instance.GetByID(pageID).Custom;

            Json.Create();
        }

        public void ActionPageGetControl(PageGetControlModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(model.ModuleCode))
                {
                    SysPageEntity currentPage = null;
                    var currentModule = SysModuleService.Instance.VSW_Core_GetByCode(model.ModuleCode);

                    if (model.PageID > 0)
                        currentPage = SysPageService.Instance.GetByID(model.PageID);

                    if (currentModule != null)
                    {
                        var currentObject = new Class(currentModule.ModuleType);

                        var filePath = (File.Exists(CPViewPage.Server.MapPath("~/Views/Design/" + currentModule.Code + ".ascx")) ?
                            "~/Views/Design/" + currentModule.Code + ".ascx" : "~/" + Setting.Sys_CPDir + "/Design/EditModule.ascx");

                        var sHtml = Utils.GetHtmlControl(CPViewPage, filePath,
                                            "CurrentObject", currentObject,
                                            "CurrentPage", currentPage,
                                            "CurrentModule", currentModule,
                                            "LangID", model.LangID);

                        if (currentObject.ExistsField("MenuID"))
                        {
                            var fieldInfo = currentObject.GetFieldInfo("MenuID");
                            var attributes = fieldInfo.GetCustomAttributes(typeof(PropertyInfo), true);
                            if (attributes.GetLength(0) > 0)
                            {
                                var propertyInfo = (PropertyInfo)attributes[0];
                                var listMenu = List.GetListByText(propertyInfo.Value.ToString());

                                var menuType = List.FindByName(listMenu, "Type").Value;

                                listMenu = List.GetList(WebMenuService.Instance, model.LangID, menuType);
                                listMenu.Insert(0, new Item(string.Empty, string.Empty));

                                for (var j = 1; j < listMenu.Count; j++)
                                {
                                    if (string.IsNullOrEmpty(listMenu[j].Name)) continue;

                                    Json.Instance.Params += "<option " + (currentPage != null && currentPage.MenuID.ToString() == listMenu[j].Value ? "selected" : "") + " value='" + listMenu[j].Value + "'>&nbsp; " + listMenu[j].Name + "</option>";
                                }
                            }
                        }

                        if (currentObject.ExistsField("BrandID"))
                        {
                            var fieldInfo = currentObject.GetFieldInfo("BrandID");
                            var attributes = fieldInfo.GetCustomAttributes(typeof(PropertyInfo), true);
                            if (attributes.GetLength(0) > 0)
                            {
                                var propertyInfo = (PropertyInfo)attributes[0];
                                var listMenu = List.GetListByText(propertyInfo.Value.ToString());

                                var menuType = List.FindByName(listMenu, "Type").Value;

                                listMenu = List.GetList(WebMenuService.Instance, model.LangID, menuType);
                                listMenu.Insert(0, new Item(string.Empty, string.Empty));

                                for (var j = 1; j < listMenu.Count; j++)
                                {
                                    if (string.IsNullOrEmpty(listMenu[j].Name)) continue;

                                    Json.Instance.Js += "<option " + (currentPage != null && currentPage.BrandID.ToString() == listMenu[j].Value ? "selected" : "") + " value='" + listMenu[j].Value + "'>&nbsp; " + listMenu[j].Name + "</option>";
                                }
                            }
                        }

                        Json.Instance.Html = sHtml.Replace("{CPPath}", CPViewPage.CPPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Error.Write(ex);
            }

            Json.Create();
        }

        public void ActionGetProperties(GetPropertiesModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var _Menu = WebMenuService.Instance.GetByID(model.MenuID);
            if (_Menu == null) Json.Create();

            var _Property = WebPropertyService.Instance.GetByID(_Menu.PropertyID);
            if (_Property == null) Json.Create();

            var listProperty = WebPropertyService.Instance.CreateQuery()
                                        .Select(o => new { o.ID, o.Name, o.Code, o.Multiple })
                                        .Where(o => o.Activity == true && o.ParentID == _Property.ID)
                                        .OrderByAsc(o => o.Order)
                                        .ToList_Cache();

            for (var i = 0; listProperty != null && i < listProperty.Count; i++)
            {
                var listPropertyValue = WebPropertyService.Instance.CreateQuery()
                                                    .Select(o => new { o.ID, o.Name })
                                                    .Where(o => o.ParentID == listProperty[i].ID)
                                                    .OrderByAsc(o => o.Order)
                                                    .ToList_Cache();

                if (listProperty[i].Multiple)
                {
                    Json.Instance.Html += @"<div class=""form-group row"">
                                                <label class=""col-md-3 col-form-label text-right"">" + listProperty[i].Name + @":</label>
                                                <div class=""col-md-9"">
                                                    <div class=""checkbox-list"">";

                    for (var j = 0; listPropertyValue != null && j < listPropertyValue.Count; j++)
                    {
                        var exists = model.ProductID > 0 ? ModPropertyService.Instance.GetByID(model.ProductID, model.MenuID, listProperty[i].ID, listPropertyValue[j].ID) != null : false;
                        Json.Instance.Html += @"        <label class=""itemCheckBox itemCheckBox-sm"">
                                                            <input type=""checkbox"" " + (exists ? "checked" : "") + @" name=""Property_" + listProperty[i].ID + @"_" + listPropertyValue[j].ID + @""" id=""Property_" + listProperty[i].ID + @"_" + listPropertyValue[j].ID + @""" value=""" + listPropertyValue[j].ID + @""" />
                                                            <i class=""check-box""></i>
                                                            <span>" + listPropertyValue[j].Name + @"</span>
                                                        </label>";
                    }

                    Json.Instance.Html += @"        </div>
                                                </div>
                                            </div>";
                }
                else
                {
                    Json.Instance.Html += @" <div class=""form-group row"">
                                                <label class=""col-md-3 col-form-label text-right"">" + listProperty[i].Name + @":</label>
                                                <div class=""col-md-9"">
                                                    <select class=""form-control"" name=""Property_" + listProperty[i].ID + @""" id=""Property_" + listProperty[i].ID + @""">
                                                        <option value=""0"">Root</option>";

                    for (var j = 0; listPropertyValue != null && j < listPropertyValue.Count; j++)
                    {
                        var exists = model.ProductID > 0 ? ModPropertyService.Instance.GetByID(model.ProductID, model.MenuID, listProperty[i].ID, listPropertyValue[j].ID) != null : false;
                        Json.Instance.Html += @"        <option value=""" + listPropertyValue[j].ID + @""" " + (exists ? "selected" : "") + @">" + listPropertyValue[j].Name + @"</option>";
                    }

                    Json.Instance.Html += @"        </select>
                                                </div>
                                            </div>";
                }
            }

            Json.Create();
        }

        #region set defaul SAPrice

        public void ActionSetDefault(SetDefaultModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (!string.IsNullOrEmpty(model.Value))
            {
                try
                {
                    Configuration config = WebConfigurationManager.OpenWebConfiguration("~");

                    config.AppSettings.Settings["Berich.SAPrice"].Value = model.Value;
                    config.Save();
                    Json.Instance.Html = "true";
                }
                catch (Exception ex)
                {
                    Json.Instance.Params = ex.ToString();
                    throw;
                }

            }

            ViewBag.Model = model;

            Json.Create();
        }

        #endregion

        #region File
        public void ActionAddFile(FileModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (string.IsNullOrEmpty(model.Name))
            {
                Json.Instance.Params += "Nhập tên sản phẩm trước.";
                ViewBag.Model = model;

                Json.Create();
            }

            if (model.MenuID < 1)
            {
                Json.Instance.Params += "Chọn chuyên mục sản phẩm trước.";
                ViewBag.Model = model;

                Json.Create();
            }

            ModProductEntity product = null;
            if (model.ProductID > 0)
                product = ModProductService.Instance.GetByID(model.ProductID);

            if (product == null)
            {
                //luu san pham
                product = new ModProductEntity()
                {
                    ID = 0,
                    MenuID = model.MenuID,
                    Name = model.Name,
                    Code = Data.GetCode(model.Name),
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxProductOrder(),
                    Activity = false
                };

                ModProductService.Instance.Save(product);

                Json.Instance.Js += product.ID;
            }

            if (ModProductFileService.Instance.Exists(product.ID, model.File))
            {
                Json.Instance.Params += "Ảnh đã tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            ModProductFileService.Instance.Save(new ModProductFileEntity()
            {
                ID = 0,
                ProductID = product.ID,
                File = model.File,
                Order = GetMaxProductFileOrder(product.ID),
                Activity = true
            });

            var listItem = ModProductFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == product.ID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionDeleteFile(FileModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var item = ModProductFileService.Instance.CreateQuery()
                                        .Where(o => o.ProductID == model.ProductID && o.File == model.File)
                                        .ToSingle();

            if (item != null)
                ModProductFileService.Instance.Delete(item);

            var listItem = ModProductFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionUpFile(FileModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (model.ProductID < 1)
            {
                Json.Instance.Params += "Sản phẩm không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var item = ModProductFileService.Instance.CreateQuery()
                                        .Where(o => o.ProductID == model.ProductID && o.File == model.File)
                                        .ToSingle();
            if (item == null)
            {
                Json.Instance.Params += "Ảnh không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var listItem = ModProductFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            int index = listItem.FindIndex(o => o.ID == item.ID);
            int indexPrev = index == 0 ? listItem.Count - 1 : (index - 1);

            var itemPrev = listItem[indexPrev];

            int order = item.Order;

            item.Order = itemPrev.Order;
            ModProductFileService.Instance.Save(item, o => o.Order);

            itemPrev.Order = order;
            ModProductFileService.Instance.Save(itemPrev, o => o.Order);

            listItem = ModProductFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionDownFile(FileModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (model.ProductID < 1)
            {
                Json.Instance.Params += "Sản phẩm không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var item = ModProductFileService.Instance.CreateQuery()
                                        .Where(o => o.ProductID == model.ProductID && o.File == model.File)
                                        .ToSingle();
            if (item == null)
            {
                Json.Instance.Params += "Ảnh không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var listItem = ModProductFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            int index = listItem.FindIndex(o => o.ID == item.ID);
            int indexNext = index == listItem.Count - 1 ? 0 : (index + 1);

            var itemNext = listItem[indexNext];

            int order = item.Order;

            item.Order = itemNext.Order;
            ModProductFileService.Instance.Save(item, o => o.Order);

            itemNext.Order = order;
            ModProductFileService.Instance.Save(itemNext, o => o.Order);

            listItem = ModProductFileService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />

                                            <a href=""javascript:void(0)"" onclick=""deleteFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downFile('" + listItem[i].File + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }
        #endregion

        #region Gift
        public void ActionAddGift(GiftModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (string.IsNullOrEmpty(model.Name))
            {
                Json.Instance.Params += "Nhập tên sản phẩm trước.";
                ViewBag.Model = model;

                Json.Create();
            }

            if (model.MenuID < 1)
            {
                Json.Instance.Params += "Chọn chuyên mục sản phẩm trước.";
                ViewBag.Model = model;

                Json.Create();
            }

            ModProductEntity product = null;
            if (model.ProductID > 0)
                product = ModProductService.Instance.GetByID(model.ProductID);

            if (product == null)
            {
                //luu san pham
                product = new ModProductEntity()
                {
                    ID = 0,
                    MenuID = model.MenuID,
                    Name = model.Name,
                    Code = Data.GetCode(model.Name),
                    Published = DateTime.Now,
                    Updated = DateTime.Now,
                    Order = GetMaxProductOrder(),
                    Activity = false
                };

                ModProductService.Instance.Save(product);

                Json.Instance.Js += product.ID;
            }

            if (model.GiftID < 1)
            {
                Json.Instance.Params += "Quà tặng không tồn tại. Chọn quà tặng khác";
                ViewBag.Model = model;

                Json.Create();
            }

            var gift = ModGiftService.Instance.GetByID(model.GiftID);
            if (gift == null)
            {
                Json.Instance.Params += "Quà tặng không tồn tại. Chọn quà tặng khác";
                ViewBag.Model = model;

                Json.Create();
            }

            if (ModProductGiftService.Instance.Exists(product.ID, model.GiftID))
            {
                Json.Instance.Params += "Quà tặng đã tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            ModProductGiftService.Instance.Save(new ModProductGiftEntity()
            {
                ID = 0,
                ProductID = product.ID,
                GiftID = model.GiftID,
                Name = gift.Name,
                File = gift.File,
                Price = gift.Price,
                Order = GetMaxProductGiftOrder(product.ID),
                Activity = true
            });

            var listItem = ModProductGiftService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == product.ID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />
                                            <b>" + listItem[i].Name + @"</b>
                                            <a href=""javascript:void(0)"" onclick=""deleteGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionDeleteGift(GiftModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            var item = ModProductGiftService.Instance.CreateQuery()
                                        .Where(o => o.ProductID == model.ProductID && o.GiftID == model.GiftID)
                                        .ToSingle();

            if (item != null)
                ModProductGiftService.Instance.Delete(item);

            var listItem = ModProductGiftService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />
                                            <b>" + listItem[i].Name + @"</b>
                                            <a href=""javascript:void(0)"" onclick=""deleteGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionUpGift(GiftModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (model.ProductID < 1)
            {
                Json.Instance.Params += "Sản phẩm không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var item = ModProductGiftService.Instance.CreateQuery()
                                        .Where(o => o.ProductID == model.ProductID && o.GiftID == model.GiftID)
                                        .ToSingle();
            if (item == null)
            {
                Json.Instance.Params += "Ảnh không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var listItem = ModProductGiftService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            int index = listItem.FindIndex(o => o.ID == item.ID);
            int indexPrev = index == 0 ? listItem.Count - 1 : (index - 1);

            var itemPrev = listItem[indexPrev];

            int order = item.Order;

            item.Order = itemPrev.Order;
            ModProductGiftService.Instance.Save(item, o => o.Order);

            itemPrev.Order = order;
            ModProductGiftService.Instance.Save(itemPrev, o => o.Order);

            listItem = ModProductGiftService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />
                                            <b>" + listItem[i].Name + @"</b>
                                            <a href=""javascript:void(0)"" onclick=""deleteGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }

        public void ActionDownGift(GiftModel model)
        {
            Json.Instance.Html = Json.Instance.Params = Json.Instance.Js = string.Empty;

            if (model.ProductID < 1)
            {
                Json.Instance.Params += "Sản phẩm không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var item = ModProductGiftService.Instance.CreateQuery()
                                        .Where(o => o.ProductID == model.ProductID && o.GiftID == model.GiftID)
                                        .ToSingle();
            if (item == null)
            {
                Json.Instance.Params += "Ảnh không tồn tại.";
                ViewBag.Model = model;

                Json.Create();
            }

            var listItem = ModProductGiftService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            int index = listItem.FindIndex(o => o.ID == item.ID);
            int indexNext = index == listItem.Count - 1 ? 0 : (index + 1);

            var itemNext = listItem[indexNext];

            int order = item.Order;

            item.Order = itemNext.Order;
            ModProductGiftService.Instance.Save(item, o => o.Order);

            itemNext.Order = order;
            ModProductGiftService.Instance.Save(itemNext, o => o.Order);

            listItem = ModProductGiftService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ProductID == model.ProductID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();

            for (int i = 0; listItem != null && i < listItem.Count; i++)
            {
                Json.Instance.Html += @"<li>
                                            <img src=""" + listItem[i].File + @""" />
                                            <b>" + listItem[i].Name + @"</b>
                                            <a href=""javascript:void(0)"" onclick=""deleteGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Xóa""><i class=""fa fa-ban""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""upGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển lên trên""><i class=""fa fa-arrow-up""></i></a>
                                            <a href=""javascript:void(0)"" onclick=""downGift('" + listItem[i].GiftID + @"')"" data-toggle=""tooltip"" data-placement=""bottom"" data-original-title=""Chuyển xuống dưới""><i class=""fa fa-arrow-down""></i></a>
                                        </li>";
            }

            ViewBag.Model = model;

            Json.Create();
        }
        #endregion

        #region  private
        private static int GetMaxProductOrder()
        {
            return ModProductService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        private static int GetMaxProductFileOrder(int productID)
        {
            return ModProductFileService.Instance.CreateQuery()
                    .Where(o => o.ProductID == productID)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        private static int GetMaxProductGiftOrder(int productID)
        {
            return ModProductGiftService.Instance.CreateQuery()
                    .Where(o => o.ProductID == productID)
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }
        #endregion
    }

    public class GetChildModel
    {
        public int ParentID { get; set; }
        public int SelectedID { get; set; }
    }

    public class SetDefaultModel
    {
        public string Value { get; set; }
    }

    public class PageGetControlModel
    {
        public int LangID { get; set; }
        public int PageID { get; set; }
        public string ModuleCode { get; set; }
    }

    public class SiteGetPageModel
    {
        public int LangID { get; set; }
        public int PageID { get; set; }
    }

    public class GetPropertiesModel
    {
        public int MenuID { get; set; }
        public int LangID { get; set; }
        public int ProductID { get; set; }
    }

    #region File
    public class FileModel
    {
        public string Name { get; set; }
        public int MenuID { get; set; }

        public int ProductID { get; set; }
        public string File { get; set; }
    }
    #endregion

    #region Gift
    public class GiftModel
    {
        public string Name { get; set; }
        public int MenuID { get; set; }

        public int ProductID { get; set; }
        public int GiftID { get; set; }
    }
    #endregion
}