﻿using System;
using System.Collections.Generic;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Bảng giá sản phẩm Berich",
        Description = "Quản lý - Bảng giá sản phẩm Berich",
        Code = "ModBerichSAPrice",
        Access = 31,
        Order = 3,
        ShowInMenu = false,
        CssClass = "icon-16-article")]
    public class ModBerichSAPriceController : CPController
    {
        public ModBerichSAPriceController()
        {
            //khoi tao Service
            DataService = ModBerichSAPriceService.Instance;
            DataEntity = new ModBerichSAPriceEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModBerichSAPriceModel model)
        {

            //sap xep tu dong
            var orderBy = AutoSort(string.Empty, "[ApplyBranch] DESC");

            //tao danh sach
            var dbQuery = ModBerichSAPriceService.Instance.CreateQuery()
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

            //send to client
            //VSW.Core.SignalR.HubData<ModBerichSAPriceEntity>.SendData(ViewBag.Data);
        }


    }

    public class ModBerichSAPriceModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }

        public string MenuID { get; set; }


        public Guid SAPriceID { get; set; }

        public int BrandID { get; set; }

        public int State { get; set; }
        public List<int> ArrState { get; set; }
    }
}