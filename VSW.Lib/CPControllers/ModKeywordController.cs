﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Từ khóa tìm kiếm nhiều",
        Description = "Quản lý - Từ khóa tìm kiếm",
        Code = "ModKeyword",
        Access = 31,
        Order = 1,
        ShowInMenu = true,
        CssClass = "icon-16-article")]
    public class ModKeywordController : CPController
    {
        public ModKeywordController()
        {
            //khoi tao Service
            DataService = ModKeywordService.Instance;
            DataEntity = new ModKeywordEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModKeywordModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            var _keyword = !string.IsNullOrEmpty(model.SearchText) ? Data.GetCode(model.SearchText) : string.Empty;
            //tao danh sach
            var dbQuery = ModKeywordService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Code.Contains(_keyword)))
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

        }

        public void ActionAdd(ModKeywordModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModKeywordService.Instance.GetByID(model.RecordID);

            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                _item = new ModKeywordEntity
                {
                    Created = DateTime.Now,
                    Order = GetMaxOrder(),
                    Activity = CPViewPage.UserPermissions.Approve
                };

                var json = Global.Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModKeywordEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModKeywordModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModKeywordModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModKeywordModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
           
            //xoa Keyword
            DataService.Delete("[ID] IN (" + Core.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModKeywordEntity _item;
        private bool ValidSave(ModKeywordModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");


            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name);


            try
            {
               
                //save
                ModKeywordService.Instance.Save(_item);

                Core.Web.Cache.Clear(ModKeywordService.Instance);

                //send to client
                //HubData<ModKeywordEntity>.SendData(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModKeywordService.Instance.CreateQuery()
                    .Max(o => o.Order)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModKeywordModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }
        
    }
}