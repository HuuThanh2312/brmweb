﻿using System;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Quản lý đối tác",
        Description = "Quản lý - Đối tác",
        Code = "ModShop",
        Access = 31,
        Order = 2,
        ShowInMenu = false,
        CssClass = "icon-16-article")]
    public class ModShopController : CPController
    {
        public ModShopController()
        {
            //khoi tao Service
            DataService = ModShopService.Instance;
            DataEntity = new ModShopEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModShopModel model)
        {
            //sap xep tu dong
            var orderBy = AutoSort(model.Sort);

            //tao danh sach
            var dbQuery = ModShopService.Instance.CreateQuery()
                                    .Where(!string.IsNullOrEmpty(model.SearchText), o => (o.Name.Contains(model.SearchText) || o.Email.Contains(model.SearchText)) || o.Phone.Contains(model.SearchText))
                                    .Take(model.PageSize)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);
            
            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

            //send to client
            //VSW.Core.SignalR.HubData<ModShopEntity>.SendData(ViewBag.Data);
        }

        public void ActionAdd(ModShopModel model)
        {
            if (model.RecordID > 0)
            {
                _item = ModShopService.Instance.GetByID(model.RecordID);

                //khoi tao gia tri mac dinh khi update
                // if (_item.Updated <= DateTime.MinValue) _item.Updated = DateTime.Now;
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                //_item = new ModShopEntity
                //{
                //    CityID = model.C,
                //    BrandID = model.BrandID,
                //    Published = DateTime.Now,
                //    Updated = DateTime.Now,
                //    Order = GetMaxOrder(),
                //    Activity = CPViewPage.UserPermissions.Approve
                //};

                //var json = Global.Cookies.GetValue(DataService.ToString(), true);
                //if (!string.IsNullOrEmpty(json))
                //    _item = new JavaScriptSerializer().Deserialize<ModShopEntity>(json);
            }

            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModShopModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModShopModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModShopModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }
        
        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
           // ModCleanURLService.Instance.Delete("[Value] IN (" + Core.Global.Array.ToString(arrID) + ") AND [Type]='Shop'");

            //xoa Shop
            DataService.Delete("[ID] IN (" + Core.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModShopEntity _item;
        private bool ValidSave(ModShopModel model)
        {
            TryUpdateModel(_item);

            //chong hack
            _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            if (_item.Name.Trim() == string.Empty)
                CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            //if (ModCleanURLService.Instance.CheckCode(_item.Code, "Shop", _item.ID, model.LangID))
            //    CPViewPage.Message.ListMessage.Add("Mã đã tồn tại. Vui lòng chọn mã khác.");

            //kiem tra chuyen muc
            //if (_item.MenuID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn chuyên mục.");

            //kiem tra chuyen muc
            //if (_item.BrandID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn thương hiệu.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;

            //if (string.IsNullOrEmpty(_item.Code)) _item.Code = Data.GetCode(_item.Name);

            //cap nhat state
          //  _item.State = GetState(model.ArrState);

            try
            {
                //save
                ModShopService.Instance.Save(_item);

                //update url
               
                Core.Web.Cache.Clear(ModShopService.Instance);

                //send to client
                //HubData<ModShopEntity>.SendData(_item);
            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

       

        #endregion private func
    }

    public class ModShopModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }

        public int MenuID { get; set; }
        public int BrandID { get; set; }

        public int State { get; set; }
        public List<int> ArrState { get; set; }
    }
}