﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using VSW.Lib.Global;
using VSW.Lib.Models;
using VSW.Lib.MVC;

namespace VSW.Lib.CPControllers
{
    [CPModuleInfo(Name = "Sản phẩm Berich",
        Description = "Quản lý - Sản phẩm Berich",
        Code = "ModBerichProduct",
        Access = 31,
        Order = 3,
        ShowInMenu = false,
        CssClass = "icon-16-article")]
    public class ModBerichProductController : CPController
    {
        public ModBerichProductController()
        {
            //khoi tao Service
            DataService = ModBerichProductService.Instance;
            DataEntity = new ModBerichProductEntity();

            CheckPermissions = true;
        }

        public void ActionIndex(ModBerichProductModel model)
        {

            //sap xep tu dong
            var orderBy = AutoSort(string.Empty, "[ProductCode] DESC");

            //tao danh sach
            var dbQuery = ModBerichProductService.Instance.CreateQuery()
                                    .Take(model.PageSize)
                                    .Where(!string.IsNullOrEmpty(model.MenuID), o => o.CategoryID == model.MenuID)
                                    .OrderBy(orderBy)
                                    .Skip(model.PageIndex * model.PageSize);

            ViewBag.Data = dbQuery.ToList_Cache();
            model.TotalRecord = dbQuery.TotalRecord;
            ViewBag.Model = model;

            //send to client
            //VSW.Core.SignalR.HubData<ModBerichProductEntity>.SendData(ViewBag.Data);
        }

        public void ActionAdd(ModBerichProductModel model)
        {

            if (!string.IsNullOrEmpty(model.ProductID.ToString()))
            {
                Guid aa = new Guid(model.ProductID);
                _item = ModBerichProductService.Instance.GetByProductID_Cache(aa);
                _ProductFile = ModProductBerichFileService.Instance.GetByProductID_Cache(_item.ProductID.ToString());
                if (_ProductFile == null)
                {
                    _ProductFile = new ModProductBerichFileEntity();
                }

                //khoi tao gia tri mac dinh khi update
                if (_item.ModifiedDate <= DateTime.MinValue) _item.ModifiedDate = DateTime.Now;
            }
            else
            {
                //khoi tao gia tri mac dinh khi insert
                _item = new ModBerichProductEntity
                {
                    //MenuID = model.MenuID,
                    //BrandID = model.BrandID,
                    //Published = DateTime.Now,
                    //Updated = DateTime.Now,
                    //Order = GetMaxOrder(),
                    //Activity = CPViewPage.UserPermissions.Approve
                };

                var json = Global.Cookies.GetValue(DataService.ToString(), true);
                if (!string.IsNullOrEmpty(json))
                    _item = new JavaScriptSerializer().Deserialize<ModBerichProductEntity>(json);
            }
            ViewBag.DataFile = _ProductFile;
            ViewBag.Data = _item;
            ViewBag.Model = model;
        }

        public void ActionSave(ModBerichProductModel model)
        {
            if (ValidSave(model))
                SaveRedirect();
        }

        public void ActionApply(ModBerichProductModel model)
        {
            if (ValidSave(model))
                ApplyRedirect(model.RecordID, _item.ID);
        }

        public void ActionSaveNew(ModBerichProductModel model)
        {
            if (ValidSave(model))
                SaveNewRedirect(model.RecordID, _item.ID);
        }

        public override void ActionDelete(int[] arrID)
        {
            //xoa cleanurl
            ModCleanURLService.Instance.Delete("[Value] IN (" + Core.Global.Array.ToString(arrID) + ") AND [Type]='Product'");

            //xoa Product
            DataService.Delete("[ID] IN (" + Core.Global.Array.ToString(arrID) + ")");

            //thong bao
            CPViewPage.SetMessage("Đã xóa thành công.");
            CPViewPage.RefreshPage();
        }

        #region private func

        private ModBerichProductEntity _item;
        private ModProductBerichFileEntity _ProductFile;
        private bool ValidSave(ModBerichProductModel model)
        {
            TryUpdateModel(_item);
            TryUpdateModel(_ProductFile);

            //chong hack
            // _item.ID = model.RecordID;

            ViewBag.Data = _item;
            ViewBag.Model = model;

            CPViewPage.Message.MessageType = Message.MessageTypeEnum.Error;

            //kiem tra quyen han
            if ((model.RecordID < 1 && !CPViewPage.UserPermissions.Add) || (model.RecordID > 0 && !CPViewPage.UserPermissions.Edit))
                CPViewPage.Message.ListMessage.Add("Quyền hạn chế.");

            //kiem tra ten
            //if (_item.Name.Trim() == string.Empty)
            //    CPViewPage.Message.ListMessage.Add("Nhập tiêu đề.");

            //kiem tra chuyen muc
            //if (_item.BrandID < 1)
            //    CPViewPage.Message.ListMessage.Add("Chọn thương hiệu.");

            if (CPViewPage.Message.ListMessage.Count != 0) return false;


            //cap nhat state

            try
            {
                //save
                //ModBerichProductService.Instance.Save(_item);
                _ProductFile.Content = _item.Description;
                _ProductFile.Files = model.Files;
                _ProductFile.CategoryID = _item.CategoryID;
                _ProductFile.ProductID = _item.ProductID.ToString();
                _ProductFile.File = model.File;
                ModProductBerichFileService.Instance.Save(_ProductFile);

                VSW.Core.Web.Cache.Clear(ModProductBerichFileService.Instance);

                VSW.Core.Web.Cache.Clear(ModProductService.Instance);

            }
            catch (Exception ex)
            {
                Error.Write(ex);
                CPViewPage.Message.ListMessage.Add(ex.Message);
                return false;
            }

            return true;
        }

        private static int GetMaxOrder()
        {
            return ModBerichProductService.Instance.CreateQuery()
                    .Max(o => o.SortOrder)
                    .ToValue().ToInt(0) + 1;
        }

        #endregion private func
    }

    public class ModBerichProductModel : DefaultModel
    {
        private int _langID = 1;
        public int LangID
        {
            get { return _langID; }
            set { _langID = value; }
        }

        public string SearchText { get; set; }

        public string Files { get; set; }

        public string File { get; set; }

        public string MenuID { get; set; }


        public string ProductID { get; set; }

        public int BrandID { get; set; }

        public int State { get; set; }
        public List<int> ArrState { get; set; }
    }
}