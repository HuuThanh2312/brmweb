﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModATMPayEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int WebUserID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Email { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string BankCode { get; set; }

        [DataInfo]
        public long TotalAmount { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public string IP { get; set; }

        #endregion

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && WebUserID > 0)
                _oWebUser = ModWebUserService.Instance.GetByID(WebUserID);

            return _oWebUser ?? (_oWebUser = new ModWebUserEntity());
        }
    }

    public class ModATMPayService : ServiceBase<ModATMPayEntity>
    {
        #region Autogen by VSW

        public ModATMPayService()
            : base("[Mod_ATMPay]")
        {

        }

        private static ModATMPayService _instance;
        public static ModATMPayService Instance
        {
            get { return _instance ?? (_instance = new ModATMPayService()); }
        }

        #endregion

        public ModATMPayEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}
