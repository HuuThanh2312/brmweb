﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModWebUserEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int CityID { get; set; }//Thành phố

        [DataInfo]
        public int DistrictID { get; set; }//Quận huyện


        [DataInfo]
        public int PhuongID { get; set; }//Phường

        [DataInfo]
        public int ParentID { get; set; }//cây thành viên

        [DataInfo]
        public string UserParentIDs { get; set; }//cây thành viên

        [DataInfo]
        public double BeCoin { get; set; }//tích becoin

        [DataInfo]
        public string Email { get; set; }

        [DataInfo]
        public string LoginName { get; set; }

        [DataInfo]
        public string Password { get; set; }

        [DataInfo]
        public string TempPassword { get; set; }

        [DataInfo]
        public string OldPassword { get; set; }

        #region thông tin cmnt

        [DataInfo]
        public string AddressNo { get; set; }//địa chỉ thường chú

        [DataInfo]
        public int CityCMND { get; set; }//Tỉnh/TP

        [DataInfo]
        public int CityCACMND { get; set; }//Nơi cấp

        [DataInfo]
        public int DistrictCMND { get; set; }//Nơi cấp


        [DataInfo]
        public string CodeCMND { get; set; }//số cmnd

        [DataInfo]
        public DateTime DateCMND { get; set; }//ngày cấp

        [DataInfo]
        public override string Name { get; set; }
        #endregion

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string Logo { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string Address { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public long Point { get; set; }

        [DataInfo]
        public string IP { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public DateTime BirthDay { get; set; }

        [DataInfo]
        public bool Activity { get; set; }


        [DataInfo]
        public bool IsShop { get; set; }


        [DataInfo]
        public bool Gender { get; set; }

        [DataInfo]
        public int FirstLogin { get; set; }

        [DataInfo]
        public int Level { get; set; }

        [DataInfo]
        public string VrCode { get; set; }
        [DataInfo]
        public bool Vr { get; set; }

        #endregion Autogen by VSW

        #region get someone

        private WebMenuEntity _oCity;
        public WebMenuEntity GetCity()
        {
            if (_oCity == null && CityID > 0)
                _oCity = WebMenuService.Instance.GetByID_Cache(CityID);

            return _oCity;
        }

        private WebMenuEntity _oDistrict;
        public WebMenuEntity GetDistrict()
        {
            if (_oDistrict == null && DistrictID > 0)
                _oDistrict = WebMenuService.Instance.GetByID_Cache(DistrictID);

            return _oDistrict;
        }


        private ModShopEntity _oShop;
        public ModShopEntity GetShop()
        {
            if (_oShop == null && ID > 0)
                _oShop = ModShopService.Instance.GetByUserID_Cache(ID);

            return _oShop;
        }

        private ModShopEntity _oShopNotActive;
        public ModShopEntity GetShopNotActive()
        {
            if (_oShopNotActive == null && ID > 0)
                _oShopNotActive = ModShopService.Instance.CreateQuery().Where(o => o.UserID == ID && o.Activity == false).ToSingle_Cache();

            return _oShopNotActive;
        }

        private ModBankingUserEntity _oBank;
        public ModBankingUserEntity GetBank()
        {
            if (_oBank == null && ID > 0)
                _oBank = ModBankingUserService.Instance.GetByUserID_Default(ID);

            return _oBank ?? (_oBank = new ModBankingUserEntity());
        }

        private List<ModBankingUserEntity> _oListBank;
        public List<ModBankingUserEntity> GetListBank()
        {
            if (_oListBank == null && ID > 0)
                _oListBank = ModBankingUserService.Instance.GetListByUserID(ID);

            return _oListBank;
        }

        private ModAccBcoinCardEntity _oCardBcoin;
        public ModAccBcoinCardEntity GetCardBcoin()
        {
            if (_oCardBcoin == null && ID > 0)
                _oCardBcoin = ModAccBcoinCardService.Instance.GetByUserID(ID);

            return _oCardBcoin ?? (_oCardBcoin = new ModAccBcoinCardEntity());
        }

        private List<ModTransactionLogsEntity> _oListTransaction;
        public List<ModTransactionLogsEntity> GetListTransaction()
        {
            if (_oListTransaction == null && ID > 0)
                _oListTransaction = ModTransactionLogsService.Instance.GetListByUserID_Cache(ID);

            return _oListTransaction;
        }

        private int _oCountUser = 0;
        public int CountUser()
        {
            if (_oCountUser < 1 && this.ID > 0)
            {
                _oCountUser = ModWebUserService.Instance.CreateQuery()
                               .Where(o => o.ParentID == this.ID)
                               .Select(o => o.ID)
                               .Count()
                               .ToValue()
                               .ToInt(0);
            }
            return _oCountUser;
        }

        #endregion
    }

    public class ModWebUserService : ServiceBase<ModWebUserEntity>
    {
        #region Autogen by VSW

        private ModWebUserService()
            : base("[Mod_WebUser]")
        {
        }

        private static ModWebUserService _instance;
        public static ModWebUserService Instance => _instance ?? (_instance = new ModWebUserService());

        #endregion Autogen by VSW

        public ModWebUserEntity GetByID(int id)
        {
            return base.CreateQuery()
                   .Where(o => o.ID == id)
                   .ToSingle();
        }

        public ModWebUserEntity GetByID_Cache(int id)
        {
            return base.CreateQuery()
                   .Where(o => o.ID == id)
                   .ToSingle_Cache();
        }

        public List<ModWebUserEntity> GetByParentID_Cache(int parentid)
        {
            return CreateQuery()
               .Where(o => o.ParentID == parentid)
               .ToList_Cache();
        }

        public ModWebUserEntity GetByCode_Cache(string code)
        {
            return base.CreateQuery()
                   .Where(o => o.Code == code)
                   .ToSingle_Cache();
        }

        public int GetCount()
        {
            return base.CreateQuery()
                .Select(o => o.ID)
                .Count()
                .ToValue_Cache()
                .ToInt(0);
        }


        public ModWebUserEntity GetByLogiName(string loginName)
        {
            return base.CreateQuery()
                    .Where(o => o.LoginName == loginName)
                    .ToSingle();
        }
        public ModWebUserEntity GetByEmail(string email)
        {
            return base.CreateQuery()
                    .Where(o => o.Email == email)
                    .ToSingle();
        }



        public bool CheckPhone(string phone)
        {
            return base.CreateQuery()
                   .Where(o => o.Phone == phone)
                   .Count()
                   .ToValue()
                   .ToBool();
        }

        public bool CheckEmail(string email, int id)
        {
            return base.CreateQuery()
                   .Where(o => o.Email == email && o.ID != id)
                   .Count()
                   .ToValue()
                   .ToBool();
        }

        public ModWebUserEntity GetByCode(string code)
        {
            return base.CreateQuery()
                    .Where(o => o.Code == code)
                    .ToSingle();
        }

        public bool CheckCode(string code, int id)
        {
            return base.CreateQuery()
                   .Where(o => o.Code == code && o.ID != id)
                   .Count()
                   .ToValue()
                   .ToBool();
        }

        public ModWebUserEntity GetForLogin(int id)
        {
            if (id < 1) return null;

            return base.CreateQuery()
                      .Where(o => o.ID == id)
                      .ToSingle();
        }

        public ModWebUserEntity GetForLoginName(string loginName, string md5_passwd, string phone)
        {
            var webUser = base.CreateQuery()
                                .Where(o => (o.LoginName == loginName || o.Phone == phone) && (o.Password == md5_passwd || o.TempPassword == md5_passwd))
                                .ToSingle();

            if (webUser != null && webUser.TempPassword != string.Empty)
            {
                if (webUser.TempPassword == md5_passwd) webUser.Password = md5_passwd;
                webUser.TempPassword = string.Empty;
                Save(webUser);
            }

            return webUser;
        }

        public ModWebUserEntity GetForLogin(string email, string md5_passwd)
        {
            var webUser = base.CreateQuery()
                                .Where(o => o.Email == email && (o.Password == md5_passwd || o.TempPassword == md5_passwd))
                                .ToSingle();

            if (webUser != null && webUser.TempPassword != string.Empty)
            {
                if (webUser.TempPassword == md5_passwd) webUser.Password = md5_passwd;
                webUser.TempPassword = string.Empty;
                Save(webUser);
            }

            return webUser;
        }
    }
}