﻿using System;
using System.Collections.Generic;

using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModReviewEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int ShopID { get; set; }

        [DataInfo]
        public int WebUserID { get; set; }
        

        [DataInfo]
        public int ProductID { get; set; }

        [DataInfo]
        public override string Name { get; set; }
        

        [DataInfo]
        public int Vote { get; set; }

        [DataInfo]
        public double Point { get; set; }

        [DataInfo]
        public int Like { get; set; }


        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string IP { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && WebUserID > 0)
                _oWebUser = ModWebUserService.Instance.GetByID(WebUserID);

            return _oWebUser ?? (_oWebUser = new ModWebUserEntity());
        }

        private ModProductEntity _oProduct;
        public ModProductEntity GetProduct()
        {
            if (_oProduct == null && ProductID > 0)
                _oProduct = ModProductService.Instance.GetByID(ProductID);

            return _oProduct ?? (_oProduct = new ModProductEntity());
        }
        
        public string Time
        {
            get
            {
                if ((DateTime.Now - Created).TotalDays >= 365) return Math.Round((DateTime.Now - Created).TotalDays / 365) + " năm trước.";
                else if ((DateTime.Now - Created).TotalDays >= 30) return Math.Round((DateTime.Now - Created).TotalDays / 30) + " tháng trước.";
                else if ((DateTime.Now - Created).TotalDays >= 7) return Math.Round((DateTime.Now - Created).TotalDays / 7) + " tuần trước.";
                else if ((DateTime.Now - Created).TotalDays >= 1) return Math.Round((DateTime.Now - Created).TotalDays) + " ngày trước.";
                else if ((DateTime.Now - Created).TotalHours >= 1) return Math.Round((DateTime.Now - Created).TotalHours) + " giờ trước.";
                else if ((DateTime.Now - Created).TotalMinutes >= 1) return Math.Round((DateTime.Now - Created).TotalMinutes) + " phút trước.";
                else return Math.Round((DateTime.Now - Created).TotalMilliseconds) + " giây trước.";
            }
        }
        public double Star
        {
            get
            {
                if (Vote == 0) return 0;
                return Math.Round(Point / Vote);
            }
        }
    }

    public class ModReviewService : ServiceBase<ModReviewEntity>
    {
        #region Autogen by VSW

        public ModReviewService()
            : base("[Mod_Review]")
        {

        }

        private static ModReviewService _Instance = null;
        public static ModReviewService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModReviewService();

                return _Instance;
            }
        }

        #endregion

        public ModReviewEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}
