﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModGalleryEntity : EntityBase, Search.ISearchIndex
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public int View { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by VSW

        #region ISearchIndex Members

        public int IndexID
        {
            get { return ID; }
        }

        public int IndexLangID
        {
            get { return GetMenu().LangID; }
        }

        public string IndexType
        {
            get { return "Gallery"; }
        }

        public string IndexContent
        {
            get
            {
                string _Content = Name;
                return (_Content + " " + Global.Data.RemoveVietNamese(_Content)).ToLower();
            }
        }

        #endregion

        public void UpView()
        {
            View++;
            ModGalleryService.Instance.Save(this, o => o.View);
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }
    }

    public class ModGalleryService : ServiceBase<ModGalleryEntity>
    {
        #region Autogen by VSW

        public ModGalleryService()
            : base("[Mod_Gallery]")
        {
        }

        private static ModGalleryService _instance;

        public static ModGalleryService Instance
        {
            get { return _instance ?? (_instance = new ModGalleryService()); }
        }

        #endregion Autogen by VSW

        public ModGalleryEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}