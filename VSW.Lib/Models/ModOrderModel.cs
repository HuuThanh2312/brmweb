﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModOrderEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int WebUserID { get; set; }

        [DataInfo]
        public string ShopID { get; set; }

        [DataInfo]
        public int StatusID { get; set; }

        [DataInfo]
        public int CityID { get; set; }

        [DataInfo]
        public int DistrictID { get; set; }

        [DataInfo]
        public int CommunesID { get; set; }

        [DataInfo]
        public int PaymentID { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public long Total { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Email { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string Address { get; set; }

        [DataInfo]
        public string Title { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string IP { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        #endregion Autogen by VSW

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && WebUserID > 0)
                _oWebUser = ModWebUserService.Instance.GetByID(WebUserID);

            return _oWebUser ?? (_oWebUser = new ModWebUserEntity());
        }

        private WebMenuEntity _oStatus;
        public WebMenuEntity GetStatus()
        {
            if (_oStatus == null && StatusID > 0)
                _oStatus = WebMenuService.Instance.GetByID_Cache(StatusID);

            return _oStatus ?? (_oStatus = new WebMenuEntity());
        }

        private List<ModOrderDetailEntity> _oGetOrderDetail = null;
        public List<ModOrderDetailEntity> GetOrderDetail()
        {
            if (_oGetOrderDetail == null)
            {
                _oGetOrderDetail = ModOrderDetailService.Instance.CreateQuery()
                                                    .Where(o => o.OrderID == ID)
                                                    .ToList_Cache();
            }

            return _oGetOrderDetail ?? (_oGetOrderDetail = new List<ModOrderDetailEntity>());
        }

        private WebMenuEntity _oPayment;
        public WebMenuEntity GetPayment()
        {
            if (_oPayment == null && PaymentID > 0)
                _oPayment = WebMenuService.Instance.GetByID_Cache(PaymentID);

            return _oPayment ?? (_oPayment = new WebMenuEntity());
        }
        private WebMenuEntity _oCity;
        public WebMenuEntity GetCity()
        {
            if (_oCity == null && CityID > 0)
                _oCity = WebMenuService.Instance.GetByID_Cache(CityID);

            return _oCity ?? (_oCity = new WebMenuEntity());
        }

        private WebMenuEntity _oDistrict;
        public WebMenuEntity GetDistrict()
        {
            if (_oDistrict == null && DistrictID > 0)
                _oDistrict = WebMenuService.Instance.GetByID_Cache(DistrictID);

            return _oDistrict ?? (_oDistrict = new WebMenuEntity());
        }
        //private List<List<int>> _oGetOrderDetail;

        //public List<List<int>> GetOrderDetail()
        //{
        //    if (_oGetOrderDetail == null && !string.IsNullOrEmpty(Orders))
        //    {
        //        var arrDetail = Orders.Split('|');
        //        _oGetOrderDetail = new List<List<int>>();
        //        foreach (var o in arrDetail)
        //        {
        //            if (string.IsNullOrEmpty(o)) continue;

        //            var arrValue = Array.ToInts(o);
        //            if (arrValue == null || arrValue.Length < 2) continue;

        //            var listTemp = new List<int>();
        //            for (int i = 0; i < arrValue.Length; i++)
        //                if (arrValue[i] > 0) listTemp.Add(arrValue[i]);

        //            _oGetOrderDetail.Add(listTemp);
        //        }
        //    }

        //    return _oGetOrderDetail ?? (_oGetOrderDetail = new List<List<int>>());
        //}
    }

    public class ModOrderService : ServiceBase<ModOrderEntity>
    {
        #region Autogen by VSW

        private ModOrderService()
            : base("[Mod_Order]")
        {
        }

        private static ModOrderService _instance;
        public static ModOrderService Instance => _instance ?? (_instance = new ModOrderService());

        #endregion Autogen by VSW

        public ModOrderEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}