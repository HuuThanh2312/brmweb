﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModMarketingNewsEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int Percent { get; set; }//phần trăm giảm

        [DataInfo]
        public bool OrderPro { get; set; }//phần trăm giảm cho order hay sản phẩm, true order false sản phẩm

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public int View { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public DateTime EndDate { get; set; }

        [DataInfo]
        public DateTime StartDate { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by VSW

        public string PublishedTime
        {
            get
            {
                var _TimeSpan = DateTime.Now - this.Published;

                if ((_TimeSpan).TotalDays >= 365) return Math.Round((_TimeSpan).TotalDays / 365) + " năm trước.";
                else if ((_TimeSpan).TotalDays >= 30) return Math.Round((_TimeSpan).TotalDays / 30) + " tháng trước.";
                else if ((_TimeSpan).TotalDays >= 7) return Math.Round((_TimeSpan).TotalDays / 7) + " tuần trước.";
                else if ((_TimeSpan).TotalDays >= 1) return Math.Round((_TimeSpan).TotalDays) + " ngày trước.";
                else if ((_TimeSpan).TotalHours >= 1) return Math.Round((_TimeSpan).TotalHours) + " giờ trước.";
                else if ((_TimeSpan).TotalMinutes >= 1) return Math.Round((_TimeSpan).TotalMinutes) + " phút trước.";
                else return Math.Round((_TimeSpan).TotalSeconds) + " giây trước.";
            }
        }

        public string TimeFormat
        {
            get
            {
                if (Published > DateTime.MinValue) return string.Format("{0:dd/MM/yyyy HH:mm}", this.Published);
                else return string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
            }
        }

     

        public void UpView()
        {
            View++;
            ModMarketingNewsService.Instance.Save(this, o => o.View);
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }
    }

    public class ModMarketingNewsService : ServiceBase<ModMarketingNewsEntity>
    {
        #region Autogen by VSW

        public ModMarketingNewsService()
            : base("[Mod_MarketingNews]")
        {
        }

        private static ModMarketingNewsService _instance;
        public static ModMarketingNewsService Instance => _instance ?? (_instance = new ModMarketingNewsService());

        #endregion Autogen by VSW

        public ModMarketingNewsEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}