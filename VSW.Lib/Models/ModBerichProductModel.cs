﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModBerichProductEntity : EntityBase
    {
        #region Autogen by VSW

        //[DataInfo]
        //public override int ID { get; set; }

        [DataInfo]
        public Guid ProductID { get; set; }

        [DataInfo]
        public string ProductCode { get; set; }

        [DataInfo]
        public string ManufacturersProducCode { get; set; }

        //[DataInfo]
        //public override string Name { get; set; }

        [DataInfo]
        public string ProductName { get; set; }

        [DataInfo]
        public string ProductPrefix { get; set; }

        [DataInfo]
        public int ProductType { get; set; }

        [DataInfo]
        public Guid ParentID { get; set; }

        [DataInfo]
        public bool IsParent { get; set; }


        [DataInfo]
        public string UnitID { get; set; }//số lượng đã bán

        [DataInfo]
        public string Description { get; set; }//tổng số lượng 

        [DataInfo]
        public bool Inactive { get; set; }

        [DataInfo]
        public bool IsConsignment { get; set; }

        [DataInfo]
        public bool InBarcode { get; set; }

        [DataInfo]
        public string CategoryID { get; set; }

        [DataInfo]
        public string CategoryExtra { get; set; }

        [DataInfo]
        public string CatalogID { get; set; }

        [DataInfo]
        public string ProductSource { get; set; }

        [DataInfo]
        public string CatalogExtra { get; set; }

        [DataInfo]
        public string Warranty { get; set; }

        [DataInfo]
        public string Image { get; set; }

        [DataInfo]
        public int PackingType { get; set; }

        [DataInfo]
        public string ManufacturersBarcode { get; set; }

        [DataInfo]
        public string InventoryAccount { get; set; }

        [DataInfo]
        public string PriceAccountt { get; set; }
        [DataInfo]
        public string ReturnAccount { get; set; }

        [DataInfo]
        public string RevenueAccount { get; set; }
        [DataInfo]
        public string ExpenseAccount { get; set; }

        [DataInfo]
        public decimal PricePackaging { get; set; }


        [DataInfo]
        public decimal UnitConversion { get; set; }


        [DataInfo]
        public decimal RationBreakLost { get; set; }
        [DataInfo]
        public decimal RationStable { get; set; }
        [DataInfo]
        public decimal RationPromotion { get; set; }
        [DataInfo]
        public decimal RationMinProfit { get; set; }
        [DataInfo]
        public string Manufacturers { get; set; }
        [DataInfo]
        public bool RequireSerial { get; set; }
        [DataInfo]
        public string ColorID { get; set; }
        [DataInfo]
        public string SizeID { get; set; }
        [DataInfo]
        public string CreatedBy { get; set; }
        [DataInfo]
        public DateTime CreatedDate { get; set; }

        [DataInfo]
        public string ModifiedBy { get; set; }

        [DataInfo]
        public DateTime ModifiedDate { get; set; }
        [DataInfo]
        public string ApprovalBy { get; set; }
        [DataInfo]
        public DateTime ApprovalDate { get; set; }

       
        [DataInfo]
        public bool IsApproval { get; set; }
        [DataInfo]
        public int SortOrder { get; set; }
        [DataInfo]
        public decimal CostOfCapital { get; set; }
        [DataInfo]
        public decimal QuantityOpening { get; set; }
        [DataInfo]
        public int EditVersion { get; set; }

        #endregion Autogen by VSW

        public ModBerichProductEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }
           private ModProductBerichFileEntity _oFileProduct;
        public ModProductBerichFileEntity GetFileProduct()
        {
            if (_oFileProduct == null && !string.IsNullOrEmpty(ProductID.ToString()))
                _oFileProduct = ModProductBerichFileService.Instance.GetByProductID_Cache(ProductID.ToString());

            return _oFileProduct ?? (_oFileProduct = new ModProductBerichFileEntity());
        }

        private ModBerichCategoryEntity _oCategory;
        public ModBerichCategoryEntity GetCategory()
        {
            if (_oCategory == null && !string.IsNullOrEmpty(CategoryID))
                _oCategory = ModBerichCategoryService.Instance.GetByCode_Cache(CategoryID);

            return _oCategory ?? (_oCategory = new ModBerichCategoryEntity());
        }

        private ModBerichProductPriceEntity _oPrice;
        public ModBerichProductPriceEntity GetCostPrice()
        {
            if (_oPrice == null && !string.IsNullOrEmpty(ProductID.ToString()))
                _oPrice = ModBerichProductPriceService.Instance.GetByProductID_Cache(ProductID);

            return _oPrice ?? (_oPrice = new ModBerichProductPriceEntity());
        }

        private ModBerichBarCodeEntity _oBarCode;
        public ModBerichBarCodeEntity GetBarCode()
        {
            if (_oBarCode == null && !string.IsNullOrEmpty(ProductID.ToString("D")))
                _oBarCode = ModBerichBarCodeService.Instance.GetByProductID_Cache(ProductID);

            return _oBarCode ?? (_oBarCode = new ModBerichBarCodeEntity());
        }

    }

    public class ModBerichProductService : ServiceBase<ModBerichProductEntity>
    {
        #region Autogen by VSW

        public ModBerichProductService() : base("[DMProduct]")
        {
            DBConfigKey = "DBConnection2";
            //DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModBerichProductService _instance;
        public static ModBerichProductService Instance
        {
            get { return _instance ?? (_instance = new ModBerichProductService()); }
        }

        #endregion Autogen by VSW

        public ModBerichProductEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModBerichProductEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public ModBerichProductEntity GetByProductID_Cache(Guid productid)
        {
            string s = productid.ToString("D");

            return CreateQuery()
               .Where(o => o.ProductID == new Guid(s))
               .ToSingle_Cache();
        }
        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}