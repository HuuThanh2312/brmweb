﻿using System;
using System.Collections.Generic;

using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModKeywordEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public int Click { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }
        #endregion

       
        
        public string Time
        {
            get
            {
                if ((DateTime.Now - Created).TotalDays >= 365) return Math.Round((DateTime.Now - Created).TotalDays / 365) + " năm trước.";
                else if ((DateTime.Now - Created).TotalDays >= 30) return Math.Round((DateTime.Now - Created).TotalDays / 30) + " tháng trước.";
                else if ((DateTime.Now - Created).TotalDays >= 7) return Math.Round((DateTime.Now - Created).TotalDays / 7) + " tuần trước.";
                else if ((DateTime.Now - Created).TotalDays >= 1) return Math.Round((DateTime.Now - Created).TotalDays) + " ngày trước.";
                else if ((DateTime.Now - Created).TotalHours >= 1) return Math.Round((DateTime.Now - Created).TotalHours) + " giờ trước.";
                else if ((DateTime.Now - Created).TotalMinutes >= 1) return Math.Round((DateTime.Now - Created).TotalMinutes) + " phút trước.";
                else return Math.Round((DateTime.Now - Created).TotalMilliseconds) + " giây trước.";
            }
        }
     
    }

    public class ModKeywordService : ServiceBase<ModKeywordEntity>
    {
        #region Autogen by VSW

        public ModKeywordService()
            : base("[Mod_Keyword]")
        {

        }

        private static ModKeywordService _Instance = null;
        public static ModKeywordService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModKeywordService();

                return _Instance;
            }
        }

        #endregion

        public ModKeywordEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}
