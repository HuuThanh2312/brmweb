﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModProductEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        #region data from db Berich
        [DataInfo]
        public string BerichProductID { get; set; }

        [DataInfo]
        public string CategoryName { get; set; }

        [DataInfo]
        public string BerichCategoryID { get; set; }

        [DataInfo]
        public decimal CostPrice { get; set; }

        [DataInfo]
        public string Barcode { get; set; }
        

        #endregion

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int ShopID { get; set; }

        [DataInfo]
        public int FromCityID { get; set; }

        [DataInfo]
        public int BrandID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string Model { get; set; }

        [DataInfo]
        public long Price { get; set; }

        [DataInfo]
        public long Price2 { get; set; }

        [DataInfo]
        public string File { get; set; }


        [DataInfo]
        public int Sold { get; set; }//số lượng đã bán

        [DataInfo]
        public int Inventory { get; set; }//tổng số lượng 

        [DataInfo]
        public long View { get; set; }

        [DataInfo]
        public int Favorite { get; set; }//yêu thích

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string Specifications { get; set; }

        [DataInfo]
        public string Feature { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public DateTime FlastSale { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public int Vote { get; set; }


        [DataInfo]
        public double VotePoint { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public bool ActivityPrice { get; set; }//Duyệt giá bán

        #endregion Autogen by VSW

        public ModProductEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }

        private WebMenuEntity _oBrand;
        public WebMenuEntity GetBrand()
        {
            if (_oBrand == null && BrandID > 0)
                _oBrand = WebMenuService.Instance.GetByID_Cache(BrandID);

            return _oBrand ?? (_oBrand = new WebMenuEntity());
        }

        private ModShopEntity _oShop;
        public ModShopEntity GetShop()
        {
            if (_oShop == null && ShopID > 0)
                _oShop = ModShopService.Instance.GetByID_Cache(ShopID);

            return _oShop ?? (_oShop = new ModShopEntity());
        }

        public void UpView()
        {
            View++;
            ModProductService.Instance.Save(this, o => o.View);
        }

        private long _oSellOff = 0;
        public long SellOff
        {
            get
            {
                if (_oSellOff == 0 && Price2 > Price)
                    _oSellOff = Price2 - Price;

                return _oSellOff;
            }
        }

        private long _oSellOffPercent = 0;
        public long SellOffPercent
        {
            get
            {
                if (_oSellOffPercent == 0 && Price2 > 0 && SellOff > 0)
                    _oSellOffPercent = SellOff * 100 / Price2;

                return _oSellOffPercent;
            }
        }


        private int _oSold = 0;
        public int getSold
        {
            get
            {
                if (_oSold == 0 && Sold > 0 && Inventory > 0)
                    _oSold = Sold * 100 / Inventory;

                return _oSold;
            }
        }

        private Dictionary<ModPropertyEntity, List<ModPropertyEntity>> _oGetProperty;
        public Dictionary<ModPropertyEntity, List<ModPropertyEntity>> GetProperty()
        {
            if (_oGetProperty == null)
            {
                _oGetProperty = new Dictionary<ModPropertyEntity, List<ModPropertyEntity>>();

                var listProperty = ModPropertyService.Instance.CreateQuery()
                                            .Select(o => o.PropertyID)
                                            .Distinct()
                                            .Where(o => o.ProductID == ID)
                                            .ToList_Cache();

                for (int i = 0; listProperty != null && i < listProperty.Count; i++)
                {
                    var propertyValue = ModPropertyService.Instance.CreateQuery()
                                                        .Where(o => o.ProductID == ID && o.PropertyID == listProperty[i].PropertyID)
                                                        .ToList_Cache();

                    if (propertyValue != null) _oGetProperty.Add(listProperty[i], propertyValue);
                }
            }

            return _oGetProperty ?? (_oGetProperty = new Dictionary<ModPropertyEntity, List<ModPropertyEntity>>());
        }

        private List<ModProductFileEntity> _oGetFile;
        public List<ModProductFileEntity> GetFile()
        {
            if (_oGetFile == null && MenuID > 0)
                _oGetFile = ModProductFileService.Instance.CreateQuery()
                                                .Where(o => o.ProductID == ID)
                                                .ToList_Cache();

            return _oGetFile ?? (_oGetFile = new List<ModProductFileEntity>());
        }

        private List<ModProductGiftEntity> _oGetGift;
        public List<ModProductGiftEntity> GetGift()
        {
            if (_oGetGift == null && MenuID > 0)
                _oGetGift = ModProductGiftService.Instance.CreateQuery()
                                                .Where(o => o.ProductID == ID)
                                                .ToList_Cache();

            return _oGetGift ?? (_oGetGift = new List<ModProductGiftEntity>());
        }

        private string _oSummaryHtml = string.Empty;
        public string SummaryHtml
        {
            get
            {
                if (string.IsNullOrEmpty(_oSummaryHtml) && !string.IsNullOrEmpty(Summary))
                {
                    string[] _ArrSummary = Regex.Split(Summary, "\n");
                    for (int i = 0; i < _ArrSummary.Length; i++)
                    {
                        if (string.IsNullOrEmpty(_ArrSummary[i])) continue;
                        _oSummaryHtml += "<p>" + _ArrSummary[i].Trim() + "</p>";
                    }
                }

                return _oSummaryHtml;
            }
        }

        private string _oPropertyHtml = string.Empty;
        public string PropertyHtml
        {
            get
            {
                if (string.IsNullOrEmpty(_oPropertyHtml))
                {
                    var listItem = GetProperty();
                    foreach (var item in listItem.Keys)
                    {
                        var temp = WebPropertyService.Instance.GetByID_Cache(item.PropertyID);
                        if (temp == null) continue;

                        var listChildItem = listItem[item];
                        for (var k = 0; listChildItem != null && k < listChildItem.Count; k++)
                        {
                            string property = "";

                            var tempValue = WebPropertyService.Instance.GetByID_Cache(listChildItem[k].PropertyValueID);
                            if (tempValue == null) continue;

                            property += (!string.IsNullOrEmpty(property) ? ", " : "") + tempValue.Name;
                            _oPropertyHtml += @"<a href=""javascript:void(0)"" class=""btn btn-col-thuoctinh"">" + temp.Name + @": <b>" + property + @"</b></a>";
                        }
                    }
                }

                return _oPropertyHtml;
            }
        }
        public double Star
        {
            get
            {
                if (Vote == 0) return 0;
                return Math.Round(VotePoint / Vote);
            }
        }
    }

    public class ModProductService : ServiceBase<ModProductEntity>
    {
        #region Autogen by VSW

        public ModProductService() : base("[Mod_Product]")
        {
            DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModProductService _instance;
        public static ModProductService Instance
        {
            get { return _instance ?? (_instance = new ModProductService()); }
        }

        #endregion Autogen by VSW

        public ModProductEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModProductEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public ModProductEntity GetByShopID_Cache(string id, int shopID)
        {
            return CreateQuery()
               .Where(o => o.ShopID == shopID && o.BerichProductID == id)
               .ToSingle_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}