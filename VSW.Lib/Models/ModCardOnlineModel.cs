﻿using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModCardOnlineEntity : EntityBase
    {
        #region Autogen by VSW
        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int TypeID { get; set; }

        [DataInfo]
        public int BrandID { get; set; }

        [DataInfo]
        public int PriceID { get; set; }

        [DataInfo]
        public int PaymentID { get; set; }

        [DataInfo]
        public int Quantity { get; set; }

        [DataInfo]
        public string IP { get; set; }

        #endregion Autogen by VSW
    }

    public class ModCardOnlineService : ServiceBase<ModCardOnlineEntity>
    {
        #region Autogen by VSW

        public ModCardOnlineService()
            : base("[Mod_CardOnline]")
        {
        }

        private static ModCardOnlineService _instance;
        public static ModCardOnlineService Instance => _instance ?? (_instance = new ModCardOnlineService());

        #endregion Autogen by VSW

        public ModCardOnlineEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}