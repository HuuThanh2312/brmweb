﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModBerichCategoryEntity : EntityBase
    {
        #region Autogen by VSW

        //[DataInfo]
        //public override int ID { get; set; }

        [DataInfo]
        public string CategoryID { get; set; }

        [DataInfo]
        public string CategoryName { get; set; }

        [DataInfo]
        public string ParentID { get; set; }

        //[DataInfo]
        //public override string Name { get; set; }

        [DataInfo]
        public bool IsParent { get; set; }

        [DataInfo]
        public int Level { get; set; }

        [DataInfo]
        public string TreeCode { get; set; }

        [DataInfo]
        public int SortCode { get; set; }

        [DataInfo]
        public int SortOrder { get; set; }


        [DataInfo]
        public bool Inactive { get; set; }//số lượng đã bán

        [DataInfo]
        public string Description { get; set; }//tổng số lượng 

        [DataInfo]
        public Guid WarehouseID { get; set; }

        [DataInfo]
        public string RevenueAccount { get; set; }

        [DataInfo]
        public string ExpenseAccount { get; set; }

        [DataInfo]
        public decimal RationBreakLost { get; set; }

        [DataInfo]
        public decimal RationPromotion { get; set; }

        [DataInfo]
        public decimal RationStable { get; set; }

        [DataInfo]
        public decimal RationMinProfit { get; set; }

        [DataInfo]
        public Guid RefID { get; set; }

        [DataInfo]
        public int EditVersion { get; set; }
        

        #endregion Autogen by VSW

        public ModBerichCategoryEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }

        //private WebMenuEntity _oMenu;
        //public WebMenuEntity GetMenu()
        //{
        //    if (_oMenu == null && MenuID > 0)
        //        _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

        //    return _oMenu ?? (_oMenu = new WebMenuEntity());
        //}

        //private WebMenuEntity _oBrand;
        //public WebMenuEntity GetBrand()
        //{
        //    if (_oBrand == null && BrandID > 0)
        //        _oBrand = WebMenuService.Instance.GetByID_Cache(BrandID);

        //    return _oBrand ?? (_oBrand = new WebMenuEntity());
        //}

        //public void UpView()
        //{
        //    View++;
        //    ModBerichCategoryService.Instance.Save(this, o => o.View);
        //}

        //private long _oSellOff = 0;
        //public long SellOff
        //{
        //    get
        //    {
        //        if (_oSellOff == 0 && Price2 > Price)
        //            _oSellOff = Price2 - Price;

        //        return _oSellOff;
        //    }
        //}

        //private long _oSellOffPercent = 0;
        //public long SellOffPercent
        //{
        //    get
        //    {
        //        if (_oSellOffPercent == 0 && Price2 > 0 && SellOff > 0)
        //            _oSellOffPercent = SellOff * 100 / Price2;

        //        return _oSellOffPercent;
        //    }
        //}


        //private int _oSold = 0;
        //public int getSold
        //{
        //    get
        //    {
        //        if (_oSold == 0 && Sold > 0 && Inventory > 0)
        //            _oSold = Sold * 100 / Inventory;

        //        return _oSold;
        //    }
        //}

        //private Dictionary<ModPropertyEntity, List<ModPropertyEntity>> _oGetProperty;
        //public Dictionary<ModPropertyEntity, List<ModPropertyEntity>> GetProperty()
        //{
        //    if (_oGetProperty == null)
        //    {
        //        _oGetProperty = new Dictionary<ModPropertyEntity, List<ModPropertyEntity>>();

        //        var listProperty = ModPropertyService.Instance.CreateQuery()
        //                                    .Select(o => o.PropertyID)
        //                                    .Distinct()
        //                                    .Where(o => o.BerichCategoryID == ID)
        //                                    .ToList_Cache();

        //        for (int i = 0; listProperty != null && i < listProperty.Count; i++)
        //        {
        //            var propertyValue = ModPropertyService.Instance.CreateQuery()
        //                                                .Where(o => o.BerichCategoryID == ID && o.PropertyID == listProperty[i].PropertyID)
        //                                                .ToList_Cache();

        //            if (propertyValue != null) _oGetProperty.Add(listProperty[i], propertyValue);
        //        }
        //    }

        //    return _oGetProperty ?? (_oGetProperty = new Dictionary<ModPropertyEntity, List<ModPropertyEntity>>());
        //}

        //private List<ModBerichCategoryFileEntity> _oGetFile;
        //public List<ModBerichCategoryFileEntity> GetFile()
        //{
        //    if (_oGetFile == null && MenuID > 0)
        //        _oGetFile = ModBerichCategoryFileService.Instance.CreateQuery()
        //                                        .Where(o => o.BerichCategoryID == ID)
        //                                        .ToList_Cache();

        //    return _oGetFile ?? (_oGetFile = new List<ModBerichCategoryFileEntity>());
        //}

        //private List<ModBerichCategoryGiftEntity> _oGetGift;
        //public List<ModBerichCategoryGiftEntity> GetGift()
        //{
        //    if (_oGetGift == null && MenuID > 0)
        //        _oGetGift = ModBerichCategoryGiftService.Instance.CreateQuery()
        //                                        .Where(o => o.BerichCategoryID == ID)
        //                                        .ToList_Cache();

        //    return _oGetGift ?? (_oGetGift = new List<ModBerichCategoryGiftEntity>());
        //}

        //private string _oSummaryHtml = string.Empty;
        //public string SummaryHtml
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_oSummaryHtml) && !string.IsNullOrEmpty(Summary))
        //        {
        //            string[] _ArrSummary = Regex.Split(Summary, "\n");
        //            for (int i = 0; i < _ArrSummary.Length; i++)
        //            {
        //                if (string.IsNullOrEmpty(_ArrSummary[i])) continue;
        //                _oSummaryHtml += "<p>" + _ArrSummary[i].Trim() + "</p>";
        //            }
        //        }

        //        return _oSummaryHtml;
        //    }
        //}

        //private string _oPropertyHtml = string.Empty;
        //public string PropertyHtml
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_oPropertyHtml))
        //        {
        //            var listItem = GetProperty();
        //            foreach (var item in listItem.Keys)
        //            {
        //                var temp = WebPropertyService.Instance.GetByID_Cache(item.PropertyID);
        //                if (temp == null) continue;

        //                var listChildItem = listItem[item];
        //                for (var k = 0; listChildItem != null && k < listChildItem.Count; k++)
        //                {
        //                    string property = "";

        //                    var tempValue = WebPropertyService.Instance.GetByID_Cache(listChildItem[k].PropertyValueID);
        //                    if (tempValue == null) continue;

        //                    property += (!string.IsNullOrEmpty(property) ? ", " : "") + tempValue.Name;
        //                    _oPropertyHtml += @"<a href=""javascript:void(0)"" class=""btn btn-col-thuoctinh"">" + temp.Name + @": <b>" + property + @"</b></a>";
        //                }
        //            }
        //        }

        //        return _oPropertyHtml;
        //    }
        //}
    }

    public class ModBerichCategoryService : ServiceBase<ModBerichCategoryEntity>
    {
        #region Autogen by VSW

        public ModBerichCategoryService() : base("[DMCategory]")
        {
            DBConfigKey = "DBConnection2";
            //DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModBerichCategoryService _instance;
        public static ModBerichCategoryService Instance
        {
            get { return _instance ?? (_instance = new ModBerichCategoryService()); }
        }

        #endregion Autogen by VSW

        public ModBerichCategoryEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }


        public ModBerichCategoryEntity GetByCode_Cache(string id)
        {
            return CreateQuery()
               .Where(o => o.CategoryID == id)
               .ToSingle_Cache();
        }

        public ModBerichCategoryEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}