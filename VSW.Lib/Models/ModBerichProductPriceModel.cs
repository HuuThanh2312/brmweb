﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;
using VSW.Lib.Global;

namespace VSW.Lib.Models
{
    public class ModBerichProductPriceEntity : EntityBase
    {
        #region Autogen by VSW

        //[DataInfo]
        //public override int ID { get; set; }

        [DataInfo]
        public Guid RefDetailID { get; set; }

        [DataInfo]
        public Guid RefID { get; set; }

        [DataInfo]
        public Guid ProductID { get; set; }

        //[DataInfo]
        //public override string Name { get; set; }

        [DataInfo]
        public string ProductCode { get; set; }

        [DataInfo]
        public string ProductPrefix { get; set; }

        [DataInfo]
        public string UnitID { get; set; }

        [DataInfo]
        public Guid ParentID { get; set; }

        [DataInfo]
        public string CategoryID { get; set; }


        [DataInfo]
        public DateTime StartDate { get; set; }//số lượng đã bán

        [DataInfo]
        public decimal ImportPrice { get; set; }//tổng số lượng 

        [DataInfo]
        public decimal SalePrice { get; set; }

        [DataInfo]
        public decimal ProfitSellingPrice { get; set; }

        [DataInfo]
        public decimal Profit { get; set; }

        [DataInfo]
        public decimal ProfitQD { get; set; }

        [DataInfo]
        public decimal UnDiscountedPrice { get; set; }

        [DataInfo]
        public decimal PricePackaging { get; set; }

        [DataInfo]
        public decimal UnitConversion { get; set; }

        [DataInfo]
        public decimal RationMinProfit { get; set; }

        [DataInfo]
        public int QuantityMax { get; set; }

        [DataInfo]
        public int QuantityMin { get; set; }

        [DataInfo]
        public bool Inactive { get; set; }

        [DataInfo]
        public DateTime ModifiedDate { get; set; }

        [DataInfo]
        public string ModifiedBy { get; set; }

        [DataInfo]
        public DateTime ApprovalDate { get; set; }
        [DataInfo]
        public string ApprovalBy { get; set; }

        [DataInfo]
        public int EditVersion { get; set; }
       

        #endregion Autogen by VSW

        public ModBerichProductPriceEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }

        //private ModBerichCategoryEntity _oCategory;
        //public ModBerichCategoryEntity GetCategory()
        //{
        //    if (_oCategory == null && !string.IsNullOrEmpty(CatalogID))
        //        _oCategory = ModBerichCategoryService.Instance.GetByCode_Cache(CatalogID);

        //    return _oCategory ?? (_oCategory = new ModBerichCategoryEntity());
        //}

        //private WebMenuEntity _oBrand;
        //public WebMenuEntity GetBrand()
        //{
        //    if (_oBrand == null && BrandID > 0)
        //        _oBrand = WebMenuService.Instance.GetByID_Cache(BrandID);

        //    return _oBrand ?? (_oBrand = new WebMenuEntity());
        //}

        //public void UpView()
        //{
        //    View++;
        //    ModBerichProductPriceService.Instance.Save(this, o => o.View);
        //}

        //private long _oSellOff = 0;
        //public long SellOff
        //{
        //    get
        //    {
        //        if (_oSellOff == 0 && Price2 > Price)
        //            _oSellOff = Price2 - Price;

        //        return _oSellOff;
        //    }
        //}

        //private long _oSellOffPercent = 0;
        //public long SellOffPercent
        //{
        //    get
        //    {
        //        if (_oSellOffPercent == 0 && Price2 > 0 && SellOff > 0)
        //            _oSellOffPercent = SellOff * 100 / Price2;

        //        return _oSellOffPercent;
        //    }
        //}


        //private int _oSold = 0;
        //public int getSold
        //{
        //    get
        //    {
        //        if (_oSold == 0 && Sold > 0 && Inventory > 0)
        //            _oSold = Sold * 100 / Inventory;

        //        return _oSold;
        //    }
        //}

        //private Dictionary<ModPropertyEntity, List<ModPropertyEntity>> _oGetProperty;
        //public Dictionary<ModPropertyEntity, List<ModPropertyEntity>> GetProperty()
        //{
        //    if (_oGetProperty == null)
        //    {
        //        _oGetProperty = new Dictionary<ModPropertyEntity, List<ModPropertyEntity>>();

        //        var listProperty = ModPropertyService.Instance.CreateQuery()
        //                                    .Select(o => o.PropertyID)
        //                                    .Distinct()
        //                                    .Where(o => o.BerichProductPriceID == ID)
        //                                    .ToList_Cache();

        //        for (int i = 0; listProperty != null && i < listProperty.Count; i++)
        //        {
        //            var propertyValue = ModPropertyService.Instance.CreateQuery()
        //                                                .Where(o => o.BerichProductPriceID == ID && o.PropertyID == listProperty[i].PropertyID)
        //                                                .ToList_Cache();

        //            if (propertyValue != null) _oGetProperty.Add(listProperty[i], propertyValue);
        //        }
        //    }

        //    return _oGetProperty ?? (_oGetProperty = new Dictionary<ModPropertyEntity, List<ModPropertyEntity>>());
        //}

        //private List<ModBerichProductPriceFileEntity> _oGetFile;
        //public List<ModBerichProductPriceFileEntity> GetFile()
        //{
        //    if (_oGetFile == null && MenuID > 0)
        //        _oGetFile = ModBerichProductPriceFileService.Instance.CreateQuery()
        //                                        .Where(o => o.BerichProductPriceID == ID)
        //                                        .ToList_Cache();

        //    return _oGetFile ?? (_oGetFile = new List<ModBerichProductPriceFileEntity>());
        //}

        //private List<ModBerichProductPriceGiftEntity> _oGetGift;
        //public List<ModBerichProductPriceGiftEntity> GetGift()
        //{
        //    if (_oGetGift == null && MenuID > 0)
        //        _oGetGift = ModBerichProductPriceGiftService.Instance.CreateQuery()
        //                                        .Where(o => o.BerichProductPriceID == ID)
        //                                        .ToList_Cache();

        //    return _oGetGift ?? (_oGetGift = new List<ModBerichProductPriceGiftEntity>());
        //}

        //private string _oSummaryHtml = string.Empty;
        //public string SummaryHtml
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_oSummaryHtml) && !string.IsNullOrEmpty(Summary))
        //        {
        //            string[] _ArrSummary = Regex.Split(Summary, "\n");
        //            for (int i = 0; i < _ArrSummary.Length; i++)
        //            {
        //                if (string.IsNullOrEmpty(_ArrSummary[i])) continue;
        //                _oSummaryHtml += "<p>" + _ArrSummary[i].Trim() + "</p>";
        //            }
        //        }

        //        return _oSummaryHtml;
        //    }
        //}

        //private string _oPropertyHtml = string.Empty;
        //public string PropertyHtml
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_oPropertyHtml))
        //        {
        //            var listItem = GetProperty();
        //            foreach (var item in listItem.Keys)
        //            {
        //                var temp = WebPropertyService.Instance.GetByID_Cache(item.PropertyID);
        //                if (temp == null) continue;

        //                var listChildItem = listItem[item];
        //                for (var k = 0; listChildItem != null && k < listChildItem.Count; k++)
        //                {
        //                    string property = "";

        //                    var tempValue = WebPropertyService.Instance.GetByID_Cache(listChildItem[k].PropertyValueID);
        //                    if (tempValue == null) continue;

        //                    property += (!string.IsNullOrEmpty(property) ? ", " : "") + tempValue.Name;
        //                    _oPropertyHtml += @"<a href=""javascript:void(0)"" class=""btn btn-col-thuoctinh"">" + temp.Name + @": <b>" + property + @"</b></a>";
        //                }
        //            }
        //        }

        //        return _oPropertyHtml;
        //    }
        //}
    }

    public class ModBerichProductPriceService : ServiceBase<ModBerichProductPriceEntity>
    {
        #region Autogen by VSW

        public ModBerichProductPriceService() : base("[SAPriceDetail]")
        {
            DBConfigKey = "DBConnection2";
            //DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModBerichProductPriceService _instance;
        public static ModBerichProductPriceService Instance
        {
            get { return _instance ?? (_instance = new ModBerichProductPriceService()); }
        }

        #endregion Autogen by VSW

        public ModBerichProductPriceEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModBerichProductPriceEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public ModBerichProductPriceEntity GetByProductID_Cache(Guid productid)
        {
            string s = productid.ToString("D");
            string IsDefault = Setting.BerichSAPrice;
            return CreateQuery()
               .Where(o => o.ProductID == new Guid(s) && o.RefID==new Guid(IsDefault))
               .ToSingle_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}