﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModShopEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }


        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int CityID { get; set; }

        [DataInfo]
        public int DistrictID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Url { get; set; }

        [DataInfo]
        public string Logo { get; set; }

        [DataInfo]
        public string Banner { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Address { get; set; }

        [DataInfo]
        public string Phone { get; set; }

        [DataInfo]
        public string Email { get; set; }

        [DataInfo]
        public int Vote { get; set; }

        [DataInfo]
        public double Point { get; set; }

        [DataInfo]
        public int Flower { get; set; }

        [DataInfo]
        public int Flowers { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime LogsLogin { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public string VrCode { get; set; }
        [DataInfo]
        public bool Vr { get; set; }


        #endregion Autogen by VSW

        public ModShopEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }

        public string OnlineTime
        {
            get
            {
                var _TimeSpan = DateTime.Now - this.LogsLogin;

                if ((_TimeSpan).TotalDays >= 365) return Math.Round((_TimeSpan).TotalDays / 365) + " năm trước.";
                else if ((_TimeSpan).TotalDays >= 30) return Math.Round((_TimeSpan).TotalDays / 30) + " tháng trước.";
                else if ((_TimeSpan).TotalDays >= 7) return Math.Round((_TimeSpan).TotalDays / 7) + " tuần trước.";
                else if ((_TimeSpan).TotalDays >= 1) return Math.Round((_TimeSpan).TotalDays) + " ngày trước.";
                else if ((_TimeSpan).TotalHours >= 1) return Math.Round((_TimeSpan).TotalHours) + " giờ trước.";
                else if ((_TimeSpan).TotalMinutes >= 1) return Math.Round((_TimeSpan).TotalMinutes) + " phút trước.";
                else return Math.Round((_TimeSpan).TotalSeconds) + " giây trước.";
            }
        }


        public string PublishedTime
        {
            get
            {
                var _TimeSpan = DateTime.Now - this.Published;

                if ((_TimeSpan).TotalDays >= 365) return Math.Round((_TimeSpan).TotalDays / 365) + " năm trước.";
                else if ((_TimeSpan).TotalDays >= 30) return Math.Round((_TimeSpan).TotalDays / 30) + " tháng trước.";
                else if ((_TimeSpan).TotalDays >= 7) return Math.Round((_TimeSpan).TotalDays / 7) + " tuần trước.";
                else if ((_TimeSpan).TotalDays >= 1) return Math.Round((_TimeSpan).TotalDays) + " ngày trước.";
                else if ((_TimeSpan).TotalHours >= 1) return Math.Round((_TimeSpan).TotalHours) + " giờ trước.";
                else if ((_TimeSpan).TotalMinutes >= 1) return Math.Round((_TimeSpan).TotalMinutes) + " phút trước.";
                else return Math.Round((_TimeSpan).TotalSeconds) + " giây trước.";
            }
        }

        // private int _oCountProduct = 0;
        public int CountProduct
        {
            get
            {

                return ModProductService.Instance.CreateQuery()
                               .Where(o => o.ShopID == this.ID)
                               .Select(o => o.ID)
                               .Count()
                               .ToValue()
                               .ToInt(0);

            }
        }

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && UserID > 0)
            {
                _oWebUser = ModWebUserService.Instance.GetByID_Cache(UserID);
            }
            return _oWebUser ?? new ModWebUserEntity();
        }

        private WebMenuEntity _oCity;
        public WebMenuEntity GetCity()
        {
            if (_oCity == null && CityID > 0)
            {
                _oCity = WebMenuService.Instance.GetByID_Cache(CityID);
            }
            return _oCity ?? new WebMenuEntity();
        }

        private WebMenuEntity _oDistrict;
        public WebMenuEntity GetDistrict()
        {
            if (_oDistrict == null && DistrictID > 0)
            {
                _oDistrict = WebMenuService.Instance.GetByID_Cache(DistrictID);
            }
            return _oDistrict ?? new WebMenuEntity();
        }

        public double Star
        {
            get
            {
                if (Vote == 0) return 0;
                return Math.Round(Point / Vote);
            }
        }

    }

    public class ModShopService : ServiceBase<ModShopEntity>
    {
        #region Autogen by VSW

        public ModShopService() : base("[Mod_Shop]")
        {
            DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModShopService _instance;
        public static ModShopService Instance
        {
            get { return _instance ?? (_instance = new ModShopService()); }
        }

        #endregion Autogen by VSW

        public ModShopEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModShopEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public ModShopEntity GetByUrl_Cache(string url)
        {
            return CreateQuery()
               .Where(o => o.Url == url)
               .ToSingle_Cache();
        }

        public ModShopEntity GetByUserID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.Activity == true)
               .Where(o => o.UserID == id)
               .ToSingle_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }

    }

}