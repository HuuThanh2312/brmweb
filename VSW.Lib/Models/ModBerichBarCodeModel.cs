﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModBerichBarCodeEntity : EntityBase
    {
        #region Autogen by VSW
        

        [DataInfo]
        public Guid BarCodeID { get; set; }

        [DataInfo]
        public Guid ProductID { get; set; }

        [DataInfo]
        public string BarcodeCode { get; set; }
        
        [DataInfo]
        public bool IsManufacturers { get; set; }

       

        [DataInfo]
        public int EditVersion { get; set; }
        

        #endregion Autogen by VSW

        public ModBerichBarCodeEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }

       
        
    }

    public class ModBerichBarCodeService : ServiceBase<ModBerichBarCodeEntity>
    {
        #region Autogen by VSW

        public ModBerichBarCodeService() : base("[DMBarcode]")
        {
            DBConfigKey = "DBConnection2";
            //DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModBerichBarCodeService _instance;
        public static ModBerichBarCodeService Instance
        {
            get { return _instance ?? (_instance = new ModBerichBarCodeService()); }
        }

        #endregion Autogen by VSW

        public ModBerichBarCodeEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }


        public ModBerichBarCodeEntity GetByCode_Cache(string id)
        {
            return CreateQuery()
               .Where(o => o.BarCodeID == new Guid(id))
               .ToSingle_Cache();
        }

        public ModBerichBarCodeEntity GetByProductID_Cache(Guid proid)
        {
            string pro = proid.ToString("D");
            return CreateQuery()
               .Where(o => o.ProductID == new Guid(pro))
               .ToSingle_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}