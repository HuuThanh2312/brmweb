﻿using System;
using System.Collections.Generic;

using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModCommentEntity :EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int WebUserID { get; set; }

        [DataInfo]
        public int ProductID { get; set; }

        [DataInfo]
        public int ParentID { get; set; }

        [DataInfo]
        public override string Name { get; set; }
        

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string IP { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && WebUserID > 0)
                _oWebUser = ModWebUserService.Instance.GetByID(WebUserID);

            return _oWebUser ?? (_oWebUser = new ModWebUserEntity());
        }

        private ModProductEntity _oProduct;
        public ModProductEntity GetProduct()
        {
            if (_oProduct == null && ProductID > 0)
                _oProduct = ModProductService.Instance.GetByID(ProductID);

            return _oProduct ?? (_oProduct = new ModProductEntity());
        }

        private ModCommentEntity _oParent;
        public ModCommentEntity GetParent()
        {
            if (_oParent == null && ParentID > 0)
                _oParent = ModCommentService.Instance.GetByID(ParentID);

            return _oParent ?? (_oParent = new ModCommentEntity());
        }

        public List<ModCommentEntity> GetComment()
        {
            return ModCommentService.Instance.CreateQuery()
                            .Where(o => o.ParentID == ID)
                            .OrderByDesc(o => o.ID)
                            .ToList_Cache();
        }


        public string PublishedTime
        {
            get
            {
                var _TimeSpan = DateTime.Now - this.Created;

                if ((_TimeSpan).TotalDays >= 365) return Math.Round((_TimeSpan).TotalDays / 365) + " năm trước.";
                else if ((_TimeSpan).TotalDays >= 30) return Math.Round((_TimeSpan).TotalDays / 30) + " tháng trước.";
                else if ((_TimeSpan).TotalDays >= 7) return Math.Round((_TimeSpan).TotalDays / 7) + " tuần trước.";
                else if ((_TimeSpan).TotalDays >= 1) return Math.Round((_TimeSpan).TotalDays) + " ngày trước.";
                else if ((_TimeSpan).TotalHours >= 1) return Math.Round((_TimeSpan).TotalHours) + " giờ trước.";
                else if ((_TimeSpan).TotalMinutes >= 1) return Math.Round((_TimeSpan).TotalMinutes) + " phút trước.";
                else return Math.Round((_TimeSpan).TotalSeconds) + " giây trước.";
            }
        }
    }

    public class ModCommentService : ServiceBase<ModCommentEntity>
    {
        #region Autogen by VSW

        public ModCommentService()
            : base("[Mod_Comment]")
        {

        }

        private static ModCommentService _Instance = null;
        public static ModCommentService Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new ModCommentService();

                return _Instance;
            }
        }

        #endregion

        public ModCommentEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}
