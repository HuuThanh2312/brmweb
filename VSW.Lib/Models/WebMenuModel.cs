﻿using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class WebMenuEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public string CategoryBerichID { get; set; }

        [DataInfo]
        public int LangID { get; set; }

        [DataInfo]
        public int ParentID { get; set; }

        [DataInfo]
        public int PropertyID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public string Type { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string CityCode { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by VSW

        public bool Root { get { return (ParentID == 0); } }

        private WebMenuEntity _oParent;
        public WebMenuEntity Parent
        {
            get
            {
                if (_oParent == null)
                    _oParent = WebMenuService.Instance.GetByID_Cache(ParentID);

                return _oParent ?? (_oParent = new WebMenuEntity());
            }
        }
    }

    public class WebMenuService : ServiceBase<WebMenuEntity>
    {
        #region Autogen by VSW

        public WebMenuService()
            : base("[Web_Menu]")
        {
        }

        private static WebMenuService _instance;

        public static WebMenuService Instance => _instance ?? (_instance = new WebMenuService());

        #endregion Autogen by VSW

        public WebMenuEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public WebMenuEntity GetByType_Cache(string type)
        {
            return base.CreateQuery()
               .Where(o => o.Type == type && o.ParentID==0)
               .ToSingle_Cache();
        }

        public List<WebMenuEntity> GetByParentIDType_Cache(int parentId,string type)
        {
            return base.CreateQuery()
               .Where(o => o.ParentID == parentId && o.Type==type)
               .OrderByAsc(o => new { o.Order, o.ID })
               .ToList_Cache();
        }

        public WebMenuEntity GetByCategoryID_Cache(string categoryid)
        {
            return base.CreateQuery()
               .Where(o => o.CategoryBerichID == categoryid)
               .ToSingle_Cache();
        }
        public WebMenuEntity GetByID_Cache(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public WebMenuEntity GetByCode_Cache(string code)
        {
            return base.CreateQuery()
               .Where(o => o.Code == code)
               .ToSingle_Cache();
        }

        public WebMenuEntity GetByCode_Cache(string type, string code)
        {
            return base.CreateQuery()
               .Where(o => o.Code == code && o.Type == type)
               .ToSingle_Cache();
        }

        public List<WebMenuEntity> GetByParentID_Cache(int parentId)
        {
            return base.CreateQuery()
               .Where(o => o.ParentID == parentId)
               .OrderByAsc(o => new { o.Order, o.ID })
               .ToList_Cache();
        }

        public List<WebMenuEntity> GetCity()
        {
            var root = base.CreateQuery()
                            .Where(o => o.Type == "City" && o.ParentID == 0)
                            .ToSingle_Cache();

            if (root != null)
            {
                return base.CreateQuery()
                               .Where(o => o.Type == "City" && o.ParentID == root.ID)
                               .OrderByAsc(o => new { o.Order, o.ID })
                               .ToList_Cache();
            }

            return null;
        }

        public List<WebMenuEntity> GetCity(int state)
        {
            var listItem = GetCity();

            if (listItem != null) return listItem.FindAll(o => o.State == 1);

            return null;
        }

        public string GetChildIDForCP(int menuId, int langId)
        {
            return GetChildIDForCP(string.Empty, menuId, langId);
        }

        public string GetChildIDForCP(string type, int menuId, int langId)
        {
            var list = new List<int>();

            var listWebMenu = base.CreateQuery()
                      .Where(o => o.LangID == langId)
                      .Where(type != string.Empty, o => o.Type == type)
                      .Select(o => new { o.ID, o.ParentID })
                      .ToList();

            GetChildIDForCP(ref list, listWebMenu, menuId);

            return Core.Global.Array.ToString(list.ToArray());
        }

        private void GetChildIDForCP(ref List<int> list, List<WebMenuEntity> listWebMenu, int menuId)
        {
            list.Add(menuId);

            if (listWebMenu == null)
                return;

            var listMenu = listWebMenu.FindAll(o => o.ParentID == menuId);

            for (int i = 0; i < listMenu.Count; i++)
            {
                GetChildIDForCP(ref list, listWebMenu, listMenu[i].ID);
            }
        }

        public string GetChildIDForWeb_Cache(int menuId, int langId)
        {
            return GetChildIDForWeb_Cache(string.Empty, menuId, langId);
        }

        public string GetChildIDForWeb_Cache(string type, int menuId, int langId)
        {
            string keyCache = "Lib.App.WebMenu.GetChildIDForWeb." + type + "." + menuId + "." + langId;

            string cacheValue = null;
            object obj = Core.Web.Cache.GetValue(keyCache);
            if (obj != null)
            {
                cacheValue = obj.ToString();
            }
            else
            {
                var list = new List<int>();

                var listWebMenu = base.CreateQuery()
                                    .Where(o => o.Activity == true && o.LangID == langId)
                                    .Where(type != string.Empty, o => o.Type == type)
                                    .Select(o => new { o.ID, o.ParentID })
                                    .ToList_Cache();

                GetChildIDForWeb_Cache(ref list, listWebMenu, menuId);

                cacheValue = Core.Global.Array.ToString(list.ToArray());

                Core.Web.Cache.SetValue(keyCache, cacheValue);
            }

            return cacheValue;
        }

        private void GetChildIDForWeb_Cache(ref List<int> list, List<WebMenuEntity> listWebMenu, int menuId)
        {
            list.Add(menuId);

            if (listWebMenu == null)
                return;

            var listMenu = listWebMenu.FindAll(o => o.ParentID == menuId);

            for (int i = 0; i < listMenu.Count; i++)
            {
                GetChildIDForWeb_Cache(ref list, listWebMenu, listMenu[i].ID);
            }
        }
    }
}