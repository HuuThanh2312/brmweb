﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModMaketingShopRegisterEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int ShopID { get; set; }

        [DataInfo]
        public int Percent { get; set; }

        [DataInfo]
        public int MktNewsID { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public DateTime StartDate { get; set; }

        [DataInfo]
        public DateTime EndDate { get; set; }

        #endregion Autogen by VSW
    }

    public class ModMaketingShopRegisterService : ServiceBase<ModMaketingShopRegisterEntity>
    {
        #region Autogen by VSW

        public ModMaketingShopRegisterService()
            : base("[Mod_MaketingShopR]")
        {
        }

        private static ModMaketingShopRegisterService _instance;
        public static ModMaketingShopRegisterService Instance => _instance ?? (_instance = new ModMaketingShopRegisterService());

        #endregion Autogen by VSW

        public ModMaketingShopRegisterEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}