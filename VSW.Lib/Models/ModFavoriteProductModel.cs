﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModFavoriteEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int ProductID { get; set; }


        [DataInfo]
        public DateTime Created { get; set; }

        #endregion Autogen by VSW
    }

    public class ModFavoriteService : ServiceBase<ModFavoriteEntity>
    {
        #region Autogen by VSW

        public ModFavoriteService()
            : base("[Mod_FavoriteProduct]")
        {
        }

        private static ModFavoriteService _instance;
        public static ModFavoriteService Instance => _instance ?? (_instance = new ModFavoriteService());

        #endregion Autogen by VSW

        public ModFavoriteEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}