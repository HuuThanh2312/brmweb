﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModFollowersEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int ShopID { get; set; }
        

        [DataInfo]
        public DateTime Created { get; set; }

        #endregion Autogen by VSW
    }

    public class ModFollowersService : ServiceBase<ModFollowersEntity>
    {
        #region Autogen by VSW

        public ModFollowersService()
            : base("[Mod_Followers]")
        {
        }

        private static ModFollowersService _instance;
        public static ModFollowersService Instance => _instance ?? (_instance = new ModFollowersService());

        #endregion Autogen by VSW

        public ModFollowersEntity GetByID(int id)
        {
            return base.CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }
    }
}