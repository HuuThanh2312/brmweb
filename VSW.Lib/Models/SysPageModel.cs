﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Interface;
using VSW.Core.Models;
using VSW.Core.MVC;
using VSW.Core.Web;

namespace VSW.Lib.Models
{
    public class SysPageEntity : EntityBase, IPageInterface
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int TemplateID { get; set; }

        [DataInfo]
        public int TemplateMobileID { get; set; }

        [DataInfo]
        public int TemplateTabletID { get; set; }

        [DataInfo]
        public string ModuleCode { get; set; }

        [DataInfo]
        public int LangID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int BrandID { get; set; }

        [DataInfo]
        public int ParentID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string Icon { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Faicon { get; set; }

        //[DataInfo]
        //public string BannerBottom { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string TopContent { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public string LinkTitle { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by VSW

        #region SEO

        private bool _pageState = true;
        public bool PageState
        {
            get
            {
                return _pageState;
            }
            set
            {
                _pageState = value;
            }
        }

        private string _pageUrl = string.Empty;
        public string PageURL
        {
            get
            {
                return _pageUrl;
            }
            set
            {
                _pageUrl = value;
            }
        }

        private string _pageFile = string.Empty;
        public string PageFile
        {
            get
            {
                if (_pageFile.IndexOf("base64", StringComparison.OrdinalIgnoreCase) > -1)
                    return string.Empty;

                return _pageFile;
            }
            set
            {
                _pageFile = value;
            }
        }

        #endregion SEO

        public bool HasEnd { get; set; }

        public bool Root => (ParentID == 0);

        public bool End => Items.GetValue("End").ToBool();

        private SysPageEntity _oParent;
        public SysPageEntity Parent
        {
            get
            {
                if (_oParent == null)
                    _oParent = SysPageService.Instance.GetByID_Cache(ParentID);

                return _oParent ?? (_oParent = new SysPageEntity());
            }
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity Menu
        {
            get
            {
                if (_oMenu == null && MenuID > 0)
                    _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

                return _oMenu ?? (_oMenu = new WebMenuEntity());
            }
        }

        private List<WebPropertyEntity> _oGetBrand;
        public List<WebPropertyEntity> GetBrand
        {
            get
            {
                if (_oGetBrand == null)
                {
                    var listProperty = WebPropertyService.Instance.GetByParent_Cache(Menu.PropertyID);
                    if (listProperty != null)
                        _oGetBrand = WebPropertyService.Instance.GetByParent_Cache(listProperty[0].ID);
                }

                return _oGetBrand ?? (_oGetBrand = new List<WebPropertyEntity>());
            }
        }

        public long Count
        {
            get
            {
                return ModProductService.Instance.CreateQuery()
                                    .Select(o => o.ID)
                                    .Where(o => o.Activity == true)
                                    .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, LangID))
                                    .Count()
                                    .ToValue_Cache()
                                    .ToLong(0);
            }
        }

        public long CountByShop(int webUserID)
        {
            return ModProductService.Instance.CreateQuery()
                                     .Select(o => o.ID)
                                     .Where(o => o.Activity == true)
                                     .WhereIn(o => o.MenuID, WebMenuService.Instance.GetChildIDForWeb_Cache("Product", MenuID, LangID))
                                     .Count()
                                     .ToValue_Cache()
                                     .ToLong(0);
        }
    }

    public class SysPageService : ServiceBase<SysPageEntity>, IPageServiceInterface
    {
        #region Autogen by VSW

        public SysPageService()
            : base("[Sys_Page]")
        {
        }

        private static SysPageService _instance;

        public static SysPageService Instance => _instance ?? (_instance = new SysPageService());

        #endregion Autogen by VSW

        public SysPageEntity GetByID(int id)
        {
            return CreateQuery().Where(o => o.ID == id).ToSingle();
        }

        public SysPageEntity GetByID_Cache(int id)
        {
            if (GetAll_Cache() == null) return null;

            return GetAll_Cache().Find(o => o.ID == id);
        }

        public List<SysPageEntity> GetByParent_Cache(int parentID)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID);

            if (list == null) return null;

            list.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return list;
        }

        public List<SysPageEntity> GetByParent_Cache(SysPageEntity page, bool brand)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == page.ID);
            if (list.Count < 1)
                list = GetAll_Cache().FindAll(o => o.ParentID == page.ParentID);

            if (brand)
                list = list.FindAll(o => o.BrandID > 0);
            else
                list = list.FindAll(o => o.BrandID < 1);

            list.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return list;
        }

        public List<SysPageEntity> GetByGrandParent_Cache(int parentID)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID);

            if (list == null) return null;

            List<SysPageEntity> listGrand = new List<SysPageEntity>();
            for (int i = 0; i < list.Count; i++)
            {
                var temp = GetAll_Cache().FindAll(o => o.ParentID == list[i].ID);
                if (temp != null) listGrand.AddRange(temp);
            }

            if (listGrand.Count > 0)
                listGrand.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return listGrand;
        }

        public List<SysPageEntity> GetByParent_Cache(int parentID, string module)
        {
            if (GetAll_Cache() == null) return null;

            var list = GetAll_Cache().FindAll(o => o.ParentID == parentID && o.ModuleCode == module);

            if (list.Count == 0) return null;

            list.Sort((o1, o2) => o1.Order.CompareTo(o2.Order));

            return list;
        }

        public string GetMapCode_Cache(SysPageEntity page)
        {
            var keyCache = Cache.CreateKey(page.ID.ToString());

            string mapCode;
            var obj = Cache.GetValue(keyCache);
            if (obj != null)
            {
                mapCode = obj.ToString();
            }
            else
            {
                var tempPage = page;

                mapCode = tempPage.Code;
                while (tempPage.ParentID > 0)
                {
                    var parentId = tempPage.ParentID;

                    tempPage = CreateQuery()
                           .Where(o => o.ID == parentId)
                           .ToSingle_Cache();

                    if (tempPage == null || tempPage.Root)
                        break;

                    mapCode = tempPage.Code + "/" + mapCode;
                }

                Cache.SetValue(keyCache, mapCode);
            }

            return mapCode;
        }

        private List<SysPageEntity> GetAll_Cache()
        {
            return CreateQuery().Where(o => o.Activity == true).ToList_Cache();
        }

        #region IPageServiceInterface Members

        public IPageInterface VSW_Core_GetByID(int id)
        {
            return GetAll_Cache().Find(o => o.ID == id);
        }

        public IPageInterface VSW_Core_CurrentPage(ViewPage viewPage)
        {
            var code = viewPage.CurrentVQS.GetString(0);

            if (code == string.Empty || code.Length > 260) return null;



            if (code.IndexOf("shop", StringComparison.OrdinalIgnoreCase) > -1)
            {

                string _shopUrl = HttpRequest.RawUrl.Replace(Setting.Sys_PageExt, "");
                var arrCode = Regex.Split(_shopUrl, "/");
                if (arrCode.Length == 3)
                {
                    //  int pageindex = 0;
                    code = arrCode[2];
                    if (code.IndexOf("?", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        string[] arrBegin = code.Split('?');
                        code = arrBegin[0];


                    }
                    var item = ModShopService.Instance.CreateQuery()
                                                           .Where(o => o.Activity == true && o.Url == code)
                                                           .ToSingle_Cache();

                    if (item != null)
                    {
                        viewPage.ViewBag.Shop = item;
                        return base.CreateQuery().Where(o => o.Activity == true)
                                                 .Where(o => o.ModuleCode == "MShop")
                                                 .ToSingle_Cache();
                    }
                    else
                        return null;
                }
            }

            if (code.ToLower() == "ajax" || code.ToLower().StartsWith("sitemap") || code.ToLower() == "rss")
            {
                var template = SysTemplateService.Instance.CreateQuery()
                                            .Select(o => o.ID)
                                            .ToSingle_Cache();

                if (template == null) return null;

                viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, 1);

                if (code.ToLower().StartsWith("sitemap"))
                {
                    return new SysPageEntity
                    {
                        TemplateID = template.ID,
                        TemplateMobileID = template.ID,
                        Name = "Sitemap",
                        Code = "sitemap",
                        ModuleCode = "MSitemap",
                        Activity = true
                    };
                }

                switch (code.ToLower())
                {
                    case "ajax":
                        return new SysPageEntity
                        {
                            TemplateID = template.ID,
                            TemplateMobileID = template.ID,
                            Name = "Ajax",
                            Code = "ajax",
                            ModuleCode = "MAjax",
                            Activity = true
                        };

                    case "rss":
                        return new SysPageEntity
                        {
                            TemplateID = template.ID,
                            TemplateMobileID = template.ID,
                            Name = "Đọc tin RSS",
                            Code = "rss",
                            ModuleCode = "MRss",
                            Activity = true
                        };
                }
            }

            var cleanUrl = ModCleanURLService.Instance.GetByCode(code, viewPage.CurrentLang.ID);
            if (cleanUrl == null)
            {
                cleanUrl = ModCleanURLService.Instance.CreateQuery().Where(o => o.Code.StartsWith(code) && o.LangID == viewPage.CurrentLang.ID).ToSingle();
                if (cleanUrl != null)
                    HttpRequest.Redirect301(viewPage.GetURL(cleanUrl.Code));

                return null;
            }

            viewPage.ViewBag.CleanURL = cleanUrl;

            if (cleanUrl.Type == "Page")
            {
                viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, 1);
                return GetByID_Cache(cleanUrl.Value);
            }

            //viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, 0);
            viewPage.CurrentVQSMVC.Trunc(viewPage.CurrentVQS, viewPage.CurrentVQS.Count - 1);

            SysPageEntity page = null;

            var menuId = cleanUrl.MenuID;
            while (menuId > 0)
            {
                page = base.CreateQuery().Where(o => o.MenuID == menuId && o.Activity == true).ToSingle_Cache();

                if (page != null) break;

                var menu = WebMenuService.Instance.GetByID_Cache(menuId);

                if (menu == null || menu.ParentID == 0) break;

                menuId = menu.ParentID;
            }

            return page;
        }

        public void VSW_Core_CPSave(IPageInterface item)
        {
            Save(item as SysPageEntity);
        }

        #endregion IPageServiceInterface Members
    }
}