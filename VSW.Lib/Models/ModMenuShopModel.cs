﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModMenuShopEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int ShopID { get; set; }

        [DataInfo]
        public int ParentID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int PropertyID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

       


        #endregion Autogen by VSW

        public ModMenuShopEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }
       // private int _oCountProduct = 0;
        public int CountProduct
        {
            get
            {

                return ModProductService.Instance.CreateQuery()
                               .Where(o => o.MenuID == this.MenuID && o.ShopID==this.ShopID)
                               .Select(o => o.ID)
                               .Count()
                               .ToValue()
                               .ToInt(0);

            }
        }

        private ModShopEntity _oShop;
        public ModShopEntity GetShop()
        {
            if (_oShop == null && ShopID > 0)
            {
                _oShop = ModShopService.Instance.GetByID_Cache(ShopID);
            }
            return _oShop ?? new ModShopEntity();
        }

       

    }

    public class ModMenuShopService : ServiceBase<ModMenuShopEntity>
    {
        #region Autogen by VSW

        public ModMenuShopService() : base("[Mod_MenuShop]")
        {
            DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModMenuShopService _instance;
        public static ModMenuShopService Instance
        {
            get { return _instance ?? (_instance = new ModMenuShopService()); }
        }

        #endregion Autogen by VSW

        public ModMenuShopEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModMenuShopEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }
        

        public bool Exists(int shopid, int menuID)
        {
            return CreateQuery()
                           .Where(o=>o.MenuID==menuID && o.ShopID==shopid)
                           .Count()
                           .ToValue()
                           .ToBool();
        }

    }

}