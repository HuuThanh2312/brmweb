﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModNewsEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int MenuID { get; set; }

        [DataInfo]
        public int State { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public long View { get; set; }

        [DataInfo]
        public string Custom { get; set; }

        [DataInfo]
        public string PageTitle { get; set; }

        [DataInfo]
        public string PageDescription { get; set; }

        [DataInfo]
        public string PageKeywords { get; set; }

        [DataInfo]
        public DateTime Published { get; set; }

        [DataInfo]
        public DateTime Updated { get; set; }

       

        [DataInfo]
        public int Order { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        #endregion Autogen by VSW


        public string PublishedTime
        {
            get
            {
                var _TimeSpan = DateTime.Now - this.Published;

                if ((_TimeSpan).TotalDays >= 365) return Math.Round((_TimeSpan).TotalDays / 365) + " năm trước.";
                else if ((_TimeSpan).TotalDays >= 30) return Math.Round((_TimeSpan).TotalDays / 30) + " tháng trước.";
                else if ((_TimeSpan).TotalDays >= 7) return Math.Round((_TimeSpan).TotalDays / 7) + " tuần trước.";
                else if ((_TimeSpan).TotalDays >= 1) return Math.Round((_TimeSpan).TotalDays) + " ngày trước.";
                else if ((_TimeSpan).TotalHours >= 1) return Math.Round((_TimeSpan).TotalHours) + " giờ trước.";
                else if ((_TimeSpan).TotalMinutes >= 1) return Math.Round((_TimeSpan).TotalMinutes) + " phút trước.";
                else return Math.Round((_TimeSpan).TotalSeconds) + " giây trước.";
            }
        }

        public string TimeFormat
        {
            get
            {
                if (Published > DateTime.MinValue) return string.Format("{0:dd/MM/yyyy HH:mm}", this.Published);
                else return string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
            }
        }


        public void UpView()
        {
            View++;
            ModNewsService.Instance.Save(this, o => o.View);
        }

        private WebMenuEntity _oMenu;
        public WebMenuEntity GetMenu()
        {
            if (_oMenu == null && MenuID > 0)
                _oMenu = WebMenuService.Instance.GetByID_Cache(MenuID);

            return _oMenu ?? (_oMenu = new WebMenuEntity());
        }
    }

    public class ModNewsService : ServiceBase<ModNewsEntity>
    {
        #region Autogen by VSW

        public ModNewsService()
            : base("[Mod_News]")
        {
        }

        private static ModNewsService _instance;
        public static ModNewsService Instance => _instance ?? (_instance = new ModNewsService());

        #endregion Autogen by VSW

        public ModNewsEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}