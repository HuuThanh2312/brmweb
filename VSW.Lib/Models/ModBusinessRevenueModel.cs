﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModBusinessRevenueEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int ShopID { get; set; }

        [DataInfo]
        public int OrderDetailID { get; set; }

        [DataInfo]
        public int OrderID { get; set; }
        [DataInfo]
        public int ProductID { get; set; }

        [DataInfo]
        public double Revenue { get; set; }//Tổng doanh thu

        [DataInfo]
        public override string Name { get; set; }


        [DataInfo]
        public DateTime Published { get; set; }
        

        #endregion Autogen by VSW

        public string PublishedTime
        {
            get
            {
                var _TimeSpan = DateTime.Now - this.Published;

                if ((_TimeSpan).TotalDays >= 365) return Math.Round((_TimeSpan).TotalDays / 365) + " năm trước.";
                else if ((_TimeSpan).TotalDays >= 30) return Math.Round((_TimeSpan).TotalDays / 30) + " tháng trước.";
                else if ((_TimeSpan).TotalDays >= 7) return Math.Round((_TimeSpan).TotalDays / 7) + " tuần trước.";
                else if ((_TimeSpan).TotalDays >= 1) return Math.Round((_TimeSpan).TotalDays) + " ngày trước.";
                else if ((_TimeSpan).TotalHours >= 1) return Math.Round((_TimeSpan).TotalHours) + " giờ trước.";
                else if ((_TimeSpan).TotalMinutes >= 1) return Math.Round((_TimeSpan).TotalMinutes) + " phút trước.";
                else return Math.Round((_TimeSpan).TotalSeconds) + " giây trước.";
            }
        }

        public string TimeFormat
        {
            get
            {
                if (Published > DateTime.MinValue) return string.Format("{0:dd/MM/yyyy HH:mm}", this.Published);
                else return string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now);
            }
        }


        private ModShopEntity _oShop;
        public ModShopEntity GetShop()
        {
            if (_oShop == null && ShopID > 0)
                _oShop = ModShopService.Instance.GetByID_Cache(ShopID);

            return _oShop;
        }
    }

    public class ModBusinessRevenueService : ServiceBase<ModBusinessRevenueEntity>
    {
        #region Autogen by VSW

        public ModBusinessRevenueService()
            : base("[Mod_BusinessRevenue]")
        {
        }

        private static ModBusinessRevenueService _instance;
        public static ModBusinessRevenueService Instance => _instance ?? (_instance = new ModBusinessRevenueService());

        #endregion Autogen by VSW

        public ModBusinessRevenueEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModBusinessRevenueEntity GetByShopID(int id)
        {
            return CreateQuery()
               .Where(o => o.ShopID == id)
               .ToSingle();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }

    }
}