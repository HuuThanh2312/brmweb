﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModBankingUserEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public double TotalAmount { get; set; }//Số dư hiện tại

        [DataInfo]
        public double TotalAvailable { get; set; }//số dư khả dụng
        [DataInfo]
        public int UserID { get; set; }

        //[DataInfo]
        //public int CityID { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string NameBank { get; set; }

        [DataInfo]
        public string AccountCode { get; set; }

        [DataInfo]
        public string CardCode { get; set; }

        //[DataInfo]
        //public string FileBack { get; set; }

        //[DataInfo]
        //public string FileFont { get; set; }

        [DataInfo]
        public string AddressBank { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }


        [DataInfo]
        public DateTime Update { get; set; }

        #endregion Autogen by VSW

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && UserID > 0)
                _oWebUser = ModWebUserService.Instance.GetByID(UserID);

            return _oWebUser ?? (_oWebUser = new ModWebUserEntity());
        }

        //private WebMenuEntity _oCity;
        //public WebMenuEntity GetCity()
        //{
        //    if (_oCity == null && CityID > 0)
        //        _oCity = WebMenuService.Instance.GetByID_Cache(CityID);

        //    return _oCity ?? (_oCity = new WebMenuEntity());
        //}

    }

    public class ModBankingUserService : ServiceBase<ModBankingUserEntity>
    {
        #region Autogen by VSW

        public ModBankingUserService() : base("[Mod_BankingUser]") { }

        private static ModBankingUserService _instance;
        public static ModBankingUserService Instance => _instance ?? (_instance = new ModBankingUserService());

        #endregion Autogen by VSW

        public ModBankingUserEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public List<ModBankingUserEntity> GetListByUserID(int uid)
        {
            return CreateQuery()
               .Where(o => o.UserID == uid)
               .ToList_Cache ();
        }
        public ModBankingUserEntity GetByUserID(int uid)
        {
            return CreateQuery()
               .Where(o => o.UserID == uid)
               .ToSingle();
        }

        public ModBankingUserEntity GetByUserID_Default(int uid)
        {
            return CreateQuery()
               .Where(o => o.Activity == true)
               .Where(o => o.UserID == uid)
               .ToSingle_Cache();
        }
    }
}