﻿using System;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModBerichSAPriceEntity : EntityBase
    {
        #region Autogen by VSW
        [DataInfo]
        public Guid RefID { get; set; }

        [DataInfo]
        public int RefType { get; set; }

        [DataInfo]
        public string RefNo { get; set; }

        [DataInfo]
        public string ApplyBranch { get; set; }


        #endregion Autogen by VSW

        public ModBerichSAPriceEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }



    }

    public class ModBerichSAPriceService : ServiceBase<ModBerichSAPriceEntity>
    {
        #region Autogen by VSW

        public ModBerichSAPriceService() : base("[SAPrice]")
        {
            DBConfigKey = "DBConnection2";
            //DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModBerichSAPriceService _instance;
        public static ModBerichSAPriceService Instance
        {
            get { return _instance ?? (_instance = new ModBerichSAPriceService()); }
        }

        #endregion Autogen by VSW

        public ModBerichSAPriceEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }
    }
}