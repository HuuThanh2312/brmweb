﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModAccBcoinCardEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public double TotalAmount { get; set; }//Số dư hiện tại

        //[DataInfo]
        //public double TotalAvailable { get; set; }//số dư khả dụng
        [DataInfo]
        public int UserID { get; set; }
        
        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string AccountBcoin { get; set; }

        [DataInfo]
        public string CardCode { get; set; }

        [DataInfo]
        public string Password { get; set; }

        [DataInfo]
        public string Summary { get; set; }

        [DataInfo]
        public bool Activity { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }


        [DataInfo]
        public DateTime Update { get; set; }

        #endregion Autogen by VSW

        private ModWebUserEntity _oWebUser;
        public ModWebUserEntity GetWebUser()
        {
            if (_oWebUser == null && UserID > 0)
                _oWebUser = ModWebUserService.Instance.GetByID(UserID);

            return _oWebUser ?? (_oWebUser = new ModWebUserEntity());
        }

       

    }

    public class ModAccBcoinCardService : ServiceBase<ModAccBcoinCardEntity>
    {
        #region Autogen by VSW

        public ModAccBcoinCardService() : base("[Mod_AccBcoinCard]") { }

        private static ModAccBcoinCardService _instance;
        public static ModAccBcoinCardService Instance => _instance ?? (_instance = new ModAccBcoinCardService());

        #endregion Autogen by VSW

        public ModAccBcoinCardEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public List<ModAccBcoinCardEntity> GetListByUserID(int uid)
        {
            return CreateQuery()
               .Where(o => o.UserID == uid)
               .ToList_Cache ();
        }
        public ModAccBcoinCardEntity GetByUserID(int uid)
        {
            return CreateQuery()
               .Where(o => o.UserID == uid)
               .ToSingle();
        }

        public ModAccBcoinCardEntity GetByUserID_Default(int uid)
        {
            return CreateQuery()
               .Where(o => o.Activity == true)
               .Where(o => o.UserID == uid)
               .ToSingle_Cache();
        }
    }
}