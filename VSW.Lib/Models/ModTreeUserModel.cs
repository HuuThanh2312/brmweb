﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModTreeUserEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int ParentUserID { get; set; }

        [DataInfo]
        public int Level { get; set; }


        [DataInfo]
        public DateTime Published { get; set; }
        

        #endregion Autogen by VSW

        public ModTreeUserEntity()
        {
            Items.SetValue("IsName", true);
            Items.SetValue("IsSummary", true);
        }

       
    }

    public class ModTreeUserService : ServiceBase<ModTreeUserEntity>
    {
        #region Autogen by VSW

        public ModTreeUserService() : base("[Mod_TreeUser]")
        {
            DBExecuteMode = DBExecuteType.DataReader;
        }

        private static ModTreeUserService _instance;
        public static ModTreeUserService Instance
        {
            get { return _instance ?? (_instance = new ModTreeUserService()); }
        }

        #endregion Autogen by VSW

        public ModTreeUserEntity GetByID(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle();
        }

        public ModTreeUserEntity GetByID_Cache(int id)
        {
            return CreateQuery()
               .Where(o => o.ID == id)
               .ToSingle_Cache();
        }

        public List<ModTreeUserEntity> GetByParentID_Cache(int parentid)
        {
            return CreateQuery()
               .Where(o => o.ParentUserID == parentid)
               .ToList_Cache();
        }

        public bool Exists(string query)
        {
            return CreateQuery()
                           .Where(query)
                           .Count()
                           .ToValue()
                           .ToBool();
        }

    }

}