﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModProductBerichFileEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public string ProductID { get; set; }

        [DataInfo]
        public string File { get; set; }

        [DataInfo]
        public string Files { get; set; }

        [DataInfo]
        public string CategoryID { get; set; }

        [DataInfo]
        public string Content { get; set; }
        

        #endregion Autogen by VSW

        private ModBerichProductEntity _oProduct;
        public ModBerichProductEntity GetProduct()
        {
            if (_oProduct == null && !string.IsNullOrEmpty(ProductID))
            {
                Guid ProductIDGuid = new Guid(ProductID);
                _oProduct = ModBerichProductService.Instance.GetByProductID_Cache(ProductIDGuid);
            }
            return _oProduct;
        }

        private List<string> _oGetFiles;
        public List<string> GetFiles()
        {
            if (_oGetFiles == null && !string.IsNullOrEmpty(Files))
            {
                var _ArrFile = Files.Replace("\r", "").Split('\n');
                _oGetFiles = new List<string>(_ArrFile);
            }

            return _oGetFiles ?? (_oGetFiles = new List<string>());
        }
    }

    public class ModProductBerichFileService : ServiceBase<ModProductBerichFileEntity>
    {
        #region Autogen by VSW

        public ModProductBerichFileService() : base("[Mod_ProductBerichFile]") { }

        private static ModProductBerichFileService _instance;
        public static ModProductBerichFileService Instance => _instance ?? (_instance = new ModProductBerichFileService());

        #endregion Autogen by VSW

        public ModProductBerichFileEntity GetByID(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle();
        }

        public ModProductBerichFileEntity GetByID_Cache(int id)
        {
            return CreateQuery()
                .Where(o => o.ID == id)
                .ToSingle_Cache();
        }

        public ModProductBerichFileEntity GetByProductID_Cache(string productId)
        {

            return CreateQuery()
                .Where(o => o.ProductID == productId)
                .ToSingle_Cache();
        }
    }
}