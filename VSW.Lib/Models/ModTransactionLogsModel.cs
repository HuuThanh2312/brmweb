﻿using System;
using System.Collections.Generic;
using VSW.Core.Models;

namespace VSW.Lib.Models
{
    public class ModTransactionLogsEntity : EntityBase
    {
        #region Autogen by VSW

        [DataInfo]
        public override int ID { get; set; }

        [DataInfo]
        public int UserID { get; set; }

        [DataInfo]
        public int BankID { get; set; }

        [DataInfo]
        public string Code { get; set; }

        [DataInfo]
        public override string Name { get; set; }

        [DataInfo]
        public string Content { get; set; }

        [DataInfo]
        public bool Status { get; set; }

        [DataInfo]
        public DateTime Created { get; set; }

        [DataInfo]
        public DateTime Update { get; set; }

        [DataInfo]
        public double Price { get; set; }

        #endregion Autogen by VSW

        
    }

    public class ModTransactionLogsService : ServiceBase<ModTransactionLogsEntity>
    {
        #region Autogen by VSW

        private ModTransactionLogsService()
            : base("[Mod_TransactionLogs]")
        {
        }

        private static ModTransactionLogsService _instance;
        public static ModTransactionLogsService Instance => _instance ?? (_instance = new ModTransactionLogsService());

        #endregion Autogen by VSW

        public ModTransactionLogsEntity GetByID(int id)
        {
            return base.CreateQuery()
                   .Where(o => o.ID == id)
                   .ToSingle();
        }

        public ModTransactionLogsEntity GetByID_Cache(int id)
        {
            return base.CreateQuery()
                   .Where(o => o.ID == id)
                   .ToSingle_Cache();
        }

        public ModTransactionLogsEntity GetByCode_Cache(string code)
        {
            return base.CreateQuery()
                   .Where(o => o.Code == code)
                   .ToSingle_Cache();
        }


        public List<ModTransactionLogsEntity> GetListByUserID_Cache(int uid)
        {
            return base.CreateQuery()
                   .Where(o => o.UserID == uid)
                   .ToList_Cache();
        }


        public int GetCount()
        {
            return base.CreateQuery()
                .Select(o => o.ID)
                .Count()
                .ToValue_Cache()
                .ToInt(0);
        }
        
    }
}