﻿using System;
using VSW.Core.Design;
using VSW.Core.Web;
using VSW.Lib.Global;
using VSW.Lib.Models;
using File = System.IO.File;

namespace VSW.Lib.Design
{
    public class EditTemplate : ViewPageDesign
    {
        public EditTemplate()
        {
            PageService = SysPageService.Instance;
            TemplateService = SysTemplateService.Instance;
            ModuleService = SysModuleService.Instance;

            var recordId = HttpQueryString.GetValue("id").ToInt();

            if (recordId > 0)
                CurrentTemplate = SysTemplateService.Instance.GetByID(recordId);
        }

        protected override void OnPreInit(EventArgs e)
        {
            if (CurrentTemplate == null)
            {
                Response.End();
                return;
            }

            if (CPLogin.CurrentUser == null || !CPLogin.CurrentUser.IsAdministrator)
            {
                Response.End();
                return;
            }

            var masterPageFile = "~/Views/Design/" + CurrentTemplate.File;
            if (!File.Exists(Server.MapPath(masterPageFile)))
            {
                Response.End();
                return;
            }

            MasterPageFile = masterPageFile;

            base.OnPreInit(e);
        }
    }
}