﻿using System;
using VSW.Core.Interface;
using VSW.Lib.Global;
using VSW.Lib.Models;

namespace VSW.Lib.MVC
{
    public class ViewPage : Core.MVC.ViewPage
    {
        public ViewPage()
        {
            LangService = SysLangService.Instance;
            ModuleService = SysModuleService.Instance;
            SiteService = SysSiteService.Instance;
            TemplateService = SysTemplateService.Instance;
            PageService = SysPageService.Instance;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ResourceService = new IniSqlResourceService(CurrentLang);

            //khong thi bo di nhe
            //Lib.Global.Utils.UpdateOnline();
        }

        protected override IPageInterface PageNotFound()
        {
            Error404();

            return null;
        }

        public void Error404()
        {
            Core.Web.HttpRequest.Error404();
        }

        public new SysSiteEntity CurrentSite => base.CurrentSite as SysSiteEntity;
        public new SysTemplateEntity CurrentTemplate => base.CurrentTemplate as SysTemplateEntity;
        public new SysPageEntity CurrentPage => base.CurrentPage as SysPageEntity;
        public new SysLangEntity CurrentLang => base.CurrentLang as SysLangEntity;

        public ModCleanURLEntity CurrentCleanUrl => ViewBag.CleanURL as ModCleanURLEntity;

        private string _currentUrl;
        public string CurrentURL => _currentUrl ?? (_currentUrl = GetPageURL(CurrentPage));

        //Module

        #region UrlBuniess vs user
        private string _buninesskUrl;
        public string BuninesskUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninesskUrl)) return _buninesskUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessProduct" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _buninesskUrl = GetURL(item.Code);

                return _buninesskUrl;
            }
        }

        private string _buninessMNGUrl;
        public string BuninessMNGUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessMNGUrl)) return _buninessMNGUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessProductMng" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _buninessMNGUrl = GetURL(item.Code);

                return _buninessMNGUrl;
            }
        }

        private string _registerbuninessUrl;
        public string RegisterBuninessUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_registerbuninessUrl)) return _registerbuninessUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MRegisterShop" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _registerbuninessUrl = GetURL(item.Code);

                return _registerbuninessUrl;
            }
        }

        private string _treeUserUrl;
        public string TreeUserUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_treeUserUrl)) return _treeUserUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MTreeMembers" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _treeUserUrl = GetURL(item.Code);

                return _treeUserUrl;
            }
        }

        private string _buninessSettingCpUrl;
        public string BuninessSettingCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessSettingCpUrl)) return _buninessSettingCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessSettingCP" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessSettingCpUrl = GetURL(item.Code);

                return _buninessSettingCpUrl;
            }
        }
        private string _buninessOrderCpUrl;
        public string BuninessOrderCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessOrderCpUrl)) return _buninessOrderCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessOrder" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessOrderCpUrl = GetURL(item.Code);

                return _buninessOrderCpUrl;
            }
        }

        private string _buninessReviewCpUrl;
        public string BuninessReviewCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessReviewCpUrl)) return _buninessReviewCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessReview" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessReviewCpUrl = GetURL(item.Code);

                return _buninessReviewCpUrl;
            }
        }


        private string _buninessMarketingCpUrl;
        public string BuninessMarketingCpUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_buninessMarketingCpUrl)) return _buninessMarketingCpUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessMaketing" && o.LangID == CurrentLang.ID)
                    .OrderByDesc(o => new { o.Order, o.ID })
                    .ToSingle_Cache();

                if (item != null) _buninessMarketingCpUrl = GetURL(item.Code);

                return _buninessMarketingCpUrl;
            }
        }

        #endregion

        private string _feedbackUrl;
        public string FeedbackUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_feedbackUrl)) return _feedbackUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MFeedback" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _feedbackUrl = GetURL(item.Code);

                return _feedbackUrl;
            }
        }

        private string _searchUrl;
        public string SearchUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_searchUrl)) return _searchUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MSearch" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _searchUrl = GetURL(item.Code);

                return _searchUrl;
            }
        }

        private string _favoriteUrl;
        public string FavoriteUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_favoriteUrl)) return _favoriteUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MFavorite" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _favoriteUrl = GetURL(item.Code);

                return _favoriteUrl;
            }
        }

        private string _viewCartUrl;
        public string ViewCartUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_viewCartUrl)) return _viewCartUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MViewCart" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _viewCartUrl = GetURL(item.Code);

                return _viewCartUrl;
            }
        }

        private string _AddCartUrl;
        public string AddCartUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_AddCartUrl)) return _AddCartUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MViewCart" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _AddCartUrl = GetURL(item.Code);

                return _AddCartUrl.Replace(Setting.Sys_PageExt, "/Add" + Setting.Sys_PageExt);
            }
        }

        private string _checkOutUrl;
        public string CheckOutUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_checkOutUrl)) return _checkOutUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MCheckOut" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _checkOutUrl = GetURL(item.Code);

                return _checkOutUrl;
            }
        }

        private string _completeUrl;
        public string CompleteUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_completeUrl)) return _completeUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MComplete" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _completeUrl = GetURL(item.Code);

                return _completeUrl;
            }
        }
        private string _PayAtm = "";
        public string PayAtmURL
        {
            get
            {
                if (string.IsNullOrEmpty(_PayAtm))
                {
                    var item = SysPageService.Instance.CreateQuery()
                                        .Where(o => o.Activity == true && o.ModuleCode == "MATMPay" && o.LangID == CurrentLang.ID)
                                        .ToSingle_Cache();

                    if (item != null) _PayAtm = item.Code;
                }

                return GetURL(_PayAtm);
            }
        }

        


        private string _loginUrl;
        public string LoginUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_loginUrl)) return _loginUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MLogin" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _loginUrl = GetURL(item.Code);

                return _loginUrl;
            }
        }

        private string _registerUrl;
        public string RegisterUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_registerUrl)) return _registerUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MRegister" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _registerUrl = GetURL(item.Code);

                return _registerUrl;
            }
        }



        private string _forgotUrl;
        public string ForgotUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_forgotUrl)) return _forgotUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MForgot" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _forgotUrl = GetURL(item.Code);

                return _forgotUrl;
            }
        }

        #region user

        private string _oBankUserCPUrl;
        public string BankUserCPUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_oBankUserCPUrl)) return _oBankUserCPUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MCardBank" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _oBankUserCPUrl = GetURL(item.Code);

                return _oBankUserCPUrl;
            }
        }

        private string _oWebUserCPUrl;
        public string WebUserCPUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_oWebUserCPUrl)) return _oWebUserCPUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MWebUserCP" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _oWebUserCPUrl = GetURL(item.Code);

                return _oWebUserCPUrl;
            }
        }

        private string _oWebUserCPUpdateUrl;
        public string WebUserCPUpdateUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_oWebUserCPUpdateUrl)) return _oWebUserCPUpdateUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MWebUserCPUpdate" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _oWebUserCPUpdateUrl = GetURL(item.Code);

                return _oWebUserCPUpdateUrl;
            }
        }

        private string _logoutUrl;
        public string LogoutUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_logoutUrl)) return _logoutUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MWebUserCP" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _logoutUrl = GetURL(item.Code, "Logout", "");

                return _logoutUrl;
            }
        }

        private string _ProductCPUrl;
        public string ProductCPUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_ProductCPUrl)) return _ProductCPUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MBusinessProductMng" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _ProductCPUrl = GetURL(item.Code);

                return _ProductCPUrl;
            }
        }

        private string _ProductUPUrl;
        public string ProductUPUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_ProductUPUrl)) return _ProductUPUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MProductUP" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _ProductUPUrl = GetURL(item.Code);

                return _ProductUPUrl;
            }
        }

        private string _ProductDELUrl;
        public string ProductDELUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_ProductDELUrl)) return _ProductDELUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MProductUP" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _ProductDELUrl = GetURL(item.Code, "Delete", "");

                return _ProductDELUrl;
            }
        }

        private string _MngOrderUrl;
        public string MngOrderUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(_MngOrderUrl)) return _MngOrderUrl;

                var item = SysPageService.Instance.CreateQuery()
                    .Select(o => o.Code)
                    .Where(o => o.Activity == true && o.ModuleCode == "MMngOrder" && o.LangID == CurrentLang.ID)
                    .ToSingle_Cache();

                if (item != null) _MngOrderUrl = GetURL(item.Code);

                return _MngOrderUrl;
            }
        }

        #endregion

        public Message Message { get; } = new Message();

        public string GetURL(int MenuID, string Code)
        {
            return GetURL(Code);
        }

        public string GetURL(ModShopEntity shop)
        {
            return GetURL("shop/" + shop.Url);
        }

        public string GetPageURL(SysPageEntity page)
        {
            var typeValue = page.Items.GetValue("Type").ToString();

            if (typeValue == string.Empty)
            {
                if (ViewBag.WebUser != null)
                    return GetURL("user--" + ViewBag.WebUser.Username);

                if (ViewBag.City != null)
                    return GetURL(page.Code + "--" + ViewBag.City.Code);

                return GetURL(page.Code);
            }

            if (!typeValue.Equals("http", StringComparison.OrdinalIgnoreCase)) return "#";

            var target = page.Items.GetValue("Target").ToString();
            var url = page.Items.GetValue("URL").ToString();

            if (url == string.Empty)
            {
                url = page.Code;
            }

            return url.Replace("{URLBase}/", URLBase).Replace("{PageExt}", PageExt) + (target == string.Empty ? string.Empty : "\" target=\"" + target);
        }

        public bool IsPageActived(SysPageEntity pageToCheck)
        {
            if (CurrentPage.ID == pageToCheck.ID)
            {
                return true;
            }

            var page = (SysPageEntity)CurrentPage.Clone();
            while (true)
            {
                page = SysPageService.Instance.GetByID_Cache(page.ParentID);

                if (page == null || page.ParentID == 0)
                {
                    return false;
                }

                if (page.ID == pageToCheck.ID)
                {
                    return true;
                }
            }
        }

        public bool IsPageActived(SysPageEntity page, int index)
        {
            return CurrentPage.ID == page.ID || CurrentVQS.Equals(index, page.Code);
        }

        public void Back(int step)
        {
            JavaScript.Back(step, Page);
        }

        public void Navigate(string url)
        {
            JavaScript.Navigate(url, Page);
        }

        public void Close()
        {
            JavaScript.Close(Page);
        }

        public void Script(string key, string script)
        {
            JavaScript.Script(key, script, Page);
        }

        public void RefreshPage()
        {
            Response.Redirect(Request.RawUrl);
        }

        #region zebradialog

        public void Alert(string title, string content)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content))
            {
                return;
            }

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "AlertScript", string.Format("<script type=\"text/javascript\">zebra_alert('" + title + "', '" + content + "');</script>"));
        }

        public void Alert(string content)
        {
            Alert("Thông báo !", content);
        }

        public void AlertThenRedirect(string title, string content, string redirect)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content) || string.IsNullOrEmpty(redirect))
            {
                return;
            }

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ConfirmScript", string.Format("<script type=\"text/javascript\">zebra_infor('" + title + "','" + content + "','" + redirect + "');</script>"));
        }

        public void AlertThenRedirect(string content, string redirect)
        {
            AlertThenRedirect("Thông báo !", content, redirect);
        }

        public void AlertThenRedirect(string content)
        {
            AlertThenRedirect("Thông báo !", content, "/");
        }

        public void Confirm(string title, string content, string redirect)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(content) || string.IsNullOrEmpty(redirect))
            {
                return;
            }

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "ConfirmScript", string.Format("<script type=\"text/javascript\">zebra_confirm('" + title + "','" + content + "','" + redirect + "');</script>"));
        }

        public void Confirm(string content, string redirect)
        {
            Confirm("Thông báo !", content, redirect);
        }

        public void Confirm(string content)
        {
            Confirm("Thông báo !", content, "/");
        }

        #endregion zebradialog
    }
}