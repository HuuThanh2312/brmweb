SET IDENTITY_INSERT [dbo].[Mod_News] ON
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (1, 926, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga', N'https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg', 0, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (2, 926, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga 2', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-2', N'https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg', 0, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (3, 926, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga 3', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-3', N'https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg', 0, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (4, 987, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga 4', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-4', N'/Data/upload/images/Adv/voucher.jpg', 7, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (6, 987, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga 5', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-5', N'/Data/upload/images/Adv/1(1).jpg', 7, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (7, 987, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga 6', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-6', N'/Data/upload/images/Adv/4.jpg', 9, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (8, 987, 0, N'Torres lập cú đúp trong ngày chia tay Atletico và La Liga 7', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-7', N'/Data/upload/images/Adv/3.png', 7, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (9, 987, 0, N'Voucher tuần 32', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-8', N'/Data/upload/images/Adv/1_small.jpg', 7, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
INSERT INTO [dbo].[Mod_News] ([ID], [MenuID], [State], [Name], [Code], [File], [View], [Summary], [Content], [Custom], [PageTitle], [PageDescription], [PageKeywords], [Published], [Updated], [Order], [Activity]) VALUES (10, 987, 0, N'Voucher tháng 7', 'torres-lap-cu-dup-trong-ngay-chia-tay-atletico-va-la-liga-9', N'/Data/upload/images/Adv/4.png', 17, N'Ở vòng 38 La Liga, Torres đã lập cú đúp trong trận đấu Atletico hòa Eibar 2-2. Ngôi sao người Tây Ban Nha đã có màn chia tay ý nghĩa và giàu cảm xúc với sân Metropolitano, nơi các cổ động viên Madrid coi anh như một huyền thoại.', N'<p>Torres lập c&uacute; đ&uacute;p trong ng&agrave;y chia tay Atletico v&agrave; La Liga</p>

<p><img alt="
Các đồng đội xếp hàng vỗ tay để tỏ sự tri ân với Fernando Torres
" id="img_d637a1a0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79856a00000578-5750771-image-a-271526841701560-1526866351686317833499.jpg" /></p>

<p>C&aacute;c đồng đội xếp h&agrave;ng vỗ tay để tỏ sự tri &acirc;n với Fernando Torres</p>

<p>&nbsp;</p>

<p><img alt="
Eibar đã gây bất ngờ khi có được bàn thắng dẫn trước nhờ công Kike ở phút 35
" id="img_d5865300-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c792efb00000578-5750771-image-a-31526837912362-152686635166656179508.jpg" /></p>

<p>Eibar đ&atilde; g&acirc;y bất ngờ khi c&oacute; được b&agrave;n thắng dẫn trước nhờ c&ocirc;ng Kike ở ph&uacute;t 35</p>

<p>&nbsp;</p>

<p><img alt="
Torres gỡ hòa 1-1 cho Atletico ở phút 42
" id="img_d60bfdc0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79659100000578-5750771-image-a-91526839009326-15268663516951045404918.jpg" /></p>

<p>Torres gỡ h&ograve;a 1-1 cho Atletico ở ph&uacute;t 42</p>

<p>&nbsp;</p>

<p><img alt="
Torres mang băng đội trưởng ở trận đấu cuối cùng trong màu áo Atletico tại La Liga
" id="img_d5f80090-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c79653900000578-5750771-image-a-101526839184538-15268663516931368823861.jpg" /></p>

<p>Torres mang băng đội trưởng ở trận đấu cuối c&ugrave;ng trong m&agrave;u &aacute;o Atletico tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Phút 60, Torres hoàn tất cú đúp giúp Atletico dẫn Eibar 2-1
" id="img_d5dde8e0-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79501300000578-5750771-image-a-71526838014347-15268663516892129967536.jpg" /></p>

<p>Ph&uacute;t 60, Torres ho&agrave;n tất c&uacute; đ&uacute;p gi&uacute;p Atletico dẫn Eibar 2-1</p>

<p>&nbsp;</p>

<p><img alt="
Ngôi sao người Tây Ban Nha hôn lên chiếc áo Atletico, CLB giúp anh vươn tầm trở thành tiền đạo hàng đầu thế giới
" id="img_d5cb2430-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c794f6a00000578-5750771-image-a-81526838017459-15268663516691147808989.jpg" /></p>

<p>Ng&ocirc;i sao người T&acirc;y Ban Nha h&ocirc;n l&ecirc;n chiếc &aacute;o Atletico, CLB gi&uacute;p anh vươn tầm trở th&agrave;nh tiền đạo h&agrave;ng đầu thế giới</p>

<p>&nbsp;</p>

<p><img alt="
Torres sẽ chia tay bóng đá đỉnh cao sau trận đấu với Eibar
" id="img_d5b7ea50-5c96-11e8-84e9-5f30966a6978" src="https://dantricdn.com/2018/5/21/4c79467c00000578-5750771-image-a-41526837960468-15268663516831760605019.jpg" /></p>

<p>Torres sẽ chia tay b&oacute;ng đ&aacute; đỉnh cao sau trận đấu với Eibar</p>

<p>&nbsp;</p>

<p><img alt="
Cổ động viên Atletico thể hiện sự tri ân với huyền thoại số 9 của mình trên sân Metropolitano
" id="img_d59dab90-5c96-11e8-8d75-618d6a57fed6" src="https://dantricdn.com/2018/5/21/4c7931fa00000578-0-image-a-11526837693573-15268663516781837536452.jpg" /></p>

<p>Cổ động vi&ecirc;n Atletico thể hiện sự tri &acirc;n với huyền thoại số 9 của m&igrave;nh tr&ecirc;n s&acirc;n Metropolitano</p>

<p>&nbsp;</p>

<p><img alt="
Torres đã có những thời khắc đầy cảm xúc ở trận đấu cuối cùng tại La Liga
" id="img_d570cf30-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c791b6f00000578-0-image-a-21526837774903-15268663516631261133161.jpg" /></p>

<p>Torres đ&atilde; c&oacute; những thời khắc đầy cảm x&uacute;c ở trận đấu cuối c&ugrave;ng tại La Liga</p>

<p>&nbsp;</p>

<p><img alt="
Ngày chia tay của Torres kém vui khi Pena nhận thẻ đỏ ở phút 63 và Eibar ấn định kết quả hòa 2-2 ở phút 70 nhờ công Pena
" id="img_d624dcf0-5c96-11e8-b5dc-1be09ba74b54" src="https://dantricdn.com/2018/5/21/4c797b7a00000578-5750771-image-a-261526840610894-1526866351672365980600.jpg" /></p>

<p>Ng&agrave;y chia tay của Torres k&eacute;m vui khi Pena nhận thẻ đỏ ở ph&uacute;t 63 v&agrave; Eibar ấn định kết quả h&ograve;a 2-2 ở ph&uacute;t 70 nhờ c&ocirc;ng Pena</p>

<p>&nbsp;</p>

<p><em><strong>Đội h&igrave;nh thi đấu</strong></em></p>

<p><strong>Atletico Madrid</strong>&nbsp;(4-4-2): Oblak; Juanfran; Savic; Gomez; Luis; Correa (Griezmann 59); Fernandez; Niguez; Vitolo (Gimenez 66); Koke (Costa 59); Torres</p>

<p>Thẻ đỏ: Gomez 63</p>

<p>B&agrave;n thắng: Torres 42, 60</p>

<p><strong>Eibar&nbsp;</strong>(4-2-3-1): Dmitrovic; Pena; Oliveira; Lomban (Arbilla 66); Angel; Escalante; Carrillo; Alejo (Charles 78); Orellana; Kike (Regis 90+3)</p>

<p>B&agrave;n thắng: Kike 35, Pena 70</p>

<p><strong>Th&ugrave;y Anh</strong></p>
', NULL, N'', N'', N'', '2018-05-21 09:36:35.507', '2018-05-21 09:36:35.507', 1, 1)
SET IDENTITY_INSERT [dbo].[Mod_News] OFF
