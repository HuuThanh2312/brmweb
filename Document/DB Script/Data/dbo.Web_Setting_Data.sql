SET IDENTITY_INSERT [dbo].[Web_Setting] ON
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (1, N'Số lượt truy cập', 'VISIT', N'8965')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (2, N'Thời gian Cache', 'TIMECACHE', N'120')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (3, N'Truy cập hôm nay', 'TODAY', N'0')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (4, N'Truy cập hôm qua', 'YESTERDAY', N'0')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (5, N'Truy cập tháng này', 'THISMONTH', N'0')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (6, N'Truy cập tháng trước', 'LASTMONTH', N'0')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (7, N'Truy cập năm nay', 'THISYEAR', N'0')
INSERT INTO [dbo].[Web_Setting] ([ID], [Name], [Code], [Value]) VALUES (8, N'Truy cập năm ngoái', 'LASTYEAR', N'0')
SET IDENTITY_INSERT [dbo].[Web_Setting] OFF
