SET IDENTITY_INSERT [dbo].[Web_Resource] ON
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (296, 1, 'Web_Email', N'support@webviet24h.com')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (304, 1, 'Google_Analytics', N'')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (305, 1, 'Web_Address', N'<div class="text-center">
            <p>
                <img src="images/icon/dangkycongthuong.png" alt="" class="mr10" style="height: 24px;">Công ty TNHH Berichmart
            </p>
            <p>Địa chỉ: Tầng 29, Tòa nhà trung tâm Lotte Hà Nội, 54 Liễu Giai, phường Cống Vị, Quận Ba Đình, Hà Nội. Tổng đài hỗ trợ: 19001221 - Email: support@shopee.vn</p>
            <p>Mã số doanh nghiệp: 0106773786 do Sở Kế hoạch & Đầu tư TP Hà Nội cấp lần đầu ngày 10/02/2015</p>
            <p>© 2018 - Bản quyền thuộc về Công ty TNHH Shopee</p>
        </div>')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (306, 1, 'Web_SearchText', N'Từ khóa tìm kiếm')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (308, 1, 'Web_Social', N'<li class="footer-item-list-icon">
                        <a href="">
                            <img class="footer__category-list-item-list-icon" src="/Content/skins/images/icon/ft-facebook.png">Facebook</a>
                    </li>
                    <li class="footer-item-list-icon">
                        <a href="">
                            <img class="footer__category-list-item-list-icon" src="/Content/skins/images/icon/ft-ist.png">Instagram</a>
                    </li>
                    <li class="footer-item-list-icon">
                        <a href="">
                            <img class="footer__category-list-item-list-icon" src="/Content/skins/images/icon/ft-linke.png">LinkedIn</a>
                    </li>')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (310, 1, 'Web_Hotline', N'0966.223.158')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (318, 1, 'Detail_ShipTime', N'Thời gian giao hàng: 4- 12 tiếng , cả T7 và CN.')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (324, 1, 'Web_DownApp', N'<div class="footer__category-list-item-qr">
                    <img src="/Content/skins/images/icon/bar-code.png" alt="download_qr_code" class="footer__category-list-item-qr-image">
                    <div class="footer__category-list-item-qr-store">
                        <a href="">
                            <img src="/Content/skins/images/icon/appstore.png" alt="App Store">
                        </a>
                        <a href="">
                            <img src="/Content/skins/images/icon/chplay.png" alt="Play Store">
                        </a>
                    </div>
                </div>')
INSERT INTO [dbo].[Web_Resource] ([ID], [LangID], [Code], [Value]) VALUES (325, 1, 'Web_QuyDinh', N' Quy định tham gia phát triển hệ thống người tiêu dùng tại Công ty cổ phần TMDV BeRichMart Việt Nam.
    Bằng việc đăng ký tham gia trở thành Thành Viên của Công ty, Bạn đồng ý chấp nhận và tuân thủ các quy định sau đây:
      1. Thông tin cá nhân:
        - Thành viên đồng ý cung cấp cho Công ty cổ phần TMDV BeRichMart Việt Nam các thông tin trung thực về bản thân bằng cách điền vào "Phiếu đăng ký" và "Hồ sơ". Các thông tin này sẽ giúp Công ty thực hiện tốt hơn việc quản lý hồ sơ và công tác chăm sóc khách hàng.
        - BeRichMart cam kết bảo mật thông tin cá nhân của thành viên và chỉ cung cấp cho bên thứ 3 khi có sự đồng ý của thành viên.

      2. Quyền lợi Thành viên:
        - Thành viên được sử dụng các tiện ích của Công ty:
        - Các giao dịch thanh toán trên tài khoản thanh toán BRM.
        - Hưởng quyền lợi về chính sách bán hàng dành cho Thành viên :
        - Hoa hồng tiêu dùng 10% khi thành viên thanh toán bằng thẻ và 5% khi thành viên thanh toán bằng tiền mặt (trừ thẳng vào đơn hàng khi mua hàng áp dụng cho tất cả các thành viên chính thức và thành viên kết nối)
        - Hoa hồng giới thiệu Thành viên.
        - Hoa hồng thụ động.
        - Được tham gia quỹ thưởng dành cho Thành viên.
        - Được chuyển đổi hoa hồng thành tiền mặt.
        - Được cấp văn phòng làm việc tại website: www.BeRichMart.com.vn

      3. Nghĩa vụ Thành viên:
        a. Thanh toán tiền hàng đầy đủ cho Công ty.
        b. Tư vấn chính xác về chính sách bán hàng và chế độ trả thưởng của Công ty.
        c. Thành viên có trách nhiệm hướng dẫn, hỗ trợ Thành viên trực thuộc hệ thống Thành viên của mình.
        d. Cung cấp đầy đủ, chính xác thông tin xác thực thành viên trực thuộc hệ thống Thành viên của Công ty BRM.

      4. Những điều khoản cấm:
        a. Nghiêm cấm tuyệt đối việc đăng ký giả mạo hoặc dùng nhiều địa chỉ chứng minh thư khác nhau để đăng ký nhiều tài khoản cho một người.
        b. Thành viên sẽ không được phép sử dụng bất kỳ nhãn hiệu thương mại, tên, biểu tượng, khẩu ngữ của Công ty vào mục đích khác mà không được sự đồng ý của Công ty ngoài việc phân phối, quảng cáo như đã được Công ty đồng ý.
        c. Nghiêm cấm tuyệt đối việc dùng danh nghĩa Công ty cổ phần TMDV BeRichMart Việt Nam và lợi dụng mạng lưới thành viên của Công ty vào mục đích cá nhân, để quảng bá các chương trình kinh doanh không có liên quan đến Công ty bằng e-mail hoặc trên mạng Internet.
        d. Công ty cổ phần TMDV BeRichMart Việt Nam sẽ áp dụng các chế tài xử lý vi phạm đối với các trường hợp vi phạm những điều khoản nói trên. Các chế tài xử lý vi phạm có thể là cảnh cáo, phạt tiền, xóa tài khoản  vĩnh viễn khỏi danh sách thành viên hoặc tổng hợp cả 3 biện pháp trên. Việc lựa chọn biện pháp phạt phụ thuộc vào mức độ và số lần vi phạm của thành viên.  

      5. Hủy bỏ tài khoản:
        a. Thành viên có quyền thông báo cho Công ty cổ phần TMDV BeRichMart Việt Nam về quyết định ngừng tham gia phát triển hệ thống mạng lưới vào bất cứ lúc nào. Trường hợp này đồng nghĩa với việc Thành viên đồng ý hủy bỏ toàn bộ những quyền lợi của mình với tư cách là thành viên hệ thống tính đến thời điểm đó.
        b. Công ty cổ phần TMDV BeRichMart Việt Nam có quyền xóa tài khoản của Bạn nếu tài khoản đó không có hoạt động mua hàng trong vòng 6 tháng
        c. Công ty cổ phần TMDV BeRichMart Việt Nam có quyền xóa tên và tài khoản của Bạn nếu Bạn có những hành động đi ngược lại với quyền lợi của công ty như nêu trong mục 4 ("Những điều khoản cấm").
        d. Trong trường hợp tài khoản bị xóa vì các lý do nêu trên, việc đăng ký lại tài khoản khác tại Công ty cổ phần TMDV BeRichMart Việt Nam chỉ có thể được thực hiện sau thời gian ít nhất là 6 tháng.   
        e. Trong trường hợp thành viên không nâng cấp lên thành viên chính thức thì công ty có thể thay thế hoặc xóa bỏ bất cứ lúc nào.

      6. Các quy định khác:
        a. Khi quý khách giới thiệu cho khách hàng, quý khách tuyệt đối phải giới thiệu một cách trung thực.
        b. Công ty cổ phần TMDV BeRichMart Việt Nam có quyền thay đổi điều kiện và phương thức rút tiền và thông báo đến thành viên bằng việc công bố công khai trên website hoặc gửi tin nhắn tới mã số Thành viên .
        c. Công ty cổ phần TMDV BeRichMart Việt Nam có thể bổ sung, mở rộng hoặc thu hẹp các chương trình kinh doanh theo nhu cầu. Những thay đổi (nếu có) sẽ được thông báo đến các Thành viên bằng tin nhắn về mã số Thành viên hoặc công bố trên website và chỉ được áp dụng từ sau ngày công bố.  

      7. Thuế (Thuế Giá Trị Gia Tăng và Thuế Thu Nhập cá nhân)
        7.1. Mỗi bên tự chịu trách nhiệm đối với các nghĩa vụ thuế của mình trước cơ quan thuế của Việt Nam theo quy định của phát luật. khi được BeRichMart yêu cầu, các Thành viên sẽ phải cung cấp các bằng chứng chứng minh mình đã hoàn thành các nghĩa vụ thuế (nếu có). Theo quy định của pháp luật,  BeRichMart sẽ khấu trừ thuế thu nhập cá nhân 10% - 20% trên thu nhập hoa hồng của Thành Viên để thực hiện việc nộp thuế thay cho các thành viên.
        7.2. Thành viên đồng ý rằng BeRichMart có quyền
          a) Thông báo cho các cơ quan có thẩm quyền của Việt Nam mọi thông tin liên quan đến việc bán hợp tác giữa BeRichMart và Thành viên; và 
          b) Nếu pháp luật quy định hoặc theo yêu cầu của các cơ quan thuế Việt Nam, BeRichMart sẽ thực hiệp việc thu các khoản thuế đến hạn áp dụng cho các hoạt động của Thành viên theo hợp đồng này và trực tiếp nộp các khoản thuế đó cho các cơ quan thuế của Việt Nam.
                             

                            Công ty cổ phần TMDV BeRichMart Việt Nam
                            E-mail: Website: http://www.BeRichMart.com.vn')
SET IDENTITY_INSERT [dbo].[Web_Resource] OFF
