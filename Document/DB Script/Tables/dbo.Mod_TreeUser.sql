CREATE TABLE [dbo].[Mod_TreeUser]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[ParentUserID] [int] NULL,
[Level] [int] NULL,
[Published] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_TreeUser] ADD CONSTRAINT [PK_Mod_TreeUser] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
