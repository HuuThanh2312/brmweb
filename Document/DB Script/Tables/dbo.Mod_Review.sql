CREATE TABLE [dbo].[Mod_Review]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ShopID] [int] NULL,
[WebUserID] [int] NULL,
[ProductID] [int] NULL,
[Name] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Vote] [int] NULL,
[Point] [decimal] (18, 0) NULL,
[Like] [int] NULL,
[Content] [nvarchar] (4000) COLLATE Vietnamese_CI_AS NULL,
[IP] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Review] ADD CONSTRAINT [PK_Mod_Review] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
