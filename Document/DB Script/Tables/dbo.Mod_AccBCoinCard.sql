CREATE TABLE [dbo].[Mod_AccBCoinCard]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[Name] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[CardCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[AccountBcoin] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Password] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[TotalAmount] [float] NULL,
[Summary] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL,
[Update] [datetime] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_AccBCoinCard] ADD CONSTRAINT [PK_Mod_AccBCoin] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
