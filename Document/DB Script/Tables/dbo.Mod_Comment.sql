CREATE TABLE [dbo].[Mod_Comment]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ParentID] [int] NULL,
[WebUserID] [int] NULL,
[ProductID] [int] NULL,
[Name] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Content] [nvarchar] (4000) COLLATE Vietnamese_CI_AS NULL,
[IP] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Comment] ADD CONSTRAINT [PK_Mod_Comment] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
