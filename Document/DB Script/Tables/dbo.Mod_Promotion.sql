CREATE TABLE [dbo].[Mod_Promotion]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[ShopID] [int] NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Type] [varchar] (20) COLLATE Vietnamese_CI_AS NULL,
[Name] [nvarchar] (250) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (250) COLLATE Vietnamese_CI_AS NULL,
[PromotionCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Activity] [bit] NULL,
[IsAdmin] [bit] NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Promotion] ADD CONSTRAINT [PK_Mod_Promotion] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
