CREATE TABLE [dbo].[Mod_Online]
(
[SessionID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeValue] [bigint] NULL,
[UserID] [int] NULL,
[IsLogin] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Online] ADD CONSTRAINT [PK_Mod_Online] PRIMARY KEY CLUSTERED  ([SessionID]) ON [PRIMARY]
GO
