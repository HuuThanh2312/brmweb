CREATE TABLE [dbo].[Mod_TransactionLogs]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[BankID] [int] NULL,
[Code] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Name] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Content] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Status] [bit] NULL,
[Price] [float] NULL,
[Created] [datetime] NULL,
[Update] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_TransactionLogs] ADD CONSTRAINT [PK_Mod_TransactionLogs] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
