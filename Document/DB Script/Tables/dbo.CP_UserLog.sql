CREATE TABLE [dbo].[CP_UserLog]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[IP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Note] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CP_UserLog] ADD CONSTRAINT [PK__CP_UserL__3214EC2707F6335A] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
