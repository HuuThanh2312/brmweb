CREATE TABLE [dbo].[Mod_AutoLinks]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Link] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Target] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Quantity] [int] NULL,
[Published] [datetime] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_AutoLinks] ADD CONSTRAINT [PK__Mod_Auto__3214EC27108B795B] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
