CREATE TABLE [dbo].[Web_Property]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LangID] [int] NULL,
[ParentID] [int] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Multiple] [bit] NULL,
[IsSize] [bit] NULL,
[IsColor] [bit] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Web_Property] ADD CONSTRAINT [PK__Web_Prop__3214EC27725BF7F6] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
