CREATE TABLE [dbo].[Mod_Gift]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[State] [int] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Model] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Price] [bigint] NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[View] [bigint] NULL,
[Summary] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Content] [ntext] COLLATE Vietnamese_CI_AS NULL,
[Custom] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[PageTitle] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[PageDescription] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[PageKeywords] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Published] [datetime] NULL,
[Updated] [datetime] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Gift] ADD CONSTRAINT [PK__Mod_Gift__3214EC27220B0B18] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
