CREATE TABLE [dbo].[Mod_MaketingShopR]
(
[ID] [int] NOT NULL,
[MktNewsID] [int] NULL,
[ShopID] [int] NULL,
[Percent] [int] NULL,
[Created] [datetime] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[ProductIDs] [varchar] (1000) COLLATE Vietnamese_CI_AS NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_MaketingShopR] ADD CONSTRAINT [PK_Mod_MaketingShopR] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
