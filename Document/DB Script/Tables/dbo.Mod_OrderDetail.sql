CREATE TABLE [dbo].[Mod_OrderDetail]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[StatusID] [int] NULL,
[CancelID] [int] NULL,
[ShopID] [int] NULL,
[OrderID] [int] NULL,
[ProductID] [int] NULL,
[Quantity] [int] NULL,
[Name] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Price] [bigint] NULL,
[Content] [nvarchar] (2000) COLLATE Vietnamese_CI_AS NULL,
[ShippingPrice] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_OrderDetail] ADD CONSTRAINT [PK_Mod_OrderDetail] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
