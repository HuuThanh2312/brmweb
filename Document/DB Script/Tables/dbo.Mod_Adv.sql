CREATE TABLE [dbo].[Mod_Adv]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvCode] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Summary] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [int] NULL,
[Height] [int] NULL,
[AddInTag] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [nvarchar] (511) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsExpires] [bit] NULL,
[Expires] [datetime] NULL,
[Target] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Order] [int] NULL,
[Activity] [bit] NULL,
[Show] [bit] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Adv] ADD CONSTRAINT [PK__Mod_Adv__3214EC270CBAE877] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
