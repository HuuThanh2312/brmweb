CREATE TABLE [dbo].[Mod_Followers]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ShopID] [int] NULL,
[UserID] [int] NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Followers] ADD CONSTRAINT [PK_Mod_Followers] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
