CREATE TABLE [dbo].[Mod_CleanURL]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LangID] [int] NULL,
[Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [int] NULL,
[MenuID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_CleanURL] ADD CONSTRAINT [PK__Mod_Clea__3214EC27182C9B23] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
