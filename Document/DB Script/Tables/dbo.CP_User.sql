CREATE TABLE [dbo].[CP_User]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LoginName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CP_User] ADD CONSTRAINT [PK__CP_User__3214EC270425A276] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
