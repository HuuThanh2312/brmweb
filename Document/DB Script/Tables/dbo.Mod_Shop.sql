CREATE TABLE [dbo].[Mod_Shop]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CityID] [int] NULL,
[DistrictID] [int] NULL,
[UserID] [int] NULL,
[Name] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Url] [varchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Logo] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Summary] [nvarchar] (2000) COLLATE Vietnamese_CI_AS NULL,
[Banner] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Address] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Phone] [varchar] (11) COLLATE Vietnamese_CI_AS NULL,
[Email] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Vote] [int] NULL,
[Point] [int] NULL,
[Published] [datetime] NULL,
[Activity] [bit] NULL,
[LogsLogin] [datetime] NULL,
[Vr] [bit] NULL,
[VrCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Flowers] [int] NULL,
[Flower] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Shop] ADD CONSTRAINT [PK_Mod_Shop] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
