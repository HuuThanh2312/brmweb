CREATE TABLE [dbo].[Mod_ProductFile]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Default] [bit] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_ProductFile] ADD CONSTRAINT [PK__Mod_BSCa__3214EC276FB49575] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
