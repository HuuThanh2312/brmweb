CREATE TABLE [dbo].[Web_Resource]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LangID] [int] NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Web_Resource] ADD CONSTRAINT [PK__Web_Reso__3214EC2774AE54BC] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
