CREATE TABLE [dbo].[Mod_ProductGift]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NULL,
[GiftID] [int] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Price] [bigint] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_ProductGift] ADD CONSTRAINT [PK_Mod_ProductGift] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
