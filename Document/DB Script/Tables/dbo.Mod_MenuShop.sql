CREATE TABLE [dbo].[Mod_MenuShop]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ShopID] [int] NULL,
[MenuID] [int] NULL,
[ParentID] [int] NULL,
[PropertyID] [int] NULL,
[Name] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_MenuShop] ADD CONSTRAINT [PK__Mod_MenuShop__3214EC27693CA210] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
