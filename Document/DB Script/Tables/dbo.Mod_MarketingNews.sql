CREATE TABLE [dbo].[Mod_MarketingNews]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[Percent] [int] NULL,
[OrderPro] [bit] NULL,
[State] [int] NULL,
[Name] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (255) COLLATE Vietnamese_CI_AS NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[View] [int] NULL,
[Summary] [nvarchar] (1000) COLLATE Vietnamese_CI_AS NULL,
[Content] [ntext] COLLATE Vietnamese_CI_AS NULL,
[Custom] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[PageTitle] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[PageDescription] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[PageKeywords] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Published] [datetime] NULL,
[Updated] [datetime] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[PriceSale] [bigint] NULL,
[FreeShip] [bit] NULL,
[Voucher] [varchar] (20) COLLATE Vietnamese_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_MarketingNews] ADD CONSTRAINT [PK_Mod_MarketingNews] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
