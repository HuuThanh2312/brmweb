CREATE TABLE [dbo].[Web_Redirection]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Url] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Redirect] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Web_Redirection] ADD CONSTRAINT [PK__Web_Redi__3214EC2770DDC3D8] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
