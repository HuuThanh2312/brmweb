CREATE TABLE [dbo].[Web_Menu]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CategoryBerichID] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[LangID] [int] NULL,
[ParentID] [int] NULL,
[PropertyID] [int] NULL,
[State] [int] NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CityCode] [varchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Summary] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Web_Menu] ADD CONSTRAINT [PK__Web_Menu__3214EC27693CA210] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
