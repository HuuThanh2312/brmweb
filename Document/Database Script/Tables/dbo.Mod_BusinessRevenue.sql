CREATE TABLE [dbo].[Mod_BusinessRevenue]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ShopID] [int] NULL,
[OrderDetailID] [int] NULL,
[OrderID] [int] NULL,
[ProductID] [int] NULL,
[Revenue] [float] NULL,
[Name] [nvarchar] (150) COLLATE Vietnamese_CI_AS NULL,
[Published] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_BusinessRevenue] ADD CONSTRAINT [PK_Mod_BusinessRevenue] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
