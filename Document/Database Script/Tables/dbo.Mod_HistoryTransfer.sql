CREATE TABLE [dbo].[Mod_HistoryTransfer]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[FromUserID] [int] NULL,
[AccountID] [int] NULL,
[Name] [nvarchar] (250) COLLATE Vietnamese_CI_AS NULL,
[Price] [float] NULL,
[ToUserID] [int] NULL,
[AccountToUser] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Content] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Price2] [float] NULL,
[Status] [bit] NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_HistoryTransfer] ADD CONSTRAINT [PK_Mod_HistoryTransfer] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
