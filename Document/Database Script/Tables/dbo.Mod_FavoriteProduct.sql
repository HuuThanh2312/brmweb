CREATE TABLE [dbo].[Mod_FavoriteProduct]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ProductID] [int] NULL,
[UserID] [int] NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_FavoriteProduct] ADD CONSTRAINT [PK_Mod_FollowProduct] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
