CREATE TABLE [dbo].[Mod_Keyword]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Order] [int] NULL,
[Activity] [bit] NULL,
[Click] [int] NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Keyword] ADD CONSTRAINT [PK_Mod_Keyword] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
