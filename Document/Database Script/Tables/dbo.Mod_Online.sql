CREATE TABLE [dbo].[Mod_Online]
(
[SessionID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeValue] [bigint] NULL,
[UserID] [int] NULL,
[IsLogin] [bit] NULL
) ON [PRIMARY]
GO
