CREATE TABLE [dbo].[Mod_BankingUser]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[TotalAmount] [float] NULL,
[TotalAvailable] [float] NULL,
[Name] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[NameBank] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[AccountCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[CardCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[TypeAccount] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[AddressBank] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Activity] [bit] NULL,
[Created] [datetime] NULL,
[Update] [datetime] NULL,
[BankID] [int] NULL,
[BranchID] [int] NULL,
[Ten] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[CMND] [varchar] (50) COLLATE Vietnamese_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_BankingUser] ADD CONSTRAINT [PK__Mod_Bank__3214EC27182C9B23] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
