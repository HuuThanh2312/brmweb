CREATE TABLE [dbo].[Web_Setting]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Web_Setting] ADD CONSTRAINT [PK__Web_Sett__3214EC27787EE5A0] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
