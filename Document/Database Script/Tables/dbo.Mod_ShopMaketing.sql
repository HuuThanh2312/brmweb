CREATE TABLE [dbo].[Mod_ShopMaketing]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ShopID] [int] NULL,
[MenuID] [int] NULL,
[ProductIDs] [varchar] (1000) COLLATE Vietnamese_CI_AS NULL,
[Name] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Content] [ntext] COLLATE Vietnamese_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Percent] [int] NULL,
[PriceSale] [bigint] NULL,
[FreeShip] [bit] NULL,
[Voucher] [varchar] (20) COLLATE Vietnamese_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_ShopMaketing] ADD CONSTRAINT [PK_Mod_ShopMaketing] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
