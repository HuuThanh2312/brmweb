CREATE TABLE [dbo].[Mod_Order]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ShopID] [varchar] (500) COLLATE Vietnamese_CI_AS NULL,
[WebUserID] [int] NULL,
[StatusID] [int] NULL,
[CityID] [int] NULL,
[DistrictID] [int] NULL,
[CommunesID] [int] NULL,
[PaymentID] [int] NULL,
[Code] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Total] [bigint] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Email] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Phone] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Address] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Title] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Content] [nvarchar] (1000) COLLATE Vietnamese_CI_AS NULL,
[IP] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Order] ADD CONSTRAINT [PK__Mod_Orde__3214EC270EF836A4] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
