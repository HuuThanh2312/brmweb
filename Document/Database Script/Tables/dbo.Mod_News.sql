CREATE TABLE [dbo].[Mod_News]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[State] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[View] [bigint] NULL,
[Summary] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageTitle] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageDescription] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageKeywords] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Published] [datetime] NULL,
[Updated] [datetime] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_News] ADD CONSTRAINT [PK__Mod_News__3214EC272F10007B] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
