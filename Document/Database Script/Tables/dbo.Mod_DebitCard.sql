CREATE TABLE [dbo].[Mod_DebitCard]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[CardNumber] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Month] [int] NULL,
[Year] [int] NULL,
[Cvv] [nvarchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Address] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_DebitCard] ADD CONSTRAINT [PK_Mod_DebitCard] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
