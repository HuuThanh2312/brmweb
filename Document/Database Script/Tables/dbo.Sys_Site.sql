CREATE TABLE [dbo].[Sys_Site]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PageID] [int] NULL,
[LangID] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Default] [bit] NULL,
[Order] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sys_Site] ADD CONSTRAINT [PK__Sys_Site__3214EC27619B8048] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
