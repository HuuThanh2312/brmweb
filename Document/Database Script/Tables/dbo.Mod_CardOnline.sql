CREATE TABLE [dbo].[Mod_CardOnline]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[TypeID] [int] NULL,
[BrandID] [int] NULL,
[PriceID] [int] NULL,
[PaymentID] [int] NULL,
[Quantity] [int] NULL,
[IP] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_CardOnline] ADD CONSTRAINT [PK_Mod_CardOnline] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
