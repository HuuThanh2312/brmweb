CREATE TABLE [dbo].[CP_Access]
(
[RefCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleID] [int] NULL,
[UserID] [int] NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [int] NULL
) ON [PRIMARY]
GO
