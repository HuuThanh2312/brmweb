CREATE TABLE [dbo].[CP_Role]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Lock] [bit] NULL,
[Order] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CP_Role] ADD CONSTRAINT [PK__CP_Role__3214EC2700551192] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
