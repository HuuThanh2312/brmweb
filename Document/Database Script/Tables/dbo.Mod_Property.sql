CREATE TABLE [dbo].[Mod_Property]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[ProductID] [int] NULL,
[PropertyID] [int] NULL,
[PropertyValueID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Property] ADD CONSTRAINT [PK__Mod_Prop__3214EC276E8B6712] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
