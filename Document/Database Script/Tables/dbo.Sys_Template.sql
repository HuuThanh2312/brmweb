CREATE TABLE [dbo].[Sys_Template]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LangID] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[File] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Custom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Device] [int] NULL,
[Order] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sys_Template] ADD CONSTRAINT [PK__Sys_Temp__3214EC27656C112C] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
