CREATE TABLE [dbo].[Mod_ProductBerichFile]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[ProductID] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Files] [nvarchar] (4000) COLLATE Vietnamese_CI_AS NULL,
[CategoryID] [varchar] (20) COLLATE Vietnamese_CI_AS NULL,
[Content] [ntext] COLLATE Vietnamese_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_ProductBerichFile] ADD CONSTRAINT [PK_Mod_ProductBerichFile] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
