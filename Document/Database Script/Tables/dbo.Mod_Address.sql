CREATE TABLE [dbo].[Mod_Address]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Address] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Note] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Email] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Phone] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Map] [ntext] COLLATE Vietnamese_CI_AS NULL,
[Published] [datetime] NULL,
[Updated] [datetime] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL,
[UserID] [int] NULL,
[CityID] [int] NULL,
[DistrictID] [int] NULL,
[PhuongID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Address] ADD CONSTRAINT [PK_Mod_Address] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
