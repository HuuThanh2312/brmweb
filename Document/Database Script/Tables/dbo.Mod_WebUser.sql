CREATE TABLE [dbo].[Mod_WebUser]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CityID] [int] NULL,
[DistrictID] [int] NULL,
[ParentID] [int] NULL,
[UserParentIDs] [varchar] (500) COLLATE Vietnamese_CI_AS NULL,
[LoginName] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TempPassword] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Logo] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [ntext] COLLATE Vietnamese_CI_AS NULL,
[Point] [bigint] NULL,
[IP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created] [datetime] NULL,
[Activity] [bit] NULL,
[IsShop] [bit] NULL,
[BeCoin] [float] NULL,
[BirthDay] [datetime] NULL,
[Gender] [bit] NULL,
[AddressNo] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[CityCMND] [int] NULL,
[CodeCMND] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[DateCMND] [datetime] NULL,
[FirstLogin] [int] NULL,
[Level] [int] NULL,
[VrCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[Vr] [bit] NULL,
[OldPassword] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[PhuongID] [int] NULL,
[DistrictCMND] [int] NULL,
[CityCACMND] [int] NULL,
[PhuongCMND] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_WebUser] ADD CONSTRAINT [PK__Mod_WebU__3214EC275629CD9C] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
