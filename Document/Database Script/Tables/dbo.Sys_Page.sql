CREATE TABLE [dbo].[Sys_Page]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ParentID] [int] NULL,
[TemplateID] [int] NULL,
[TemplateMobileID] [int] NULL,
[TemplateTabletID] [int] NULL,
[ModuleCode] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[LangID] [int] NULL,
[MenuID] [int] NULL,
[BrandID] [int] NULL,
[State] [int] NULL,
[Name] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Code] [varchar] (100) COLLATE Vietnamese_CI_AS NULL,
[Faicon] [varchar] (50) COLLATE Vietnamese_CI_AS NULL,
[File] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Icon] [nvarchar] (255) COLLATE Vietnamese_CI_AS NULL,
[Summary] [nvarchar] (1000) COLLATE Vietnamese_CI_AS NULL,
[Custom] [ntext] COLLATE Vietnamese_CI_AS NULL,
[TopContent] [ntext] COLLATE Vietnamese_CI_AS NULL,
[Content] [ntext] COLLATE Vietnamese_CI_AS NULL,
[LinkTitle] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[PageTitle] [nvarchar] (100) COLLATE Vietnamese_CI_AS NULL,
[PageDescription] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[PageKeywords] [nvarchar] (500) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL,
[Updated] [datetime] NULL,
[Order] [int] NULL,
[Activity] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sys_Page] ADD CONSTRAINT [PK__Sys_Page__3214EC275DCAEF64] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
