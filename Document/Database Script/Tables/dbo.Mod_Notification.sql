CREATE TABLE [dbo].[Mod_Notification]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ToUserID] [int] NULL,
[FromUserID] [int] NULL,
[Name] [nvarchar] (200) COLLATE Vietnamese_CI_AS NULL,
[Content] [nvarchar] (2000) COLLATE Vietnamese_CI_AS NULL,
[Created] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_Notification] ADD CONSTRAINT [PK_Mod_Notification] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
