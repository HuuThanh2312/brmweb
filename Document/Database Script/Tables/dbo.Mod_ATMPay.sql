CREATE TABLE [dbo].[Mod_ATMPay]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[UserID] [int] NULL,
[OrderID] [int] NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalAmount] [bigint] NULL,
[Content] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created] [datetime] NULL,
[IP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mod_ATMPay] ADD CONSTRAINT [PK__Mod_ATMP__3214EC27297722B6] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
