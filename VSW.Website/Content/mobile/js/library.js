$(function () {
    $('#load-more').click(function () {
        get_product();
    })

    //so luong chi tiet
    $('.buy_now').click(function () {
        add_cart($('#ProductID').val(), $('#ColorID').length ? $('#ColorID').val() : 0, $('#SizeID').length ? $('#SizeID').val() : 0, $('#Quantity').val(), $('#ReturnPath').val());
    })

    $('.thumbs li a').click(function () {
        $('.slider img').attr('src', $(this).data('img'));
    })

    $('nav#menu').mmenu({
        extensions: ['effect-slide-menu', 'pageshadow'],
        searchfield: true,
        counters: true,
        navbar: {
            title: 'Danh mục menu'
        },
        navbars: [
          {
              position: 'top',
              content: ['searchfield']
          }, {
              position: 'top',
              content: [
                'prev',
                'title',
                'close'
              ]
          }, {
              position: 'bottom',
              content: [
              ]
          }
        ]
    });

    'use strict';
    $(".home-menu-logo").owlCarousel({
        slideSpeed: 200,
        items: 6,
        itemsCustom: false,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [979, 4],
        itemsTablet: [768, 3],
        itemsTabletSmall: false,
        itemsMobile: [479, 2],
        autoPlay: true,
        stopOnHover: true,
        autoHeight: true,
        responsive: true,
        navigation: true,
        pagination: false,
        navigationText: ["", ""],
    });
    $(".choosed").click(function () {
        $(".listarrange").slideToggle()
    });
    $(".listarrange a").click(function () {
        $(this).hasClass("choose") || ($(".listarrange a.choose").removeClass("choose"), $(this).addClass("choose"), $(".choosed").text($(this).data("text")), $("input[name=sortby]").val($(this).data("id")), $(".listarrange").slideToggle(), categoryFilter(!0))
    });

    $('.buylink').click(function () {
        location.href = '/dat-hang.html?returnpath=' + $(this).data('returnpath');
    })

    //slide top
    var slideTop = $('#akr_home');
    slideTop.owlCarousel({
        autoPlay: 3000,
        navigation: true,
        pagination: true,
        slideSpeed: 500,
        paginationSpeed: 500,
        singleItem: true
    });
    $('#akr_home_prev').click(function () {
        slideTop.trigger('owl.next');
    })
    $('#akr_home_next').click(function () {
        slideTop.trigger('owl.prev');
    })

    $(".filter li div").click(function (e) {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $(this).next().slideToggle(100);
            return;
        }
        $(".filter li div.selected").next().slideUp(100);
        $(".filter li div.selected").removeClass('selected');
        $(this).addClass('selected');
        $(this).next().slideToggle(100)
    });
});

function change_captcha() {
    var e = Math.floor(Math.random() * 999999); document.getElementById('imgValidCode').src = '/Ajax/Security.html?Code=' + e
}

function increment(e) {
    e.html((+parseInt(e.html()) + 1) || 0);
}
function decrement(e) {
    if (parseInt(e.html()) === 1) {
        zebra_alert('Thông báo !', 'Bạn phải mua ít nhất 1 sản phẩm.');
        return;
    }
    e.html((parseInt(e.html()) - 1) || 0)
}

function add_cart(ProductID, ColorID, SizeID, Quantity, ReturnPath) {
    if (ProductID < 1) {
        zebra_alert('Thông báo !', 'Sản phẩm không tồn tại.');
        return;
    }

    if ($('#ColorID').length && $('#ColorID').val() < 1) {
        zebra_alert('Thông báo !', 'Bạn chưa chọn màu sắc.');
        return;
    }

    if ($('#SizeID').length && $('#SizeID').val() < 1) {
        zebra_alert('Thông báo !', 'Bạn chưa chọn kích cỡ.');
        return;
    }

    if (Quantity < 1) {
        zebra_alert('Thông báo !', 'Chưa nhập số lượng mua');
        return;
    }

    location.href = '/gio-hang/Add.html?ProductID=' + ProductID + '&Quantity=' + Quantity + '&returnpath=' + ReturnPath;
}

function update_cart(Index, Quantity, ReturnPath) {
    location.href = '/gio-hang/Update.html?Index=' + Index + '&Quantity=' + Quantity + '&returnpath=' + ReturnPath;
}

function delete_cart(index) {
    zebra_confirm('Thông báo !', 'Bạn chắc chắn muốn xóa ?', '/gio-hang/Delete.html?Index=' + index)
}

function add_favorite(ProductID, ReturnPath) {
    location.href = '/yeu-thich/Add.html?ProductID=' + ProductID + '&returnpath=' + ReturnPath;
}

function delete_favorite(index) {
    zebra_confirm('Thông báo !', 'Bạn chắc chắn muốn xóa ?', '/yeu-thich/Delete.html?Index=' + index)
}

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=565526433591379';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));