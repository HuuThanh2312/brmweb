﻿function get_product() {
    var dataString = 'page=' + $('#Page').val() + '&MenuID=' + $('#MenuID').val() + '&sort=' + $('#Sort').val() + '&atr=' + $('#Atr').val();
    $('#loading').show();
    $.ajax({
        url: "/ajax/GetProduct.html",
        type: "post",
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var html = data.Html;
            if (html !== '') {
                $('#Page').val($('#Page').val() + 1);
                $('.temp-pro-item').append(html);
            }
            else
                $('#load-more').hide();

            $('#loading').hide();
        },
        error: function (status) { }
    });
}