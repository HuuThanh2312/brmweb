﻿function setTabActive(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/SetTabActive.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });

    $.ajax({
        url: "/ajax/SetTabActive.html",
        type: "post",
        data: $('#login-form').serialize(),
        dataType: 'json',
        success: function (data) {
            var params = data.Params;

            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }
            else {
                location.reload();
            }
        },
        error: function (status) { }
    });
}

function login() {
    $.ajax({
        url: "/ajax/Login.html",
        type: "post",
        data: $('#login-form').serialize(),
        dataType: 'json',
        success: function (data) {
            var params = data.Params;

            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }
            else {
                location.reload();
            }
        },
        error: function (status) { }
    });
}

function register() {
    $.ajax({
        url: "/ajax/Register.html",
        type: "post",
        data: $('#register-form').serialize(),
        dataType: 'json',
        success: function (data) {
            var params = data.Params;

            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }
            else {
                location.reload();
            }
        },
        error: function (status) { }
    });
}