﻿$(function () {


    $(".close").click(function () {
        $('#login_form')[0].reset();
        $('#register_form')[0].reset();
        $('#messenger').html('');
        $('#email').html('');
        $('#inputPassword').val('');
        $('#Phone').html('');
        $('#Pass').html('');
        $('#Password2').html('');
        
    });
    $(".onclick").click(function () {
        $('#login_form')[0].reset();

    });
    $(".login").click(function () {
        $('#register_form')[0].reset();

    });

    $('.akr-add-cart-shop').click(function () {
        add_cart_shop($(this).data('url'), $(this).data('sid'), $(this).data('pid'), $('#Quantity').length ? $('#Quantity').val() : 1, $(this).data('returnpart'), $('#ShippingPrice').val().length > 0 ? $('#ShippingPrice').val() : 0, $('#name_cty').text());
    });

    // show
    $('.infoother').click(function () {
        $(this).toggleClass('arrow');
        $('.subother').slideToggle('fast');
        $('html, body').animate({
            scrollTop: $(document).height()
        });
    });
    // filter
    $('.criteria').click(function () {
        $('.criteria').not(this).next().hide();
        $(this).next().slideToggle(200);
        $('body,html').animate({
            scrollTop: $('.barpage').position().top
        }, 600)
    });
    $('.closefilter').click(function () {
        $('.filter li div').fadeOut()
    });

    //rating
    $('#rating-input-sp').rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'smm',
        showClear: false,
        showCaption: false,
        readonly: true
    });

    $('#rating-input-shop').rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'sm',
        showClear: false,
        showCaption: false,

    });
    $('#rating-input-shop').rating('update', 0);
    $('#rating-input-shop').on('rating.change', function (event, value, caption) {
        $('#Star').val($('#rating-input-shop').val());
    });

    $('#rating-input-sp').rating('update', 0);
    $('#rating-input-sp').on('rating.change', function (event, value, caption) {
        $('#Star_sp').val($('#rating-input-sp').val());
    });




    $("input[id^='rating-input_']").rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'xs',
        showClear: false,
        showCaption: false,
        readonly: true
    });


});


// Back to top
$(function () {
    $(".back-to-top a").click(function (n) {
        n.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 500)
    });
    $(window).scroll(function () {
        $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
    });
})

//Slide Show
$(function () {
    var slideTop = $('#akr_home');
    slideTop.owlCarousel({
        autoPlay: 3000,
        navigation: false,
        pagination: false,
        slideSpeed: 500,
        paginationSpeed: 500,
        singleItem: true,
        pagination: true,
        autoHeight: true,
    });
    $('#akr_home_prev').click(function () {
        slideTop.trigger('owl.next');
    })
    $('#akr_home_next').click(function () {
        slideTop.trigger('owl.prev');
    })
})

// number input
$(document).on('click', '.number-spinner button', function () {
    var btn = $(this),
        oldValue = btn.closest('.number-spinner').find('input').val().trim(),
        newVal = 0;

    if (btn.attr('data-dir') == 'up') {
        newVal = parseInt(oldValue) + 1;
    } else {
        if (oldValue > 1) {
            newVal = parseInt(oldValue) - 1;
        } else {
            newVal = 1;
        }
    }
    btn.closest('.number-spinner').find('input').val(newVal);
});

//OWl Slide
jQuery(document).ready(function ($) {
    'use strict';
    $(".owl-voucher").owlCarousel({
        slideSpeed: 300,
        items: 4,
        itemsCustom: false,
        autoPlay: true,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: true,
        navigation: true,
        pagination: false,
        navigationText: ["<i class=\"fa fa-caret-left\"></i>", "<i class=\"fa fa-caret-right\"></i>"],
    });

    $(".list-flast-sale").owlCarousel({
        slideSpeed: 300,
        items: 5,
        itemsCustom: false,
        autoPlay: false,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: true,
        navigation: true,
        pagination: false,
        navigationText: ["\<i class=\"fa fa-chevron-left\"></i>", "\<i class=\"fa fa-chevron-right\"></i>"],
    });

    $(".list-brank-prd").owlCarousel({
        slideSpeed: 300,
        items: 7,
        itemsCustom: false,
        autoPlay: false,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: false,
        navigation: true,
        pagination: false,
        navigationText: ["\<i class=\"fa fa-chevron-left\"></i>", "\<i class=\"fa fa-chevron-right\"></i>"],
    });
    $(".owl-prd-phobien").owlCarousel({
        slideSpeed: 300,
        items: 6,
        itemsCustom: false,
        autoPlay: false,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: false,
        navigation: true,
        pagination: false,
        navigationText: ["\<i class=\"fa fa-chevron-left\"></i>", "\<i class=\"fa fa-chevron-right\"></i>"],
    });

    //Fix header
    var section = $('#fix_header');
    var start = $(section).offset().top;
    $.event.add(window, "scroll", function () {
        var img = $('.logo');
        var nav = $('.header-nav');
        var p = $(window).scrollTop(); $(section).css('position', ((p) > start) ? 'fixed' : 'relative'); $(section).css('top', ((p) > start) ? '0px' : '');
        $(section).css('min-width', ((p) > start) ? '1200px' : '');
        $(section).css('width', ((p) > start) ? '100%' : '');
        $(section).css('box-shadow', ((p) > start) ? '0 2px 2px rgba(0, 0, 0, 0.17)' : '');
        $(img).css('width', ((p) > start) ? '' : '');
        $(img).css('height', ((p) > start) ? '' : '');
        $(img).css('margin-left', ((p) > start) ? '0px' : '');
        $(img).css('margin-top', ((p) > start) ? '0px' : '');
        $(nav).css('display', ((p) > start) ? 'none' : '');
        if (p <= 30) { $(section).removeClass('scrollHeader'); } else { $(section).addClass('scrollHeader'); }
    });

})


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).parent().find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function get_code(str) {
    return remove_unicode(str).replace(/[^A-Z0-9]/gi, '');
}

function remove_unicode(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");

    str = str.replace(/-+-/g, "-");
    str = str.replace(/^\-+|\-+$/g, "");

    return str;
}

function formatDollar(value) {
    return value.split("").reverse().reduce(function (acc, value, i, orig) {
        return value + (i && !(i % 3) ? "." : "") + acc;
    }, "");
}

function createCookie(name, value, minutes) {
    var expires = "";

    if (minutes) {
        var date = new Date();
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function add_cart_shop(ViewCart, ShopID, ProductID, Quantity, ReturnPath, ShippingPrice, textcontent) {
    if (ShopID < 1) {
        zebra_alert('Thông báo !', 'Shop không tồn tại');
        return;
    }
    if (ProductID < 1) {
        zebra_alert('Thông báo !', 'Sản phẩm không tồn tại');
        return;
    }
    if (textcontent === 'Chọn nơi vận chuyển') {
        zebra_alert('Thông báo !', 'Bạn chưa chọn điểm giao hàng.');
        return;
    }

    if (Quantity < 1) Quantity = 1;

    location.href = ViewCart + '?ShopID=' + ShopID + '&ProductID=' + ProductID + '&Quantity=' + Quantity + '&ShippingPrice=' + ShippingPrice + '&returnpath=' + ReturnPath;
}

function add_cart(ProductID, ReturnPath) {
    if (ProductID < 1) {
        zebra_alert('Thông báo !', 'Sản phẩm không tồn tại.');
        return;
    }

    location.href = '/gio-hang/Add.html?ProductID=' + ProductID + '&returnpath=' + ReturnPath;
}

function update_cart(Index, Quantity, ReturnPath) {
    location.href = '/gio-hang/Update.html?Index=' + Index + '&Quantity=' + Quantity + '&returnpath=' + ReturnPath;
}

function delete_cart(Index, ReturnPath) {
    zebra_confirm('Thông báo !', 'Bạn chắc chắn muốn xóa ?', '/gio-hang/Delete.html?Index=' + Index + '&returnpath=' + ReturnPath)
}

function add_favorite(ProductID, ReturnPath) {
    location.href = '/yeu-thich/Add.html?ProductID=' + ProductID + '&returnpath=' + ReturnPath;
}

function delete_favorite(index) {
    zebra_confirm('Thông báo !', 'Bạn chắc chắn muốn xóa ?', '/yeu-thich/Delete.html?Index=' + index)
}

function change_captcha() {
    var e = Math.floor(Math.random() * 999999); document.getElementById('imgValidCode').src = '/Ajax/Security.html?Code=' + e
}

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=565526433591379';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function clearText() {
    $('#Password').val('');
    $('#Password1').val('');
    $('#Password2').val('');
    $('#ValidCode').val('');
}