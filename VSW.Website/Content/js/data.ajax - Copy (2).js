﻿function getBSCar(Page, PageSize, CityID, Sort) {
    $('.loading').show();
    var dataString = 'Page=' + Page + '&PageSize=' + PageSize + '&CityID=' + CityID + '&Sort=' + Sort;
    $.ajax({
        type: "get",
        url: "/ajax/GetBSCar.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
            var html = data.Html;
            var params = data.Params;
            var js = data.Js;

            $('.list-pro-post').html(html);
            $('.akr-title-home').html(js);
            $('.akr-total-home').html(params);

            $('.loading').hide();

            $('img.lazyImg').lazyload({
                effect: 'fadeIn'
            });

            ////paging
            //var surplus = params % 4;
            //var maxPage = surplus > 0 ? Math.floor(params / 4) + 1 : params / 4;

            //var paging = '';
            //for (var i = 1; i <= maxPage; i++) {
            //    paging += '<li><a href="javascript:void(0)" data-page="' + i + '" data-city="">' + i + '</a></li>';
            //}
            //$('.akr-page-home').html(paging);

            //$('.akr-page-home li a').click(function () {
            //    $('.akr-page-home li a').removeClass('active');

            //    var $this = $(this);
            //    getBSCar(Page, '4', CityID, '');
            //    $this.addClass('active')
            //})
        },
        error: function (status) { }
    });
}

function fbLike(Value) {
    alert('aa');
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/FBLike.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function fbShared(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/FBShared.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function fbComment(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/FBComment.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function setTabActive(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/SetTabActive.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function setBSCar(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/SetBSCar.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
            location.href = '/dang-tin-ban-xe.html?step=info';
        },
        error: function (status) { }
    });
}

function setBSMarket(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/SetBSMarket.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
            location.href = '/dang-tin-ban-do.html?step=info';
        },
        error: function (status) { }
    });
}

function getChildPage(ParentID, SelectedID) {

    var dataString = 'ParentID=' + ParentID + '&SelectedID=' + SelectedID;
    $('.loading').show();

    $.ajax({
        type: "get",
        url: "/ajax/GetChildPage.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
            var html = data.Html;

            $('#menu2').html(html);

            $('.loading').hide();
        },
        error: function (status) { }
    });
}