﻿$(function () {
    //bsnews
    $('.akr-city-home li a').click(function () {
        $('.akr-city-home li a').removeClass('active');

        var $this = $(this);
        getBSCar('1', '4', $this.data('city'), '');

        $('.akr-page-home li a').attr('data-city', $this.data('city'));

        $this.addClass('active');
        $('.click-city a').parent().removeClass('open');
    })
    $('.akr-page-home li a').click(function () {
        $('.akr-page-home li a').removeClass('active');

        var $this = $(this);
        getBSCar($this.data('page'), '4', $this.data('city'), '');
        alert($this.data('city'));
        $this.addClass('active')
    })

    $('.akr-page-home').jPages({
        containerID: 'akr-jpage',
        previous: '<i class="fa fa-angle-double-left"></i>',
        next: '<i class="fa fa-angle-double-right"></i>',
        perPage: 4,
        delay: 20
    });

    tinymce.init({
        selector: '.ckeditor',
        //toolbar: 'false',
        menubar: '',
        height: '300',
        language: 'vi_VN',
        theme: 'modern',
        mobile: {
            theme: 'mobile'
        }
    });

    //facebook shared
    $('.akr-shared').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '2092089297687062',
                version: 'v2.11'
            });
            FB.ui({
                method: 'share',
                title: $this.data('title'),
                description: $this.data('description'),
                href: $this.data('url'),
            }, function (response) {
                if (response && !response.error_code) {
                    fbShared($(this).data('id'));
                } else {
                    alert('Error while posting.');
                }
            });
        });
    });

    //facebook like
    $('.akr-like').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '2092089297687062',
                version: 'v2.11'
            });
            FB.Event.subscribe('edge.create', fbLike);
        });
    })

    //facebook comment
    $('.akr-comment').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '2092089297687062',
                version: 'v2.11'
            });
            FB.Event.subscribe('comment.create', fbComment($(this).data('id')));
        });
    })

    $('.menu .item').click(function () {
        $('#SearchCode').val($(this).data('code'));
        $('#SearchName').val($(this).data('name'));
    })

    $('.click-city a, .box-user-login a').click(function () {
        $(this).parent().toggleClass('open');
    })
    $(document).click(function (e) {
        if (!$(e.target).closest('.click-city').length) {
            $('.click-city').removeClass('open');
        }
        if (!$(e.target).closest('.box-user-login').length) {
            $('.box-user-login').removeClass('open');
        }
    });

    if ($('.pdl-small-images a').length) {
        $('.pdl-small-images a').bind('click', function () {
            var _src = $(this).data('img');
            var _srcbig = $(this).data('bigimg');
            $('.pdl-image img').attr({ src: _src, 'data-bigimg': _srcbig });
            $('.pdl-small-images a.active').removeClass();
            $(this).addClass('active');
        });
    }

    //video
    $('div[id^="video"]').each(function () {
        var $this = $(this);
        playVideo($this.attr('id'), $this.data('video'), $this.data('thumb'), $this.data('width'), $this.data('height'), false);
    })

    $('.tab-dkdn li').click(function () {
        setTabActive($(this).data('value'));
    })

    $('.car-box').click(function () {
        setBSCar($(this).data('value'));
    })

    $('.market-box').click(function () {
        setBSMarket($(this).data('value'));
    })

    $("img.lazyImg").lazyload({
        effect: "fadeIn"
    });

    $(".back-to-top a").click(function (n) {
        n.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 500)
    });
    $(window).scroll(function () {
        $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
    });

    $('.selectOptions span').click(function () {
        $('#MenuCode').val($(this).data('value'));
        $('#search_text').html($(this).text());
        $('.selectOptions').removeClass('select_hover');
    });
    $('.selected').hover(function () {
        $('.selectOptions').addClass('select_hover');
    }, function () {
        $('.selectOptions').removeClass('select_hover');
    });

    //OWl Slide
    'use strict';
    var slideTop = $('#slideshow-top, #dhm-intro');
    slideTop.owlCarousel({
        autoPlay: 3000,
        navigation: false,
        pagination: false,
        slideSpeed: 500,
        paginationSpeed: 500,
        singleItem: true,
        pagination: true,
        autoHeight: true,
    });

    $("#owl-hangxe-top").owlCarousel({
        slideSpeed: 500,
        items: 5,
        autoPlay: true,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: true,
        navigation: true,
        pagination: false,
        //navigationText: ["<i class=\"fa fa-caret-left\"></i>", "<i class=\"fa fa-caret-right\"></i>"],
        navigationText: ["", "<i class=\"fa fa-caret-right\"></i>"],
    });
    $("#owl-pro-hot").owlCarousel({
        slideSpeed: 900,
        items: 3,
        itemsCustom: false,
        autoPlay: true,
        stopOnHover: true,
        addClassActive: true,
        autoHeight: false,
        responsive: false,
        navigation: false,
        pagination: false
    });
    
    //hinh anh chi tiet
    $("#elevatezoom").elevateZoom({
        gallery: 'list_small',
		zoomWindowWidth: 500,
        zoomWindowHeight: 500,
        cursor: 'pointer',
        galleryActiveClass: 'active',
        imageCrossfade: true
    });

    $('input[type="file"]').change(function () {
        var id = $(this).data('id');
        var arrID = id.split('_');
        if (arrID.length === 2) {
            var index = id.replace(arrID[0] + '_', '');
            readURL(this, arrID[0] + '_Preview_' + index);
        }
        else {
            readURL(this, id + '_Preview');
        }

        return;
    });

    $('.akr-up').click(function () {
        updateCount($(this).data('code'), $(this).data('id'));
    })
    $('.akr-selled').click(function () {
        updateSelled($(this).data('code'), $(this).data('id'));
    })
    
    //$('#gallery-photo-add').on('change', function () {
    //    readURL(this, 'div.gallery');
    //});
});

sizeSegment();
$(window).on("resize", function () {
    sizeSegment();
});

//// Multiple images preview in browser
//var readURL = function (input, placeToInsertImagePreview) {
//    if (input.files) {
//        for (i = 0; i < input.files.length; i++) {
//            var reader = new FileReader();
//            reader.onload = function (event) {
//                $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
//            }
//            reader.readAsDataURL(input.files[i]);
//        }
//    }
//};

function readURL(input, preview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.' + preview).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function sizeSegment() {
    windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
    windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    
    if (windowHeight < 250) {
        windowHeight = windowHeight * 1.5;
    }

    $(".main-img-bg").css({
        'height': windowHeight + "px",
        'width': windowWidth + "px"
    });
}

function updateCount(code, id) {
    location.href = '/' + code + '/UpPOST.html?id=' + id;
}

function updateSelled(code, id) {
    location.href = '/' + code + '/SelledPOST.html?id=' + id;
}

function get_code(str) {
    return remove_unicode(str).replace(/[^A-Z0-9]/gi, '');
}

function remove_unicode(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");

    str = str.replace(/-+-/g, "-");
    str = str.replace(/^\-+|\-+$/g, "");

    return str;
}

function formatDollar(value) {
    return value.split("").reverse().reduce(function (acc, value, i, orig) {
        return value + (i && !(i % 3) ? "." : "") + acc;
    }, "");
}


function createCookie(name, value, minutes) {
    var expires = "";

    if (minutes) {
        var date = new Date();
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function add_cart(ProductID, ColorID, SizeID, Quantity, ReturnPath) {
    if (ProductID < 1) {
        zebra_alert('Thông báo !', 'Sản phẩm không tồn tại.');
        return;
    }

    if ($('#ColorID').length && $('#ColorID').val() < 1) {
        zebra_alert('Thông báo !', 'Bạn chưa chọn màu sắc.');
        return;
    }

    if ($('#SizeID').length && $('#SizeID').val() < 1) {
        zebra_alert('Thông báo !', 'Bạn chưa chọn kích cỡ.');
        return;
    }

    if (Quantity < 1) {
        zebra_alert('Thông báo !', 'Chưa nhập số lượng mua');
        return;
    }

    location.href = '/gio-hang/Add.html?ProductID=' + ProductID + '&Quantity=' + Quantity + '&returnpath=' + ReturnPath;
}

function update_cart(Index, Quantity, ReturnPath) {
    location.href = '/gio-hang/Update.html?Index=' + Index + '&Quantity=' + Quantity + '&returnpath=' + ReturnPath;
}

function delete_cart(index) {
    zebra_confirm('Thông báo !', 'Bạn chắc chắn muốn xóa ?', '/gio-hang/Delete.html?Index=' + index)
}

function add_favorite(ProductID, ReturnPath) {
    location.href = '/yeu-thich/Add.html?ProductID=' + ProductID + '&returnpath=' + ReturnPath;
}

function delete_favorite(index) {
    zebra_confirm('Thông báo !', 'Bạn chắc chắn muốn xóa ?', '/yeu-thich/Delete.html?Index=' + index)
}

function playVideo(pos, file, image, width, height, auto) {
    jwplayer(pos).setup({
        file: file,
        image: image,
        abouttext: 'ANGKORICH - jwplayer build 01032015',
        width: width,
        height: height,
        stretching: 'exactfit',
        autostart: auto,
        logo: {
            file: '',
            link: '',
        }
    });
}

function change_captcha() {
    var e = Math.floor(Math.random() * 999999); document.getElementById('imgValidCode').src = '/Ajax/Security.html?Code=' + e
}

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=565526433591379';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));