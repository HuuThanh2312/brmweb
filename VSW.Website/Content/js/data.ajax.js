﻿//thuoc tinh
function getProperties(MenuID, LangID, ProductID) {
    var ranNum = Math.floor(Math.random() * 999999);
    var dataString = 'MenuID=' + MenuID + '&LangID=' + LangID + '&ProductID=' + ProductID + '&rnd=' + ranNum;

    $.ajax({
        url: '/ajax/GetProperties.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#DistrictID').html(content);
        },
        error: function (status) { }
    });
}

//show
function show_city(FromCityID) {

    var dataString = "DistrictID=" + FromCityID;

    $.ajax({
        url: '/ajax/GhnShow.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;
            var js = data.Js;
            if (content !== '') {
                $('#name_cty').html(content);
            }
        },
        error: function (status) { }
    });


}

// quan huyen gnh

function getDistrict_ghn() {
    var ranNum = Math.floor(Math.random() * 999999);
    var dataString = "";

    $.ajax({
        url: '/ajax/Ghn.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;
            var js = data.Js;
            if (content !== '') {
                $('#list_district').html(content);
                $('.districtID_ghn').on("click", function () {
                    var _id = $(this).data('cityid');
                    var _text = $(this).text();
                    $('#FromCityID').val(_id);

                    document.getElementById("name_cty").textContent = _text;


                    showCity();
                });
            }
            if (js !== '') {
                $('#list_city').html(js);
                $('.districtID_ghn').on("click", function () {
                    var _id = $(this).data('cityid');
                    var _text = $(this).text();
                    $('#FromCityID').val(_id);

                    document.getElementById("name_cty").textContent = _text;
                    var _id2 = $('#FromCityID_2').val();
                    if (_id2 > 0) {
                        getFee_ghn(_id, _id2);
                        search_city(_text);
                    }

                    else {
                        showCity();
                    }
                });
            }
        },
        error: function (status) { }
    });
}

function getFee_ghn(districtID, FromDistrictID) {
    var ranNum = Math.floor(Math.random() * 999999);
    var dataString = "ToDistrictID=" + districtID + "&FromDistrictID=" + FromDistrictID;

    $.ajax({
        url: '/ajax/Ghn_Fee.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;
            var js = data.Js;
            var param2 = data.Params;
            if (content !== '') {
                $('#show-fee-ghn').html(content);
            }
            if (js !== '') {
                $('#price-shiping').html(js);
            } if (param2 !== '') {
                $('#ShippingPrice').val(param2);
            }

        },
        error: function (status) { }
    });
}

// quan huyen

function getDistrict(ParentID, Select) {
    var ranNum = Math.floor(Math.random() * 999999);
    var dataString = 'ParentID=' + ParentID + '&SelectedID=' + Select + '&rnd=' + ranNum;

    $.ajax({
        url: '/ajax/GetChild.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#DistrictID').html(content);
        },
        error: function (status) { }
    });
}


function getShopProduct(ProductCode, AddressID) {
    var ranNum = Math.floor(Math.random() * 999999);
    var dataString = 'Model=' + ProductCode + '&AddressID=' + AddressID + '&rnd=' + ranNum;

    $.ajax({
        url: '/ajax/SearchShopProduct.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-shop-ganday').html(content);

        },
        error: function (status) { }
    });
}


function fbLike(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/FBLike.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function fbShared(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/FBShared.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function fbComment(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/FBComment.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}

function setTabActive(Value) {
    var dataString = 'Value=' + Value;
    $.ajax({
        type: "get",
        url: "/ajax/SetTabActive.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
        },
        error: function (status) { }
    });
}


function getChildPage(ParentID, SelectedID) {

    var dataString = 'ParentID=' + ParentID + '&SelectedID=' + SelectedID;
    $('.loading').show();

    $.ajax({
        type: "get",
        url: "/ajax/GetChildPage.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
            var html = data.Html;

            $('#menu2').html(html);

            $('.loading').hide();
        },
        error: function (status) { }
    });
}

function getHtml(type) {

    var dataString = 'Type=' + type;


    $.ajax({
        type: "get",
        url: "/ajax/ShowInputBank.html",
        data: dataString,
        dataType: "json",
        success: function (data) {
            var content = data.Html;
            $('#add_update_tk').html(content);
        },
        error: function (status) { }
    });
}

//validate text




$('.name_changer').on('input', function (evt) {
    $(this).val(function (_, val) {

        var splitStr = val.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');

    });
});


$('.loginname_changer').on('input', function (evt) {
    $(this).val(function (_, val) {

        str = val.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        str = str.replace(/\W+/g, ' ');
        str = str.replace(/\s/g, '');
        return str;

    });
});


$('.phone').on('input', function (evt) {
    $(this).val(function (_, val) {
        val = val.replace(/(\d{3})(\d{3})(\d{4})/, "$1.$2.$3");
        val = val.replace(/\s/g, '');
        return val;
    });
});


function changer_upper(name) {

    if (name.length > 0) {
        $('#Password2').val(name.topUp);
    }
}

function check_pass(pass, pass2) {

    if (pass !== pass2) {
        $('#Password2').html('Mật khẩu nhập lại không đồng nhất.');
    }
    else {
        $('#Password2').html('');
    }
}


function check_acc(loginName, type) {

    var dataString = 'LoginName=' + loginName + '&Type=' + type;

    $.ajax({
        url: '/ajax/GetAccount.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            if (type === 'LoginName') {
                $('#messenger').html(content);
            }
            if (type === 'email') {
                $('#email').html(content);
            }
            if (type === 'Phone') {
                $('#Phone').html(content);
            }
            if (type === 'Pass') {
                $('#Pass').html(content);
            }
        },
        error: function (status) { }
    });
}
//end validate

function getCardOnline(ID, type) {

    var dataString = 'ID=' + ID;

    $.ajax({
        url: '/ajax/GetCardOnline.html',
        type: 'get',
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            if (type === 'BrandID') {
                $('#BrandID').html(content);
                $('#PriceID').html('<option value="0">Mệnh giá thẻ</option>');
            }
            if (type === 'PriceID') {
                $('#PriceID').html(content);
            }
        },
        error: function (status) { }
    });
}

function get_child(ParentID, SelectedID, type) {
    var dataString = 'ParentID=' + ParentID + '&SelectedID=' + SelectedID;
    $.ajax({
        type: "get",
        url: "/ajax/GetChild.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            if (content !== '' && type === '') {
                $('#DistrictID').html(content);
            }
            if (content !== '' && type === 'CMND') {
                $('#DistrictCMND').html(content);
            }
            if (content !== '' && type === 'PhuongID') {
                $('#PhuongID').html(content);
            }
        },
        error: function (req) { }
    });
}

//coment_sp

function add_comment(ParentID, ProductID, Name, Content) {
    var dataString = 'ParentID=' + ParentID + '&ProductID=' + ProductID + '&Name=' + encodeURIComponent(Name) + '&Content=' + encodeURIComponent(Content);


    $.ajax({
        type: "post",
        url: "/ajax/CommentPOST.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {

            var parmas = data.Params;
            var content = data.Html;

            if (parmas !== '') {
                zebra_alert('Thông báo !', parmas);
                return;
            }
            if (content !== '') {
                $('#list_comment').prepend(content);
                $('html, body').animate({
                    scrollTop: $('#list_comment').offset().top - 150
                }, 2000);
            }

        },
        error: function (req) { }
    });
}

//rating_Shop

function ratting(ShopID, VotePoint) {
    var dataString = 'ShopID=' + ShopID + '&VotePoint=' + VotePoint;


    $.ajax({
        type: "post",
        url: "/ajax/RatingShop.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {

            var parmas = data.Params;
            var content = data.Html;

            if (parmas !== '') {
                zebra_alert('Thông báo !', parmas);
                return;
            }
            if (content !== '') {
                zebra_alert('Thông báo !', content);
            }

        },
        error: function (req) { }
    });
}
//Lưu sản phẩm yêu thích
function save_product(ProductID) {

    var dataString = 'ProductID=' + ProductID;

    $.ajax({
        type: "get",
        url: "/ajax/SaveProduct.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var _Content = data.Html;

            if (_Content !== '') {
                zebra_alert('Thông báo !', _Content);
                return;
            }
        },
        error: function (data) { }
    });
}


//theo dõi shop
function flowShop(ShopID) {

    var dataString = 'ShopID=' + ShopID;

    $.ajax({
        type: "get",
        url: "/ajax/FlowShop.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {
            var _Content = data.Html;

            if (_Content !== '') {
                zebra_alert('Thông báo !', _Content);
                return;
            }
        },
        error: function (data) { }
    });
}

//cancel_order
function cancel_order(OrderID, StatusID, Content) {
    var dataString = 'OrderID=' + OrderID + '&StatusID=' + StatusID + '&Content=' + Content;


    $.ajax({
        type: "post",
        url: "/ajax/CancelOrder.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {

            var parmas = data.Params;
            var content = data.Html;

            if (parmas !== '') {
                zebra_alert('Thông báo !', parmas);
                return;
            }
            if (content !== '') {
                zebra_alert('Thông báo !', content);
                location.reload();
            }

        },
        error: function (req) { }
    });
}

//cancel_order
function update_order(OrderID, StatusID, Content) {
    var dataString = 'OrderID=' + OrderID + '&StatusID=' + StatusID + '&Content=' + Content;


    $.ajax({
        type: "post",
        url: "/ajax/UpdateOrder.html",
        data: dataString,
        dataType: 'json',
        success: function (data) {

            var parmas = data.Params;
            var content = data.Html;

            if (parmas !== '') {
                zebra_alert('Thông báo !', parmas);
                return;
            }
            if (content !== '') {
                zebra_alert('Thông báo !', content);
                //location.reload();
            }

        },
        error: function (req) { }
    });
}