function get_district(CityID) {

    var dataString = 'CityID=' + CityID + '&DistrictID=' + $('#DistrictID').val();
    //$('.loading').show();

    $.ajax({
        type: "get",
        url: "/ajax/GetDistrict.html",
        data: dataString,
        dataType: "xml",
        success: function (req) {

            //$('.loading').hide();
            //$('.phantrang a.xemthem').show();

            $(req).find('Item').each(function () {
                var content = $(this).find('Html').text();

                $('#list_district').html(content);
            });

        },
        error: function (req) { }
    });
}

function get_search(keyword) {

    var dataString = 'keyword=' + encodeURIComponent(keyword);
    //$('.loading').show();

    $.ajax({
        type: "get",
        url: "/ajax/GetSearch.html",
        data: dataString,
        dataType: "xml",
        success: function (req) {

            //$('.loading').hide();
            //$('.phantrang a.xemthem').show();

            $(req).find('Item').each(function () {
                var content = $(this).find('Html').text();

                $('#search-autocomplete').show();
                $('#search-autocomplete').html(content);
                
            });

        },
        error: function (req) { }
    });
}