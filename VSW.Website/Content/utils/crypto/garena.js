function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function refreshCaptcha() {
    captcha_key = uuid().replace(/-/g, '');
    var sso_captcha = $('#sso_captcha_image');
    if (sso_captcha.is(":visible")) {
        $(sso_captcha).find('img').attr('src', CAPTCHA_SERVICE + '?key=' + captcha_key);
    }
}

function preLogin(loginName, password) {

    var time = new Date().getTime();
    var dataString = 'LoginName=' + loginName + '&Time=' + time;

    jQuery.ajax({
        type: "get",
        url: "/CP/Ajax/GarenaPreLogin.aspx",
        data: dataString,
        dataType: "xml",
        success: function (req) {

            jQuery(req).find("Item").each(function () {
                var content = jQuery(this).find("Html").text();

                var v1 = JSON.parse(content).v1;
                var v2 = JSON.parse(content).v2;
                var account = JSON.parse(content).account;
                
                var passwordMd5 = CryptoJS.MD5(password);
                var passwordKey = CryptoJS.SHA256(CryptoJS.SHA256(passwordMd5 + v1) + v2);
                var encryptedPassword = CryptoJS.AES.encrypt(passwordMd5, passwordKey, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.NoPadding });
                encryptedPassword = CryptoJS.enc.Base64.parse(encryptedPassword.toString()).toString(CryptoJS.enc.Hex);

                login(loginName, encryptedPassword, time);
            });

        },
        error: function () { }
    });
}

function login(loginName, password, time) {

    var dataString = 'LoginName=' + loginName + '&Password=' + password + '&Time=' + time;

    jQuery.ajax({
        type: "get",
        url: "/CP/Ajax/GarenaLogin.aspx",
        data: dataString,
        dataType: "xml",
        success: function (req) {

            jQuery(req).find("Item").each(function () {
                var content = jQuery(this).find("Html").text();

                var j = JSON.parse(content);
                var error = j.error;

                if (error == 'error_auth' || error == 'error_params') {
                    jQuery('.acc-check a').html('<span class="red">Tài khoản sai</span>');
                }
                else {
                    jQuery('.acc-check a').html('<span class="blue">Tài khoản đúng</span>');
                }
            });

        },
        error: function () { }
    });
}