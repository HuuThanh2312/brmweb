﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<ModMarketingNewsEntity>;
    var model = ViewBag.Model as MBusinessMaketingModel;

%>

<div class="row">
    <div class="col-md-12">
        <div class="header-tabs-doitac">
            <div class="tabs-find-doitac" style="width: 40%;">
                <ul class="nav-justified">
                     <li class="<%=(model.StartDate==DateTime.MinValue && model.EndDate==DateTime.MinValue)?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>">Tất cả</a>
                    </li>
                    <li class="<%=(model.StartDate==DateTime.Now.AddDays(1))?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>?StartDate=<%=string.Format("{0:MM-dd-yyyy}", DateTime.Now.AddDays(1)) %>">Sắp diễn ra</a>
                    </li>
                    <li class="<%=(model.StartDate==DateTime.Now.AddDays(-5))?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>?StartDate=<%=string.Format("{0:MM-dd-yyyy}", DateTime.Now.AddDays(-5)) %>">Đang diễn ra</a>
                    </li>
                    <li class="<%=model.EndDate>DateTime.MinValue&&model.StartDate==DateTime.MinValue ?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>?EndDate=<%=string.Format("{0:MM-dd-yyyy}", DateTime.Now)%>">Đã kết thúc</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row mt20">
    <div class="col-md-12">
        <ul class="list-event-creat">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                {
                    string _url = ViewPage.BuninessMarketingCpUrl + "?id=" + listItem[i].ID;
            %>
            <li>
                <div>
                    <div class="thumb-img-creat-event">
                        <div class="">
                            <a href="<%=_url %>">
                                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 500,300) %>" alt="<%=listItem[i].Name %>">
                            </a>
                        </div>
                    </div>
                    <div class="title-right-event-creat">
                        <h3 class="title-name eclip-1">
                            <a href="<%=_url %>"><%=listItem[i].Name %> - GIẢM <%=listItem[i].Percent %>% (<%=string.Format("{0:dd.MM}",listItem[i].StartDate) +" - "+string.Format("{0:dd.MM}",listItem[i].EndDate) %>)</a>
                        </h3>
                        <div class="description-event-item">
                            <%=listItem[i].Summary %>
                        </div>
                        <p><i class="fa fa-calendar"></i>&nbsp;Chương trình diễn ra từ: 00:00 - <%=string.Format("{0:dd/MM/yyyy}",listItem[i].StartDate) %></p>
                        <p>
                            <i class="fa fa-clock-o"></i>&nbsp;Thời gian đăng ký còn lại: <span class="reatime" accid="<%=listItem[i].ID%>" time="<%=Math.Round((decimal)(listItem[i].EndDate-DateTime.Now).TotalSeconds)%>"></span>
                        </p>
                        <p><i class="fa fa-money "></i>&nbsp;Áp dụng cho: <span style="color: red"><%=listItem[i].OrderPro?"Giá trị sản phẩm trong đơn hàng":"Sản phẩm" %></span></p>
                    </div>
                </div>

                <a href="<%=listItem[i].EndDate>=DateTime.Now?_url:"" %>" class="btn btn-join-event"><%=listItem[i].EndDate>=DateTime.Now?"Đăng ký ngay !":"Đã kết thúc" %></a>
            </li>
            <%} %>
        </ul>
        <div class="text-center">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
                </ul>
            </nav>
        </div>
    </div>
</div>

<script type="text/javascript" src="<%=Static.Tag("/Content/js/jquery.countdown.js")%>"></script>
<script type="text/javascript" src="<%=Static.Tag("/Content/js/jquery.countdown.min.js")%>"></script>
<script type="text/javascript">


    var time_count = 0;
    function time_format(time_end, accID) {
        if (time_end >= time_count) {
            time = time_end - time_count;
            day = Math.floor(time / 60 / 60 / 24);
            hour = Math.floor((time - (day * 24 * 60 * 60)) / 60 / 60);
            min = Math.floor((time - (day * 24 * 60 * 60) - (hour * 60 * 60)) / 60);
            sec = Math.floor((time - (day * 24 * 60 * 60) - (hour * 60 * 60) - (min * 60)));
            //if (day >= 1) {
            //    hour = hour + (day * 24);
            //}
            //if (hour < 10) {
            //    hour = "0" + hour;
            //}
            //if (min < 10) {
            //    min = "0" + min;
            //}
            //if (sec < 10) {
            //    sec = "0" + sec;
            //}
            string = day + " ngày " + hour + " giờ " + min + " phút " ;
        } else {
            string = "Đã kết thúc";
        }
        return (string);
    }

    function get_time_format() {
        var myClasses = document.getElementsByClassName('reatime');
        i = 0,
            l = myClasses.length;
        for (i; i < l; i++) {
            var time = myClasses[i].attributes['time'].value;
            var ID = myClasses[i].attributes['accid'].value;
            myClasses[i].innerHTML = time_format(time, ID);
        }
        time_count = time_count + 1;
    }
    setInterval(get_time_format, 1000);
</script>
