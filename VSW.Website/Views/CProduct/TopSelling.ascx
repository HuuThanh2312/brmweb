﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;

    var listItem = ViewBag.Data as List<ModProductEntity>;
%>

<div class="sidebar-box mt10 ">
    <div class="ttl">
        <div class="txt">
            <a href="<%=ViewPage.GetPageURL(page) %>"><%=page.Name %></a>
        </div>
    </div>
    <div class="sidebar-content">
        <ul class="listpro-view">
            <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                    string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
            %>
            <li class="item d-flex-wrap ">
                <div class="img-thumb-view">
                    <p class="reponsive-img">
                        <a href="<%=url %>">
                            <%=Utils.GetResizeFile(listItem[i].File, 4, 236, 236, "lazy", listItem[i].Name) %>
                        </a>
                    </p>
                </div>
                <div class="txt-thumb-view">
                    <h3 class="name"><a href="<%=url %>"><%=listItem[i].Name %></a></h3>
                    <p class="price-old"><%= string.Format("{0:#,##0}", listItem[i].Price) %>&nbsp;₫</p>
                    <p class="price-new"><%= string.Format("{0:#,##0}", listItem[i].Price2) %>&nbsp;₫</p>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
</div>