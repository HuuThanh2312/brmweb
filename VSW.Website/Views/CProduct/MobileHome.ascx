﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;

    var listItem = ViewBag.Data as List<ModProductEntity>;
%>

<div class="box-pro all">
    <div class="title01 all">
        <h2><a href="<%=ViewPage.GetPageURL(page) %>"><%=page.Name %></a></h2>
    </div>
    <div class="bg-pro all">
        <ul class="list_pro all">
            <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
            %>
            <li>
                <p class="image">
                    <a href="<%=url %>" title="<%=listItem[i].Name %>">
                        <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 300, 300) %>" alt="<%=listItem[i].Name %>" />
                    </a>
                </p>
                <div class="pro_info">
                    <div class="Price">
                        <span class="curr-price"><%= string.Format("{0:#,##0}", listItem[i].Price) %> VNĐ</span>
                    </div>
                    <h4><a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></h4>
                    <p class="discout">
                        <span class="priceline"><%= string.Format("{0:#,##0}", listItem[i].Price2) %> VNĐ</span>
                    </p>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
</div>