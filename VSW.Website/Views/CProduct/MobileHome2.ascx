﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;
    
    string Title = ViewBag.Title;
%>

<div class="product_all">
    <h2><a href="<%=ViewPage.GetPageURL(page) %>"><%=page.Name %></a></h2>

    <div class="swiper-container product_list1 swiper_home_<%=VSWID %>">
        <div class="swiper-wrapper">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
            %>
            <div class="swiper-slide">
                <div class="pro_img">
                    <a href="<%=url %>" title="<%=listItem[i].Name %>">
                        <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 250, 250) %>" title="<%=listItem[i].Name %>" alt="<%=listItem[i].Name %>" />
                    </a>
                </div>
                <p class="pro_price"><%=string.Format("{0:#,##0}", listItem[i].Price) %> đ</p>
                <h3><a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></h3>
            </div>
            <%} %>
        </div>

        <div class="swiper-pagination pagination_home swiper-pagination_home_<%=VSWID %>"></div>
    </div>
</div>

<script type="text/javascript">
    var swiper_home_<%=VSWID %> = new Swiper('.swiper_home_<%=VSWID %>', {
        slidesPerView: 2,
        pagination: '.swiper-pagination_home_<%=VSWID %>',
        paginationClickable: true,
        freeMode: true,
        loop: true
    });
</script>