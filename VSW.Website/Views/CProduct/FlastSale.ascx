﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var _Page = ViewBag.Page as SysPageEntity;
    if (_Page == null) return;

    var listItem = ViewBag.Data as List<ModProductEntity>;
    var _timeSale = ViewBag.Sale;

    List<ModProductEntity> randomList = new List<ModProductEntity>();

    System.Random r = new System.Random();
    int randomIndex = 0;
    while (listItem!=null && listItem.Count > 0)
    {
        randomIndex = r.Next(0, listItem.Count); //Choose a random object in the list
        randomList.Add(listItem[randomIndex]); //add it to the new, random list
        listItem.RemoveAt(randomIndex); //remove to avoid duplicates
    }
    if(listItem!=null) listItem = randomList;

%>
<script type="text/javascript" src="<%=Static.Tag("/Content/js/jquery.countdown.js")%>"></script>
<script type="text/javascript" src="<%=Static.Tag("/Content/js/jquery.countdown.min.js")%>"></script>
<section class="box-02 mb20">
    <div class="container">
        <div class="title-flash-sale positionR">
            <h2 class="title-main"><i class="fa fa-bolt"></i>&nbsp;Giá sốc mỗi giờ</h2>
            <div id="getting-started"></div>
            <%--<span class="titme-coundown">00</span>
            <span class="titme-coundown">00</span>
            <span class="titme-coundown">00</span>--%>
            <a href="<%=ViewPage.GetPageURL(_Page) %>" class="view-next" style="position: absolute; top: 0; right: 0">Xem thêm&nbsp;<i class="fa fa-angle-double-right"></i></a>
        </div>
        <div class="box-white">
            <ul class="list-flast-sale list-owl-home">
                <%for (int i = 0; listItem != null && i < (listItem.Count > 5 ? 5 : listItem.Count); i++)
                    { %>
                <li class="item-prd-sale">
                    <a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code) %>" class="click-all"></a>
                    <div class="responsive-img">
                        <img src="<%=listItem[i].File.Replace("~/","/") %>" alt="<%=listItem[i].Name %>">
                    </div>
                    <div class="price-prd-sales">
                        <%=string.Format("{0:#,##0}", listItem[i].Price) %><sup>đ</sup>
                    </div>
                    <div class="flash-sale-progress-bar flash-sale-progress-bar--home-page">
                        <div class="flash-sale-progress-bar__text">Đã bán <%=listItem[i].Sold %></div>
                        <div class="flash-sale-progress-bar__complement-wrapper flash-sale-progress-bar__complement-wrapper--home-page">
                            <div class="flash-sale-progress-bar__complement-sizer flash-sale-progress-bar__complement-sizer--home-page" style="width: <%=listItem[i].getSold%>%;">
                                <div class="flash-sale-progress-bar__complement-color"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <%} %>
            </ul>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("#getting-started")
        .countdown('<%=string.Format("{0:yyyy/MM/dd HH:mm}", _timeSale)%>', function (event) {
            //$(this).text(
            //   // event.strftime('%D ngày %H:%M:%S')

            //);
            var $this = $(this).html(event.strftime(''
                + '<span class="titme-coundown "><b> %H </b></span> '
                + '<span class="titme-coundown "><b> %M </b></span> '
                + '<span class="titme-coundown" style="color:red;"><b> %S </b></span>'));
        });
</script>
