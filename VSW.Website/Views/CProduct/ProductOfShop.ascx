﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    if (ViewPage.CurrentPage.ModuleCode == "MSearch") return;

    var listItem = ViewPage.ViewBag.ListOfShop as List<ModProductEntity>;
%>


<section class="box-sanphamphobien mb20">
    <div class="container">
        <div class="positionR">
            <h2 class="title-main">CÁC SẢN PHẨM KHÁC CÙNG SHOP&nbsp;</h2>
        </div>

        <div class="clearfix"></div>
        <div class="owl-prd-main">
            <ul class="owl-prd-phobien list-owl-home">
                <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                    {
                        string _Url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
                %>
                <li>
                    <a href="<%=_Url %>" class="click-all"></a>
                    <%-- <div class="shopee-item-card__preferred-badge-wrapper">
                        <div class="shopee-horizontal-badge shopee-preferred-seller-badge">
                            <svg class="shopee-svg-icon icon-tick" enable-background="new 0 0 15 15" viewBox="0 0 15 15" x="0" y="0">
                                <g>
                                    <path d="m6.5 13.6c-.2 0-.5-.1-.7-.2l-5.5-4.8c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l4.7 4 6.8-9.4c.3-.4.9-.5 1.4-.2.4.3.5 1 .2 1.4l-7.4 10.3c-.2.2-.4.4-.7.4 0 0 0 0-.1 0z"></path>
                                </g>
                            </svg>Yêu thích
                        </div>
                    </div>--%>
                    <!-- ribon yêu thích -->
                    <%if (listItem[i].Price2 > listItem[i].Price)
                        { %>
                    <div class="shopee-item-card__badge-wrapper">
                        <div class="shopee-badge shopee-badge--fixed-width shopee-badge--promotion">
                            <div class="shopee-badge--promotion__label-wrapper shopee-badge--promotion__label-wrapper--vi-VN">
                                <span class="percent"><%=listItem[i].SellOffPercent %>%</span>
                                <span class="shopee-badge--promotion__label-wrapper__off-label shopee-badge--promotion__label-wrapper__off-label--vi-VN">giảm</span>
                            </div>
                        </div>
                    </div>
                    <%} %>
                    <!--- ribon sale -->

                    <div class="item-product">
                        <div class="thumb-img-prd">
                            <div class="hm-reponsive">
                                <img src="<%=listItem[i].File.Replace("~/","/") %>" alt="<%=listItem[i].Name %>">
                            </div>
                            <%--<img src="images/icon/revodich.png" class="shopee-item-card__lowest-price" alt="lowest price">--%>
                        </div>

                        <div class="info-item-product">
                            <h3 class="title-product eclip-2"><%=listItem[i].Name %>
                            </h3>
                            <div class="box-price-prd">
                                <div class="shopee-item-card_original-price">₫ <%=string.Format("{0:#,##0}",listItem[i].Price2)%></div>
                                <div class="shopee-item-card__current-price shopee-item-card__current-price--free-shipping">₫ <%=string.Format("{0:#,##0}",listItem[i].Price)%></div>
                            </div>
                            <div class="bottom-icon">
                                <div class="city-prd">
                                    <i class="fa fa-heart"></i>&nbsp;<%=listItem[i].Favorite %>
                                </div>
                                <div class="lead">
                                    <div id="stars-existing" class="starrr" data-rating="3">
                                        <input id="rating-input_<%=listItem[i].ID%>" type="number" class="stars" value="<%=listItem[i].Star%>" />
                                    </div>
                                    &nbsp;<span class="gray">(<%=listItem[i].Vote %>)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <%} %>
            </ul>
        </div>
    </div>
</section>
