﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var _Page = ViewBag.Page as SysPageEntity;
    if (_Page == null) return;

    var listItem = ViewBag.Data as List<ModProductEntity>;
%>

<section class="box-02 mb20">
    <div class="container">
        <div class="title-flash-sale positionR">
            <h2 class="title-main"><%=_Page.Name %></h2>
            <a href="<%=ViewPage.GetPageURL(_Page) %>" class="view-next" style="position: absolute; top: 0; right: 0">Xem thêm&nbsp;<i class="fa fa-angle-double-right"></i></a>
        </div>
        <div class="box-white">
            <ul class="list-flast-sale list-owl-home">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                    {
                %>
                <li class="item-prd-sale">
                    <a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code) %>" class="click-all"></a>
                    <div class="responsive-img">
                        <%--<img src="<%=listItem[i]?.File.Replace("~/","/") %>" alt="<%=listItem[i].Name %>">--%>
                    </div>
                    <p class="title-trending">
                        <%=listItem[i].Name %>
                    </p>
                    <div class="price-prd-sales price-trending">
                        Từ&nbsp;<%=string.Format("{0:#,##0}", listItem[i].Price) %><sup>đ</sup>
                    </div>
                </li>
                <%} %>
            </ul>
        </div>
    </div>
</section>
