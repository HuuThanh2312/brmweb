﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;
    string _avatar = !string.IsNullOrEmpty(item.Logo) ? item.Logo.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MBusinessProductMngModel;
    var listCategory = ViewBag.Category as List<ModMenuShopEntity>;
 
%>


<!-- END PAGE BAR -->
<div class="row">
    <div class="col-md-12">
        <div class="header-tabs-doitac">
            <div class="tabs-find-doitac">
                <ul class="nav-justified">
                    <li class="active">
                        <a href="">Tất cả</a>
                    </li>
                    <li>
                        <a href="">Còn hàng</a>
                    </li>
                    <li>
                        <a href="">Hết hàng</a>
                    </li>
                    <li>
                        <a href="">Đã bị khóa</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark positionR">
                    <i class="fa fa-history"></i>
                    <span class="caption-subject bold uppercase">Quản lý sản phẩm</span>
                   <%-- <div class="div-export-import-oder">
                        <div class="dropdown">
                            <button class="dropbtn"><i class="fa fa-file-text"></i>&nbsp;Tải danh sách và nhập hàng loạt</button>
                            <div class="dropdown-content">
                                <a href="#"><i class="fa fa-upload"></i>&nbsp;tải danh sách sản phẩm</a>
                            </div>
                        </div>
                    </div>--%>
                </div>
                <div class="tools"></div>
            </div>
            <div class="portlet-body">
                <input type="text" id="keyword" value="" onchange="search_dataname(this.value)" style="display: none" />
                <select name="CategoryID" class="form-control" onchange="redriect(this.value)" id="CategoryID" style="width: 35%">
                    <option value="">Chọn Danh mục</option>
                    <%for (int i = 0; listCategory != null && i < listCategory.Count; i++)
                        {%>
                    <option class="excel" <%=listCategory[i].MenuID==model.CategoryID?"selected=\"selected\"":string.Empty %> data-name="<%=listCategory[i].Name %>" value="<%=listCategory[i].MenuID %>"><%=listCategory[i].Name %></option>
                    <%} %>
                </select>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="center">#</th>
                            <th class="center">
                                <input type="checkbox" id="SelectAll" name="SelectAll" /></th>
                            <th class="center">Sản phẩm</th>
                            <th class="center" style="width: 15%;">Danh mục
                               
                            </th>
                            <th class="center">Giá sản phẩm</th>
                            <th class="center">Kho hàng</th>
                        </tr>
                    </thead>

                    <tbody class="center">
                        <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                            {%>
                        <tr>

                            <td><%=i+1 %></td>
                            <td>
                                <input type="checkbox"  name="ProductID"  value="<%=listItem[i].ID %>" /></td>
                            <td>
                                <!-- Left-aligned media object -->
                                <div class="media">
                                    <div class="media-left">
                                        <img src="<%=string.IsNullOrEmpty(listItem[i].File)?"https://cf.shopee.vn/file/f5131b7d0cd68c2b5c3577d76a2278ad_tn": listItem[i].File.Replace("~/","/") %>" class="media-object" style="width: 60px">
                                    </div>
                                    <div class="media-body text-left">
                                        <h5>
                                            <a href="<%=ViewPage.ProductCPUrl %>?idprd=<%=listItem[i].ID %>"><%=listItem[i].Name %></a>
                                        </h5>
                                        <p>
                                            <%=listItem[i].Model %>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td><%=listItem[i].GetMenu().Name %></td>
                            <td>
                                <b><%=string.Format("{0:#,##0}", listItem[i].Price) %><sup>đ</sup></b>
                            </td>
                            <td>Số lượng: <%=listItem[i].Inventory %></td>
                        </tr>
                        <%} %>
                    </tbody>

                </table>
                <div class="text-center mt40 mb40">
                    <ul class="pagination">
                        <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
   
    //function show() {$('#keyword').slideToggle();}


    //function search_dataname(KeywordEmail) {
    //    var arrVal = document.getElementsByClassName('excel');

    //        var _showLi = '';

    //        for (var i = 0; i < arrVal.length; i++) {
    //            var name = arrVal[i].getAttribute('data-name');

    //             if (name.includes(KeywordEmail)) {
    //                _showLi += '<option class="excel" data-name="' + name + '" value="' + arrVal[i].value + '">' + name + '</option>';
    //            }
    //            else {
    //                _showLi += '<option class="excel" data-name="' + name + '" value="' + arrVal[i].value + '">' + name + '</option>';
    //            }
    //        }
    //        //alert(_showLi);
    //        $("#CategoryID").html(_showLi);
    //        $('#keyword').val('');

    //    }
    function redriect(url) {
        location.href = ' <%=ViewPage.CurrentURL +"?CategoryID="%>' + url;
    }

    
</script>