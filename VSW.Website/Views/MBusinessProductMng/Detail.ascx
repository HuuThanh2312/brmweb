﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;

    var _Product = ViewBag.Product as ModProductEntity;
    string _avatar = !string.IsNullOrEmpty(_Product.File) ? _Product.File.Replace("~/", "/") : string.Empty;
    var model = ViewBag.Model as MBusinessProductMngModel;
    var listFiles = _Product.GetFile();
%>
<script type="text/javascript" src="<%=Static.Tag("/Content/utils/ckeditor/ckeditor.js")%>"></script>

<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin sản phẩm - <%=_Product.Name%></span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Thay đổi thông tin</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">Thay ảnh mô tả</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">Ảnh chi tiết sản phẩm</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form method="post" name="form_info">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Tên sản phẩm</label>
                                                    <input type="text" placeholder="Phụ kiện mai hoàng" disabled="" value="<%=_Product.Name %>" class="form-control" />
                                                </div>

                                                <div class="col-md-4">
                                                    <label class="control-label">Mã sản phẩm</label>
                                                    <input type="text" disabled="" value="<%=_Product.Model %>" class="form-control" />
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="hidden" id="FromCityID" value="<%=_Product.FromCityID %>" name="FromCityID" class="form-control" />
                                                    <label class="control-label">Kho hàng (Vận chuyển từ Quận/Huyện)</label>
                                                    <div class="_2wGlTU">
                                                        <div class="-TChkK">
                                                            <span class="_17013z" onclick="getDistrict_ghn();showCity();" id="name_cty"><%=WebLogin.CurrentUser!=null? WebLogin.CurrentUser.Address:"Chọn nơi vận chuyển" %></span><i onclick="getDistrict_ghn();showCity();" style="margin-left: 5px" class="fa fa-chevron-down"></i>

                                                        </div>
                                                        <div class="shopee-address-picker" style="display: none;">
                                                            <div class="shopee-address-picker__search-bar">
                                                                <input placeholder="Tìm" value="" id="keyword-search" onchange="search_city(this.value)">
                                                            </div>
                                                            <div class="shopee-address-picker__list-wrapper">
                                                                <ul class="shopee-address-picker__current-level-list" id="list_city">
                                                                </ul>
                                                                <ul class="shopee-address-picker__current-level-list" style="display: none!important;" id="list_district">
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Giá bán</label>
                                                    <input type="text" placeholder="Giá bán" name="Price" value="<%= _Product.Price %>" class="form-control" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Giá gốc <span style="text-decoration: line-through; color: #7e7e7e">(giá gạch chân)</span></label>
                                                    <input type="text" placeholder="Giác gạch chân" value="<%= _Product.Price2 %>" name="Price2" class="form-control" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Giá nhập từ Berich</label>
                                                    <input type="text" disabled="" value="<%=string.Format("{0:#,##0}", _Product.CostPrice) +" VNĐ"%>" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <label class="control-label">Số lượng còn</label>
                                                    <input type="number" name="Inventory" value="<%=_Product.Inventory %>" class="form-control" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Số lượng đã bán</label>
                                                    <input type="number" disabled="" value="<%=_Product.Sold %>" class="form-control" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Số lượt xem</label>
                                                    <input type="number" disabled="" value="<%=_Product.View %>" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Chuyên mục</label>
                                                    <select id="MenuID" name="MenuID" class="form-control" onchange="GetProperties(this.value)">
                                                        <option value="">- Chọn chuyên mục sản phẩm -</option>
                                                        <%=Utils.ShowDdlMenuByType2("Product", 1, _Product.MenuID) %>
                                                    </select>
                                                </div>
                                                <div class="col-md-8" id="list_property">
                                                    <label>Thuộc tính</label>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="">SEO - Tiêu đề trang:</label>
                                                    <textarea name="PageTitle" class="form-control" rows="3" placeholder="SEO - Tiêu đề trang sản phẩm"><%=_Product.PageTitle %></textarea>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">SEO - Từ khóa:</label>
                                                    <textarea name="PageKeywords" class="form-control" rows="3" placeholder="SEO - Từ khóa sản phẩm"><%=_Product.PageKeywords %></textarea>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">SEO - Mô tả:</label>
                                                    <textarea name="PageDescription" class="form-control" rows="3" placeholder="SEO - Mô tả sản phẩm"><%=_Product.PageDescription %></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="">Mô tả sản phẩm:</label>
                                                    <textarea name="Summary" class="form-control" rows="5" placeholder="mô tả sản phẩm"><%=_Product.Summary %></textarea>
                                                </div>
                                                <div class="col-md-8">
                                                    <label for="">Mô tả chi tiết sản phẩm:</label>
                                                    <textarea class="form-control ckeditor" name="Content" id="Content" rows="" placeholder="Chi tiết sản phẩm"><%=_Product.Content %></textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Thời gian khuyến mại</label>
                                                    <input type="datetime-local" name="FlastSale" value="<%=string.Format("{0:yyyy-MM-ddThh:mm}", _Product.FlastSale<=DateTime.MinValue ? DateTime.Now : _Product.FlastSale )%>" class="form-control date-picker input-daterange" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Mã bảo mật <sup>(*)</sup></label>
                                                    <div>
                                                        <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                                        <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                                            <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Nhập mã bảo mật</label>
                                                    <input type="text" placeholder="Mã bảo mật" name="ValidCode" id="ValidCode" required="required" class="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="margiv-top-10 text-center">
                                            <button type="submit" style="margin-top: 10px" name="_vsw_action[ProductMngPOST]" class="btn green">Cập nhật</button>
                                        </div>

                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_2">
                                    <form method="post" name="form_avatar" enctype="multipart/form-data" role="form">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="<%=_avatar %>" alt="" />
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                <div class="text-center">
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new">Chọn ảnh đại diện </span>
                                                        <span class="fileinput-exists">Chọn ảnh khác </span>
                                                        <input type="file" name="upload-logo" onchange="previewFile('logo')">
                                                    </span>
                                                    <%--<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">Xóa </a>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" name="_vsw_action[ImagePost]" class="btn green">Lưu lại thay đổi</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_3">
                                    <form method="post" name="form_banner" enctype="multipart/form-data" role="form">
                                        <div class="form-group">
                                            <div class="list_upload_image">
                                                <%for (int i = 0; listFiles != null && i < listFiles.Count; i++)
                                                    {%>
                                                <div class="item-upload">
                                                    <div class="avatar-uploader_avatar">
                                                        <input type="file" class="avatar-uploader_file-input file_view" data-id="FileOverView_<%=i %>" name="FileOverView" title="<%=File.GetFileName(listFiles[i].File) %>" accept="image/*" />
                                                        <div class="avatar-uploader_avatar-image">
                                                            <img src="<%=listFiles[i].File.Replace("~/","/") %>" width="150" height="150" class="FileOverView_Preview_<%=i %>" />
                                                        </div>

                                                    </div>
                                                </div>
                                                <%} %>
                                                <%for (int i = 0; (listFiles == null || listFiles.Count < 6) && i < (listFiles != null ? 6 - listFiles.Count : 6); i++)
                                                    {%>
                                                <div class="item-upload">
                                                    <div class="avatar-uploader_avatar">
                                                        <input type="file" class="avatar-uploader_file-input file_view" data-id="FileOverView_<%=i %>" name="FileOverView" accept="image/*" />
                                                        <div class="avatar-uploader_avatar-image">
                                                            <img src="" class="FileOverView_Preview_<%=i %>" />
                                                        </div>

                                                    </div>
                                                </div>
                                                <%} %>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" name="_vsw_action[ImagesPOST]" class="btn green">Lưu lại thay đổi</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    <%if (_Product.FromCityID > 0)
    { %>

    show_city('<%=_Product.FromCityID%>');
    <%} %>

    $('.districtID_ghn').on("click", function () {
        var _id = $(this).data('districtid');
        var _text = $(this).text();
        $('#FromCityID').val(_id);

        document.getElementById("name_cty").textContent = _text;
        // onclick="document.getElementById(\'FromCityID\').value=\''+$(list_district[i]).data('districtid')+'\';document.getElementById(\'name_cty\').textContent=\''+$(list_district[i]).text()+'\';"
        showCity();
    })

    function search_city(keyword) {
        var list_district = $('.districtIDghn');
        var hshs = "";
        for (var i = 0; list_district != null && i < list_district.length; i++) {
            var haah = $(list_district[i]).text().toUpperCase();

            if (haah.includes(keyword.toUpperCase())) {
                hshs += '<li class="shopee-address-picker__current-level-list-item districtID_ghn" data-districtid="' + $(list_district[i]).data('districtid') + '">' + $(list_district[i]).text() + '</li>';

            }
        }

        $('#list_city').html(hshs);

        $('.districtID_ghn').on("click", function () {
            var _id = $(this).data('districtid');
            var _text = $(this).text();
            $('#FromCityID').val(_id);

            document.getElementById("name_cty").textContent = _text;
            // onclick="document.getElementById(\'FromCityID\').value=\''+$(list_district[i]).data('districtid')+'\';document.getElementById(\'name_cty\').textContent=\''+$(list_district[i]).text()+'\';"
            showCity();
        });
    }
    function showCity() {

        $('.shopee-address-picker').slideToggle();
    }


    $('input[type="file"]').change(function () {
        var id = $(this).data('id');
        var arrID = id.split('_');
        if (arrID.length === 2) {
            var index = id.replace(arrID[0] + '_', '');
            readURL(this, arrID[0] + '_Preview_' + index);
        }
        else {
            readURL(this, id + '_Preview');
        }

        return;
    });

    function readURL(input, preview) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.' + preview).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function previewFile(type) {
        if (type === 'logo') {
            var img = $('.fileinput-new img');
            var img2 = $('.profile-userpic img');
            var file = document.querySelector('input[name=upload-logo]').files[0];
            var reader = new FileReader();

            if (file) {
                reader.readAsDataURL(file);
            } else {
                img.attr('src', '');
            }

            reader.onloadend = function () {
                img.attr('src', reader.result);
                img2.attr('src', reader.result);
            }
        }

    }
    //thuoc tinh
    function GetProperties(MenuID) {
        var ranNum = Math.floor(Math.random() * 999999);
        var dataString = "MenuID=" + MenuID + "&LangID=1&ProductID=<%=_Product.ID%>&rnd=" + ranNum;

        $.ajax({
            url: "/Ajax/GetProperties2.aspx",
            type: "get",
            data: dataString,
            dataType: 'json',
            success: function (data) {
                var content = data.Html;

                $("#list_property").html('<label>Thuộc tính</label>' + content);
            },
            error: function (status) { }
        });
    }

    if (<%=_Product.MenuID%> > 0) GetProperties('<%=_Product.MenuID%>');
    else GetProperties($('#MenuID').val());
</script>

