﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<Level>;
    if (listItem == null) return;
    //int _level1 = listItem.FindAll(o => o.Level == 1).Count;
    //int _level2 = listItem.FindAll(o => o.Level == 2).Count;
    //int _level3 = listItem.FindAll(o => o.Level == 3).Count;
    //int _level4 = listItem.FindAll(o => o.Level > 3 && o.Level < 14).Count;
    //int _level14 = listItem.FindAll(o => o.Level >= 14).Count;
%>


<h1 class="page-title">Chi tiết doanh thu hoa hồng</h1>

<div class="row">
    <div class="col-xs-12 user-info-name">
        <div class="tiles mr0">
            <div class="tile bg-blue-hoki tile-user-main" style="width: 100% !important; cursor: auto; min-height: 230px">
                <div class="tile-body">
                    <img src="<%=_avatar %>" alt="" style="margin-bottom: 90px;">
                    <h4 class="name"><%=item.Name %></h4>
                    <p><i class="fa fa-line-chart ic-mg"></i>&nbsp;Doanh số tiêu dùng (Tháng 5/2018):&nbsp;<span class=""><b>0&nbsp;<small>VNĐ</small></b></span>&nbsp;-&nbsp;</p>
                    <p><i class="fa fa-bookmark-o ic-mg"></i>&nbsp;Cấp độ thành viên:&nbsp;<strong><%=item.Activity?"Chính thức":"Chưa chính thức" %></strong></p>
                    <%--<p>
                        <i class="fa fa-money ic-mg"></i>&nbsp;Hoa hồng (Tháng 5/2018) - Hiện tại:&nbsp;<span class=""><b>0&nbsp;<small>VNĐ</small></b></span>
                    </p>--%>
                   <%-- <p>Tổng số thành viên cấp 1:&nbsp;<span><strong><%=_level1 %></strong></span></p>
                    <p>Tổng số thành viên cấp 2:&nbsp;<span><strong><%=_level2 %></strong></span></p>
                    <p>Tổng số thành viên cấp 3:&nbsp;<span><strong><%=_level3 %></strong></span></p>
                    <p>Tổng số thành viên cấp 4 - 13 :&nbsp;<span><strong><%=_level4 %></strong></span></p>
                    <p>Tổng số thành viên cấp 14 - 22 :&nbsp;<span><strong><%=_level14 %></strong></span></p>--%>
                </div>
                <div class="tile-object">
                    <div class="number">Tham gia <%=string.Format("{0:dd/MM/yyyy}", item.Created) %></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money font-blue-hoki"></i>
                    <span class="caption-subject bold font-blue-hoki uppercase">Hoa hồng thụ động </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-bordered table-hover center">
                        <thead>
                            <tr class="active">
                                <th class="center">Level thành viên </th>
                                <th class="center">Tổng số thành viên </th>
                                <th class="center">Tổng số doanh thu </th>
                                <th class="center" colspan="2">Tổng hoa hồng (VNĐ)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for (int i = 0; i < listItem.Count; i++)
                                {
                                    // int levelCount = listItem.FindAll(o => o.Level == listItem[i].Level).Count;
                                    var data = listItem[i].Data;

                            %>
                            <tr>
                               <%-- <td><%=listItem[i].Data.FindAll %> </td>
                                <td><%=levelCount %></td>--%>
                                <td>0 </td>
                                <td>0 </td>
                                <td>
                                    <span class="label label-sm label-danger">Không đạt</span><br>
                                    <a class="lydo" data-toggle="modal" href="#modal-lydo">&nbsp;Lý do&nbsp;<i class="fa fa-question-circle"></i></a>
                                </td>
                            </tr>
                            <%} %>
                        </tbody>
                        <thead>
                            <tr class="success">
                                <th class="center">Tổng số </th>
                                <th class="center"><%=listItem.Count %> thành viên </th>
                                <th class="center price">100 <small>VNĐ</small></th>
                                <th class="center price" colspan="2">100 <small>VNĐ</small></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money font-blue-hoki"></i>
                    <span class="caption-subject bold font-blue-hoki uppercase">Hoa hồng hỗ trợ phát triển hệ thống </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-bordered table-hover center">
                        <thead>
                            <tr class="active">
                                <th class="center">Cây thành viên </th>
                                <th class="center">Tổng số thành viên </th>
                                <th class="center">Tổng số doanh thu </th>
                                <th class="center" colspan="2">Tổng hoa hồng (VNĐ)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1 </td>
                                <td>2 </td>
                                <td>0 </td>
                                <td>0 </td>
                                <td>
                                    <span class="label label-sm label-danger">Không đạt</span><br>
                                    <a class="lydo" data-toggle="modal" href="#modal-lydo">&nbsp;Lý do&nbsp;<i class="fa fa-question-circle"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2 </td>
                                <td>7 </td>
                                <td>0 </td>
                                <td>0 </td>
                                <td>
                                    <span class="label label-sm label-danger">Không đạt </span>
                                    <br>
                                    <a class="lydo" data-toggle="modal" href="#modal-lydo">&nbsp;Lý do&nbsp;<i class="fa fa-question-circle"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>3 </td>
                                <td>11 </td>
                                <td>100 </td>
                                <td>100 </td>
                                <td>
                                    <span class="label label-sm label-success">Đạt điều kiện </span>
                                </td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="success">
                                <th class="center">Tổng số </th>
                                <th class="center">20 thành viên </th>
                                <th class="center price">100 <small>VNĐ</small></th>
                                <th class="center price" colspan="2">100 <small>VNĐ</small></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money font-blue-hoki"></i>
                    <span class="caption-subject bold font-blue-hoki uppercase">Hoa hồng phát triển hệ thống </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-bordered table-hover center">
                        <thead>
                            <tr class="active">
                                <th class="center">Cây thành viên </th>
                                <th class="center">Tổng số thành viên </th>
                                <th class="center">Tổng số doanh thu </th>
                                <th class="center" colspan="2">Tổng hoa hồng (VNĐ)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1 </td>
                                <td>2 </td>
                                <td>0 </td>
                                <td>0 </td>
                                <td>
                                    <span class="label label-sm label-danger">Không đạt</span><br>
                                    <a class="lydo" data-toggle="modal" href="#modal-lydo">&nbsp;Lý do&nbsp;<i class="fa fa-question-circle"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2 </td>
                                <td>7 </td>
                                <td>0 </td>
                                <td>0 </td>
                                <td>
                                    <span class="label label-sm label-danger">Không đạt </span>
                                    <br>
                                    <a class="lydo" data-toggle="modal" href="#modal-lydo">&nbsp;Lý do&nbsp;<i class="fa fa-question-circle"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>3 </td>
                                <td>11 </td>
                                <td>100</td>
                                <td>100</td>
                                <td>
                                    <span class="label label-sm label-success">Đạt điều kiện </span>
                                </td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="success">
                                <th class="center">Tổng số </th>
                                <th class="center">20 thành viên </th>
                                <th class="center price">100 <small>VNĐ</small></th>
                                <th class="center price" colspan="2">100 <small>VNĐ</small></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
