﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;

%>


<div class="row mt20">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="fa fa-history"></i>
                    <span class="caption-subject bold uppercase">Lịch sử hoa hồng</span>
                </div>
                <div class="tools"></div>
            </div>
            <div class="portlet-body">
                <div class="form-group input-find-hoahong-date">
                    <div class="input-group input-medium date date-picker pull-left" data-date-format="dd-mm-yyyy">
                        <input type="text" class="form-control" readonly>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <a href="javascript:;" class="btn blue pull-left ml5">
                        <i class="fa fa-filter"></i>&nbsp;Xem hoa hồng
                                            </a>
                </div>

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="center">Ngày thanh toán </th>
                            <th class="center">Hoa hồng tiêu dùng </th>
                            <th class="center">Hoa hồng thụ động </th>
                            <th class="center">Hoa hồng hỗ trợ phát triển hệ thống </th>
                            <th class="center">Hoa hồng phát triển hệ thống </th>
                            <th class="center">Tổng </th>
                            <th class="center">Chi tiết </th>
                        </tr>
                    </thead>

                    <tbody class="center">
                        <tr>
                            <td>"N/a"</td>
                            <td>"N/a"</td>
                            <td>"N/a"</td>
                            <td>"N/a"</td>
                            <td>"N/a"</td>
                            <td>"N/a"</td>
                            <td>"N/a"</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
