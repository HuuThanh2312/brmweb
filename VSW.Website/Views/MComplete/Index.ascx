﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModOrderEntity;
    if (item == null) return;
    var model = ViewBag.Model as MCompleteModel;
    var listDetail = item.GetOrderDetail();
    ModProductEntity _Product = null;
    string _product = string.Empty;
    foreach (var o in listDetail)
    {
        if (o == null) continue;

        _Product = ModProductService.Instance.GetByID(o[0]);
        _product += _Product != null ?  _Product.Name  + "<br/>": "";
    }
    

%>

<section class="box-cate">
    <div class="container">
        <div class="row">
            <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="Orderssuccess">
                    <p class="check text-center">
                        <i class="fa fa-check-square-o"></i>
                    </p>
                    <%if (item != null)
                        { %>
                    <p class="text-center text-uppercase f140">Cảm ơn Quý khách đã mua khóa học <span style="color: red;"><%=_product%></span> tại Hệ thống đào tạo trực tuyến thông minh SMARTRAIN.VN!</p>
                    <p class="text-center f100 fontItalic">Mã đăng ký khóa học: <%=item.Code %></p>
                    <p class="f120">Quý khách thanh toán cho chúng tôi bằng cách <span><%=item.GetPayment().Name %></span></p>
                    <p class="f120">Chúng tôi sẽ tiến hàng kiểm tra và gửi email thanh toán cho Quý khách.</p>
                    <p class="f120">Nếu trong 24h chưa nhận được phản hồi, Quý khách có thể liên hệ Hotline: 0918.924.388 để được giải đáp.</p>
                    <p class="complate" style="color: red; font-weight: 600; font-size: 15px">Xin chân thành cảm ơn!</p>
                    <%} %>
                </div>
            </section>
            <!-- /leftcate -->
            <!-- /rightcate -->
        </div>
    </div>
</section>
