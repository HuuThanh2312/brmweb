﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl"%>

<%
    var item = ViewBag.Data as ModOrderEntity;
    var model = ViewBag.Model as MCompleteModel;

    var _Cart = new Cart();

    long TotalPrice = 0;
    int TotalQuantity = 0;
    
    var _WebUser = WebLogin.CurrentUser;
    if (_WebUser != null)
    {
        item.Name = _WebUser.Name;
        item.Email = _WebUser.Email;
        item.Phone = _WebUser.Phone;
        item.Address = _WebUser.Address;
        item.CityID = _WebUser.CityID;
        item.DistrictID = _WebUser.DistrictID;
    }
%>

<div class="giohang">
    <ul>
        <% for (int i = 0; i < _Cart.Items.Count; i++){
               var _Product = ModProductService.Instance.GetByID(_Cart.Items[i].ProductID);
               if (_Product == null) continue;

               long _PriceCombo = _Product.PriceCombo(_Cart.Items[i].Quantity);
               
               var _Color = ModColorService.Instance.GetByID(_Cart.Items[i].ColorID);
               var _Size = ModSizeService.Instance.GetByID(_Cart.Items[i].SizeID);

               TotalPrice += _Cart.Items[i].Quantity * _Product.Price;
               string _Url = ViewPage.GetURL(_Product.MenuID, _Product.Code);
        %>
        <li>
            <p class="cart_image">
                <a href="<%=_Url %>">
                    <img src="<%=Utils.GetResizeFile(_Product.File, 4, 500, 500)%>" alt="<%=_Product.Name %>" />
                </a>
            </p>
            <h3><a href="<%=_Url %>"><%=_Product.Name %></a></h3>

            <%if (_Color != null){%>
            <p><span>- Màu sắc: <%=_Color.Name%></span></p>
            <%} %>

            <%if (_Size != null){%>
            <p><span>- Kích cỡ: <%=_Size.Name%></span></p>
            <%} %>

            <p>
                <span class="price_cart"><%=string.Format("{0:#,##0}", _Product.Price) %></span> x 
                <%= _Cart.Items[i].Quantity %>
                =
                <span class="price_cart"><%=string.Format("{0:#,##0}", _PriceCombo) %></span>
            </p>
        </li>
        <%} %>
    </ul>

    <div class="customer_info">Thông tin khách hàng</div>
    <ul class="customer_complete">
        <li><label>Họ và tên:</label><span><%=item.Name %></span></li>
        <li><label>Email:</label><span><%=item.Email %></span></li>
        <li><label>Điện thoại:</label><span><%=item.Phone %></span></li>
        <li><label>Địa chỉ giao hàng:</label><span><%=item.Address %></span></li>
        <li><label>Tỉnh thành:</label><span><%=item.GetDistrict().Name + " - " + item.GetCity().Name %></span></li>
    </ul>

    <div class="customer_info">Thông tin giao hàng</div>
    <ul class="customer_complete">
        <li><label>Tình trạng:</label><span>{RS:Checkout_Status}</span></li>
        <li><label>Thời gian giao hàng dự kiến:</label><span>{RS:Checkout_Date}</span></li>
    </ul>

    <form method="post" name="complete_form">
        <div class="complete_button"><a href="javascript:void(0)" onclick="document.complete_form.submit(); this.disabled = true" class="quick_button">Gửi đơn hàng</a></div>

        <input type="hidden" name="_vsw_action[AddPOST]" />
        <input type="submit" name="_vsw_action[AddPOST]" style="display: none;" />
    </form>

    <div class="customer_info">Thông tin liên hệ</div>
    <div class="complete_content">{RS:Checkout_Contact}</div>
</div>