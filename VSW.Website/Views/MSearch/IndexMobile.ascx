﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MSearchModel;
%>

<input type="hidden" id="Page" value="<%=model.page + 1 %>" />
<input type="hidden" id="MenuID" value="<%=ViewPage.CurrentPage.MenuID %>" />
<input type="hidden" id="Sort" value="<%=VSW.Core.Web.HttpQueryString.GetValue("sort").ToString() %>" />
<input type="hidden" id="Atr" value="<%=VSW.Core.Web.HttpQueryString.GetValue("atr").ToString() %>" />

<section class="box-home all">
    <div class="container-fluid">
        <div class="panel-home-body all">
            <ul class="temp-pro-item">
                <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                        string _Url = ViewPage.GetURL(listItem[i].Code);
                %>
                <li>
                    <p class="p-image">
                        <a href="<%=_Url %>" title="<%=listItem[i].Name %>">
                            <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 500, 500) %>" title="<%=listItem[i].Name %>" alt="<%=listItem[i].Name %>" />
                        </a>
                    </p>
                    <p class="name"><%=listItem[i].Name %></p>
                    <p class="gift">Chương trình khuyến mại</p>
                    <div class="price-box">
                        <span class="c-price"><%=string.Format("{0:#,##0}", listItem[i].Price) %>&nbsp;₫</span>
                        <span class="c-price-old"><%=string.Format("{0:#,##0}", listItem[i].Price2) %>&nbsp;₫</span>
                    </div>
                    <a href="<%=_Url %>" class="alink-all" title="<%=listItem[i].Name %>" rel="nofollow"></a>
                </li>
                <%} %>
            </ul>
            <div id="load-more">Xem thêm sản phẩm</div>
        </div>
    </div>
</section>
<div id="loading"></div>