﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MSearchModel;
%>

<div class="product_all">
    <h2><a href="<%=ViewPage.CurrentURL %>"><%=ViewPage.CurrentPage.Name %></a></h2>
    <ul class="product_list1">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){
            string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
        %>
        <li>
            <div class="pro_img">
                <a href="<%=url %>">
                    <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 250, 250) %>" alt="<%=listItem[i].Name %>" />
                </a>
            </div>
            <p class="pro_price"><%=string.Format("{0:#,##0}", listItem[i].Price) %> đ</p>
            <h3><a href="<%=url %>"><%=listItem[i].Name %></a></h3>
        </li>
        <%} %>
    </ul>
</div>

<div class="viewmore">
    <a href="javascript:void(0)" class="xemthem" onclick="get_product()">Xem thêm</a>
</div>

<input type="hidden" name="Command" id="Command" value="<%=ViewPage.ViewBag.Command != null ? ViewPage.ViewBag.Command : "" %>" />
<input type="hidden" name="Keyword" id="Keyword" value="<%=model.keyword %>" />
<input type="hidden" name="MenuID" id="MenuID" value="<%=ViewPage.CurrentPage.MenuID %>" />
<input type="hidden" name="ManufacturerID" id="ManufacturerID" value="<%=model.t %>" />
<input type="hidden" name="CountryID" id="CountryID" value="<%=model.c %>" />
<input type="hidden" name="State" id="State" value="0" />
<input type="hidden" name="PageSize" id="PageSize" value="<%=model.PageSize %>" />
<input type="hidden" name="Page" id="Page" value="1" />