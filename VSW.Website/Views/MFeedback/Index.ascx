﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModFeedbackEntity;
    var model = ViewBag.Model as MFeedbackModel;
%>

<div id="static-page">
    <div class="entry-content border border-radius">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase font-family-bold">Công ty Cổ phần Dịch vụ Thương Mại Tổng hợp VinCommerce</h2>
            </div>
        </div>
        <p><i class="adr icon home"></i><span class="text-uppercase font-family-bold">Trụ sở chính:</span> Số 72 Lê Thánh Tôn, Phường Bến Nghé, Quận 1, Thành phố Hồ Chí Minh, Việt Nam.</p>
        <p><i class="adr icon home"></i><span class="text-uppercase font-family-bold">Địa chỉ giao dịch:</span></p>
        <div class="row">
            <div class="col-md-6">
                <span class="color-text-strong font-family-bold">Tại thành phố Hồ Chí Minh:</span>
                <p>Tầng B1, Vincom Mega Mall Thảo Điền, 159-161 Xa Lộ Hà Nội, Phường Thảo Điền, Quận 2, Thành Phố Hồ Chí Minh.</p>
                <p><i class="adr icon phone"></i>HCM: 028-3975.9568</p>
                <p><i class="adr icon email"></i>Email: <a href="mailto:cskh@adayroi.com">cskh@adayroi.com</a></p>
            </div>
            <div class="col-md-6">
                <span class="color-text-strong font-family-bold">Tại Hà Nội:</span>
                <p>Tower 2, Times City, 458 Minh Khai, Phường Vĩnh Tuy,<br />
                    Quận Hai Bà Trưng, Thành Phố Hà Nội.</p>
                <p><i class="adr icon phone"></i>HN: 024-3975.9568</p>
            </div>
        </div>
        <div class="contact-form">
            <form method="post" name="feedback_form" id="contactForm">
                <div class="form-group clearfix">
                    <label class="control-label col-xs-4">Họ và tên <span>*</span></label>
                    <div class="col-xs-4">
                        <input type="text" name="Name" class="form-control pull-left" value="<%=item.Name %>" placeholder="Nhập họ và tên đầy đủ" maxlength="100" />
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="control-label col-xs-4">Địa chỉ email <span>*</span></label>
                    <div class="col-xs-4">
                        <input type="text" name="Email" class="form-control pull-left" value="<%=item.Email %>" placeholder="Nhập địa chỉ email" maxlength="100" />
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="control-label col-xs-4">Số điện thoại <span>*</span></label>
                    <div class="col-xs-4">
                        <input type="text" name="Phone" class="form-control pull-left" value="<%=item.Phone %>" placeholder="Nhập số điện thoại liên hệ" maxlength="100" />
                    </div>
                </div>
                
                <div class="form-group clearfix">
                    <label class="control-label col-xs-4">Địa chỉ</label>
                    <div class="col-xs-4">
                        <input type="text" name="Address" class="form-control pull-left" value="<%=item.Address %>" placeholder="Nhập địa chỉ liên hệ" maxlength="100" />
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="control-label col-xs-4">Tiêu đề <span>*</span></label>
                    <div class="col-xs-4">
                        <input type="text" name="Title" class="form-control pull-left" value="<%=item.Title %>" placeholder="Nhập vắn tắt yêu cầu" maxlength="100" />
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="control-label col-xs-4">Câu hỏi <span>*</span></label>
                    <div class="col-xs-4">
                        <textarea name="Content" class="form-control" placeholder="Nhập mô tả chi tiết yêu cầu" rows="5" maxlength="4000"><%=item.Content %></textarea>
                    </div>
                </div>

                <div class="form-submit form-group clearfix">
                    <label class="control-label col-xs-4"></label>
                    <div class="col-xs-4">
                        <button type="submit"name="_vsw_action[AddPOST]" class="adr button bg-blue">Gửi yêu cầu</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>