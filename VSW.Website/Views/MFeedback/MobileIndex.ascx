﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl"%>

<% 
    var item = ViewBag.Data as ModFeedbackEntity;
    var model = ViewBag.Model as MFeedbackModel;
%>

<div class="giohang">

    <h1><a href="<%=ViewPage.CurrentUrl %>"><%=ViewPage.CurrentPage.Name %></a></h1>

    <div class="feedback_content">
        <div class="text-contact">{RS:Web_Feedback}</div>
    </div>

    <form method="post" name="feedback_form">
        <ul class="customer_form">
            <li>
                <label>Họ và tên<span>*</span></label>
                <p><input type="text" name="Name" value="<%=item.Name %>" placeholder="Nhập họ và tên" /></p>
            </li>
            <li>
                <label>Email<span>*</span></label>
                <p><input type="email" name="Email" value="<%=item.Email %>" placeholder="Nhập địa chỉ email" /></p>
            </li>
            <li>
                <label>Điện thoại</label>
                <p><input type="text" name="Phone" value="<%=item.Phone %>" placeholder="Nhập số điện thoại" /></p>
            </li>
            <li>
                <label>Nội dung<span>*</span></label>
                <p><textarea rows="" cols="" name="Content" placeholder="Nhập nội dung liên hệ"><%=item.Content %></textarea></p>
            </li>
            <li>
                <label>Mã bảo mật</label>
                <p>
                    <input type="text" class="security" name="ValidCode" id="ValidCode" required="required" autocomplete="off" placeholder="Mã bảo mật" />
                    <img src="/ajax/Security.html" class="vAlignMiddle pl10" onclick="change_captcha()" id="imgValidCode" alt="security code" />
                </p>
            </li>
        </ul>

        <div class="float_button">
            <a href="javascript:void(0)" onclick="document.feedback_form.submit()" class="datmua">Gửi liên hệ</a>
            <input type="hidden" name="_vsw_action[AddPOST]" />
            <input type="submit" name="_vsw_action[AddPOST]" style="display: none" />
        </div>
    </form>

</div>