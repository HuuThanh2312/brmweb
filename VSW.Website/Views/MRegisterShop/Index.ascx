﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModShopEntity;
    if (item == null) return;
    string _avatar = !string.IsNullOrEmpty(item.Logo) ? item.Logo.Replace("~/", "/") : string.Empty;
    string _banner = !string.IsNullOrEmpty(item.Banner) ? item.Banner.Replace("~/", "/") : string.Empty;
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>

<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Trạng thái: <%=item.Activity?"Đã xác thực":"Chưa xác thực" %> </div>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin tài khoản của đối tác</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Thông tin đăng ký</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form method="post" name="form_info">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Tên hiển thị shop</label>
                                                    <input type="text" placeholder="Phụ kiện mai hoàng" name="Name" value="<%=item.Name %>" class="form-control" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Đường dẫn url shop <sup id="email" style="color:red">(*)</sup></label>
                                                    <input type="text" placeholder="VD: shop.bee" name="Url" onchange="check_acc(this.value,'Shop'); return false;" value="<%=item.Url %>" class="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Email shop</label>
                                                    <input type="text" placeholder="" name="Email" value="<%=item.Email %>" class="form-control" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Số điện thoại</label>
                                                    <input type="text" placeholder="" value="<%=item.Phone %>" name="Phone" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Tỉnh/Thành phố</label>
                                                    <select class="form-control" name="CityID" id="CityID" onchange="get_child(this.value, '<%=item.DistrictID %>')" required="required">
                                                        <option value="0"></option>
                                                        <%=Utils.ShowDdlMenuByType2("City",ViewPage.CurrentLang.ID, item.CityID) %>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Quận/Huyện</label>
                                                    <select class="form-control" name="DistrictID" id="DistrictID" required="required">
                                                        <option value="0"></option>
                                                        <%=Utils.ShowDdlMenuByType2("City",ViewPage.CurrentLang.ID, item.DistrictID) %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Địa chỉ chi tiết</label>
                                                    <input type="text" placeholder="" name="Address" value="<%=item.Address %>" class="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Mã bảo mật <sup>(*)</sup></label>
                                                <div style="height: 10px;">
                                                    <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                                    <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                                        <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Nhập mã bảo mật</label>
                                                <input type="text" placeholder="Mã bảo mật" name="ValidCode" id="ValidCode" required="required" class="form-control" />
                                            </div>
                                        </div>

                                        <div class="margiv-top-10 text-center">
                                            <button type="submit" style="margin-top: 10px" name="_vsw_action[RegisterShopPOST]" class="btn green">Đăng ký</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


