﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    // var _accBank = item.GetBank();
    //var listBank = ViewBag.Data as List<ModBankingUserEntity>;
    // var listTranctions = item.GetListTransaction();
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>


<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                </div>

                <%--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle red btn-sm">Chi tiết hoa hồng</button>
                </div>--%>

                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                <i class="fa fa-home"></i>
                                <span class="title">Trình quản lý</span>
                                <%-- <span class="selected"></span>--%>
                            </a>
                        </li>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>

            </div>

        </div>

        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light" style="min-height: 388px;">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Đăng ký sử dụng dịch vụ chuyển khoản</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form action="" method="get" accept-charset="utf-8">
                                <p><strong>Tên tôi là:&nbsp;</strong><%=item.Name %></p>
                                <p><strong>Tôi đăng ký hạn mức chuyển khoản trong ngày trên chương trình VCB như sau:</strong></p>
                                <div class="mt-radio-list">
                                    <label class="mt-radio">
                                        <input type="radio" name="Payment" id="hanmuc1" value="option1" checked="">
                                        20.000.000 VNĐ(Hai mươi triệu đồng)
                                                           
                                        <span></span>
                                    </label>
                                    <label class="mt-radio">
                                        <input type="radio" name="Payment" id="hanmuc2" value="option2">
                                        50.000.000 VNĐ(Năm mươi triệu đồng)
                                                           
                                        <span></span>
                                    </label>
                                </div>
                                <div class="portlet box blue-hoki">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            Hợp đồng sử dụng dịch vụ NHDT: 
                                                           
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height: 400px; line-height: 1.6" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                                            <p style="margin-top: 0px;">
                                                Quy định tham gia phát triển hệ thống người tiêu dùng tại Công ty cổ phần TMDV BeRichMart Việt Nam. Bằng việc đăng ký tham gia trở thành Thành Viên của Công ty, Bạn đồng ý chấp nhận và tuân thủ các quy định sau đây:<br>
                                                <strong class="blue">1. Thông tin cá nhân:</strong><br>
                                                - Thành viên đồng ý cung cấp cho Công ty cổ phần TMDV BeRichMart Việt Nam các thông tin trung thực về bản thân bằng cách điền vào "Phiếu đăng ký" và "Hồ sơ". Các thông tin này sẽ giúp Công ty thực hiện tốt hơn việc quản lý hồ sơ và công tác chăm sóc khách hàng.<br>
                                                - BeRichMart cam kết bảo mật thông tin cá nhân của thành viên và chỉ cung cấp cho bên thứ 3 khi có sự đồng ý của thành viên.<br>
                                                <br>
                                                <strong class="blue">2. Quyền lợi Thành viên:</strong><br>
                                                - Thành viên được sử dụng các tiện ích của Công ty:<br>
                                                - Các giao dịch thanh toán trên tài khoản thanh toán BRM.<br>
                                                - Hưởng quyền lợi về chính sách bán hàng dành cho Thành viên :<br>
                                                - Hoa hồng tiêu dùng 10% khi thành viên thanh toán bằng thẻ và 5% khi thành viên thanh toán bằng tiền mặt (trừ thẳng vào đơn hàng khi mua hàng áp dụng cho tất cả các thành viên chính thức và thành viên kết nối)<br>
                                                - Hoa hồng giới thiệu Thành viên.<br>
                                                - Hoa hồng thụ động.<br>
                                                - Được tham gia quỹ thưởng dành cho Thành viên.<br>
                                                - Được chuyển đổi hoa hồng thành tiền mặt.<br>
                                                - Được cấp văn phòng làm việc tại website: www.BeRichMart.com.vn<br>
                                                <br>
                                                <strong class="blue">3. Nghĩa vụ Thành viên:</strong><br>
                                                a. Thanh toán tiền hàng đầy đủ cho Công ty.<br>
                                                b. Tư vấn chính xác về chính sách bán hàng và chế độ trả thưởng của Công ty.<br>
                                                c. Thành viên có trách nhiệm hướng dẫn, hỗ trợ Thành viên trực thuộc hệ thống Thành viên của mình.<br>
                                                d. Cung cấp đầy đủ, chính xác thông tin xác thực thành viên trực thuộc hệ thống Thành viên của Công ty BRM.<br>
                                                <br>
                                                <strong class="blue">4. Những điều khoản cấm:</strong><br>
                                                a. Nghiêm cấm tuyệt đối việc đăng ký giả mạo hoặc dùng nhiều địa chỉ chứng minh thư khác nhau để đăng ký nhiều tài khoản cho một người.<br>
                                                b. Thành viên sẽ không được phép sử dụng bất kỳ nhãn hiệu thương mại, tên, biểu tượng, khẩu ngữ của Công ty vào mục đích khác mà không được sự đồng ý của Công ty ngoài việc phân phối, quảng cáo như đã được Công ty đồng ý.<br>
                                                c. Nghiêm cấm tuyệt đối việc dùng danh nghĩa Công ty cổ phần TMDV BeRichMart Việt Nam và lợi dụng mạng lưới thành viên của Công ty vào mục đích cá nhân, để quảng bá các chương trình kinh doanh không có liên quan đến Công ty bằng e-mail hoặc trên mạng Internet.<br>
                                                d. Công ty cổ phần TMDV BeRichMart Việt Nam sẽ áp dụng các chế tài xử lý vi phạm đối với các trường hợp vi phạm những điều khoản nói trên. Các chế tài xử lý vi phạm có thể là cảnh cáo, phạt tiền, xóa tài khoản vĩnh viễn khỏi danh sách thành viên hoặc tổng hợp cả 3 biện pháp trên. Việc lựa chọn biện pháp phạt phụ thuộc vào mức độ và số lần vi phạm của thành viên.
                                                <br>
                                                <strong class="blue">5. Hủy bỏ tài khoản:</strong><br>
                                                a. Thành viên có quyền thông báo cho Công ty cổ phần TMDV BeRichMart Việt Nam về quyết định ngừng tham gia phát triển hệ thống mạng lưới vào bất cứ lúc nào. Trường hợp này đồng nghĩa với việc Thành viên đồng ý hủy bỏ toàn bộ những quyền lợi của mình với tư cách là thành viên hệ thống tính đến thời điểm đó.<br>
                                                b. Công ty cổ phần TMDV BeRichMart Việt Nam có quyền xóa tài khoản của Bạn nếu tài khoản đó không có hoạt động mua hàng trong vòng 6 tháng<br>
                                                c. Công ty cổ phần TMDV BeRichMart Việt Nam có quyền xóa tên và tài khoản của Bạn nếu Bạn có những hành động đi ngược lại với quyền lợi của công ty như nêu trong mục 4 ("Những điều khoản cấm").<br>
                                                d. Trong trường hợp tài khoản bị xóa vì các lý do nêu trên, việc đăng ký lại tài khoản khác tại Công ty cổ phần TMDV BeRichMart Việt Nam chỉ có thể được thực hiện sau thời gian ít nhất là 6 tháng.
                                                <br>
                                                e. Trong trường hợp thành viên không nâng cấp lên thành viên chính thức thì công ty có thể thay thế hoặc xóa bỏ bất cứ lúc nào.<br>
                                                <br>
                                                <strong class="blue">6. Các quy định khác:</strong><br>
                                                a. Khi quý khách giới thiệu cho khách hàng, quý khách tuyệt đối phải giới thiệu một cách trung thực.<br>
                                                b. Công ty cổ phần TMDV BeRichMart Việt Nam có quyền thay đổi điều kiện và phương thức rút tiền và thông báo đến thành viên bằng việc công bố công khai trên website hoặc gửi tin nhắn tới mã số Thành viên .<br>
                                                c. Công ty cổ phần TMDV BeRichMart Việt Nam có thể bổ sung, mở rộng hoặc thu hẹp các chương trình kinh doanh theo nhu cầu. Những thay đổi (nếu có) sẽ được thông báo đến các Thành viên bằng tin nhắn về mã số Thành viên hoặc công bố trên website và chỉ được áp dụng từ sau ngày công bố.
                                                <br>
                                                <br>
                                                <strong class="blue">7. Thuế (Thuế Giá Trị Gia Tăng và Thuế Thu Nhập cá nhân)</strong> 7.1. Mỗi bên tự chịu trách nhiệm đối với các nghĩa vụ thuế của mình trước cơ quan thuế của Việt Nam theo quy định của phát luật. khi được BeRichMart yêu cầu, các Thành viên sẽ phải cung cấp các bằng chứng chứng minh mình đã hoàn thành các nghĩa vụ thuế (nếu có). Theo quy định của pháp luật, BeRichMart sẽ khấu trừ thuế thu nhập cá nhân 10% - 20% trên thu nhập hoa hồng của Thành Viên để thực hiện việc nộp thuế thay cho các thành viên.<br>
                                                7.2. Thành viên đồng ý rằng BeRichMart có quyền<br>
                                                a) Thông báo cho các cơ quan có thẩm quyền của Việt Nam mọi thông tin liên quan đến việc bán hợp tác giữa BeRichMart và Thành viên; và
                                                <br>
                                                b) Nếu pháp luật quy định hoặc theo yêu cầu của các cơ quan thuế Việt Nam, BeRichMart sẽ thực hiệp việc thu các khoản thuế đến hạn áp dụng cho các hoạt động của Thành viên theo hợp đồng này và trực tiếp nộp các khoản thuế đó cho các cơ quan thuế của Việt Nam.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-checkbox-list">
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox">
                                        Tôi đã tìm hiểu về cách thức bảo mật và có trách nhiệm bảo mật thông tin về tên truy cập và mật khẩu sử dụng dịch vụ VCB- IB@nking của Ngân hàng TMCP Ngoại Thương Việt Nam cung cấp.
                                                           
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox">
                                        Tôi đã đọc, hiểu rõ, đồng ý và cam kết tuân thủ các điều khoản, điều khiển của Hợp đồng sử dụng dịch vụ Ngân hàng điện tử. Hướng dẫn sử dụng dịch vụ VCB-iB@nking và các quy định, thông báo của Ngân hàng TMCP Ngoại Thương Việt Nam liên quan đến dịch vụ VCB-iB@nking
                                                           
                                        <span></span>
                                    </label>
                                </div>
                                <p class="text-center">
                                    <button type="submit" class="btn blue btn-outline">Xác nhận</button>
                                </p>
                            </form>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
        </div>

    </div>

</div>
