﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>

<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Trạng thái: <%=item.Activity?"Chính thức":"Chưa chính thức" %> </div>
                </div>
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle red btn-sm">Chi tiết hoa hồng</button>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                               <i class="fa fa-home"></i>
                                <span class="title">Trình quản lý</span>
                                <%-- <span class="selected"></span>--%>
                            </a>
                        </li>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light" style="min-height: 388px;">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">SMS Banking - Thay đổi số điện thoại truy vấn</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <form method="post" class="form-horizontal form-thanhtoan" accept-charset="utf-8">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tên truy cập</label>
                                        <div class="col-md-8">
                                            <input type="text" name="" class="form-control" value="<%=item.LoginName %>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Số điện thoại sử dụng</label>
                                        <div class="col-md-8">
                                            <input type="text" name="" class="form-control" value="<%=item.Phone %>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Số tài khoản mặc định</label>
                                        <div class="col-md-8">
                                            <input type="text" name="" class="form-control" value="<%=item.GetCardBcoin()!=null ? item.GetCardBcoin().AccountBcoin :"" %>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Số điện thoại mới</label>
                                        <div class="col-md-8">
                                            <input type="text" name="Phone" id="Password" class="form-control" placeholder="Nhập số đt mới" value="">
                                            <span class="help-block">Qúy khách đã đăng ký số điện thoại nhận bằng SMS. Khi xác nhận hoàn tất, sau giây lát BeRichMart sẽ gửi mã giao dịch vào số điện thoại đăng ký của quý khách </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Số điện thoại nhận tin:</label>
                                        <div class="col-md-8">
                                            <input type="text" name="" class="form-control" value="<%=item.Phone %>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <label class="col-md-4 control-label">Mã bảo mật:</label>
                                            <div class="col-xs-2">
                                                <input type="text" name="ValidCode" id="ValidCode" required="required" class="form-control" placeholder="">
                                            </div>
                                            <div class="col-xs-4">
                                                <img src="/ajax/Security.html" id="imgValidCode" style="height: 35px;">
                                                <a href="javascript:void(0)" onclick="change_captcha()" class="btn btn-icon-only default">
                                                    <i class="fa fa-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center mt10">
                                        <button type="submit" name="_vsw_action[InfoPost]" class="btn blue btn-outline">Cập nhật</button>
                                        <button type="button" onclick="clearText()" class="btn dark btn-outline">Nhập lại</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    function previewFile() {
        var img = $('.fileinput-new img');
        var file = document.querySelector('input[type=file]').files[0];
        var reader = new FileReader();

        if (file) {
            reader.readAsDataURL(file);
        } else {
            img.attr('src', '');
        }

        reader.onloadend = function () {
            img.attr('src', reader.result);
        }
    }
</script>

