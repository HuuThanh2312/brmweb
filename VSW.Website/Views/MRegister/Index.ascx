﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModWebUserEntity;
%>

<h1 class="title-manage">Đăng ký tài khoản mới</h1>
<div class="bgmanage-content">
    <form method="post" class="form-list" name="product_up" enctype="multipart/form-data">
        <div class="account-profile col-lg-8 col-md-12 col-sm-12 col-12 fl">
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Email đăng nhập</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Email" value="<%=item.Email %>" placeholder="Nhập địa chỉ email của bạn" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Mật khẩu</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="password" class="form-control" name="Password" placeholder="Nhập mật khẩu từ 6 - 12 ký tự" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Nhập lại mật khẩu</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="password" class="form-control" name="Password2" placeholder="Nhập mật khẩu từ 6 - 12 ký tự" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Họ và tên</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Name" value="<%=item.Name %>" placeholder="Nhập họ tên đầy đủ của bạn" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Số điện thoại</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Phone" value="<%=item.Phone %>" placeholder="Nhập số điện thoại liên hệ" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Địa chỉ</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Address" value="<%=item.Address %>" placeholder="Nhập địa chỉ liên hệ" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Mã bảo mật</label>
                </div>
                <div class="text-left col-md-9 security">
                    <div class="row">
                        <div class="text-left col-md-5">
                            <input type="text" class="form-control" name="ValidCode" id="ValidCode" value="" />
                        </div>
                        <div class="text-left col-md-5">
                            <img src="/ajax/Security.html" class="form-control" id="imgValidCode" alt="ValidCode" />
                        </div>
                        <div class="text-left col-md-2">
                            <a href="javascript:void(0)" class="form-control" onclick="change_captcha()" title="Đổi mã khác"><i class="fa fa-refresh fa-lg"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label></label>
                </div>
                <div class="text-left col-md-9">
                    <button type="submit" class="btn btn-blue" name="_vsw_action[AddPOST]">Đăng ký</button>
                </div>
            </div>
        </div>
    </form>
</div>