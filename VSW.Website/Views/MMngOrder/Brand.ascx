﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MProductModel;

    string icon = ViewPage.CurrentPage.Icon;
    if (string.IsNullOrEmpty(ViewPage.CurrentPage.Icon))
        icon = ViewPage.CurrentPage.Parent.Icon;

    if (!string.IsNullOrEmpty(icon))
        icon = icon.Replace("~/", "/");
%>

<div class="slide-top-ctgr">
    <div id="owl-banner-hang-ctgr-top" class="owl-carousel">
        <a href="">
            <img src="http://saigonhomekitchen.vn/image/data/khuyen-mai-30-4/electrolux/km_bosch_banner.jpg" alt="">
        </a>
    </div>
</div>
<div class="pls-menu">
    <h1 class="pls-menu-head">
        <a href="<%=ViewPage.CurrentURL %>" title="<%=ViewPage.CurrentPage.Name %>">
            <span><img src="<%=icon %>" alt="<%=ViewPage.CurrentPage.Name %>" /></span><%=ViewPage.CurrentPage.Name %>
        </a>
    </h1>
</div>
<span class="cl"></span>
<div class="note-ctgr"><%=Utils.GetHtmlForSeo(ViewPage.CurrentPage.TopContent) %></div>

<ul class="list-prd-hang-ctgr">
    <%for (var i = 0; listItem != null && i < listItem.Count; i++){
            string url = ViewPage.GetURL(listItem[i].Code);
    %>
    <li class="bi">
        <a href="<%=url %>" class="cen" title="<%=listItem[i].Name %>">
            <img src="https://beptu.cdn.vccloud.vn/wp-content/uploads/2016/06/bep-tu-taka-i2b2-350x214.png" class="img-100" alt="">
        </a>
        <div class="bii">
            <a href="<%=url %>" title="<%=listItem[i].Name %>">
                <h3><%=listItem[i].Name %></h3>
            </a>
            <p><%=listItem[i].Summary %></p>
            <ul class="advan">
                <li>Bộ lọc nhôm có chức năng ngăn ko cho dầu mỡ đi vào bên trong máy</li>
                <li>Thiết kế lắp âm nên tiết kiệm diện tích, phù hợp với không gian bếp nhỏ</li>
                <li>Độ ồn 68 dB, không phát ra tiếng ồn lớn</li>
                <li>Công suất hút mùi lớn có thể loại bỏ toàn bộ mùi thức ăn cho không gian bếp thêm sạch sẽ</li>
            </ul>
        </div>
        <div class="flexCol">
            <span class="s s5"></span>
            <ul>
                <%=listItem[i].PropertyHtml %>
            </ul>
            <div class="bpri">Giá bán<span><%=string.Format("{0:#,##0}", listItem[i].Price) %>₫</span></div>
            <div>
                <a href="<%=url %>" class="bb1 bcam">XEM NGAY</a>
                <p class="bb2 bac bxanh add-cart" data-id="<%=listItem[i].ID %>">MUA NGAY</p>
            </div>
        </div>
    </li>
    <%} %>
</ul>

<ul class="phantrang">
    <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
</ul>