﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listOrder = ViewBag.Data as List<ModOrderEntity>;
    var model = ViewBag.Model as MMngOrderModel;
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;

    var listPage = ViewBag.Page as List<SysPageEntity>;
    var RStatus = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.Type == "Status" && o.ParentID == 0)
                                            .ToSingle_Cache();

    List<WebMenuEntity> listStatus = null;
    if (RStatus != null)
    {
        listStatus = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ParentID == RStatus.ID)
                                             .OrderByAsc(o => new { o.Order, o.ID })
                                            .ToList_Cache();
    }
%>


<section class="manager-login">
    <div class="container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="">
                <!-- BEGIN PAGE TITLE-->

                <div class="row mt20">
                    <div class="col-md-12">
                        <div class="profile-sidebar">
                            <!-- PORTLET MAIN -->
                            <div class="portlet light profile-sidebar-portlet ">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name"><%=item.Name %></div>
                                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                                </div>
                                <div class="profile-usermenu">
                                    <ul class="nav">
                                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                                            { %>
                                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="profile-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="nav-menu-donmua">
                                        <ul class="item-menu-donmua nav-justified">
                                            <li class="<%=model.StatusID<1 ?"active":string.Empty %>">
                                                <a href="<%=ViewPage.CurrentURL %>">Tất cả</a>
                                            </li>
                                            <%for (int i = 0; listStatus != null && i < listStatus.Count; i++)
                                                { %>
                                            <li class="<%=listStatus[i].ID==model.StatusID?"active":string.Empty %>">
                                                <a href="<%=ViewPage.CurrentURL %>?StatusID=<%=listStatus[i].ID %>"><%=listStatus[i].Name %></a>
                                            </li>
                                            <%} %>
                                        </ul>
                                    </div>
                                    <div class="body-content-mng">

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr class="title">
                                                        <th class="text-center">#</th>
                                                        <th class="text-center" style="width: 30%;">Sản phẩm</th>
                                                        <th class="text-center">Mã đơn hàng</th>
                                                        <th class="text-center">Số lượng</th>
                                                        <th class="text-center">Tình trạng</th>
                                                        <th class="text-center">Hình thức thanh toán</th>
                                                        <th class="text-center">Thao tác</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%for (int i = 0; listOrder != null && i < listOrder.Count; i++)
                                                        {
                                                            var _OrderDetail = ModOrderDetailService.Instance.CreateQuery()
                                                                                .Where(o => o.OrderID == listOrder[i].ID)
                                                                                .Where(model.StatusID > 0, o => o.StatusID == model.StatusID)
                                                                                .ToList_Cache();

                                                    %>
                                                    <%for (int j = 0; _OrderDetail != null && j < _OrderDetail.Count; j++)
                                                        {

                                                            var _Product = ModProductService.Instance.GetByID_Cache(_OrderDetail[j].ProductID);
                                                            if (_Product == null) continue;
                                                            string _File = !string.IsNullOrEmpty(_Product.File) ? _Product.File.Replace("~/", "/") : string.Empty;
                                                            // long _total = _OrderDetail[j].Price * _OrderDetail[j].Quantity;
                                                    %>
                                                    <tr>
                                                        <td><%=i +1 + j %></td>
                                                        <td>
                                                            <!-- Left-aligned media object -->
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <img src="<%=_File %>" class="media-object" style="width: 60px">
                                                                </div>
                                                                <div class="media-body text-left">
                                                                    <h5><a href="<%=ViewPage.GetURL(_Product.MenuID, _Product.Code) %>"><%=_Product.Name %></a></h5>
                                                                    <p>Mã SP: <%=_Product.Model %></p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td style="color: red; font-weight: 600;"><%=listOrder[i].Code %></td>
                                                        <td><%=_OrderDetail[j].Quantity %></td>
                                                        <td><%=_OrderDetail[j].GetStatus().Name %></td>
                                                        <td><%=listOrder[i].GetPayment().Name %></td>
                                                        <td><a href="<%=ViewPage.MngOrderUrl %>?id=<%=_OrderDetail[j].ID %>" title="Cập nhật" class="btn-edit-prd text-red">Chi tiết đơn hàng</a></td>
                                                    </tr>
                                                    <%} %>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PROFILE CONTENT -->
                    </div>

                </div>
                <!-- END CONTENT BODY -->
            </div>
        </div>
        <!-- END CONTAINER -->
    </div>
</section>
