﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModOrderEntity;
    var model = ViewBag.Model as MViewCartModel;
    var cart = new Cart();
    var IsView = new Cart("Store");
    var listProduct = ModProductService.Instance.CreateQuery()
                                           .Where(o => o.Activity == true && o.ActivityPrice == true)
                                           .OrderByDesc(o => new { o.View, o.Order })
                                           .Take(20)
                                           .ToList_Cache();
    List<ModProductEntity> randomList = new List<ModProductEntity>();

    System.Random r = new System.Random();
    int randomIndex = 0;
    while (listProduct != null && listProduct.Count > 0)
    {
        randomIndex = r.Next(0, listProduct.Count);
        randomList.Add(listProduct[randomIndex]);
        listProduct.RemoveAt(randomIndex);
    }
    listProduct = randomList;

    long total = 0;
%>

<form method="post" name="cart_form">


    <section class="section-cart mb20">
        <div class="container">
            <div class="bx-cart bx-cart-01 all">
                <div class="col-table-01">
                    <div class="checkbox checkbox-warning">
                        <input id="checkboxa" type="checkbox" checked="">
                        <label for="checkboxa"></label>
                    </div>
                </div>
                <div class="col-table-02">
                    <span>Sản phẩm</span>
                </div>
                <div class="col-table-03">
                    <span>Đơn giá</span>
                </div>
                <div class="col-table-03">
                    <span>Số lượng</span>
                </div>
                <div class="col-table-03">
                    <span>Phí vận chuyển</span>
                </div>
                <div class="col-table-03">
                    <span>Thành tiền</span>
                </div>

                <div class="col-table-04">
                    <span>Thao tác</span>
                </div>
            </div>

            <div class="bx-cart-02 all">
                <div class="header-bx-cart all">
                    <div class="col-table-01">
                        <div class="checkbox checkbox-warning">
                            <input id="checkboxb" type="checkbox" checked="">
                            <label for="checkboxb"></label>
                        </div>
                    </div>
                    <div class="col-table-02">
                        <span>
                            <img src="/Content/skins/images/icon/shop.png" style="margin-right: 5px; height: 15px" alt="">dupham92</span>
                    </div>
                    <div class="w40 pull-right p10 text-right">
                        <img src="/Content/skins/images/icon/xu.png" style="height: 20px; margin-top: -4px;" alt="">&nbsp;Có Thể Sử Dụng Becoin Xu
                    </div>
                </div>
                <div class="clearfix"></div>
                <%for (int i = 0; i < cart.Items.Count; i++)
                    {
                        var product = ModProductService.Instance.GetByID(cart.Items[i].ProductID);
                        if (product == null) continue;

                        var listGift = product.GetGift();

                        total += cart.Items[i].Quantity * product.Price + cart.Items[i].ShippingPrice;
                        string url = ViewPage.GetURL(product.MenuID, product.Code);
                %>
                <ul class="list-cart-shop">
                    <li class="col-table-01">
                        <div class="checkbox checkbox-warning">
                            <input id="checkboxc" type="checkbox" checked="">
                            <label for="checkboxc"></label>
                        </div>
                    </li>

                    <li class="col-table-02">
                        <div class="thumb-img-name-prd">
                            <div class="w70 pull-left">
                                <div class="thumnail-img-left">
                                    <div class="hm-reponsive">
                                        <a href="<%=url %>">
                                            <img src="<%=product.File.Replace("~/","/") %>" alt="<%=product.Name %>" style="width: 100%">
                                        </a>
                                    </div>
                                </div>
                                <div class="thumb-body-right">
                                    <a href="<%=url %>" class="eclip-2 gray"><%=product.Name %></a>
                                </div>
                            </div>
                            <%--  <div class="w30 pull-left">
                                <select name="" class="form-control" style="margin-top: 15px">
                                    <option value="">Màu đen</option>
                                </select>
                            </div>--%>
                        </div>
                    </li>
                    <li class="col-table-03 text-center">
                        <span class="cart-item__unit-price cart-item__unit-price--before">₫ <%=string.Format("{0:#,##0}",product.Price2) %></span>
                        <span class="cart-item__unit-price cart-item__unit-price--after">₫  <%=string.Format("{0:#,##0}",product.Price) %></span>
                    </li>
                    <li class="col-table-03">
                        <div class="input-group number-spinner" style="">
                            <span class="input-group-btn">
                                <button class="btn btn-number-gr" type="button" onclick="changeQuantity('<%=i %>','<%=url %>','down')"><i class="fa fa-minus"></i></button>
                            </span>
                            <input style="border-color: #d4d4d4; color: #d7381b;" type="text" id="Quantity" class="form-control text-center" value="<%=cart.Items[i].Quantity %>">
                            <span class="input-group-btn">
                                <button class="btn btn-number-gr" type="button" onclick="changeQuantity('<%=i %>','<%=url %>','up')"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </li>
                    <li class="col-table-03">
                        <span class="text-color text-center-block">₫ <%=Utils.FormatNumber(cart.Items[i].ShippingPrice) %></span>
                    </li>
                    <li class="col-table-03">
                        <span class="text-color text-center-block">₫ <%=string.Format("{0:#,##0}",cart.Items[i].Quantity * product.Price + cart.Items[i].ShippingPrice) %></span>
                    </li>
                    <li class="col-table-04">
                        <a href="javascrpit:void(0)" onclick="delete_cart('<%=i %>','<%=url %>')" class="delete-cart gray text-center-block">Xóa</a>
                    </li>
                </ul>
                <%} %>

                <div class="clearfix"></div>
               <%-- <div class="footer-bx-cart gray">
                    <i class="fa fa-truck blues"></i>&nbsp;Miễn Phí Vận Chuyển for orders from ₫99.000 (up to ₫40.000)
                </div>--%>
            </div>
            <div class="clearfix"></div>
            <div class="mb30"></div>

            <div class="bx-total-cart all">
                <div class="col-table-01">
                    <div class="checkbox checkbox-warning">
                        <input id="checkboxa" type="checkbox" checked="">
                        <label for="checkboxa"></label>
                    </div>
                </div>
                <div class="col-table-02">
                    <a href="" class="gray">Chọn tất cả (<%=cart.Items.Count %>)</a>
                </div>
                <div class="w40 pull-left text-right mr20">
                    <div class="total-price-cart mb10">
                        Tổng tiền hàng (<%=cart.Items.Count %> sản phẩm):&nbsp;<span>₫ <%=string.Format("{0:#,##0}",total) %></span>
                    </div>
                    <p class="text-color mb0">Lựa chọn giảm giá có thể được tìm thấy ở trang Thanh Toán</p>
                </div>
                <div class="w18 pull-left">
                    <a href="<%=ViewPage.CheckOutUrl %>" class="btn button-muahang-cart">Mua Hàng</a>
                </div>
            </div>
        </div>
    </section>

    <section class="box-sanphamphobien box-prl-like-bottom mb20">
        <div class="container">
            <div class="positionR">
                <h2 class="title-main">CÓ THỂ BẠN CŨNG THÍCH</h2>
            </div>

            <div class="clearfix"></div>
            <div class="owl-prd-main">
                <ul class="owl-prd-phobien list-owl-home">
                    <%for (int i = 0; listProduct != null && i < listProduct.Count; i++)
                        { %>
                    <li>
                        <a href="<%=ViewPage.GetURL(listProduct[i].MenuID, listProduct[i].Code) %>" class="click-all"></a>
                        <div class="shopee-item-card__preferred-badge-wrapper">
                            <div class="shopee-horizontal-badge shopee-preferred-seller-badge">
                                <svg class="shopee-svg-icon icon-tick" enable-background="new 0 0 15 15" viewBox="0 0 15 15" x="0" y="0">
                                    <g>
                                        <path d="m6.5 13.6c-.2 0-.5-.1-.7-.2l-5.5-4.8c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l4.7 4 6.8-9.4c.3-.4.9-.5 1.4-.2.4.3.5 1 .2 1.4l-7.4 10.3c-.2.2-.4.4-.7.4 0 0 0 0-.1 0z"></path>
                                    </g>
                                </svg>Yêu thích
                            </div>
                        </div>
                        <!-- ribon yêu thích -->

                        <div class="shopee-item-card__badge-wrapper">
                            <div class="shopee-badge shopee-badge--fixed-width shopee-badge--promotion">
                                <div class="shopee-badge--promotion__label-wrapper shopee-badge--promotion__label-wrapper--vi-VN">
                                    <span class="percent"><%=listProduct[i].SellOffPercent %>%</span>
                                    <span class="shopee-badge--promotion__label-wrapper__off-label shopee-badge--promotion__label-wrapper__off-label--vi-VN">giảm</span>
                                </div>
                            </div>
                        </div>
                        <!--- ribon sale -->

                        <div class="item-product">
                            <div class="thumb-img-prd">
                                <div class="hm-reponsive">
                                    <img src="<%=!string.IsNullOrEmpty(listProduct[i].File)?Utils.GetResizeFile(listProduct[i].File, 4, 500,500):"" %>" alt="<%=listProduct[i].Name %>">
                                </div>
                                <%--<img src="images/icon/revodich.png" class="shopee-item-card__lowest-price" alt="lowest price">--%>
                            </div>

                            <div class="info-item-product">
                                <h3 class="title-product eclip-2"><%=listProduct[i].Name %> </h3>
                                <div class="box-price-prd">
                                    <div class="shopee-item-card_original-price">₫ <%=Utils.FormatNumber(listProduct[i].Price2) %></div>
                                    <div class="shopee-item-card__current-price shopee-item-card__current-price--free-shipping">₫ <%=Utils.FormatNumber(listProduct[i].Price) %></div>
                                </div>
                                <div class="bottom-icon">
                                    <div class="city-prd">
                                        <i class="fa fa-heart"></i>&nbsp;<%=listProduct[i].Favorite %>
                                    </div>
                                    <div class="lead">
                                        <div id="stars-existing" class="starrr" data-rating="3"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></div>
                                        &nbsp;<span class="gray">(<%=listProduct[i].Vote %>)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>
        </div>
    </section>

    <section class="box-sanphamphobien prd_vuaxem mb20">
        <div class="container">
            <div class="positionR">
                <h2 class="title-main">VỪA XEM</h2>
            </div>
            <div class="bx-vuaxem">
                <ul>
                    <%for (int i = 0; IsView != null && i < IsView.Items.Count; i++)
                        {
                            var _Product = ModProductService.Instance.GetByID_Cache(IsView.Items[i].ProductID);
                            if (_Product == null) continue;
                    %>
                    <li>
                        <a href="<%=ViewPage.GetURL(_Product.MenuID, _Product.Code) %>" class="click-all"></a>
                        <div class="hm-responsive">
                            <img src="<%=!string.IsNullOrEmpty(_Product.File)?_Product.File.Replace("~/","/"):"" %>" alt="<%=_Product.Name %>">
                        </div>
                        <p class="price text-center">
                            ₫ <%=string.Format("{0:#,##0}",_Product.Price) %>
                        </p>
                    </li>
                    <%} %>
                </ul>
            </div>
        </div>
    </section>

</form>
<script type="text/javascript">

    function changeQuantity(index, returnpart, type) {
        var _value = parseInt($('#Quantity').val());

        if (type === 'up') {
            var _valueTotal = parseInt(_value + 1);
            $('#Quantity').val(_valueTotal);
            update_cart(index, _valueTotal, returnpart);
        }
        if (type === 'down') {

            var _valueTotal = parseInt(_value - 1);
            if (_valueTotal < 1) {
                alert('Số lượng phải lớn hơn 0');
                return;
            }
            else {
                $('#Quantity').val(_valueTotal);
                update_cart(index, _valueTotal, returnpart);
            }
        }

    }

</script>
