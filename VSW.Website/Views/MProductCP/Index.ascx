﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MProductCPModel;

    var webUser = ViewBag.WebUser as ModWebUserEntity;
%>

<h1 class="title-manage">Đơn đặt hàng</h1>
<div class="bgmanage-content">
    <div class="orders-page">

        <ul class="list-order-teaser all">
            <li class="item_order">
                <div class="item_order_body">
                    <div class="order-teaser_rows">
                        <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                                string url = ViewPage.ProductUPUrl + "?id=" + listItem[i].ID;
                        %>
                        <div class="order-teaser_product">
                            <div class="order-product-bg">
                                <div class="img">
                                    <a href="<%=url %>">
                                        <img src="<%=Utils.GetResizeFile(listItem[i].File, 2, 80, 80) %>" alt="<%=listItem[i].Name %>" />
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="<%=url %>" class="name-product"><%=listItem[i].Name %></a>
                                    <ul class="status">
                                        <li><a href="<%=ViewPage.ProductDELUrl + "?id=" + listItem[i].ID %>" class="remove"><i class="fa fa-remove"></i>Xóa</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </div>
                </div>
            </li>
        </ul>

        <ul class="pagination flex-wrap all justify-content-end">
            <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
        </ul>
    </div>
</div>