﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var _accBank = item.GetBank();
    var listBank = item.GetListBank();
    var listTranctions = item.GetListTransaction();
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>


<div class="row mt20">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="row">
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin tài khoản</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table table-light">
                                    <tr>
                                        <th class="active">Tên tài khoản
                                                            </th>
                                        <td><%=item.Name %></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Tên đăng nhập</th>
                                        <td><%=item.LoginName %></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Địa chỉ</th>
                                        <td><%=item.Address %></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Số CMND/Hộ chiếu</th>
                                        <td><%=item.CodeCMND %></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Loại tiền</th>
                                        <td>VNĐ</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin số tài khoản</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table table-light">
                                    <tr>
                                        <th class="active">Số tài khoản</th>
                                        <td><%=_accBank.AccountCode %></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Số dư hiện tại</th>
                                        <td><%=string.Format("{0:#,##0}", _accBank.TotalAmount) %>&nbsp;VNĐ</td>
                                    </tr>
                                    <tr>
                                        <th class="active">Số dư khả dụng</th>
                                        <td><%=string.Format("{0:#,##0}", _accBank.TotalAvailable) %>&nbsp;VNĐ</td>
                                    </tr>
                                    <tr>
                                        <th class="active">Ngày mở tài khoản</th>
                                        <td><%=string.Format("{0:dd/MM/yyyy}", _accBank.Created<=DateTime.MinValue ? DateTime.Now:_accBank.Created) %></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Ngày thực hiện giao dịch gần nhất</th>
                                        <td><%=string.Format("{0:dd/MM/yyyy}", _accBank.Update<=DateTime.MinValue ? DateTime.Now:_accBank.Update) %></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Danh sách tài khoản</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table">
                                    <thead>
                                        <tr class="active">
                                            <th>Số tài khoản</th>
                                            <th>Loại tài khoản</th>
                                            <th>Loại tiền</th>
                                        </tr>
                                    </thead>
                                    <%for (int i = 0; listBank != null && i < listBank.Count; i++)
                                        { %>
                                    <tr>
                                        <td><%=listBank[i].AccountCode %></td>
                                        <td><%=listBank[i].Activity ? "TK Mặc định":"TK thường" %></td>
                                        <td>VNĐ</td>
                                    </tr>
                                    <%} %>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
                <div class="col-md-12">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Chi tiết giao dịch</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group input-find-hoahong-date">
                                <div class="input-group input-large date-picker input-daterange pull-left" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                    <input type="text" class="form-control" name="from" placeholder="Từ ngày">
                                    <span class="input-group-addon">to </span>
                                    <input type="text" class="form-control" name="to" placeholder="Đến ngày">
                                </div>
                                <a href="javascript:;" class="btn blue pull-left ml5">
                                    <i class="fa fa-filter"></i>&nbsp;Xem sao kê
                                                    </a>
                            </div>

                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th class="text-center">Ngày giao dịch </th>
                                        <th class="text-center">Tên giao dịch </th>
                                        <th class="text-center">Mã số giao dịch </th>
                                        <th class="text-center">Trạng thái </th>
                                        <th class="text-center">Số tiền </th>
                                        <th class="text-center">Mô tả </th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <%for (int i = 0; listTranctions != null && i < listTranctions.Count; i++)
                                        { %>
                                    <tr>
                                        <td><%=string.Format("{0:dd/MM/yyyy}" ,listTranctions[i].Created) %></td>
                                        <td><%=listTranctions[i].Name %></td>
                                        <td><%=listTranctions[i].Code %></td>
                                        <td><%=listTranctions[i].Status ? "Thành công":"Không thành công" %></td>
                                        <td><%=string.Format("{0:#,##0}", listTranctions[i].Price) %></td>
                                        <td><%=listTranctions[i].Content %></td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>

</div>
