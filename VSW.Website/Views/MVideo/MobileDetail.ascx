﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModVideoEntity;
    var listOther = ViewBag.Other as List<ModVideoEntity>;
%>
<div class="box-pro all">
    <div class="bg-new">
        <h3 class="title"><%=item.Name %></h3>
        <div class="post-meta"><b><%=string.Format("{0:F}", item.Published) %></b> </div>
        <div class="post-content clear">
            <div id="video_zone"></div>
        </div>
        <%if (listOther != null){ %>
        <div class="news_other all">
            <h3>Video cùng chuyên mục</h3>
            <ul>
                <%for (int i = 0; listOther != null && i < listOther.Count; i++){
                      string url = ViewPage.GetURL(listOther[i].MenuID, listOther[i].Code);
                %>
                <li><a href="<%=url %>"><%=listOther[i].Name %></a></li>
                <%} %>
            </ul>
        </div>
        <%} %>
    </div>
</div>

<script type="text/javascript">
    play_video('video_zone', '<%=item.File%>', '<%=item.Thumbnail%>', '100%', ($('#video_zone').width() * 2 / 3), true);
</script>
