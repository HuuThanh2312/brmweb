﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModVideoEntity>;
    var model = ViewBag.Model as MVideoModel;
%>
<div class="box-pro all">
    <div class="bg-new">
        <ul class="cont-new all">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                  string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
            %>
            <li>
                <div class="img-new">
                    <a href="<%=url %>" title="<%=listItem[i].Name %>">
                        <img src="<%=Utils.GetResizeFile(listItem[i].File, 2, 240, 116) %>" width="240" height="116" alt="<%=listItem[i].Name %>" />
                    </a>
                </div>
                <div class="right-new">
                    <h3><a href="<%=url %>"><%=listItem[i].Name %></a></h3>
                    <p><a href="<%=url %>"><%=listItem[i].Summary %></a></p>
                    <p class="list_link"><a href="<%=url %>">Xem chi tiết</a></p>
                </div>
            </li>
            <%} %>
        </ul>
        <ul class="pagination all">
            <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
        </ul>
    </div>
</div>
