﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModVideoEntity;
    var listOther = ViewBag.Other as List<ModVideoEntity>;
%>

<div class="title-cate-other"><span><%=ViewPage.CurrentPage.Name %></span></div>
<div class="box-video all">
    <div class="left-video fl ">
        <div class="dt-video all">

            <div id="video_main"></div>
            <script type="text/javascript">
                play_video('video_main', '<%=item.File%>', '<%=item.Thumbnail%>', '100%', '315', true)
            </script>

            <h1><%=item.Name %></h1>
            <div class="share-box all mt20">
                <div class="pull-left text">Chia sẻ bài viết qua:</div>
                <div class="block_share pull-right">
                    <a href="javascript:void(0)" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a>
                    <a href="javascript:void(0)" class="google" rel="nofollow"><i class="fa fa-google-plus"></i></a>
                    <a href="javascript:void(0)" class="twitter" rel="nofollow"><i class="fa fa-twitter-square"></i></a>
                    <a href="javascript:void(0)" class="email" rel="nofollow"><i class="fa fa-envelope-o"></i></a>
                    <a href="javascript:void(0)" class="print" rel="nofollow"><i class="fa fa-print"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="righ-video fr">
        <ul class="list-video-item all">
            <%for (var i = 0; listOther != null && i < listOther.Count; i++){
                string url = ViewPage.GetURL(listOther[i].MenuID, listOther[i].Code);
            %>
            <li>
                <a href="<%=url %>" class="thumb">
                    <img src="<%=Utils.GetResizeFile(listOther[i].Thumbnail, 4, 500, 500) %>" alt="<%=listOther[i].Name %>" />
                </a>
                <h3><a href="<%=url %>"><%=listOther[i].Name %></a></h3>
            </li>
            <%} %>
        </ul>
    </div>
</div>