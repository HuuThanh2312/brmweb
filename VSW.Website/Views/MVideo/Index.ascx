﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModVideoEntity>;
    var model = ViewBag.Model as MVideoModel;
%>

<ul class="gallery_list">
    <%for (var i = 0; listItem != null && i < listItem.Count; i++){
            if (string.IsNullOrEmpty(listItem[i].File)) continue;
            string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
    %>
    <li class="col1-4">
        <p class="gallery_img">
            <a class="various fancybox.iframe" href="<%=listItem[i].File.Replace("watch?v=", "v/") %>?autoplay=1">
                <img src="<%=Utils.GetResizeFile(listItem[i].Thumbnail, 4, 500, 500) %>" alt="<%=listItem[i].Name %>" />
            </a>
        </p>
        <a href="javascript:void(0)" class="gallery_name"><%=listItem[i].Name %></a>
    </li>
    <%} %>
</ul>

<ul class="paging">
    <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
</ul>
