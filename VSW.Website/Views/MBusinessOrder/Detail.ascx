﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var _detailOrder = ViewBag.Order as ModOrderDetailEntity;
    if (_detailOrder == null) return;

    var model = ViewBag.Model as MBusinessOrderModel;
    var _webUser = ViewBag.User as ModWebUserEntity;

    var _Root = WebMenuService.Instance.CreateQuery()
                                   .Where(o => o.Activity == true && o.ParentID == 0 && o.Type == "Status")
                                   .ToSingle_Cache();
    var listStatus = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.ParentID == _Root.ID)
                                    .OrderByAsc(o => new { o.Order, o.ID })
                                    .ToList_Cache();

    var _Product = _detailOrder.GetProduct();

    var _Root2 = WebMenuService.Instance.CreateQuery().Where(o => o.Activity == true && o.ParentID == 0 && o.Type == "CancelOrder").ToSingle_Cache();
    var listCancel = WebMenuService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.ParentID == _Root2.ID)
                                    .OrderByDesc(o => new { o.ID, o.Order })
                                    .ToList_Cache();
%>

<div class="row">
    <div class="col-md-12">

        <div class="manage-right">
            <input type="hidden" value="<%=_detailOrder.StatusID %>" id="StatusID" />
            <h1 class="title-manage">
                <button type="button" onclick="update_order('<%=_detailOrder.ID %>',$('#StatusID').val(),$('textarea#Content').val())" class="update_order form-control">Cập nhật đơn hàng</button></h1>
            <div class="bgmanage-content all">
                <div class="order-details_section ">
                    <div><a href="<%=ViewPage.BuninessOrderCpUrl %>"><i class="fa fa-arrow-left"></i>Quay lại</a></div>
                    <div>Mã đơn hàng <%=_detailOrder.GetOrder().Code %> </div>
                    <div>Đặt ngày  <%=string.Format("{0:dd/MM/yyyy hh:mm}", _detailOrder.GetOrder().Created) %></div>
                    <div>Tổng tiền: <b class="red"><%=string.Format("{0:#,##0}", _detailOrder.Price + _detailOrder.ShippingPrice)%> VND </b></div>
                </div>
                <div class="order-details_shipping">
                    <div class="order-details_shipping_header">
                        <div class="title"><i class="fa fa-gift"></i>ID Đơn hàng #<%=_detailOrder.GetOrder().ID %></div>
                        <%if (_detailOrder.CancelID < 1)
                            { %>
                        <div class="seller"><a href="javascript:void(0)" class="cancel">Hủy đơn hàng</a></div>
                        <%}
                            else
                            { %>
                        <div class="seller"><a href="javascript:void(0)" class="">Đã hủy đơn</a></div>
                        <%} %>
                        <div class="order-details__receipt-row" id="password-group" style="display: none;">
                            <div class="order-details__receipt-column order-details__receipt-column_right">
                                <select class="selectBox form-control" name="CancelID" id="CancelID">
                                    <option value="0">Chọn lý do hủy đơn hàng</option>
                                    <%for (int i = 0; listCancel != null && i < listCancel.Count; i++)
                                        { %>
                                    <option value="<%=listCancel[i].ID %>" <%=listCancel[i].ID == _detailOrder.CancelID?"selected":""%>><%=listCancel[i].Name %></option>
                                    <%} %>
                                </select>
                                <button type="button" class="order-submit form-control" onclick="cancel_order('<%=_detailOrder.ID %>',$('#CancelID').val(),$('textarea#Content').val())">Hủy đơn</button>
                            </div>
                        </div>
                    </div>

                    <div class="order-details_shipping_wrapper">
                        <div class="order-details_state mb20">
                            <div class="order-details_progress">
                                <div class="order-details_progress-bar">
                                    <div class="order-details_progress-bar-item"></div>
                                    <div class="order-details_progress-bar-item"></div>
                                    <div class="order-details_progress-bar-item"></div>
                                </div>
                                <div class="order-details_progress-states">
                                    <%if (_detailOrder.CancelID < 1)
                                        { %>

                                    <%for (int i = 0; listStatus != null && i < listStatus.Count; i++)
                                        {
                                            bool _isStatus = false;
                                            if (listStatus[i].ID == _detailOrder.StatusID) _isStatus = true;
                                    %>
                                    <div class="order-details_progress-states-item <%=_isStatus ? "is-current":"" %>" data-id="<%=listStatus[i].ID %>">
                                        <div class="order-details_progress-states-name <%=_isStatus ? "is-current":"" %>">
                                            <%=listStatus[i].Name %>
                                        </div>
                                    </div>
                                    <%} %>
                                    <%}
                                        else
                                        { %>
                                    <div class="order-details_progress-states-item is-current">
                                        <div class="order-details_progress-states-name is-current">
                                            Đơn hàng đã hủy: Lý do - <%=_detailOrder.GetCancel().Name %>
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                            </div>
                        </div>
                        <table class="table  order-pro-table">
                            <tbody>
                                <tr>
                                    <td class="order-pro-table-img">
                                        <a href="<%=ViewPage.GetURL(_Product.MenuID, _Product.Code) %>" title="<%=_Product.Name %>">
                                            <img src="<%=Utils.GetResizeFile(_Product.File, 4, 500,500) %>" alt="<%=_Product.Name %>"></a>
                                    </td>
                                    <td class="order-pro-table-detail">
                                        <a href="<%=ViewPage.GetURL(_Product.MenuID, _Product.Code) %>" title="<%=_Product.Name %>" class="title"><%=_Product.Name %></a>
                                        <a href="<%=ViewPage.GetURL( _Product.GetShop()) %>" title="<%=_Product.GetShop().Name %>" target="_blank">
                                            <p class="value">Shop: <%=_Product.GetShop().Name %></p>
                                        </a>
                                    </td>
                                    <td class="order-pro-table-price"><%=string.Format("{0:#,##0}", _detailOrder.Price)%> VND
                                    </td>
                                    <td class="order-pro-table-quantity">
                                        <span>×</span>
                                        <span class="value"><%=_detailOrder.Quantity %></span>
                                    </td>
                                    <td class="order-pro-table-actions"><%=string.Format("{0:#,##0}", _detailOrder.Price * _detailOrder.Quantity)%> VND
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="order-details_footer">

                    <div class="order-details_footer-column">
                        <div class="title">Thông tin giao hàng</div>
                        <div class="order-details_address">
                            <p><%=_detailOrder.GetOrder().Name %></p>
                            <p><%=_detailOrder.GetOrder().Address + " - " + _detailOrder.GetOrder().GetDistrict().Name + " - " + _detailOrder.GetOrder().GetCity().Name %></p>
                            <p><%=_detailOrder.GetOrder().Phone %></p>
                        </div>
                    </div>
                    <div class="order-details_footer-column">
                        <div class="title">Ghi chú</div>
                        <div class="order-details_address">
                            <textarea class="form-control" rows="4" name="Content" id="Content" placeholder="Ghi chú đơn hàng"><%=_detailOrder.GetOrder().Content %></textarea>
                        </div>
                    </div>

                    <div class="order-details_footer-column">
                        <div class="title">Chi tiết thanh toán</div>
                        <div class="order-details_receipt">
                            <div class="order-details_receipt-row">
                                <div>Tạm Tính </div>
                                <div><%=string.Format("{0:#,##0}", _detailOrder.Price)%> VND </div>
                            </div>
                            <div class="order-details_receipt-row">
                                <div>Phí vận chuyển </div>
                                <div><%=string.Format("{0:#,##0}",  _detailOrder.ShippingPrice)%> VND</div>
                            </div>
                            <%--<div class="order-details_receipt-row">
                        <div>Phí vận chuyển </div>
                        <div>10.000 VND</div>
                    </div><div class="order-details_receipt-row">
                        <div>Khuyến mãi </div>
                        <div><%=string.Format("{0:#,##0}",_detailOrder.Total-_pricerforOrder) %> VND </div>
                    </div>--%>
                            <div class="order-details_receipt-summary">
                                <div class="order-details_receipt-row">
                                    <div>Tổng (Chưa bao gồm VAT.)</div>
                                    <div><%=string.Format("{0:#,##0}", _detailOrder.Price * _detailOrder.Quantity +_detailOrder.ShippingPrice)%> VND </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('.cancel').click(function () {
            $('#password-group').slideToggle('slow');
        });
        $('.order-details_progress-states-item').click(function () {
            var _data = $(this).attr('data-id');
            var li = document.querySelectorAll('.order-details_progress-states-item');
            var li2 = document.querySelectorAll('.order-details_progress-states-name');

            for (var i = 0; i < li.length && i < li2.length; i++) {
                var _data2 = li[i].getAttribute('data-id');

                if (_data2 === _data) {
                    li[i].classList.add('is-current');
                    li2[i].classList.add('is-current');
                    $('#StatusID').val(_data);
                } else {
                    li[i].classList.remove('is-current');
                    li2[i].classList.remove('is-current');
                }
            }
        });
    });
</script>
