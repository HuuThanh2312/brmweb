﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;
    string _avatar = !string.IsNullOrEmpty(item.Logo) ? item.Logo.Replace("~/", "/") : string.Empty;
    var listOrder = ViewBag.Data as List<ModOrderEntity>;
    var RStatus = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.Type == "Status" && o.ParentID == 0)
                                            .ToSingle_Cache();

    List<WebMenuEntity> listStatus = null;
    if (RStatus != null)
    {
        listStatus = WebMenuService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.ParentID == RStatus.ID)
                                             .OrderByAsc(o => new { o.Order, o.ID })
                                            .ToList_Cache();
    }
    var model = ViewBag.Model as MBusinessOrderModel;
%>
<div class="row">
    <div class="col-md-12">
        <div class="header-tabs-doitac">
            <div class="tabs-find-doitac" style="width: 80%;">
                <ul class="nav-justified">
                    <li class="<%=model.StatusID<1 ?"active":string.Empty %>">
                        <a href="<%=ViewPage.CurrentURL %>">Tất cả</a>
                    </li>
                    <%for (int i = 0; listStatus != null && i < listStatus.Count; i++)
                        { %>
                    <li class="<%=listStatus[i].ID==model.StatusID?"active":string.Empty %>">
                        <a href="<%=ViewPage.CurrentURL %>?StatusID=<%=listStatus[i].ID %>"><%=listStatus[i].Name %></a>
                    </li>
                    <%} %>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark positionR">
                    <i class="fa fa-history"></i>
                    <span class="caption-subject bold uppercase">Lịch sử hoa hồng</span>
                    <%-- <div class="div-export-import-oder">
                        <div class="dropdown">
                            <button class="dropbtn"><i class="fa fa-file-text"></i>&nbsp;Tải danh sách và nhập hàng loạt</button>
                            <div class="dropdown-content">
                                <a href="#"><i class="fa fa-upload"></i>&nbsp;tải danh sách sản phẩm</a>
                            </div>
                        </div>
                    </div>--%>
                </div>
                <div class="tools"></div>
            </div>
            <div class="portlet-body">

                <div class="form-group input-find-hoahong-date">
                    <div class="input-group input-large <%--date-picker--%> input-daterange pull-left" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                        <input type="text" onfocus="(this.type='date')" class="form-control" name="from" id="start_date" value="<%=model.start> DateTime.MinValue? string.Format("{0:yyyy-MM-dd}", model.start)  :""%>" placeholder="Từ ngày">
                        <span class="input-group-addon">đến </span>
                        <input type="text" onfocus="(this.type='date')" class="form-control" name="to" id="end_date" value="<%=model.end> DateTime.MinValue? string.Format("{0:yyyy-MM-dd}", model.end)  :""%>" placeholder="Đến ngày">
                    </div>
                    <a href="javascript:;" onclick="time_order()" class="btn blue pull-left ml5"><i class="fa fa-filter"></i>&nbsp;Xem</a>
                </div>

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="center">#</th>
                            <th class="center" style="width: 40%;">Sản phẩm</th>
                            <th class="center">Mã đơn hàng</th>
                            <th class="center">Số lượng</th>
                            <th class="center">Tình trạng</th>
                            <th class="center">Hình thức thanh toán</th>
                            <th class="center">Thao tác</th>
                        </tr>
                    </thead>

                    <tbody class="center">
                        <%for (int i = 0; listOrder != null && i < listOrder.Count; i++)
                            {
                                var _OrderDetail = ModOrderDetailService.Instance.CreateQuery()
                                                    .Where(o => o.OrderID == listOrder[i].ID && o.ShopID == item.ID)
                                                    .Where(model.StatusID > 0, o => o.StatusID == model.StatusID)
                                                    .ToList_Cache();

                        %>
                        <%for (int j = 0; _OrderDetail != null && j < _OrderDetail.Count; j++)
                            {

                                var _Product = ModProductService.Instance.GetByID_Cache(_OrderDetail[j].ProductID);
                                if (_Product == null) continue;
                                string _File = !string.IsNullOrEmpty(_Product.File) ? _Product.File.Replace("~/", "/") : string.Empty;
                                long _total = _OrderDetail[j].Price * _OrderDetail[j].Quantity;
                        %>
                        <tr>
                            <td><%=i + 1 +j %></td>
                            <td>
                                <!-- Left-aligned media object -->
                                <div class="media">
                                    <div class="media-left">
                                        <img src="<%=_File %>" class="media-object" style="width: 60px">
                                    </div>
                                    <div class="media-body text-left">
                                        <h5><a href="<%=ViewPage.GetURL(_Product.MenuID, _Product.Code) %>"><%=_Product.Name %></a></h5>
                                        <p>Mã SP: <%=_Product.Model %></p>
                                    </div>
                                </div>
                            </td>
                            <td style="color:red; font-weight:600;"><%=listOrder[i].Code %></td>
                            <td><%=_OrderDetail[j].Quantity %></td>
                            <td><%=_OrderDetail[j].GetStatus().Name %></td>
                            <td><%=listOrder[i].GetPayment().Name %></td>
                            <td><a href="<%=ViewPage.BuninessOrderCpUrl %>?id=<%=_OrderDetail[j].ID %>" title="Cập nhật" class="btn-edit-prd text-red">Chi tiết đơn hàng</a></td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<script type="text/javascript">
    function time_order() {
        var start_time = $('#start_date').val();
        var end_time = $('#end_date').val();

        location.href = '<%=ViewPage.CurrentURL%>' + '?start=' + start_time + '&end=' + end_time;
    }
</script>
