﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var webUser = WebLogin.CurrentUser;
    if (webUser == null) return;

    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="profiles">
    <p class="image">
        <a href="<%=ViewPage.GetURL("shop", webUser.Code, "") %>">
            <img src="/Content/skins/images/avarta.png" height="45" width="45" alt="" />
        </a>
    </p>
    <p class="name">Tài khoản của</p>
    <h6><a href="<%=ViewPage.GetURL("shop", webUser.Code, "") %>"><%=webUser.Activity ? webUser.Shop : webUser.Email %></a></h6>
</div>
<ul class="menu-list">
    <%for (int i = 0; listItem != null && i < listItem.Count; i++) {
            string icon = listItem[i].Items.GetValue("Icon").ToString();
    %>
    <li class="item <%if (ViewPage.IsPageActived(listItem[i])){%>active<%} %>"><a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><i class="fa <%=icon %>"></i><%=listItem[i].Name %></a></li>
    <%} %>
</ul>
<div class="img">
    <img src="/Content/skins/images/icon_verified.png" alt="" />
</div>