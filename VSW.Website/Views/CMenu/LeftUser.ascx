﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>


<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
    if (listItem == null) return;
    var _webUser = WebLogin.CurrentUser;
    if (_webUser == null) return;
    string _avatar = !string.IsNullOrEmpty(_webUser.File) ? _webUser.File.Replace("~/", "/") : string.Empty;
    
%>

<div class="profile-sidebar">

    <div class="portlet light profile-sidebar-portlet ">

        <div class="profile-userpic">
            <img src="<%=_avatar %>" class="img-responsive" alt="<%=_webUser.Name %>">
        </div>

        <div class="profile-usertitle">
            <div class="profile-usertitle-name"><%=_webUser.Name%></div>
        </div>

        <div class="tab-menu-login">
            <p class="text-center">
                <a href="<%=ViewPage.WebUserCPUpdateUrl %>" class="<%=ViewPage.CurrentPage.ModuleCode=="MWebUserCP" ? "active":"" %>">Cập nhật thông tin</a>
                <a href="" class="">Thanh toán</a>
            </p>
        </div>

        <div class="profile-usermenu all" style="margin-top: 0">
            <div class="page-sidebar navbar-collapse collapse">
                <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                    <%for (int i = 1; listItem != null && i < listItem.Count; i++)
                        {
                            var listChilPage = SysPageService.Instance.GetByParent_Cache(listItem[i].ID); %>
                    <li class="heading">
                        <h3 class="uppercase"><%=listItem[i].Name %></h3>
                    </li>
                    <%for (int j = 0; j < listChilPage.Count; j++)
                        { %>
                    <li class="nav-item <%=ViewPage.IsPageActived(listChilPage[j]) ? "active":"" %>">
                        <a href="<%=ViewPage.GetPageURL(listChilPage[j]) %>">
                            <i class="fa <%=listChilPage[j].Faicon %>"></i>&nbsp;<%=listChilPage[j].Name %> 
                        </a>
                    </li>
                    <%} %>
                    <%} %>
                </ul>
            </div>

        </div>
        <div class="clearfix"></div>

    </div>

</div>
