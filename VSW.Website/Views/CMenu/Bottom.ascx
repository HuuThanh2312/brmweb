﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<%--<div class="container footer-category-list mt10">
    <hr>
    <h2 class="title-ft">Danh mục</h2>
    <div class="footer__category-list-columns">

        <div class="footer__category-list-column">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                {
                    var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
            %>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a>
                </div>
                <%if (listChildItem != null)
                    { %>
                <div class="footer__category-list-item-subs">
                    <%for (int j = 0; j < listChildItem.Count; j++)
                        { %>
                    <div class="footer__category-list-item-sub">
                        <a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>"><%=listChildItem[j].Name %></a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
            <%} %>
        </div>
    </div>
</div>--%>




<div class="container footer-category-list mt10">
    <hr>
    <h2 class="title-ft">Danh mục</h2>
    <div class="footer__category-list-columns">

        <div class="footer__category-list-column">
            <%for (int i = 0; listItem != null && i < (listItem.Count > 3 ? 3 : listItem.Count); i++)
                {
                    var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);%>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a>
                </div>
                <%if (listChildItem != null)
                    { %>
                <div class="footer__category-list-item-subs">
                    <%for (int j = 0; j < listChildItem.Count; j++)
                        { %>
                    <div class="footer__category-list-item-sub">
                        <a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>"><%=listChildItem[j].Name %></a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
            <%} %>
        </div>

        <div class="footer__category-list-column">
            <%for (int i = 3; listItem != null && i < (listItem.Count > 6 ? 6 : listItem.Count); i++)
                {
                    var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);%>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a>
                </div>
                <%if (listChildItem != null)
                    { %>
                <div class="footer__category-list-item-subs">
                    <%for (int j = 0; j < listChildItem.Count; j++)
                        { %>
                    <div class="footer__category-list-item-sub">
                        <a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>"><%=listChildItem[j].Name %></a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
            <%} %>
        </div>
        <div class="footer__category-list-column">
            <%for (int i = 6; listItem != null && listItem.Count > 9 && i < (listItem.Count > 9 ? 9 : listItem.Count); i++)
                {
                    var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);%>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a>
                </div>
                <%if (listChildItem != null)
                    { %>
                <div class="footer__category-list-item-subs">
                    <%for (int j = 0; j < listChildItem.Count; j++)
                        { %>
                    <div class="footer__category-list-item-sub">
                        <a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>"><%=listChildItem[j].Name %></a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
            <%} %>
        </div>

        <div class="footer__category-list-column">
            <%for (int i = 9; listItem != null && listItem.Count > 10 && i < (listItem.Count > 11 ? 11 : listItem.Count); i++)
                {
                    var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);%>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a>
                </div>
                <%if (listChildItem != null)
                    { %>
                <div class="footer__category-list-item-subs">
                    <%for (int j = 0; j < listChildItem.Count; j++)
                        { %>
                    <div class="footer__category-list-item-sub">
                        <a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>"><%=listChildItem[j].Name %></a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <%} %>
                </div>
                <%} %>
            </div>
            <%} %>
        </div>

        <%-- <div class="footer__category-list-column">
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nam</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nữ</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Nhà cửa & đời sống</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__category-list-column">
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nam</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nữ</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Nhà cửa & đời sống</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__category-list-column">
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nam</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nữ</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Nhà cửa & đời sống</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__category-list-column">
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nam</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Thời Trang Nữ</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
            <div class="footer__category-list-item">
                <div class="footer__category-list-item-main">
                    <a href="">Nhà cửa & đời sống</a>
                </div>
                <div class="footer__category-list-item-subs">
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo thun</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo sơ mi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo khoác &amp; Áo vest</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Áo nỉ/ Áo len</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ bộ/ Đồ mặc nhà</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ đôi</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Quần</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Balo/ Túi/ Ví</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Mắt kính</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Phụ kiện nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ Trung Niên</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Trang Sức Nam</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Thắt Lưng</a><span>&nbsp;|&nbsp;</span>
                    </div>
                    <div class="footer__category-list-item-sub">
                        <a href="">Đồ lót</a>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
</div>
