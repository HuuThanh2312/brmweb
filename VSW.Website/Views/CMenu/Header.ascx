﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
    string title = ViewBag.Title;
%>

<section class="head_bar">
    <div class="container">
        <div class="row justify-content-end">
            <ul class="list-bar">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                <li class="item">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" rel="nofollow"><%=listItem[i].Name %></a>
                </li>
                <%} %>

                <%if (WebLogin.IsLogin()){%>

                <%if (WebLogin.CurrentUser.Activity){%>
                <li class="item"><a href="<%=ViewPage.ProductUPUrl %>" rel="nofollow">Đăng sản phẩm</a></li>
                <%}else{%>
                <li class="item"><a href="<%=ViewPage.RegisterShopUrl %>" rel="nofollow">Đăng ký gian hàng</a></li>
                <%} %>

                <li class="item"><a href="<%=ViewPage.WebUserCPUrl %>" rel="nofollow">Chào bạn, <%=WebLogin.CurrentUser.Email %></a></li>
                <li class="item"><a href="<%=ViewPage.LogoutUrl %>">Thoát</a></li>

                <%}else {%>
                <li class="item"><a href="<%=ViewPage.RegisterUrl %>" rel="nofollow">Đăng ký</a></li>
                <li class="item"><a href="<%=ViewPage.LoginUrl %>" rel="nofollow">Đăng nhập</a></li>
                <%} %>
            </ul>
        </div>
    </div>
</section>