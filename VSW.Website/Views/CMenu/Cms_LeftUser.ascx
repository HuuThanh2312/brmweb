﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>


<%   
    var listItem = ViewBag.Data as List<SysPageEntity>;
    if (listItem == null) return;
    var _webUser = WebLogin.CurrentUser;
    if (_webUser == null) return;
    if (ViewPage.CurrentPage.ModuleCode == "MBusinessProduct" && _webUser.GetShopNotActive() != null) return;
    string _avatar = !string.IsNullOrEmpty(_webUser.File) ? _webUser.File.Replace("~/", "/") : string.Empty;
%>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Trình quản lý</span>
                    <%-- <span class="selected"></span>--%>
                </a>
            </li>
            <%if (_webUser.GetShop() != null && ViewPage.CurrentPage.ModuleCode == "MRegisterShop")
                { %>
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                {
                    //var listChilPage = SysPageService.Instance.GetByParent_Cache(listItem[i].ID); %>
            <li class="nav-item start <%=ViewPage.IsPageActived(listItem[i]) ? "active" : "" %>">
                <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" class="nav-link nav-toggle">
                    <i class="fa <%=listItem[i].Faicon %>"></i>
                    <span class="title"><%=listItem[i].Name %></span>
                    <span class="selected"></span>
                </a>
            </li>
            <%} %>
            <%}
                else if (ViewPage.CurrentPage.ModuleCode != "MRegisterShop")
                {%>
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                {
                    //var listChilPage = SysPageService.Instance.GetByParent_Cache(listItem[i].ID); %>
            <li class="nav-item start <%=ViewPage.IsPageActived(listItem[i]) ? "active" : "" %>">
                <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" class="nav-link nav-toggle">
                    <i class="fa <%=listItem[i].Faicon %>"></i>
                    <span class="title"><%=listItem[i].Name %></span>
                    <span class="selected"></span>
                </a>
            </li>
            <%} %>
            <%} %>
        </ul>
    </div>
</div>
