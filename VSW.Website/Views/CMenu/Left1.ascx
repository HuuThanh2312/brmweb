﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    void GetParentPage()
    {
        var _PageTemp = SysPageService.Instance.GetByID_Cache(_Page.ParentID);

        if (_PageTemp == null || _PageTemp.Root)
            return;

        _Page = _PageTemp;

        GetParentPage();
    }

    List<SysPageEntity> listItem = null;
    SysPageEntity _Page;
    protected void Page_Load(object sender, EventArgs e)
    {
        _Page = ViewBag.Page;
        if (_Page == null)
            _Page = (SysPageEntity)ViewPage.CurrentPage.Clone();

        listItem = SysPageService.Instance.GetByParent_Cache(_Page.ID);
        if (listItem == null)
        {
            GetParentPage();
            listItem = SysPageService.Instance.GetByParent_Cache(_Page.ID);
        }
    }

</script>

<div class="widget widget-breadcrumb border border-radius">
    <ul class="list-unstyled">
        <li class="active">
            <a href="<%=ViewPage.GetPageUrl(_Page) %>"><%=_Page.Name %></a>
            <ul class="list-unstyled">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                        var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
                %>
                <li>
                    <a href="<%=ViewPage.GetPageUrl(listItem[i]) %>" class="font-family-medium <%if (ViewPage.IsPageActived(listItem[i])){%>active<%} %>"><%=listItem[i].Name %><span class="meta">(<%=string.Format("{0:#,##0}", listItem[i].Count) %>)</span></a>
                    <%if (listChildItem != null){%>
                    <ul class="list-unstyled">
                        <%for (int j = 0; j < listChildItem.Count; j++){%>
                        <li><a href="<%=ViewPage.GetPageUrl(listChildItem[j]) %>"><%=listChildItem[j].Name %><span class="meta">(<%=string.Format("{0:#,##0}", listChildItem[j].Count) %>)</span></a></li>
                        <%} %>
                    </ul>
                    <%} %>
                </li>
                <%} %>                                                                                                                                                                               
            </ul>
        </li>
    </ul>
</div>