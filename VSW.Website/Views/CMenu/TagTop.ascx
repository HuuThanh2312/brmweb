﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="top-search">
    <span class="search-label">Top tìm kiếm: </span>
    <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a>
    <%} %>
</div>