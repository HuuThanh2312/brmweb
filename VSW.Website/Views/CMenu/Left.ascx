﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="sidebox all mb20">
    <div class="sidebox-title">
        <h3 class="txt"><a href="javascript:void(0)" rel="nofollow">Các đơn vị uy tín nhất</a></h3>
    </div>
    <ul class="ul-list-sirbar">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
        <li class="clear">
            <div class="img-left-sm">
                <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><img src="<%=Utils.GetResizeFile(listItem[i].File, 1, 0, 0) %>" alt="<%=listItem[i].Name %>" /></a>
            </div>
            <div class="txt-right-sm txt-gray">
                <h3 class="title-item"><a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a></h3>
                <p class="txt-small">Dịch vụ tốt - Bồi thường cao nhât</p>
            </div>
        </li>
        <%} %>
    </ul>
</div>