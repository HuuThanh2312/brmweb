﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
    
%>



<div class="header-nav">
    <div class="container">
        <div class="nav-menu positionR">
            <i class="fa fa-list"></i>&nbsp;Danh mục sản phẩm&nbsp;<i class="fa fa-sort-desc"></i>
            <ul class="menu-main-left">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                    {
                        string icon = !string.IsNullOrEmpty(listItem[i].Icon) ? listItem[i].Icon.Replace("~/", "/") : string.Empty;
                        var listChidPage = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
                %>
                <li class="dropdown-menu1">
                    <span>
                        <a href="<%=ViewPage.GetPageURL(listItem[i]) %>">
                            <img src="<%=icon %>" alt="<%=listItem[i].Name %>" class="icon-menu"><%=listItem[i].Name %></a>
                    </span>
                    <%if (listChidPage != null)
                        { %>
                    <div class="box-list-submenu">
                        <div class="subcate">
                            <%for (int j = 0; j < listChidPage.Count; j++)
                                {
                                    var listGrandPage = SysPageService.Instance.GetByParent_Cache(listChidPage[j].ID);
                            %>
                            <aside>
                                <strong class="title-menu2"><a href="<%=ViewPage.GetPageURL(listChidPage[j]) %>" class=""><%=listChidPage[j].Name %></a></strong>
                                <ul class="list-submenu">
                                    <%for (int z = 0; z < listGrandPage.Count; z++)
                                        { %>
                                    <li><a href="<%=ViewPage.GetPageURL(listGrandPage[z]) %>"><%=listGrandPage[z].Name %></a></li>
                                    <%} %>
                                </ul>
                            </aside>
                            <%} %>
                        </div>
                    </div>
                    <%} %>
                </li>
                <%} %>
            </ul>
        </div>
        <div class="control-suport-top">
            <img src="/Content/skins/images/icon/phone.gif" alt="Hotline" class="icon-phone">
            <span><strong>Hotline:&nbsp;<a href="tel:{RS:Web_Hotline}">{RS:Web_Hotline}</a></strong>&nbsp;(8h - 17h30)</span>
        </div>
    </div>
</div>
