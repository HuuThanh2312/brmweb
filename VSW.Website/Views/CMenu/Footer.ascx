﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listPage = ViewBag.Data as List<SysPageEntity>;
%>

<div class="div-block-bottom">
    <div class="container">
        <div class="footer__block-information">
            <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                {
                    var listChildPage = SysPageService.Instance.GetByParent_Cache(listPage[i].ID); %>
            <div class="footer__category-list-column">
                <div class="footer__category-list-item-main-info-block"><%=listPage[i].Name %></div>
                <%if (listChildPage != null)
                    { %>
                <ul class="footer__category-list-item-list">
                   <%for (int j = 0; j < listChildPage.Count; j++)
                       { %>
                    <li><a href="<%=ViewPage.GetPageURL(listChildPage[j]) %>"><%=listChildPage[j].Name %></a>
                    </li>
                   <%} %>
                </ul>
                <%} %>
            </div>
            <%} %>
            <div class="footer__category-list-column">
                <div class="footer__category-list-item-main-info-block">Thanh toán</div>
                <ul class="footer__category-list-partners -double-column">
                    <li>
                        <img src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-vn/assets/013d25d374db5a25be43a29f9781658c.png" alt="Visa">
                    </li>
                    <li>
                        <img src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-vn/assets/f57c749f21f47e2080a6be36050d9484.png" alt="Mastercard">
                    </li>
                </ul>
                <div class="footer__category-list-item-main-info-block-alt">Đơn vị vận chuyển</div>
                <ul class="footer__category-list-partners double-column">
                    <li>
                        <img src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-vn/assets/d65931b747af7bf8b29e055ac6fea674.png" alt="Giao Hàng Tiết Kiệm">
                    </li>
                    <li>
                        <img src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-vn/assets/9898c93ddf5917a6af8a482a15b6fe3a.png" alt="Giao Hàng nhanh">
                    </li>
                    <li>
                        <img src="https://cdngarenanow-a.akamaihd.net/shopee/shopee-pcmall-live-vn/assets/c869e145f6e7ec87fd71d5d6bf534186.png" alt="Viettel Post">
                    </li>
                </ul>
            </div>
            <!-- -->
            <div class="footer__category-list-column">
                <div class="footer__category-list-item-main-info-block">Theo dõi chúng tôi trên</div>
                <ul class="footer__category-list-item-list">
                    {RS:Web_Social}
                </ul>
            </div>
            <!-- -->
            <div class="footer__category-list-column">
                <div class="footer__category-list-item-main-info-block">Tải ứng dụng <%=ViewPage.Request.Url.AbsoluteUri %> ngay thôi</div>
                 {RS:Web_DownApp}
            </div>
        </div>
        <div class="coppy-right-bottom">
            <div class="row">
                <div class="col-md-6">
                    <div class="coppy-right">
                        © 2018 Shopee. Tất cả các quyền được bảo lưu.
                    </div>
                </div>
                <%--<div class="col-md-6">
                    <div class="list-khuvuc text-right">
                        <span>Quốc gia/Khu vực:</span>
                        <a href="">Việt Nam</a>
                        <a href="">Singapore</a>
                        <a href="">Indonesia</a>
                        <a href="">Đài Loan</a>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</div>
