﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var _Page = ViewBag.Page as SysPageEntity;
    if (_Page == null) return;

    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<section class="home-box">
    <div class="container">
        <div class="row">
            <div class="main-title">Thông tin   </div>
            <ul class="list_resources">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                <li>
                    <div class="bgresources text-center menuhome2">
                        <p class="img">
                            <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" title="<%=listItem[i].Name %>">
                                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 500, 500) %>" title="<%=listItem[i].Name %>" alt="<%=listItem[i].Name %>" />
                            </a>
                        </p>
                        <span class="txt"><%=listItem[i].Name %></span>
                    </div>
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" class="alink-all" title="<%=listItem[i].Name %>"></a>
                </li>
                <%} %>
            </ul>
        </div>
    </div>
</section>