﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<section class="footer-tag all">
    <div class="container">
        <div class="row">
            <h2 class="txt">Hot tags:</h2>
            <ul>
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                <li><a href="<%=ViewPage.GetPageURL(listItem[i]) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></li>
                <%} %>
            </ul>
        </div>
    </div>
</section>