﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<SysPageEntity>;
    string title = ViewBag.Title;
%>

<div class="box-service all mb20">
    <div class="sidebox-title">
        <h3 class="txt"><a href="javascript:void(0)" rel="nofollow"><%=title %></a></h3>
    </div>
    <ul class=" ul-dichvu-center all">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
        <li>
            <div class="img-sevice-main fl ">
                <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><img src="<%=Utils.GetResizeFile(listItem[i].File, 1, 0, 0) %>" alt="<%=listItem[i].Name %>" /></a>
            </div>
            <div class="info-sevice-main fr">
                <h3 class="name-sevice-main"><%=listItem[i].Name %></h3>
                <p class="des-sevice-main"><%=listItem[i].Summary %></p>
                <p class="text-right txt-timhieutham">
                    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>">Tìm hiểu thêm tại đây <i class="fa fa-angle-double-right"></i></a>
                </p>
            </div>
        </li>
        <%} %>
    </ul>
</div>