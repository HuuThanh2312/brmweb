﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="keyword all">
    <span>Tìm kiếm nhiều: </span>
    <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
    <a href="<%=ViewPage.GetPageURL(listItem[i]) %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a>
    <%} %>
</div>