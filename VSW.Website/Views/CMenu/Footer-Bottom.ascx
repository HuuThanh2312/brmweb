﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listPage = ViewBag.Data as List<SysPageEntity>;
%>
<div class="footer-bottom">
    <div class="container">
        <div class="footer__policies">
            <%for (int i = 0; i < listPage.Count; i++)
                { %>
            <div class="footer__policy-item">
                <a class="footer__policy-link" href="<%=ViewPage.GetPageURL(listPage[i]) %>">
                    <span><%=listPage[i].Name %></span>
                </a>
            </div>
            <%} %>
           
        </div>
        {RS:Web_Address}
    </div>
</div>
