﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var webUser = ViewPage.ViewBag.Shop as ModWebUserEntity;
    if (webUser == null) return;

    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="img">
    <img src="/Content/skins/images/icon_verified.png" alt="" />
</div>
<div class="cate_shop">
    <%for (int i = 0; listItem != null && i < listItem.Count; i++){
            if (listItem[i].CountByShop(webUser.ID) < 1) continue;

            var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
    %>
    <div class="ttl-shop"><span><%=listItem[i].Name %></span></div>

    <%for (int j = 0; listChildItem != null && j < listChildItem.Count; j++){
            if (listChildItem[j].CountByShop(webUser.ID) < 1) continue;

            var listGrandItem = SysPageService.Instance.GetByParent_Cache(listChildItem[j].ID);
    %>
    <div class="box_accordion_shop">
        <div class="ttl-block">
            <h2><a href="<%=ViewPage.GetURL("shop", webUser.Code, listChildItem[j].Code) %>"><%=listChildItem[j].Name %></a></h2>
        </div>
        <ul class="sub_cat">
            <%for (int k = 0; listGrandItem != null && k < listGrandItem.Count; k++){
                    if (listGrandItem[k].CountByShop(webUser.ID) < 1) continue;
            %>
            <li><a href="<%=ViewPage.GetURL("shop", webUser.Code, listGrandItem[k].Code) %>"><%=listGrandItem[k].Name %></a></li>
            <%} %>
        </ul>
    </div>
    <%} %>

    <%} %>
</div>