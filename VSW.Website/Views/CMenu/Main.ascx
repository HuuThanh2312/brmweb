﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="sidebar-megamenu">
    <div class="sidebar-megamenu-content">
        <div class="sidebar-megamenu-content-btn">
            <a href="javascript:void(0)" rel="nofollow">Danh mục sản phẩm</a>
        </div>
        <div class="sidebar-megamenu-content-menu <%if (ViewPage.Request.RawUrl == "/"){%>for-home<%} %>">
            <div class="sidebar-megamenu-content-menu-nav">
                <div class="nav-desktop">
                    <ul class="navmenu">
                        <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                                var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
                        %>
                        <li>
                            <a href="<%=ViewPage.GetPageURL(listItem[i]) %>">
                                <%if (!string.IsNullOrEmpty(listItem[i].Icon)){%>
                                <span class="icon-items">
                                    <img src="<%=listItem[i].Icon.Replace("~/", "/") %>" alt="<%=listItem[i].Name %>" />
                                </span>
                                <%} %>
                                <span class="menu-title"><%=listItem[i].Name %></span>
                            </a>
                            <div class="sub-cate">
                                <div class="sub-cate-inner">
                                    <%for (int j = 0; listChildItem != null && j < listChildItem.Count; j++){
                                            var listGrandItem = SysPageService.Instance.GetByParent_Cache(listChildItem[j].ID);
                                    %>
                                    <ul>
                                        <li><a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>" class="lstitle"><%=listChildItem[j].Name %></a></li>
                                        <%for (int k = 0; listGrandItem != null && k < listGrandItem.Count; k++){%>
                                        <li><a href="<%=ViewPage.GetPageURL(listGrandItem[k]) %>"><%=listGrandItem[k].Name %></a></li>
                                        <%} %>
                                    </ul>
                                    <%} %>

                                    <%if (!string.IsNullOrEmpty(listItem[i].File)){%>
                                    <a class="img-cate">
                                        <img src="<%=listItem[i].File.Replace("~/", "/") %>" alt="<%=listItem[i].Name %>" />
                                    </a>
                                    <%} %>
                                </div>
                            </div>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>