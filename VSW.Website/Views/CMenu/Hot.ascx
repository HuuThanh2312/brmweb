﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<section class="block-tcategories mt35">
    <div class="list-tcategories mt20" id="owl-tcategories">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                string url = ViewPage.GetPageURL(listItem[i]);
        %>
        <div class="item">
            <div class="inner">
                <figure class="reponsive-img">
                    <a href="<%=url %>" class="item">
                        <%=Utils.GetResizeFile(listItem[i].File, 2, 207, 183, listItem[i].Name) %>
                    </a>
                </figure>
                <div class="card-body text-center">
                    <h4 class="card-title"><%=listItem[i].Name %></h4>
                    <p class="text-muted">Giá từ 90.000 đ</p>
                    <a href="<%=url %>" class="btn btn-outline-primary btn-sm">Xem sản phẩm</a>
                </div>
            </div>
        </div>
        <%} %>
    </div>
</section>