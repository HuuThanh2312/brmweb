﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="content-news">
    <h1><%=ViewPage.CurrentPage.Name %></h1>
    <div class="dtop">
        <div class="clock">Đăng ngày <%=string.Format("{0:f}", ViewPage.CurrentPage.Created) %></div>
        <img src="Content/img/share.JPG" alt="">
    </div>
    <div class="post-entry"><%=Utils.GetHtmlForSeo(ViewPage.CurrentPage.Content) %></div>
    <div class="clear"></div>
</div>