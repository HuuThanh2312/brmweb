﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="c-flexible-text-and-image has-text">
    <div class="row margin-bottom-1">
        <div class="small-12 columns">

            <div class="c-flexible-text-and-image__text">
                <div id="cat_texts">
                    <h1><span class="grayDark"><%=ViewBag.Title %>Good advice. Great Bathrooms.</span></h1>
                    <hr />
                    <%=ViewBag.Text %>
                    <h2><span class="grayDark">Reuter Shop, your specialist for sanitary products and bathroom design.</span></h2>
                    <p><span class="grayDark">We help to improve your bathroom: For over 20 years, Reuter stands for more ranges and low priced bathroom products. As well as our stores with large showrooms, we have been offering an even wider range in our online shop for years. Reuter has years of experience and is the online shop for you when it comes to fitting bathrooms. Here, you can find the right <a href="/bathroom/baths.html">bath</a> or <a href="/bathroom/shower-trays.html">shower tray</a> from renowned brands and <a href="/bathroom/bathroom-furniture.html">bathroom furniture</a> such as cosmetic mirrors and vanity units.</span></p>
                    <p><span class="grayDark">So if you would like to re-fit your bathroom, we offer <a href="/bathroom/bathroom-fittings.html">fittings</a> and bathroom ceramic products such as <a href="/bathroom/bathroom-ceramics/toilets.html">toilets</a> or <a href="/bathroom/bathroom-ceramics/washbasins.html">washbasins</a> and also of course complete systems which may be required for your own wellness oasis. And with our Best-Price-Guarantee, you are sure to get the best offer with us. Simply have a look here in our shop and benefit from our expertise on brands such as Villeroy &amp; Boch, Hansgrohe, Keramag, Dornbracht, Grohe, Kaldewei, Geberit and many more manufacturers. In addition to the established brands, we offer our own brand of quality products at very attractive prices.</span></p>
                    <p><span class="grayDark">With a total of over 500,000 articles, we offer an enormous selection of wonderful products by focusing on high quality brands. With us, you can make your bathroom ideas real: Reuter is your source for a dream bathroom.</span></p>
                </div>
            </div>
        </div>
    </div>
</div>