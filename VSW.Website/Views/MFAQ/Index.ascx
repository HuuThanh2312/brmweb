﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<ModFAQEntity>;
%>

<h1>Câu hỏi thường gặp &raquo; <span id="faq_title">Tài khoản</span></h1>
<div class="tab-content term-content entry-content border bordered">
    <%for (var i = 0; listItem != null && i < listItem.Count; i++){%>
    <div id="cauhoi1" class="tab-pane" role="tabpanel">
        <div role="tablist" id="content-cauhoi1" class="panel-group list-qa">
            <div class="panel panel-qa">
                <a href="#tk1" class="qa-title"><i class="block-icon sprites icon-qa"></i><span class="block-content"><%=listItem[i].Name %></span></a>
                <div class="panel-collapse collapse in" id="tk1"><%=Utils.GetHtmlForSeo(listItem[i].Content) %></div>
            </div>
        </div>
    </div>
    <%} %>
</div>