﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<ModFAQEntity>;
%>

<div class="news_all">
    <h2><a href="<%=ViewPage.CurrentUrl %>"><%=ViewPage.CurrentPage.Name %></a></h2>
    <ul class="faq_list">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
        <li>
            <h3><%=listItem[i].Name %></h3>
            <div class="blogsummary"><%= Utils.GetHtmlForSeo(listItem[i].Content) %></div>
        </li>
        <%} %>
    </ul>
</div>