﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<WebMenuEntity>;

%>

<!-- END PAGE BAR -->
<div class="row">
    <div class="col-md-12">
        <div class="header-tabs-doitac">
            <div class="tabs-find-doitac">
                <ul class="nav-justified">
                    <li class="active">
                        <a href="">Tất cả</a>
                    </li>
                    <li>
                        <a href="">Còn hàng</a>
                    </li>
                    <li>
                        <a href="">Hết hàng</a>
                    </li>
                    <li>
                        <a href="">Đã bị khóa</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark positionR">
                    <i class="fa fa-history"></i>
                    <span class="caption-subject bold uppercase">Lịch sử hoa hồng</span>
                    <div class="div-export-import-oder">
                        <div class="dropdown">
                            <button class="dropbtn"><i class="fa fa-file-text"></i>&nbsp;Tải danh sách và nhập hàng loạt</button>
                            <div class="dropdown-content">
                                <a href="#"><i class="fa fa-upload"></i>&nbsp;tải danh sách sản phẩm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tools"></div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="center">#</th>
                            <th class="center">Sản phẩm</th>
                            <th class="center" style="width: 15%;">Danh mục</th>
                            <th class="center">Giá sản phẩm</th>
                            <th class="center">Kho hàng</th>
                        </tr>
                    </thead>

                    <tbody class="center">
                        <tr>
                            <td>1</td>
                            <td>
                                <!-- Left-aligned media object -->
                                <div class="media">
                                    <div class="media-left">
                                        <img src="https://cf.shopee.vn/file/f5131b7d0cd68c2b5c3577d76a2278ad_tn" class="media-object" style="width: 60px">
                                    </div>
                                    <div class="media-body text-left">
                                        <h5>
                                            <a href="">QUẦN SHORT ĐŨI NAM THỜI TRANG CHẤT XỊN CAO CẤP
                                                                </a>
                                        </h5>
                                        <p>
                                            SKU: QĐ21056
                                                             
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td>Thời trang nam
                                                    </td>
                            <td>
                                <b>120.000<sup>đ</sup></b>
                            </td>
                            <td>Số lượng: 100
                                                    </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
