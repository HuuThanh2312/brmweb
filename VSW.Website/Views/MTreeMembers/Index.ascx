﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<ModWebUserEntity>;
    int user1 = 0;
    int user2 = 0;
    int user3 = 0;
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>
<link href="<%=Static.Tag("/Content/js/csm_login/js/jstree/dist/themes/default/style.min.css")%>" rel="stylesheet" type="text/css" />
<script src="<%=Static.Tag("/Content/js/csm_login/js/jstree/dist/jstree.min.js")%>" type="text/javascript"></script>
<script src="<%=Static.Tag("/Content/js/csm_login/js/app.min.js") %>" type="text/javascript"></script>
<script src="<%=Static.Tag("/Content/js/csm_login/js/ui-tree.min.js")%>" type="text/javascript"></script>



<section class="manager-login">
    <div class="container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="">
                <!-- BEGIN PAGE TITLE-->

                <div class="row mt20">
                    <div class="col-md-12">
                        <!-- BEGIN PROFILE SIDEBAR -->
                        <div class="profile-sidebar">
                            <!-- PORTLET MAIN -->
                            <div class="portlet light profile-sidebar-portlet ">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name"><%=item.Name %></div>
                                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                                </div>
                                <div class="profile-usermenu">
                                    <ul class="nav">
                                        <li class="nav-item start">
                                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                                <i class="fa fa-home"></i>
                                                <span class="title">Trình quản lý</span>
                                            </a>
                                        </li>
                                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                                            { %>
                                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                            </div>

                            <!-- END PORTLET MAIN -->
                        </div>
                        <!-- END BEGIN PROFILE SIDEBAR -->
                        <!-- BEGIN PROFILE CONTENT -->
                        <div class="profile-content">
                            <div class="row">
                                <div class="col-md-7" style="padding-right: 0;">
                                    <!-- BEGIN Portlet PORTLET-->
                                    <div class="portlet light bordered" style="min-height: 567px;">
                                        <div class="portlet-title positionR">
                                            <div class="caption">
                                                <i class="fa fa-users font-blue-hoki"></i>
                                                <span class="caption-subject bold font-blue-hoki uppercase">Cây thành viên</span>
                                            </div>
                                            <%-- <a href="" class="btn btn-add-mng"><i class="fa fa-plus"></i>&nbsp;Thêm thành viên</a>--%>
                                        </div>
                                        <div class="portlet-body">

                                            <div id="tree_1" class="tree-demo">
                                                <ul>
                                                    <li class="boss-child-tree">
                                                        <span class="child-boss"><%=item.Name %> (<%=string.Format("{0:dd/MM/yyyy}",item.Created) %>)</span>
                                                        <ul class="last-child-item">
                                                            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                                                                {
                                                                    user1 = listItem.Count;
                                                                    var _user = ModWebUserService.Instance.GetByID_Cache(listItem[i].ID);
                                                                    if (_user == null) continue;
                                                                    var listChildUser = ModWebUserService.Instance.GetByParentID_Cache(listItem[i].ID);
                                                            %>
                                                            <li <%if (i == 0)
                                                                { %>
                                                                data-jstree='{ "selected" : true }' <%} %>>
                                                                <a href="javascript:void(0)">[1]&nbsp;<%=_user.Name %> (<%=string.Format("{0:dd/MM/yyyy}", listItem[i].Created) %>)</a>
                                                                <ul>
                                                                    <%for (int j = 0; listChildUser != null && j < listChildUser.Count; j++)
                                                                        {
                                                                            user2 = listChildUser.Count;
                                                                            var _user2 = ModWebUserService.Instance.GetByID_Cache(listChildUser[j].ID);
                                                                            if (_user2 == null) continue;
                                                                            var listGrandUser = ModWebUserService.Instance.GetByParentID_Cache(listChildUser[j].ID);
                                                                    %>
                                                                    <li>[2]<%=_user2.Name %> (<%=string.Format("{0:dd/MM/yyyy}", listChildUser[j].Created) %>)                  
                                            <ul>
                                                <%for (int z = 0; listGrandUser != null && z < listGrandUser.Count; z++)
                                                    {
                                                        user3 = listGrandUser.Count;
                                                        var _user3 = ModWebUserService.Instance.GetByID_Cache(listGrandUser[z].ID);
                                                        if (_user3 == null) continue;
                                                        //var listGrandUser2 = ModTreeUserService.Instance.GetByParentID_Cache(listGrandUser[z].ParentID);
                                                %>
                                                <li>[3] <%=_user3.Name %> (<%=string.Format("{0:dd/MM/yyyy}", listGrandUser[z].Created) %>)    
                                                 
                                                </li>
                                                <%} %>
                                            </ul>
                                                                    </li>
                                                                    <%} %>
                                                                </ul>
                                                            </li>
                                                            <%} %>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Portlet PORTLET-->
                                </div>
                                <div class="col-md-5 info-user-right">
                                    <!-- BEGIN Portlet PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <h3><i class="fa fa-user"></i>&nbsp;Thông Tin Thành Viên</h3>
                                            <ul>
                                                <li><a href="javascript:void()">Cấp độ: Chính thức </a></li>
                                                <li><a href="javascript:void()">Danh hiệu: </a></li>
                                                <li><a href="javascript:void()">Tên tài khoản: <%=item.LoginName %></a></li>
                                                <li><a href="javascript:void()">Số thành viên cấp 1: <%=user1 %></a></li>
                                                <li><a href="javascript:void()">Số thành viên cấp 2: <%=user2 %></a></li>
                                                <li><a href="javascript:void()">Số thành viên cấp 3 :<%=user3 %></a></li>
                                                <li><a href="javascript:void()">Tổng số thành viên:<%=user3 + user2 + user1 %></a></li>
                                                <li><a href="javascript:void()">TVKN: 4</a></li>
                                                <li><a href="javascript:void()">TVCT: 1526</a></li>
                                                <li><a href="javascript:void()">DSTD tháng 05/2018: 0 đ</a></li>
                                            </ul>

                                            <hr>
                                            <%if (item.ParentID > 0)
                                                {
                                                    var _user = ModWebUserService.Instance.GetByID_Cache(item.ParentID);
                                            %>
                                            <h3><i class="fa fa-user"></i>&nbsp;Thông Tin Thành Viên Giới Thiệu</h3>
                                            <ul>
                                                <li><a href="javascript:void()">Họ và tên : <span class="cap12"><%=_user.Name %></span></a></li>
                                                <li><a href="javascript:void()">Cấp độ:  Chính thức            </a></li>
                                                <li><a href="javascript:void()">Danh hiệu: </a></li>
                                                <li><a href="javascript:void()">Tên tài khoản: <%=_user.LoginName %></a></li>
                                                <li><a href="javascript:void()">Số thành viên cấp 1: <%=_user.CountUser()%></a></li>
                                                <%--    <li><a href="javascript:void()">Số thành viên cấp 2: 4</a></li>
                    <li><a href="javascript:void()">Số thành viên 10 cấp : 1404</a></li>
                    <li><a href="javascript:void()">TVKN: 4</a></li>
                    <li><a href="javascript:void()">TVCT: 1527</a></li>
                    <li><a href="javascript:void()">DSTD tháng 05/2018: 0 đ</a></li>--%>
                                            </ul>
                                            <%} %>
                                        </div>
                                    </div>
                                    <!-- END Portlet PORTLET-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN Portlet PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-users font-blue-hoki"></i>
                                                <span class="caption-subject bold font-blue-hoki uppercase">Chi tiết cây thành viên <%=item.Name %></span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="">
                                                <table class="table table-bordered table-hover text-center">
                                                    <thead>
                                                        <tr class="active">
                                                            <th class="text-center">Cây thành viên</th>
                                                            <th class="text-center">TVKN</th>
                                                            <th class="text-center">TVCT</th>
                                                            <th class="text-center">Silver</th>
                                                            <th class="text-center">Gold</th>
                                                            <th class="text-center">1 sao</th>
                                                            <th class="text-center">2 sao</th>
                                                            <th class="text-center">3 sao</th>
                                                            <th class="text-center">4 sao</th>
                                                            <th class="text-center">5 sao</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>2</td>
                                                            <td>2</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>2</td>
                                                            <td>7</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>13</td>
                                                            <td>11</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>61</td>
                                                            <td>8</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>147</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td>257</td>
                                                            <td>4</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>290</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>8</td>
                                                            <td>338</td>
                                                            <td>2</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>9</td>
                                                            <td>253</td>
                                                            <td>3</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>10</td>
                                                            <td>71</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>11</td>
                                                            <td>29</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>12</td>
                                                            <td>18</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>13</td>
                                                            <td>8</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <td>14</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr class="success">
                                                            <th class="text-center">Tổng</th>
                                                            <th class="text-center">1490</th>
                                                            <th class="text-center">40</th>
                                                            <td class="text-center">0</td>
                                                            <td class="text-center">0</td>
                                                            <td class="text-center">0</td>
                                                            <td class="text-center">0</td>
                                                            <td class="text-center">0</td>
                                                            <td class="text-center">0</td>
                                                            <td class="text-center">0</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Portlet PORTLET-->
                                </div>
                            </div>

                        </div>
                        <!-- END PROFILE CONTENT -->
                    </div>

                </div>
                <!-- END CONTENT BODY -->
            </div>
        </div>
        <!-- END CONTAINER -->
    </div>
</section>

