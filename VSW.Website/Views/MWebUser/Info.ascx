﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var webUser = ViewPage.ViewBag.Shop as ModWebUserEntity;
    if (webUser == null) return;
%>

<div class="profile-header mb10">
    <%if (!string.IsNullOrEmpty(webUser.File)){%>
    <div class="cover-photo" style="background-image: url(<%=webUser.File.Replace("~/", "/") %>)"></div>
    <%} %>

    <div class="profile-info-user">
        <%if (!string.IsNullOrEmpty(webUser.Logo)){%>
        <div class="img_user">
            <a href="<%=ViewPage.GetURL("shop", webUser.Code, "") %>">
                <img src="<%=webUser.Logo.Replace("~/", "/") %>" />
            </a>
        </div>
        <%} %>
        <div class="content-user">
            <p class="name"><%=webUser.Shop %></p>
            <p><%=webUser.Address %></p>
            <p>Tham gia từ: <%=string.Format("{0:dd/MM/yyyy}", webUser.Created) %></p>
        </div>
        <div class="hotline-shop"><a href="tel:<%=webUser.Phone %>"><%=webUser.Phone %></a></div>
    </div>
</div>
<div class="nav-shop">
    <ul>
        <li><a href="<%=ViewPage.GetURL("shop", webUser.Code, "") %>">Sản phẩm</a></li>
        <li class="active"><a href="<%=ViewPage.GetURL("shop", webUser.Code, "thong-tin-shop") %>">Thông tin shop</a></li>
    </ul>
</div>
<div class="shop-inf-block">
    <h2 class="ttl-shop-inf ">
        <span class="ttl shop_border_color">Thông tin shop</span>
    </h2>
    <div class="shop-inf-desc">
        <%=webUser.Content %>
        <p class="address-shop">
            <label>Địa chỉ:</label>
            <span><%=webUser.Address %></span></p>
        <p class="address-shop">
            <label>Điện thoại:</label>
            <span><%=webUser.Phone %></span>
        </p>
        <p class="address-shop">
            <label>Email:</label>
            <span><%=webUser.Email %></span>
        </p>
        <p class="address-shop">
            <label>Website:</label>
            <span><%=VSW.Core.Web.HttpRequest.Scheme %>://<%=VSW.Core.Web.HttpRequest.Host %>/shop/<%=webUser.Code %>/</span>
        </p>
    </div>
    <h2 class="ttl-shop-inf ">
        <span class="ttl shop_border_color">Đơn vị vận chuyển</span>
    </h2>
    <div class="dtvc">
        <img src="/Content/skins/images/ecom_shipping_ghn.gif" class="itemvc" alt="Giao hàng nhanh" title="Giao hàng nhanh" />
        <img src="/Content/skins/images/ecom_shipping_viettel_cpn.gif" class="itemvc" alt="VIETTEL-CPN" title="VIETTEL-CPN" />
    </div>
</div>