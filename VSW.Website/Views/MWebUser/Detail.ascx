﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var webUser = ViewPage.ViewBag.Shop as ModWebUserEntity;
    if (webUser == null) return;

    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MWebUserModel;
%>

<div class="profile-header mb10">
    <%if (!string.IsNullOrEmpty(webUser.File)){%>
    <div class="cover-photo" style="background-image: url(<%=webUser.File.Replace("~/", "/") %>)"></div>
    <%} %>

    <div class="profile-info-user">
        <%if (!string.IsNullOrEmpty(webUser.Logo)){%>
        <div class="img_user">
            <a href="<%=ViewPage.GetURL("shop", webUser.Code, "") %>">
                <img src="<%=webUser.Logo.Replace("~/", "/") %>" />
            </a>
        </div>
        <%} %>
        <div class="content-user">
            <p class="name"><%=webUser.Shop %></p>
            <p><%=webUser.Address %></p>
            <p>Tham gia từ: <%=string.Format("{0:dd/MM/yyyy}", webUser.Created) %></p>
        </div>
        <div class="hotline-shop"><a href="tel:<%=webUser.Phone %>"><%=webUser.Phone %></a></div>
    </div>
</div>
<div class="nav-shop">
    <ul>
        <li class="active"><a href="<%=ViewPage.GetURL("shop", webUser.Code, "") %>">Sản phẩm</a></li>
        <li><a href="<%=ViewPage.GetURL("shop", webUser.Code, "thong-tin-shop") %>">Thông tin shop</a></li>
    </ul>
</div>
<ul class="listpro d-flex-wrap all mt10">
    <%for (var i = 0; listItem != null && i < listItem.Count; i++){
            string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
    %>
    <li class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="itempro">
            <div class="reponsive-img">
                <a href="<%=url %>">
                    <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 220, 220) %>" class="lazy" alt="<%=listItem[i].Name %>" />
                </a>
                <span class="lablePro sale">Sale</span>
                <span class="lablePro new">New</span>
            </div>
            <div class="itempro-info">
                <h3 class="itempro-info-name"><a href="<%=url %>"><%=listItem[i].Name %></a></h3>
                <div class="itempro-info-price">
                    <span class="special-price"><%=string.Format("{0:#,##0}", listItem[i].Price) %> đ</span>
                    <span class="old-price"><%=string.Format("{0:#,##0}", listItem[i].Price2) %> đ</span>
                </div>
                <div class="social_box">
                    <div class="s_b"><i class="fa fa-tags"></i><span class="luotmua">0</span> </div>
                    <div class="s_b"><i class="fa fa-eye"></i><span class="luotxem">206</span> </div>
                    <div class="s_b"><i class="fa fa-comments"></i><span class="binhluan">206</span> </div>
                </div>
            </div>
        </div>
    </li>
    <%} %>
</ul>
<ul class="pagination flex-wrap all justify-content-end">
    <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
</ul>

