﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModVideoEntity>;
%>

<div class="tinvan list-video all">
    <div class="title-loc">
        <span>Video</span>
        <a href="" class="fr btn btn-xemthem">Xem thêm</a>
    </div>
    <div class="body-tinvan">
        <ul class="ul_video">
            <li>
                <iframe width="100%" height="300" src="https://www.youtube.com/embed/5le_91B-kvc" frameborder="0" allowfullscreen></iframe>
            </li>
            <li>
                <iframe width="100%" height="300" src="https://www.youtube.com/embed/U352RJ-Fz7c" frameborder="0" allowfullscreen></iframe>
            </li>
        </ul>
    </div>
</div>







<div class="box-pro all">
    <div class="title-video">
        ➤ Kênh video hướng dẫn lắp đặt
    </div>
    <ul class="list-video">
        <%for (var i = 0; listItem != null && i < listItem.Count; i++){
            string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
        %>
        <li>
            <a href="<%=url %>" target="_blank" rel="" title="">
                <img src="<%=Utils.GetResizeFile(listItem[i].Thumbnail, 4, 300, 300) %>" class="imgs-video" alt="<%=listItem[i].Name %>" />
                <h2><%=listItem[i].Name %></h2>
                <span class="p-video"></span>
            </a>
        </li>
        <%} %>
    </ul>
</div>
