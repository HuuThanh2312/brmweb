﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;
    string _avatar = !string.IsNullOrEmpty(item.Logo) ? item.Logo.Replace("~/", "/") : string.Empty;
    string _banner = !string.IsNullOrEmpty(item.Banner) ? item.Banner.Replace("~/", "/") : string.Empty;
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>

<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Trạng thái: <%=item.Activity?"Đã xác thực":"Chưa xác thực" %> </div>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            {
                                var listChildPage = SysPageService.Instance.GetByParent_Cache(listPage[i].ID);
                        %>
                        <h3 class="heading-menu"><%=listPage[i].Name %></h3>
                        <%if (listChildPage != null)
                            { %>
                        <%for (int j = 0; j < listChildPage.Count; j++)
                            {%>
                        <li class="<%=ViewPage.IsPageActived(listChildPage[j]) ? "active":string.Empty%>">
                            <a href="<%=ViewPage.GetPageURL(listChildPage[j]) %>"><i class="<%=listChildPage[j].Faicon %>"></i>&nbsp;<%=listChildPage[j].Name %></a>
                        </li>
                        <%} %>
                        <%} %>
                        <%} %>
                        <h3 class="heading-menu">Tài khoản</h3>
                        <li class="">
                            <a href="<%=ViewPage.WebUserCPUrl %>"><i class="fa fa-user"></i>&nbsp;Thông tin tài khoản</a>
                        </li>
                        <li class="">
                            <a href="<%=ViewPage.WebUserCPUpdateUrl %>"><i class="fa fa-key"></i>&nbsp;Thay đổi mật khẩu</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin tài khoản của đối tác</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Thay đổi thông tin</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">Thay ảnh đại diện</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">Thay đổi ảnh banner shop</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form method="post" name="form_info">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Tên hiển thị shop</label>
                                                    <input type="text" placeholder="Phụ kiện mai hoàng" name="Name" value="<%=item.Name %>" class="form-control" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Đường dẫn url shop <sup id="email" style="color: red">(*)</sup></label>
                                                    <input type="text" placeholder="VD: shop.bee" disabled="" value="<%=item.Url %>" class="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Email shop</label>
                                                    <input type="text" placeholder="" name="Email" value="<%=item.Email %>" class="form-control" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Số điện thoại</label>
                                                    <input type="text" placeholder="" value="<%=item.Phone %>" name="Phone" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Mô tả:</label>
                                            <textarea name="Summary" class="form-control" rows="5" placeholder="Chưa có mô tả cho shop của bạn"><%=item.Summary %></textarea>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Mã bảo mật <sup>(*)</sup></label>
                                                <div style="height: 10px;">
                                                    <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                                    <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                                        <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Nhập mã bảo mật</label>
                                                <input type="text" placeholder="Mã bảo mật" name="ValidCode" id="ValidCode" required="required" class="form-control" />
                                            </div>
                                        </div>

                                        <div class="margiv-top-10 text-center">
                                            <button type="submit" style="margin-top: 10px" name="_vsw_action[ShopInfoPOST]" class="btn green">Cập nhật</button>
                                        </div>

                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_2">
                                    <form method="post" name="form_avatar" enctype="multipart/form-data" role="form">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="<%=_avatar %>" alt="" />
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                <div class="text-center">
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new">Chọn ảnh đại diện </span>
                                                        <span class="fileinput-exists">Chọn ảnh khác </span>
                                                        <input type="file" name="upload-logo" onchange="previewFile('logo')">
                                                    </span>
                                                    <%--<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">Xóa </a>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" name="_vsw_action[ImagePost]" class="btn green">Lưu lại thay đổi</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_3">
                                    <form method="post" name="form_banner" enctype="multipart/form-data" role="form">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new " data-provides="fileinput">

                                                <div class="fileinput-new thumbnail banner-shop" style="width: 200px; height: 150px;">
                                                    <img src="<%=_banner %>" alt="" />
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                <div class="text-center">
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new">Chọn ảnh banner </span>
                                                        <span class="fileinput-exists">Chọn ảnh khác </span>
                                                        <input type="file" name="upload-banner" onchange="previewFile('banner')">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" name="_vsw_action[ImageBannerPost]" class="btn green">Lưu lại thay đổi</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">


    function previewFile(type) {
        if (type === 'logo') {
            var img = $('.fileinput-new img');
            var img2 = $('.profile-userpic img');
            var file = document.querySelector('input[name=upload-logo]').files[0];
            var reader = new FileReader();

            if (file) {
                reader.readAsDataURL(file);
            } else {
                img.attr('src', '');
            }

            reader.onloadend = function () {
                img.attr('src', reader.result);
                img2.attr('src', reader.result);
            }
        }
        if (type === 'banner') {
            var img = $('.banner-shop img');
            var file = document.querySelector('input[name=upload-banner]').files[0];
            var reader = new FileReader();

            if (file) {
                reader.readAsDataURL(file);
            } else {
                img.attr('src', '');
            }

            reader.onloadend = function () {
                img.attr('src', reader.result);
            }
        }
    }
</script>

