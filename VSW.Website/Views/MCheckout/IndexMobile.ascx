﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl"%>

<%
    var item = ViewBag.Data as ModOrderEntity;
    var model = ViewBag.Model as MCheckoutModel;
    var _Cart = new Cart();

    long TotalPrice = 0;

    var _WebUser = WebLogin.CurrentUser;
    if (_WebUser != null)
    {
        item.Name = _WebUser.Name;
        item.Email = _WebUser.Email;
        item.Phone = _WebUser.Phone;
        item.Address = _WebUser.Address;
        item.CityID = _WebUser.CityID;
        item.DistrictID = _WebUser.DistrictID;
    }
%>

<div class="giohang">
    <h1><a href="<%=ViewPage.CurrentURL %>"><%=ViewPage.CurrentPage.Name %></a></h1>

    <ul>
        <% for (int i = 0; i < _Cart.Items.Count; i++){
               var _Product = ModProductService.Instance.GetByID(_Cart.Items[i].ProductID);
               if (_Product == null) continue;

               long _PriceCombo = _Product.PriceCombo(_Cart.Items[i].Quantity);
               
               var _Color = ModColorService.Instance.GetByID(_Cart.Items[i].ColorID);
               var _Size = ModSizeService.Instance.GetByID(_Cart.Items[i].SizeID);

               TotalPrice += _Cart.Items[i].Quantity * _Product.Price;
               string _Url = ViewPage.GetURL(_Product.MenuID, _Product.Code);
        %>
        <li>
            <p class="cart_image">
                <a href="<%=_Url %>">
                    <img src="<%=Utils.GetResizeFile(_Product.File, 4, 500, 500)%>" alt="<%=_Product.Name %>" />
                </a>
            </p>
            <h3><a href="<%=_Url %>"><%=_Product.Name %></a></h3>

            <%if (_Color != null){%>
            <p><span>- Màu sắc: <%=_Color.Name%></span></p>
            <%} %>

            <%if (_Size != null){%>
            <p><span>- Kích cỡ: <%=_Size.Name%></span></p>
            <%} %>

            <p class="price_box">
                <span class="price_cart"><%=string.Format("{0:#,##0}", _Product.Price) %> x <%= _Cart.Items[i].Quantity %> = <%=string.Format("{0:#,##0}", _PriceCombo) %></span>
            </p>
        </li>
        <%} %>
    </ul>
</div>

<div class="giohang">

    <h2><a href="<%=ViewPage.CurrentURL %>">Thông tin giao hàng</a></h2>

    <form method="post" name="checkout_form">
        <ul class="customer_form">
            <li>
                <label>Họ và tên<span>*</span></label>
                <p><input type="text" name="Name" value="<%=item.Name %>" required="required" placeholder="Nhập họ và tên" /></p>
            </li>
            <li>
                <label>Email</label>
                <p><input type="email" name="Email" value="<%=item.Email %>" required="required" placeholder="Nhập địa chỉ email" /></p>
            </li>
            <li>
                <label>Điện thoại<span>*</span></label>
                <p><input type="text" name="Phone" value="<%=item.Phone %>" placeholder="Nhập số điện thoại" /></p>
            </li>
            <li>
                <label>Tỉnh / thành<span>*</span></label>
                <p>
                    <select id="CityID" name="CityID" required="required">
                        <option value="0">- chọn tỉnh / thành phố -</option>
                        <%=Utils.ShowDdlMenuByType2("City", ViewPage.CurrentLang.ID, item.CityID)%>
                    </select>
                </p>
            </li>
            <li>
                <label>Quận / huyện<span>*</span></label>
                <p>
                    <input type="hidden" id="DistrictIDValue" value="<%=item.DistrictID > 0 ? item.DistrictID : 0 %>" />
                    <select id="DistrictID" name="DistrictID" required="required">
                        <option value="0">- chọn quận / huyện -</option>
                    </select>
                </p>
            </li>
            <li>
                <label>Địa chỉ giao hàng<span>*</span></label>
                <p><input type="text" name="Address" value="<%=item.Address %>" placeholder="Địa chỉ giao hàng" /></p>
            </li>
            <li>
                <label>Ghi chú</label>
                <p><textarea rows="" cols="" name="Content" placeholder="Nhập ghi chú đơn hàng"><%=item.Content %></textarea></p>
            </li>
            <li>
                <label>Mã bảo mật</label>
                <p>
                    <input type="text" class="security" name="ValidCode" id="ValidCode" required="required" autocomplete="off" placeholder="Mã bảo mật" />
                    <img src="/ajax/Security.html" class="vAlignMiddle pl10" onclick="change_captcha()" id="imgValidCode" alt="security code" />
                </p>
            </li>
        </ul>

        <div class="float_button">
            <a href="javascript:void(0)" onclick="document.checkout_form.submit()" class="datmua">Tiếp tục</a>
            <input type="hidden" name="_vsw_action[AddPOST]" />
            <input type="submit" name="_vsw_action[AddPOST]" style="display: none" />
        </div>
    </form>

</div>

<script type="text/javascript">
    $(function() {
        if($('#CityID').val() > 0){
            get_child($('#CityID').val(), $('#DistrictIDValue').val());
        }

        $('#CityID').on('change', function() {
            get_child($(this).val(), $('#DistrictIDValue').val());
        })
    });
</script>
