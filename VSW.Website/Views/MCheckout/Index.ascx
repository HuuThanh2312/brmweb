﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModOrderEntity;
    var model = ViewBag.Model as MCheckoutModel;
    var _Cart = new Cart();
    long TotalPrice = 0;
    //int TotalQuantity = 0;
    var _rootPM = WebMenuService.Instance.GetByType_Cache("Payment");
    var listPayment = WebMenuService.Instance.GetByParentIDType_Cache(_rootPM != null ? _rootPM.ID : 0, "Payment");
    var _webUser = WebLogin.CurrentUser;

%>


<section class="">
    <form method="post" name="checkout_form">
        <div class="center-container">
            <div class="add-adress-cart">
                <div class="click-add-adr-cart" data-toggle="collapse" data-target="#add-address-cart">
                    <p>Địa chỉ nhận hàng</p>
                    <small class="gray">Vui lòng điền vào địa chỉ giao nhận của bạn</small>
                    <i class="fa fa-angle-down"></i>
                </div>
                <div id="add-address-cart" class="collapse">
                    <div class="form-add-address">
                        <div class="form-group">
                            <input type="text" class="form-control" name="Name" value="<%=_webUser!=null? _webUser.Name: item.Name %>" placeholder="Nhập Tên">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Phone" value="<%=_webUser!=null? _webUser.Phone: item.Phone %>" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Email" value="<%=_webUser!=null? _webUser.Email: item.Email %>" placeholder="Nhập Email">
                        </div>
                        <%--<div class="form-group">
                            <select class="form-control" name="CityID" onchange="get_child(this.value,'<%=_webUser==null?item.DistrictID:_webUser.DistrictID %>')">
                                <option value="0">Tỉnh/Thành Phố</option>
                                <%=Utils.ShowDdlMenuByType1("City", 1,_webUser==null?item.CityID:_webUser.CityID) %>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="DistrictID" name="DistrictID">
                                <option value="">Quận/Huyện</option>
                            </select>
                        </div>--%>
                        <%-- <div class="form-group">
                            <select class="form-control" id="CommunesID" name="CommunesID">
                                <option value="">Phường/Xã</option>
                            </select>
                        </div>--%>
                        <div class="form-group">
                            <textarea name="Address" class="form-control" placeholder="Nhập địa chỉ cụ thể"><%=_webUser!=null?_webUser.Address: item.Address %></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="list-cart2-prd">
                <h5 class="gray text-center">Sản phẩm đã đặt</h5>
                <div class="box-list-cart2-prd">

                    <ul>
                        <%for (int i = 0; i < _Cart.Items.Count; i++)
                            {
                                var product = ModProductService.Instance.GetByID(_Cart.Items[i].ProductID);
                                if (product == null) continue;

                                //var listGift = product.GetGift();
                                var _shop = ModShopService.Instance.GetByID_Cache(_Cart.Items[i].ShopID);

                                TotalPrice += _Cart.Items[i].Quantity * product.Price + _Cart.Items[i].ShippingPrice;
                                string url = ViewPage.GetURL(product.MenuID, product.Code);
                        %>
                        <li>
                            <div class="media positionR">
                                <div class="media-left">
                                    <img src="<%=product.File.Replace("~/","/") %>" class="media-object" style="width: 60px">
                                </div>
                                <div class="media-body">

                                    <p class="mb5"><%=product.Name %></p>
                                    <div class="col-prce-product">
                                        <span class="pr-sale">₫ <%=string.Format("{0:#,##0}",product.Price2) %></span> - <span class="text-color">₫ <%=string.Format("{0:#,##0}",product.Price) %></span>
                                    </div>
                                </div>
                                <span class="number-prd-cart-1 gray">X<%=_Cart.Items[i].Quantity %></span>
                                <div class="title">
                                    <a href="<%=ViewPage.GetURL(_shop!=null?_shop:new ModShopEntity()) %>">
                                        <img src="<%=_shop!=null&& !string.IsNullOrEmpty(_shop.Logo)? _shop.Logo.Replace("~/","/"):string.Empty %>" style="margin-right: 5px; height: 20px" alt=""><%=_shop!=null?"Shop: "+ _shop.Name:string.Empty %>

                                    </a>
                                </div>
                            </div>
                            <div class="total-price-item-prd mt10">
                                <%-- <div class="w50 pull-left text-left">
                                    <div class="gray">SL: <%=_Cart.Items[i].Quantity %> sản phẩm</div>
                                </div>--%>
                                <div class="w100 pull-left text-right">
                                    <div class="gray">Phí vận chuyển: <span class="text-color">₫ <%=string.Format("{0:#,##0}",_Cart.Items[i].ShippingPrice) %></span></div>
                                    <div class="gray">Thành tiền: <span class="text-color">₫ <%=string.Format("{0:#,##0}",product.Price * _Cart.Items[i].Quantity + _Cart.Items[i].ShippingPrice) %></span></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </li>
                        <%} %>
                    </ul>
                    <div class="note-cart-item mt10">
                        <textarea name="Content" class="form-control" rows="2" placeholder="Để lại lưu ý..."><%=item.Content %></textarea>
                    </div>
                </div>
            </div>

            <div class="phuongthucthanhtoan">
                <h5 class="gray text-center">Phương thức thanh toán</h5>
            </div>

            <div class="box-list-phuongthuc">
                <input type="hidden" name="PaymentID" id="PaymentID" value="" />
                <ul>
                    <%for (int i = 0; listPayment != null && i < listPayment.Count; i++)
                        { %>
                    <li>
                        <div class="radio radio-warning">
                            <input type="radio" name="Payment" id="radio<%=i+1 %>" onclick="document.getElementById('PaymentID').value = '<%=listPayment[i].ID%>'" value="<%=listPayment[i].ID %>">
                            <label for="radio<%=i+1 %>">
                                <%=listPayment[i].Name %>
                            </label>
                        </div>
                    </li>
                    <%} %>
                </ul>
            </div>

            <div class="box-total-cart all mb20">
                <ul>
                    <%--<li class="" style="display: flex;">
                        <img src="/Content/skins/images/icon/ic_promo_code.png" class="ic" alt="">
                        <input type="text" class="form-control" name="" placeholder="Vui lòng nhập mã khuyến mãi">
                     </li>
                        <li class="all">
                        <div class="w90 pull-left">
                            <img src="/Content/skins/images/icon/xu.png" class="ic" alt="" style="margin-top: -3px;">
                            <span>Bạn chưa có Becoin xu</span>
                        </div>
                        <div class="w10 pull-left">
                            <div class="checkbox checbox-switch switch-warning positionR">
                                <a href="" class="click-all"></a>
                                <label>
                                    <input type="checkbox" name="" />
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>--%>
                    <li class="all">
                        <div class="w80 pull-left">
                            <p class="mb0">Tổng thanh toán:&nbsp;<span class="text-color">₫ <%=string.Format("{0:#,##0}",TotalPrice) %></span></p>
                            <%-- <small style="color: #F6A700">Nhận 1.500 coin</small>--%>
                        </div>
                        <div class="w20 pull-left">
                            <button type="submit" name="_vsw_action[AddPost]" class="btn btn-buy-cart">Đặt Hàng</button>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</section>
<script type="text/javascript">
     <%if (_webUser != null && _webUser.CityID > 0)
    {%>
    get_child('<%=_webUser.CityID%>','<%=_webUser.DistrictID %>')
    <%}%>
</script>
