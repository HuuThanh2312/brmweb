﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModOrderEntity;
    var model = ViewBag.Model as MCheckoutModel;
    
    var _Cart = new Cart();

    long TotalPrice = 0;
    int TotalQuantity = 0;

    var _RootPM = WebMenuService.Instance.CreateQuery()
                            .Where(o => o.Type == "Payment" && o.ParentID == 0)
                            .ToSingle_Cache();

    var listPayment = _RootPM != null ? WebMenuService.Instance.CreateQuery()
                                                .Where(o => o.Activity == true && o.Type == "Payment" && o.ParentID == _RootPM.ID)
                                                .OrderByAsc(o => new { o.Order, o.ID })
                                                .ToList_Cache() : null;
%>

<section class="box-home all">
    <div class="container-fluid">
        <div class="panel-home-body all boxNewscate ">
            <section class="boxFormPayment clear">
                <div class="row">
                    <form method="post" name="checkout_form">
                        <div class="col-xs-12">
                            <strong class="title">Thông tin Khách hàng</strong>
                            <div class="form-group">
                                <input type="text" class="form-control" name="Name" value="<%=item.Name %>" required="required" placeholder="Họ và tên khách hàng *" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="Email" value="<%=item.Email %>" placeholder="Địa chỉ email" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="Phone" value="<%=item.Phone %>" required="required" placeholder="Điện thoại liên hệ *" />
                            </div>
                            <div class="form-group">
                                <select class="form-control fl w55p" name="CityID" id="CityID" onchange="get_child(this.value, '<%=item.DistrictID %>')" required="required">
                                    <option value="0" selected="selected">- chọn tỉnh / thành -</option>
                                    <%=Utils.ShowDdlMenuByType2("City", ViewPage.CurrentLang.ID, item.CityID) %>
                                </select>
                                <select class="form-control fr w40p" name="DistrictID" id="DistrictID" required="required">
                                    <option value="0" selected="selected">- chọn quận / huyện -</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="Address" value="<%=item.Address %>" required="required" placeholder="Địa chỉ liên hệ *" />
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="Content" rows="5" placeholder="Ghi chú đơn hàng"><%=item.Content %></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <strong class="title">Hình thức thanh toán</strong>
                            <div class="form-group">
                                <label class="radioPure">
                                    <input id="radio7" type="radio" name="radios" checked=""><span class="outer"><span class="inner"></span></span><i>Thanh toán khi nhận hàng (COD)</i></label>
                                <label class="radioPure">
                                    <input id="radio8" type="radio" name="radios" checked=""><span class="outer"><span class="inner"></span></span><i>Chuyển khoản qua ngân hàng</i></label>
                            </div>
                        </div>

                        <div class="col-xs-12 ">
                            <strong class="title">Đơn hàng <span>(<%=_Cart.Items.Count %> sản phẩm)</span></strong>
                            <div class="listItemCart">
                                <%for (int i = 0; i < _Cart.Items.Count; i++){
                                      var _Product = ModProductService.Instance.GetByID(_Cart.Items[i].ProductID);
                                      if (_Product == null) continue;

                                      TotalPrice += _Cart.Items[i].Quantity * _Product.Price;
                                      TotalQuantity += _Cart.Items[i].Quantity;

                                      string _Url = ViewPage.GetURL(_Product.MenuID, _Product.Code);
                                %>
                                <div class="itemCart">
                                    <div class="img">
                                        <a href="<%=_Url %>" title="<%=_Product.Name %>">
                                            <img src="<%=Utils.GetResizeFile(_Product.File, 4, 500, 500) %>" class="img-responsive" title="<%=_Product.Name %>" alt="<%=_Product.Name %>" />
                                        </a>
                                    </div>
                                    <div class="name">
                                        <a href="<%=_Url %>" title="<%=_Product.Name %>"><%=_Product.Name %></a>
                                        <strong class="price"><%=string.Format("{0:#,##0}", _Product.Price) %> VNĐ</strong>
                                    </div>

                                    <div class="option">
                                        <button type="button" class="bt-remove" onclick="delete_cart('<%=i %>')"><span class="fa fa-trash-o"></span></button>
                                        <div class="quantity">
                                            <a href="javascript:void(0)" onclick="decrement($('.cart_quantity_<%=i %>')); update_cart('<%=i %>', parseInt($('.cart_quantity_<%=i %>').html()), '<%=model.returnpath %>')" class="minus"><span class="fa fa-minus"></span></a>
                                            <a href="javascript:void(0)" class="quan cart_quantity_<%=i %>"><%=_Cart.Items[i].Quantity %></a>
                                            <a href="javascript:void(0)" onclick="increment($('.cart_quantity_<%=i %>')); update_cart('<%=i %>', parseInt($('.cart_quantity_<%=i %>').html()), '<%=model.returnpath %>')" class="plus"><span class="fa fa-plus"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <%} %>
                            </div>
                            <div class="total-payment">
                                <span>Tổng thanh toán</span>
                                <strong><%=string.Format("{0:#,##0}", TotalPrice) %> VNĐ</strong>
                            </div>
                            <button type="submit" class="bt-payment" name="_vsw_action[AddPOST]">Xác nhận</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function () {
        if(<%=item.CityID%> > 0){
            get_child('<%=item.CityID%>', '<%=item.DistrictID %>');
        }else if($('#CityID').val() > 0){
            get_child($('#CityID').val(), '<%=item.DistrictID %>');
        }
    });
</script>