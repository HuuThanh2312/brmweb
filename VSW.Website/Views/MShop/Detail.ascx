﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModNewsEntity;
    var listOther = ViewBag.Other as List<ModNewsEntity>;
%>

<div class="row-home-cate all">
    <div class="box-pro-cate all">
        <aside class="leftcate fl">
            <div class="sidebox all mb10">
                <h2 class="title">Hỗ trợ khách hàng</h2>
                <ul class="list-htkh">
                    <li>
                        <div class="img_02 fl">
                            <a href="" rel="">
                                <img src="skins/images/d1.png" alt="support online"></a>
                        </div>
                        <div class="thongtin_02 fr">
                            <h3><a href="" rel="">Tư vấn khách hàng</a></h3>
                            <p class="tomtat">
                                <b>iCare :   090.385.6668</b><br>
                                -HCM:08.54.115.125<br>
                                -HN :04.37.669.333       
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="img_02 fl">
                            <a href="" rel="">
                                <img src="skins/images/d2.png"></a>
                        </div>
                        <div class="thongtin_02 fr">
                            <h3><a href="" rel="">Sách hướng dẫn</a></h3>
                            <p class="tomtat">Những câu hỏi và trả lời về sản phẩm</p>
                            <p class="xemthem">
                                <a href="" class="link-standard" target="_self">Download</a>
                        </div>
                    </li>
                    <li>
                        <div class="img_02 fl">
                            <a href="" rel="nofollow">
                                <img src="skins/images/d3.png"></a>
                        </div>
                        <div class="thongtin_02 fr">
                            <h3><a href="" rel="nofollow">Chính sách bảo hành</a></h3>
                            <p class="tomtat">Những câu hỏi và trả lời về chính sách bảo hành</p>
                            <p class="xemthem">
                                <a href="" class="link-standard" target="_self">Xem thêm</a>
                        </div>
                    </li>
                </ul>
            </div>
        </aside>
        <section class="rightcate fr">
            <div class="nav-title">
                <span class="tl">Tin tức</span>
                <div class="clear"></div>
            </div>
            <article class="thumbnail-news-view">
                <h1>Công nghệ Siemens flexInduction</h1>
                <div class="block_timer_share">
                    <div class="block_timer pull-left"><i class="fa fa-clock-o"></i>12/12/2014</div>
                    <div class="block_share pull-right">
                        <a class="tooltip-top facebook" href="#" title="" data-original-title="Share Facebook"><i class="fa fa-facebook"></i></a>
                        <a class="google" href="" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <a class="twitter" href="" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <a class="email" href="" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <a class="print" href="" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="post_content">
                    <p>Bếp Siemens flexInduction có những tính năng như bếp cảm ứng truyền thống với bốn hoặc năm khu nấu ăn riêng biệt, điểm nổi bật là người sử dụng có thể chọn để liên kết các khu với nhau để tạo thành một hoặc nhiều bề mặt nấu hình chữ nhật (380x200mm). </p>
                    <p>
                        <figure style="text-align: center;">
                            <img src="images/banner3.jpg" alt="">
                        </figure>
                    </p>
                    <p>Ngoài ra công nghệ nhận diện nồi cũng được tích hợp giúp bếp tiết kiệm năng lượng mà vẫn đạt hiệu quả tối đa. Và sự kỳ diệu thực sự bắt đầu!</p>
                </div>
            </article>
            <ul class="list-new-cate all">
                <li class="col-md-6">
                    <figure class="col-md-3">
                        <a href="">
                            <img src="images/banner.jpg" alt=""></a>
                    </figure>
                    <article class="col-md-9 caption">
                        <h3><a href="">TouchSlider - Bước đột phá trong công nghệ bếp từ</a></h3>
                        <p class="summary">Bếp Từ trở nên dễ dàng sử dụng hơn với bảng điều khiển dạng touchSlider</p>
                    </article>
                </li>
                <li class="col-md-6">
                    <figure class="col-md-3">
                        <a href="">
                            <img src="images/banner2.jpg" alt=""></a>
                    </figure>
                    <article class="col-md-9 caption">
                        <h3><a href="">i-Dos của Siemens – cuộc cách mạng của công nghệ máy giặt</a></h3>
                        <p class="summary">Bếp Từ trở nên dễ dàng sử dụng hơn với bảng điều khiển dạng touchSlider</p>
                    </article>
                </li>
                <li class="col-md-6">
                    <figure class="col-md-3">
                        <a href="">
                            <img src="images/banner3.jpg" alt=""></a>
                    </figure>
                    <article class="col-md-9 caption">
                        <h3><a href="">TouchSlider - Bước đột phá trong công nghệ bếp từ</a></h3>
                        <p class="summary">Bếp Từ trở nên dễ dàng sử dụng hơn với bảng điều khiển dạng touchSlider</p>
                    </article>
                </li>
                <li class="col-md-6">
                    <figure class="col-md-3">
                        <a href="">
                            <img src="images/img01.jpg" alt=""></a>
                    </figure>
                    <article class="col-md-9 caption">
                        <h3><a href="">Tủ lạnh Siemens - Công nghệ cho cuộc sống đỉnh cao</a></h3>
                        <p class="summary">Bếp Từ trở nên dễ dàng sử dụng hơn với bảng điều khiển dạng touchSlider</p>
                    </article>
                </li>
            </ul>
        </section>
    </div>
</div>











<div class="bg-reviewCont">
    <h1 class="title"><%=item.Name %></h1>
    <div class="post-meta"><%=string.Format("{0:F}", item.Published) %></div>
    <div class="post-content clear">
        <%=Utils.GetHtmlForSeo(item.Content) %>
    </div>
    <div class="like-fb clear">
        <div class="cont-likefb clear">
            <div class="fb-like" data-href="<%=ViewPage.GetURL(item.MenuID, item.Code) %>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            <div class="g-plusone" data-size="medium"></div>
        </div>
    </div>
    <%if (listOther != null){%>
    <div class="news_other all">
        <h3>Tin khác</h3>
        <ul>
            <%for (int i = 0; listOther != null && i < listOther.Count; i++){
                string url = ViewPage.GetURL(listOther[i].MenuID, listOther[i].Code);
            %>
            <li><a href="<%=url %>" title="<%=listOther[i].Name %>"><%=listOther[i].Name %></a></li>
            <%} %>
        </ul>
    </div>
    <%} %>
</div>