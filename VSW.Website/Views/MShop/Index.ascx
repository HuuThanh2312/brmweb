﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModProductEntity>;
    var model = ViewBag.Model as MShopModel;
    string _url = VSW.Core.Global.Convert.ToString(VSW.Core.Web.HttpQueryString.GetValue("atr"));
    _url = !string.IsNullOrEmpty(_url) ? "?atr=" + _url : string.Empty;

%>
<div class="col-md-9 box-danhmuc-sanpham">
    <div class="heaer-filter-product">
        <ul class="nav nav-justified">
            <li class="active"><a href="">Phổ biến</a></li>
            <li><a href="">Mới nhất</a></li>
            <li><a href="">Bán chạy</a></li>
            <li class="fil-price"><a href="">Giá&nbsp;<i class="fa fa-angle-down"></i></a>
                <ul class="submenu-price">
                    <li>
                        <a href="">GIÁ: THẤP ĐẾN CAO</a>
                    </li>
                    <li>
                        <a href="">GIÁ: CAO ĐẾN THẤP</a>
                    </li>
                </ul>
            </li>
            <li class="hidden-xs">
                <div class="checkbox checkbox-warning positionR">
                    <a href="" class="click-all"></a>
                    <input id="checkbox5z" type="checkbox">
                    <label for="checkbox5z">
                        Shop yêu thích
                    </label>
                </div>
            </li>
        </ul>
    </div>
    <div class="list-prd-ctgr-right owl-prd-main">
        <ul class="list_product_main_show">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                { %>
            <li>
                <a href="<%=ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code) %>" class="click-all"></a>
                <%--<div class="shopee-item-card__preferred-badge-wrapper">
                    <div class="shopee-horizontal-badge shopee-preferred-seller-badge">
                        <svg class="shopee-svg-icon icon-tick" enable-background="new 0 0 15 15" viewBox="0 0 15 15" x="0" y="0">
                            <g>
                                <path d="m6.5 13.6c-.2 0-.5-.1-.7-.2l-5.5-4.8c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l4.7 4 6.8-9.4c.3-.4.9-.5 1.4-.2.4.3.5 1 .2 1.4l-7.4 10.3c-.2.2-.4.4-.7.4 0 0 0 0-.1 0z"></path>
                            </g>
                        </svg>Yêu thích
                    </div>
                </div>--%>
                <!-- ribon yêu thích -->
                <%if (listItem[i].Price2 > listItem[i].Price)
                    { %>
                <div class="shopee-item-card__badge-wrapper">
                    <div class="shopee-badge shopee-badge--fixed-width shopee-badge--promotion">
                        <div class="shopee-badge--promotion__label-wrapper shopee-badge--promotion__label-wrapper--vi-VN">
                            <span class="percent"><%=listItem[i].SellOffPercent %>%</span>
                            <span class="shopee-badge--promotion__label-wrapper__off-label shopee-badge--promotion__label-wrapper__off-label--vi-VN">giảm</span>
                        </div>
                    </div>
                </div>
                <%} %>
                <!--- ribon sale -->

                <div class="item-product">
                    <div class="thumb-img-prd">
                        <div class="hm-reponsive">
                            <img src="<%=listItem[i].File.Replace("~/","/") %>" alt="<%=listItem[i].Name %>">
                        </div>
                        <%--<img src="images/icon/revodich.png" class="shopee-item-card__lowest-price" alt="lowest price">--%>
                    </div>

                    <div class="info-item-product">
                        <h3 class="title-product eclip-2"><%=listItem[i].Name %>
                        </h3>
                        <div class="box-price-prd">
                            <div class="shopee-item-card_original-price">₫ <%=string.Format("{0:#,##0}",listItem[i].Price2)%></div>
                            <div class="shopee-item-card__current-price shopee-item-card__current-price--free-shipping">₫ <%=string.Format("{0:#,##0}",listItem[i].Price) %></div>
                        </div>
                        <div class="bottom-icon">
                            <div class="city-prd">
                                <i class="fa fa-heart"></i>&nbsp;<%=listItem[i].Favorite %>
                            </div>
                            <div class="lead">
                                <div id="stars-existing" class="starrr" data-rating="3"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></div>
                                &nbsp;<span class="gray">(<%=listItem[i].Vote %>)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <%} %>
        </ul>
        <div class="clearfix"></div>

        <div class="text-center mt40 mb40">
            <ul class="pagination">
                <%= GetPagination(_url,model.page, model.PageSize, model.TotalRecord)%>
                <%--<%= GetPagination(Code,model.page,model.PageSize,model.TotalRecord)%>--%>
            </ul>
        </div>
    </div>
</div>
