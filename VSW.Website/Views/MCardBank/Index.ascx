﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    //  var _accBank = item.GetBank();
    var listBank = ViewBag.Data as List<ModBankingUserEntity>;
    // var listTranctions = item.GetListTransaction();
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>


<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                </div>

                <%--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle red btn-sm">Chi tiết hoa hồng</button>
                </div>--%>

                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Trình quản lý</span>
                                <%-- <span class="selected"></span>--%>
                            </a>
                        </li>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>

            </div>

        </div>

        <div class="profile-content">
            <div class="row">
                <div class="col-md-6">

                    <div class="portlet light" style="min-height: 437px;">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Danh sách tài khoản</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table">
                                    <thead>
                                        <tr class="active">
                                            <th>Tên ngân hàng</th>
                                            <th>Số tài khoản</th>
                                            <th>Số thẻ</th>
                                            <th>Loại tiền</th>
                                            <th>Xem/Sửa</th>
                                        </tr>
                                    </thead>
                                    <%for (int i = 0; listBank != null && i < listBank.Count; i++)
                                        { %>
                                    <tr>
                                        <td><%=listBank[i].Activity? "<i class=\"fa fa-check\" style=\"color:chartreuse\"></i>":"&nbsp;&nbsp;&nbsp;&nbsp;" %> <%=listBank[i].NameBank %></td>
                                        <td><%=listBank[i].AccountCode %></td>
                                        <td><%=listBank[i].CardCode %></td>
                                        <td>VNĐ</td>
                                        <td><a href="javascript:void(0)" onclick="getHtml('<%=listBank[i].ID %>')"><i class="fa fa-eye"></i>/<i class="fa fa-pencil-square-o"></i></a></td>
                                    </tr>

                                    <%} %>
                                </table>
                                <table class="table">
                                    <tr>
                                        <td>
                                            <div class="form-group text-center mt10">
                                                <button type="button" onclick="getHtml('Add')" class="btn blue btn-outline">Thêm</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Tài khoản</span>
                            </div>
                        </div>
                        <div class="portlet-body" id="add_update_tk">
                            <form method="post" accept-charset="utf-8">
                                <div class="form-group">
                                    <input type="text" name="NameBank" class="form-control" value="" placeholder="Tên ngân hàng">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="AddressBank" class="form-control" value="" placeholder="Chi nhánh ngân hàng">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="AccountCode" class="form-control" value="" placeholder="Tài khoản ngân hàng">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="CardCode" class="form-control" value="" placeholder="Số thẻ">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="Name" class="form-control" value="" placeholder="Tên tài khoản">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="text" name="" class="form-control" placeholder="Mã bảo mật">
                                        </div>
                                        <div class="col-xs-6">
                                            <img src="http://berichmart.com.vn/member/account/captcha?v=5b0475821ee31" alt="" style="height: 35px;">
                                            <a href="javascript:;" class="btn btn-icon-only default">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center mt10">
                                    <button type="submit" class="btn blue btn-outline">Cập nhật</button>
                                    <button type="button" class="btn dark btn-outline">Nhập lại</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <%--<div class="col-md-12">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Quên mật khẩu thẻ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form action="" method="get" accept-charset="utf-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <input type="" name="" class="form-control" placeholder="Mã bảo mật">
                                        </div>
                                        <div class="col-xs-3">
                                            <img src="http://berichmart.com.vn/member/account/captcha?v=5b0475821ee31" alt="" style="height: 35px;">
                                            <a href="javascript:;" class="btn btn-icon-only default">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <button type="submit" class="btn blue btn-outline">Gửi</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="note note-info">
                                    <p>
                                        <b>Bước 1: Nhập mã bảo mật website và ấn nút gửi<br>
                                            <br>
                                            Bước 2: Hệ thống sẽ gửi một tin nhắn mã số bảo mật tới điện thoại của bạn. Nhập chính xác mã số bảo mật điện thoại.<br>
                                            <br>
                                            Bước 3: Hệ thông kiểm tra mã số bảo mật điện thoại bạn nhập vào. Nếu đúng một mật khẩu thẻ mới sẽ được gửi vào số điện thoại trong gian hàng của bạn
                                        </b>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>--%>
            </div>
        </div>

    </div>

</div>
<style type="text/css">
    img#imgValidCode {
        width: 75%;
    }
</style>
