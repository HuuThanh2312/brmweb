﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModShopEntity;
%>

<h1 class="title-manage">Thông tin tài khoản</h1>
<div class="bgmanage-content">
    <div class="account-profile col-lg-12 col-md-12 col-sm-12 col-12">
        <form method="post" class="form-list" name="password_form">
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Tên gian hàng</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Name" value="<%=item.Name %>" placeholder="Nhập tên gian hàng" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>URL (địa chỉ hiển thị trên trình duyệt)</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="URL" value="<%=item.URL %>" placeholder="Nhập tên gian hàng" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Logo</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="file" class="form-control" name="Logo" />
                    <input type="hidden" name="Logo" value="<%=item.Logo %>" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Banner</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="file" class="form-control" name="File" />
                    <input type="hidden" name="File" value="<%=item.File %>" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Số điện thoại</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Phone" value="<%=item.Phone %>" placeholder="Nhập số điện thoại liên hệ" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Địa chỉ</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Address" value="<%=item.Address %>" placeholder="Nhập địa chỉ liên hệ" />
                </div>
            </div>
            
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label></label>
                </div>
                <div class="text-left col-md-9">
                    <button type="submit" class="btn btn-blue" name="_vsw_action[AddPOST]">Cập nhật</button>
                </div>
            </div>
        </form>
    </div>
</div>