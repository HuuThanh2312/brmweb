﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var webUser = WebLogin.CurrentUser;
%>

<div class="hea-member">
    <span>Hiện có</span>
    <span class="nber"><%=string.Format("{0:#,##0}", ModWebUserService.Instance.GetCount()) %></span>
    <span>Thành viên</span>
</div>
<div class="control-user-notifi  mt5">
    <ul class="ul-box-inline thongbao-header">
        <li>
            <div class="positionR">
                <a href="#" rel="nofollow"><i class="fa fa-bell-o"></i></a>
                <span class="number-notifi"><%=string.Format("{0:#,##0}", ModBSNotificationService.Instance.GetCount(webUser)) %></span>
            </div>
        </li>
        <li>
            <div class="positionR">
                <a href="#" rel="nofollow"><i class="fa fa-commenting-o"></i></a>
                <span class="number-notifi"><%=string.Format("{0:#,##0}", ModBSMessageService.Instance.GetCount(webUser)) %></span>
            </div>
        </li>
    </ul>
</div>

<%if(webUser != null) {%>
<div class="box-user-login">
    <a href="javasscript:void(0)" class="dropdown-toggle" data-toggle="dropdown" rel="nofollow">
        <div class="img-avt fl">
            <img src="images/avatar/1.jpg" alt="">
        </div>
        <div class="info-avt-top fl">
            <h6 class="mb5 name"><b><%=webUser.Name %></b></h6>
            <p class="text-sm">Tài khoản: <%=webUser.IsVIP ? "VIP" : "Cá nhân" %></p>
            <p class="text-sm">Ngân quỹ: <span class="number-coin"><%=string.Format("{0:#,##0}", webUser.Coin) %> Coin</span></p>
        </div>
        <i class="fa fa-align-justify"></i>
    </a>
    <ul class="dropdown-menu dropdown-user" role="menu">
        <li><a href="#"><i class="fa fa-info-circle" style="color: #ffb000;"></i>&nbsp;Thông báo</a></li>
        <li><a href="#"><i class="fa fa-wechat" style="color: #5fa41f;"></i>&nbsp;Tin nhắn</a></li>
        <li><a href="#"><i class="fa fa-line-chart" style="color: #ff0047;"></i>&nbsp;Thống kê tương tác</a></li>
        <li><a href="#"><i class="fa fa-book" style="color: #337ab7;"></i>&nbsp;Nhật ký hoạt động</a></li>
        <li class="divider"></li>
        <li><a href="<%=ViewPage.LogoutUrl %>"><i class="fa fa-sign-out"></i>&nbsp;Đăng xuất</a></li>
    </ul>
</div>
<%}else {%>
<div class="box-user-login ">
    <ul class="login-box">
        <li><a href="<%=ViewPage.RegisterUrl %>" rel="nofollow">Đăng ký</a></li>
        <li><a href="<%=ViewPage.LoginUrl %>" rel="nofollow">Đăng nhập</a></li>
    </ul>
</div>
<%} %>
