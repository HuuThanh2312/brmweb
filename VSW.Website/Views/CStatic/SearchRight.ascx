﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="boloc-news">
    <div class="loc">
        <div class="title-loc">Tìm kiếm bài viết</div>
        <div class="form-group group-news all">
            <form method="post" name="news_form" onsubmit="doNews(); return false;">
                <input type="text" class="form-control search" name="news" id="news" autocomplete="off" placeholder="{RS:Web_SearchText}" />
                <a href="javascript:void(0)" class="btn search-new" onclick="doNews(); return false;"><i class="fa fa-search"></i></a>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function doNews() {
        var sURL = '';

        if ($('#news').val().length < 2) {
            zebra_alert('Thông báo !', 'Từ khóa phải nhiều hơn 1 ký tự.');
            return;
        }
        else sURL += (sURL == '' ? '?' : '&') + 'news=' + keyword.value;

        location.href = '<%=ViewPage.CurrentURL%>' + sURL;
    }
</script>