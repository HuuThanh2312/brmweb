﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewPage.ViewBag.Data;
%>

<section class="block-breadcrumb">
    <ul class="breadcrumb ">
        <li><a href="">Trang chủ</a></li>
        <li><a href="">Bếp điên, Bếp từ </a></li>
    </ul>
</section>


<section class="block-breadcrumb">
    <ul itemprop="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#" class="breadcrumb">
        <%= Utils.GetMapPage(ViewPage.CurrentPage) %>
        <%if (item != null){%>
        <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<%=ViewPage.GetURL(item.MenuID, item.Code) %>" itemprop="url"><span itemprop="title"><%=item.Name %></span></a></li>
        <%} %>
    </ul>
</section>