﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div id="main-header-top-bar" class="show-for-large dont-print">
    <div class="c-header-top-bar">
        <div class="row">
            <div class="column align-middle text-center">
                <a href="/feedback/" class="c-header-top-bar__item">
                    <i class="o-linear-icon c-header-top-bar__icon">bubbles</i>
                    <span>Góp ý</span>
                    <i class="o-linear-icon c-header-top-bar__icon">right6</i>
                </a>
            </div>
            <div class="columns align-middle text-right">
                <span class="c-header-top-bar__item">7 ngày / tuần. Từ 8h đến 22h</span>
                <a href="/customerservice/telephone-support.html" class="c-header-top-bar__item">
                    <i class="o-linear-icon c-header-top-bar__icon">phone</i>
                    <span>+49&nbsp;2161&nbsp;/&nbsp;9020&nbsp;-&nbsp;210</span>
                    <i class="o-linear-icon c-header-top-bar__icon">right6</i>
                </a>
            </div>
        </div>
    </div>
</div>