﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = WebMenuService.Instance.CreateQuery()
                                .Where(o => o.Activity == true && o.Type == "Brand" && o.ParentID > 0)
                                .OrderByAsc(o => o.Order)
                                .ToList_Cache();
%>

<div class="company-top-left">
    <h3>Các đối tác hàng đầu</h3>
</div>
<div class="company-content-left">
    <ul class="company-content">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                if (string.IsNullOrEmpty(listItem[i].File)) continue;
        %>
        <li><img src="<%=listItem[i].File.Replace("~/", "/") %>" alt="<%=listItem[i].Name %>" /></li>
        <%} %>
    </ul>
</div>