﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="c-navi-top-bar__search-full">
    <div itemscope itemtype="//schema.org/WebSite">
        <form class="search-form" method="post" itemprop="potentialAction" itemscope itemtype="//schema.org/SearchAction" onsubmit="doSearch(); return false;">
            <div class="input-group o-search-input-group">
                <input type="text" itemprop="query-input" name="keyword" id="keyword" placeholder="{RS:Web_SearchText}" value="" autocomplete="off" class="input-group-field o-search-input clearable ffsearch">
                <div class="input-group-button">
                    <button class="square button c-arrow-button" type="submit"onclick="doSearch(); return false;">
                        <span class="c-arrow-button__arrow">
                            <i class="c-triangle-arrow c-triangle-arrow--left"></i>
                        </span>
                        <i class="o-linear-icon">search</i>
                    </button>
                </div>
                <div class="input-group-button hide-for-small-only">
                    <button class="square alert button is-phone" type="button" data-open="mobile-contact-modal">
                        <i class="o-linear-icon">phone</i>
                        <span class="show-for-medium hide-for-large">until 10pm</span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function doSearch() {
        var sURL = '';

        if ($('#keyword').val().length < 2) {
            zebra_alert('Thông báo !', 'Từ khóa phải nhiều hơn 1 ký tự.');
            return;
        }
        else sURL += (sURL == '' ? '?' : '&') + 'keyword=' + keyword.value;

        location.href = '<%=ViewPage.GetURL("tim-kiem")%>' + sURL;
    }
</script>