﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div id="footer__segment_newsletter">
    <div class="container">
        <div id="footer__segment_newsletter__title">Đăng ký nhận<br />bản tin Vietbuild24h.com</div>
        <div id="footer__segment_newsletter__description">Cập nhật thông tin khuyến mãi nhanh nhất<br />Hưởng quyền lợi giảm giá riêng biệt</div>
        <form method="post" id="frmNewsLetter">
            <input type="text" class="form-control" name="Email" id="newsleterInput" placeholder="Email của bạn" maxlength="255" />
            <input type="submit" name="_vsw_action[SubscribePOST-CStatic-Subscribe]" id="footer__segment_newsletter__form__submit" class="form-control adr button" value="Gửi" />
        </form>
    </div>
</div>