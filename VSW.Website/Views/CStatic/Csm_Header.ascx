﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>
<% 
    var _webUser = WebLogin.CurrentUser;
    if (_webUser == null) return;
    string _avatar = !string.IsNullOrEmpty(_webUser.File) ? _webUser.File.Replace("~/", "/") : string.Empty;
%>


<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">

        <div class="page-logo">
            <a href="/">
                <span class="logo-csm logo-default">BeRichMart</span>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>

        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-default">2 </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3><span class="bold">2</span> Thông báo mới</h3>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">

                                <li>
                                    <a href="javascript:;">
                                        <span class="details">
                                            <span class="label label-sm label-icon label-warning">
                                                <i class="fa fa-bell-o"></i>
                                            </span>Bạn đã nhận được hoa hồng </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="details">
                                            <span class="label label-sm label-icon label-warning">
                                                <i class="fa fa-bell-o"></i>
                                            </span>Số dư của bạn đã thay đổi 
                                        </span>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<%=_avatar %>" />
                        <span class="username username-hide-on-mobile"><%=_webUser.Name %> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <%if (_webUser.GetShop() != null)
                            { %>
                        <li>
                            <a href="<%=ViewPage.BuninesskUrl %>">
                                <i class="icon-users"></i>Quản lý kênh bán hàng
                            </a>
                        </li>
                        <%} %>
                        <li>
                            <a href="<%=ViewPage.TreeUserUrl %>">
                                <i class="icon-users"></i>Quản lý thành viên
                            </a>
                        </li>
                        <li>
                            <a href="<%=ViewPage.WebUserCPUrl %>">
                                <i class="icon-user"></i>Quản lý tài khoản
                            </a>
                        </li>
                        <li>
                            <a href="<%=ViewPage.LogoutUrl %>">
                                <i class="icon-key"></i>Đăng xuất
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="<%=ViewPage.LogoutUrl %>" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
