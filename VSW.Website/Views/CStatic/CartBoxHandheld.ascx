﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var _Cart = new Cart();
    var _Favorite = new Cart("Favorite");

    long _Total = 0;
%>

<div class="columns text-right">
    <div class="c-navi-top-bar__icons c-navi-top-bar__icons--expand">
        <a href="<%=ViewPage.FavoriteURL %>" class="c-navi-top-bar__icon c-navi-top-bar__icon--square is-empty js-notepad-header-icon" rel="nofollow">
            <span class="c-bag-compartment-icon">
                <span class="c-bag-compartment-icon__badge has-1-digit js-notepad-count js-notepad-count-badge"><%=string.Format("{0:#,##0}", _Favorite.Items.Count) %></span>
                <i class="c-bag-compartment-icon__icon o-linear-icon">heart</i>
            </span>
        </a>
        <a href="<%=ViewPage.ViewCartURL %>" class="c-navi-top-bar__icon c-navi-top-bar__icon--square c-dropdown-toggle js-mini-cart-toggle is-empty" data-toggle="cartDropdownMobile" tabindex="0">
            <span class="c-dropdown-toggle__arrow c-dropdown-toggle__arrow--bottom has-cart"></span>
            <span class="c-bag-compartment-icon">
                <span class="c-bag-compartment-icon__badge c-bag-compartment-icon__badge--grow-rtl js-cart-sum-qty has-1-digit"><%=string.Format("{0:#,##0}", _Cart.Items.Count) %></span>
                <i class="c-bag-compartment-icon__icon o-linear-icon">cartEmpty</i>
            </span>
        </a>
    </div>
</div>