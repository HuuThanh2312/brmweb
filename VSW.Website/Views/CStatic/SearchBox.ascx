﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listKeyword = ViewBag.Keyword as List<ModKeywordEntity>;
    var _Cart = new Cart();
%>




<div class="search-top">
    <form method="post" method="get" accept-charset="utf-8">
        <input type="text" class="form-control" name="keyword" id="keyword" value="" placeholder="{RS:Web_SearchText}" autocomplete="off" />
        <button type="button" onclick="doSearch(); return false;" class="btn btn-search">
            <i class="fa fa-search"></i>
        </button>
    </form>
    <div class="top-key-search">
        <%for (int i = 0;listKeyword!=null && i < listKeyword.Count; i++)
            {%>
        <a href="<%=ViewPage.SearchUrl %>?keyword=<%=listKeyword[i].Code %>"><%=listKeyword[i].Name.ToLower() %></a>
        <%} %>
    </div>
</div>
<div class="cart positionR">
    <a href="<%=ViewPage.ViewCartUrl %>" class="my-cart positionR">
        <i class="fa fa-shopping-cart"></i>
        <sup><%=_Cart.Items.Count %></sup>
    </a>
    <div class="cart-hover ul-muiten">
        <p class="gray">Sản phẩm mới thêm</p>
        <ul>
            <%for (int i = 0; _Cart != null && i < _Cart.Count; i++)
                {
                    var _Product = ModProductService.Instance.GetByID_Cache(_Cart.Items[i].ProductID);
                    if (_Product == null) continue;
                    %>
            <li>
                <div class="media w80 pull-left">
                    <div class="media-left">
                        <img src="<%=_Product.File.Replace("~/","/") %>" class="media-object" style="width: 40px">
                    </div>
                    <div class="media-body">
                        <p class="eclip-1 mb5"><%=_Product.Name %></p>
                        <small class="gray">Phân loại hàng: <%=_Product.GetMenu().Name %></small>
                    </div>
                </div>
                <div class="w20 pull-left text-right">
                    <span class="text-color">₫ <%=string.Format("{0:#,##0}",_Product.Price) %></span><br>
                    <a href="javascript:void(0)" onclick="delete_cart('<%=i %>', '')" class="gray">Xóa</a>
                </div>
                <div class="clearfix"></div>
            </li>
            <%} %>
        </ul>
    </div>
</div>

<script type="text/javascript">
    function doSearch() {
        var sURL = '';
        var keyword = $('#keyword').val();

        if (keyword.length < 2 || keyword == 'Nhập từ khóa tìm kiếm') {
            zebra_alert('Thông báo !', 'Từ khóa phải nhiều hơn 1 ký tự.');
            return;
        }
        var url='<%=ViewPage.SearchUrl%>?keyword=' + keyword;

        location.href = url;
    }
</script>
