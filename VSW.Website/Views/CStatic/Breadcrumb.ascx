﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    string SortMode
    {
        get
        {
            string sort = VSW.Core.Web.HttpQueryString.GetValue("sort").ToString().ToLower().Trim();

            if (sort == "new_asc" || sort == "price_asc" || sort == "price_desc" || sort == "view_desc")
                return sort;

            return "new_asc";
        }
    }

    string GetURL(string key, string value)
    {
        string url = string.Empty;
        for (int i = 0; i < ViewPage.PageViewState.Count; i++)
        {
            var tempKey = ViewPage.PageViewState.AllKeys[i];
            var tempValue = ViewPage.PageViewState[tempKey].ToString();

            if (tempKey.ToLower() == key.ToLower() || tempKey.ToLower() == "vsw" || tempKey.ToLower().Contains("web."))
                continue;

            if (url == string.Empty)
                url = "?" + tempKey + "=" + ViewPage.Server.UrlEncode(tempValue);
            else
                url += "&" + tempKey + "=" + ViewPage.Server.UrlEncode(tempValue);
        }

        url += (url == string.Empty ? "?" : "&") + key + "=" + value;

        return url;
    }
</script>

<% 
    var item = ViewPage.ViewBag.Data;
    string module = ViewPage.CurrentModule.Code.Substring(1);
    var listItem = VSW.Lib.Global.ListItem.List.GetListByConfigkey("Mod." + module + "State");
%>



<ul itemprop="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#" class="breadcrumb">
    <%= Utils.GetMapPage(ViewPage.CurrentPage) %>
</ul>
