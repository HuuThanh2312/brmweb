﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="c-oc-fixed__body" data-oc-fixed-body="" data-off-canvas-scrollbox="">
    <div class="c-oc-dark-content">
        <p>
            <a href="tel:+4921619020210" class="c-oc-phone">
                <i class="o-linear-icon">phone</i>
                +49&nbsp;2161&nbsp;/&nbsp;9020&nbsp;-&nbsp;210
            </a>
            <small class="text-nowrap">7 ngày / tuần. Từ 8h đến 22h</small>
        </p>
        <div class="c-narrow-button-group">
            <button type="button" data-open="mobile-contact-modal" class="button light hollow" aria-controls="mobile-contact-modal" aria-haspopup="true" tabindex="0">
                Liên hệ
            </button>
            <a href="/feedback/" class="button light hollow">Góp ý</a>
        </div>
    </div>
    <VSW:StaticControl Code="CMenu" VSWID="vswMenuTopHandheld" runat="server" />
</div>