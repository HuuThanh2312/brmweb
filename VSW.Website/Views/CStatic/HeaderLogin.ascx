﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var _webUser = WebLogin.CurrentUser;
    var model = ViewBag.Model as CStaticModel ?? new CStaticModel();
    int _uid = VSW.Core.Web.HttpQueryString.GetValue("uid").ToInt();
    string _uid2 = VSW.Core.Web.HttpQueryString.GetValue("returnpath").ToString();

%>
<div class="header_top">
    <div class="container">
        <ul class="nav-left-top">
            <li>
                <a href="<%=ViewPage.BuninesskUrl %>">kênh người bán</a>
            </li>
            <li>
                <a href="javascrpit:void(0)">Kết nối</a>
                &nbsp;<span><a href="https://www.facebook.com/"><i class="fa fa-facebook-square"></i></a></span>
                &nbsp;<span><a href="https://www.facebook.com/"><i class="fa fa-instagram"></i></a></span>
            </li>
        </ul>
        <ul class="nav-left-top nav-right-top">
            <li>
                <a href=""><i class="fa fa-question-circle"></i>Trợ giúp</a>
            </li>
            <%if (_webUser != null)
                { %>
            <li>
                <a href=""><i class="fa fa-bell"></i>Thông báo</a>
            </li>
            <li>
                <a href="<%=ViewPage.WebUserCPUrl %>"><%=_webUser.LoginName %></a>
            </li>
            <li>
                <a href="<%=ViewPage.LogoutUrl %>">Đăng Xuất</a>
            </li>
            <%}
                else
                {%>

            <li>
                <a class="onclick" data-toggle="modal" data-target="#Modal_Resgister">Đăng ký</a>
                <div id="Modal_Resgister" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center"><b>ĐĂNG KÝ TÀI KHOẢN</b></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-singup row" id="register_form" method="post" name="register_form" accept-charset="utf-8">
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Tên tài khoản <sup>(*)</sup></label>
                                        <input name="LoginName" value="<%=model.LoginName %>" type="text" onchange="check_acc(this.value,'LoginName'); return false;" placeholder="Nhập tên dăng nhập" class="form-control input-md loginname_changer" required="">
                                        <p id="messenger" class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Email <sup>(*)</sup></label>
                                        <input name="Email" value="<%=model.Email %>" type="text" placeholder="Nhập địa chỉ Email" onchange="check_acc(this.value,'email'); return false;" class="form-control input-md" required="">
                                        <p id="email" class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Số điện thoại <sup>(*)</sup></label>
                                        <input name="Phone" value="<%=model.Phone %>" type="text"<%-- onfocus="(this.type='number')"--%> placeholder="Nhập số điện thoại" onchange="check_acc(this.value,'Phone'); return false;" class="form-control input-md phone" required="">
                                        <p id="Phone" class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Họ và tên <sup>(*)</sup></label>
                                        <input name="Name" value="<%=model.Name %>" type="text" onkeyup="changer_upper(this.value)" id="Name" placeholder="Nhập họ và tên" class="form-control input-md name_changer" required="">
                                        <p class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Ngày sinh <sup>(*)</sup></label>
                                        <input name="BirthDay" onfocus="(this.type='date')" id="StartDate" value="<%=model.BirthDay> DateTime.MinValue? string.Format("{0:yyyy-MM-dd}", model.BirthDay)  :""%>" type="text" placeholder="" class="form-control input-md" required="">
                                        <p class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Địa chỉ <sup>(*)</sup></label>
                                        <input name="Address" value="<%=model.Address %>" type="text" placeholder="" class="form-control input-md" required="">
                                        <p class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Mật khẩu <sup>(*)</sup></label>
                                        <input name="Password" id="Password" value="<%=model.Password %>" type="password" placeholder="" onchange="check_acc(this.value,'Pass'); return false;" class="form-control input-md" required="">
                                        <p id="Pass" class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Nhập lại mật khẩu <sup>(*)</sup></label>
                                        <input name="Password2" value="<%=model.Password2 %>" type="password" placeholder="" onchange="check_pass(this.value,$('#Password').val()); return false;" class="form-control input-md" required="">
                                        <p id="Password2" class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Nhập mã bảo mật <sup>(*)</sup></label>
                                        <input type="text" class="form-control w30p security fl" name="ValidCode" id="ValidCode" required="required" autocomplete="off" value="" size="40" placeholder="Mã bảo mật" />
                                        <p class="error"></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Mã bảo mật <sup>(*)</sup></label>
                                        <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                        <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                            <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                        </a>
                                    </div>
                                    <div class="form-group text-center mt20 col-md-12">
                                        <button type="submit" name="_vsw_action[CStatic-RegisterPOST-HeaderLogin]" class="btn button-resuser">Đăng ký tài khoản</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <li>
                <a href="" class="login" data-toggle="modal" data-target="#Modal_login">Đăng Nhập</a>
                <div id="Modal_login" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center"><b>ĐĂNG NHẬP</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="main">

                                    <h5 class="text-center">Đăng nhập bằng cách sau </h5>
                                    <%--<div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <a href="#" class="btn btn-lg btn-primary btn-block"><i class="fa fa-facebook"></i>&nbsp;Facebook</a>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <a href="#" class="btn btn-lg btn-info btn-block btn-login-gg"><i class="fa fa-google-plus"></i>&nbsp;Google</a>
                                        </div>
                                    </div>
                                    <div class="login-or">
                                        <hr class="hr-or">
                                        <span class="span-or">Hoặc</span>
                                    </div>--%>

                                    <form role="form" method="post" id="login_form" name="login_form" class="form-singup">
                                        <div class="form-group">
                                            <label for="inputUsernameEmail">Tài khoản/Email</label>
                                            <input type="text" class="form-control" name="LoginName" value="" id="inputUsernameEmail">
                                        </div>
                                        <div class="form-group">
                                            <a class="pull-right" href="<%=ViewPage.ForgotUrl %>" style="color: aqua">Bạn quên mật khẩu?</a>
                                            <label for="inputPassword">Mật khẩu</label>
                                            <input type="password" class="form-control" name="Password" value="Password" id="inputPassword">
                                        </div>
                                        <div class="checkbox pull-right mt30" style="line-height: 15px;">
                                            <label>
                                                <input type="checkbox">
                                                Nhớ mật khẩu
                                            </label>
                                        </div>
                                        <button type="submit" name="_vsw_action[CStatic-LoginPOST-HeaderLogin]" class="btn btn button-resuser mt20 mb20">
                                            Đăng nhập tài khoản
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
</div>
<%if (_uid > 0)
    { %>
<script type="text/javascript">
    $('.onclick').click();
</script>
<%} %>


<%if (!string.IsNullOrEmpty(_uid2))
    { %>
<script type="text/javascript">
    $('.login').click();
</script>
<%} %>

