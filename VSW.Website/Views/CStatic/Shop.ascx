﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var _shop = ViewBag.Data as ModShopEntity;
    if (_shop == null) return;
    string _logo = !string.IsNullOrEmpty(_shop.Logo) ? Utils.GetResizeFile(_shop.Logo, 4, 300, 300) : string.Empty;
%>


<section class="header-home">
    <div class="shop-page">
        <div class="container">
            <div class="box-main-info-shop">
                <div class="overview-shop">
                    <div class="box-flex">
                        <div class="avtshop-main">
                            <img src="<%=_logo %>" alt="<%=_shop.Name %>">
                        </div>
                        <div class="title-shop-name">
                            <h3 class="nameshopview"><%=_shop.Name %></h3>
                            <span>Online 30 phút trước</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="box-flex">
                        <div class="button-overview">
                            <a href="" class="btn btn-chat btn-overview-shop pull-left"><i class="fa fa-comment"></i>&nbsp;Chat</a>
                            <a href="javascript:void(0)" onclick="" class="btn btn-theodoi btn-overview-shop pull-left"><i class="fa fa-plus"></i>&nbsp;Theo dõi</a>
                        </div>
                    </div>
                </div>
                <div class="seller-info-list">
                    <div class="section-seller-overview__item">
                        <i class="fa fa-comments"></i>Tỷ lệ phản hồi: <span>82%</span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-opencart"></i>Sản phẩm: <span><%=_shop.CountProduct %></span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-user"></i>Đang theo dõi: <span><%=_shop.Flowers %></span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-clock-o"></i>thời gian phản hồi: <span>Trong vài giờ</span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-user-plus"></i>Tham gia: <span><%=_shop.PublishedTime %></span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-users"></i>Người theo dõi: <span><%=_shop.Flower %></span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-star"></i>Đánh giá: <span>4.8 / 5 (39517 đánh giá)</span>
                    </div>
                    <div class="section-seller-overview__item">
                        <i class="fa fa-hourglass-start"></i>Thời gian chuẩn bị: <span>Trong 1 ngày</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
