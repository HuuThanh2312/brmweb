﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    if (ViewPage.CurrentPage.ModuleCode == "MLogin") return;
%>

<div class="sticky-container hide-for-large" data-sticky-container>
    <nav class="c-navi-top-bar sticky sticky-smart" data-sticky data-sticky-smart data-options="topAnchor:0; marginTop:0; stickyOn:small;">
        <div class="row align-middle">
            <div class="c-navi-top-bar__menu-column columns">
                <button type="button" data-toggle="mainNavOffcanvas" aria-label="Menu">
                    <span class="c-ticon c-ticon--static c-ticon--dark">
                        <span class="c-ticon__lines"></span>
                    </span>
                    <span class="c-ticon-label c-ticon-label--dark">Menu
                    </span>
                </button>
            </div>
            <div class="columns medium-text-center">
                <a href="/" class="c-navi-top-bar__logo">
                    <img src="https://img.reuter.de/layout/logo_1000_v5.svg" alt="reuter.de">
                </a>
            </div>
            <div class="columns text-right">
                <div qa-data="header-user-menu__mobile" class="c-navi-top-bar__icons c-navi-top-bar__icons--expand">

                    <a qa-data="header-user-menu--button__mobile" class="c-navi-top-bar__icon c-navi-top-bar__icon--square c-dropdown-toggle" data-toggle="accountDropdownMobile" tabindex="0">
                        <span class="c-dropdown-toggle__arrow c-dropdown-toggle__arrow--bottom"></span>
                        <i class="o-linear-icon">user</i>
                    </a>
                    <div class="dropdown-pane js-header-dropdown expanded-for-small-only" id="accountDropdownMobile" data-dropdown data-options="position:bottom; alignment:center; vOffset:15; trapFocus:true; closeOnClick:true; expandedFor:small only; ">



                        <div class="o-pseudo-h3">
                            Sign in
                        </div>
                        <div class="row">
                            <div class="columns">
                                <a qa-data="header-user-menu--login-btn" href="/login/" class="button c-panel-button">Sign in
                                </a>
                            </div>
                        </div>
                        <p>
                            Are you a new customer? <a href="/login/" class="o-text-link">Register now</a>
                        </p>
                        <div class="c-grid-minimalist c-grid-minimalist--inner">
                            <div class="row">
                                <div class="columns">
                                    <a href="/account/overview/" class="tertiary button c-panel-button">Your account
                                    </a>
                                </div>
                                <div class="columns">
                                    <a href="/account/orders/" class="tertiary button c-panel-button">My Orders
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="columns">
                                    <a href="/account/rma/" class="tertiary button c-panel-button">Return item
                                    </a>
                                </div>
                                <div class="columns">
                                    <a href="/shopping/cart/" class="tertiary button c-panel-button">Activate saved shopping cart
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <a href="/notepad/" class="c-navi-top-bar__icon c-navi-top-bar__icon--square is-empty js-notepad-header-icon" rel="nofollow">
                        <span class="c-bag-compartment-icon">
                            <span class="c-bag-compartment-icon__badge has-1-digit js-notepad-count js-notepad-count-badge">0
                            </span>
                            <i class="c-bag-compartment-icon__icon o-linear-icon">heart</i>
                        </span>
                    </a>


                    <a class="c-navi-top-bar__icon c-navi-top-bar__icon--square c-dropdown-toggle js-mini-cart-toggle is-empty" data-toggle="cartDropdownMobile" tabindex="0">
                        <span class="c-dropdown-toggle__arrow c-dropdown-toggle__arrow--bottom has-cart"></span>
                        <span class="c-bag-compartment-icon">
                            <span class="c-bag-compartment-icon__badge c-bag-compartment-icon__badge--grow-rtl js-cart-sum-qty has-1-digit">0
                            </span>
                            <i class="c-bag-compartment-icon__icon o-linear-icon">cartEmpty</i>
                        </span>
                    </a>
                    <div class="dropdown-pane js-header-dropdown expanded-for-small-only padding-0 js-mini-cart is-empty" id="cartDropdownMobile" data-dropdown data-options="position:bottom; alignment:center; vOffset:15; trapFocus:true; closeOnClick:true; expandedFor:small only; ">
                    </div>


                    <div class="auto reveal o-reveal o-reveal--small" id="js-mini-cart-reveal" data-reveal data-options="vOffset:0; mobileScrollTop:false;">
                        <p class="text-center">
                            <i class="o-linear-icon is-light o-success-text">check</i>
                            Product added to shopping cart
                                                <span class="c-bag-compartment-icon c-bag-compartment-icon--large">
                                                    <span class="c-bag-compartment-icon__badge js-cart-sum-qty has-1-digit">0
                                                    </span>
                                                    <i class="c-bag-compartment-icon__icon o-linear-icon">cartEmpty</i>
                                                </span>
                        </p>
                        <div class="c-vertical-button-group">
                            <a href="/checkout/shipping/" class="narrow expanded button">Process Order
                            </a>
                            <a href="/shopping/cart/" class="narrow expanded dark hollow button">Go to cart
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="c-navi-top-bar__search-full">

            <div itemscope itemtype="//schema.org/WebSite">
                <form class="search-form" method="get" action="/catalogsearch/" itemprop="potentialAction" itemscope itemtype="//schema.org/SearchAction">
                    <div class="input-group o-search-input-group">


                        <input qa-data="page-search--input" itemprop="query-input" name="q" id="acSmall" type="text" placeholder="search term or item number" value="" autocomplete="off" class="input-group-field o-search-input clearable ffsearch">
                        <div class="input-group-button">

                            <button qa-data="page-search--button" class="square button c-arrow-button" type="submit">
                                <span class="c-arrow-button__arrow">
                                    <i class="c-triangle-arrow c-triangle-arrow--left"></i>
                                </span>
                                <i class="o-linear-icon">search</i>
                            </button>
                        </div>
                        <div class="input-group-button hide-for-small-only">
                            <button class="square alert button is-phone" type="button" data-open="mobile-contact-modal">
                                <i class="o-linear-icon">phone</i>
                                <span class="show-for-medium hide-for-large">until 10pm
                                </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </nav>
</div>










<form method="post" class="sidebar-menu pd15" action="{ActionForm}">
    <div class="form-login">
        <div class="title-login">
            <span class="fa fa-users"></span>Đăng nhập
        </div>
        <%if (!WebLogin.IsLogin()){%>
        <div class="side-login">
            <ul class="form-list">
                <li class="clear">
                    <div class="text-left col-sm-12 ">
                        <label>Email</label>
                    </div>
                    <div class="text-left col-sm-12">
                        <input type="text" class="form-control" name="Email" placeholder="Email đăng nhập" />
                    </div>
                </li>
                <li class="clear">
                    <div class="text-left col-sm-12 ">
                        <label>Mật khẩu</label>
                    </div>
                    <div class="text-left col-sm-12">
                        <input type="password" class="form-control" name="Password" placeholder="Mật khẩu đăng nhập" />
                    </div>
                </li>
                <li class="clear">
                    <div class="text-left col-sm-12 ">
                        <label></label>
                    </div>
                    <div class="text-left col-sm-12">
                        <button type="submit" class="btn btn-default" name="_vsw_action[CStatic-LoginPOST-Login]">Đăng nhập</button>
                    </div>
                </li>
            </ul>
        </div>
        <%}else{%>
        <div class="side-login">
            <ul class="sidebar-submenu block">
                <li><a href="<%=ViewPage.UserURL %>" title="Giao Cont hàng cho cảng">Chào bạn, <%=WebLogin.CurrentUser.Email %></a></li>
                <li><a href="<%=ViewPage.LogoutURL %>" title="Giao Cont rỗng cho cảng">Thoát</a></li>
            </ul>
        </div>
        <%} %>
    </div>
</form>