﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="c-footer__social-media hide-for-exhibition">
    <div class="o-pseudo-h3 c-footer__social-media-headline">We are here for you!<i class="fi-check"></i></div>
    <p>You can also find us on the following channels</p>
    <div class="c-item-group c-item-group--with-gutter">
        <div class="c-item-group__item">
            <a class="o-social-media-sprite o-social-media-sprite--youtube" href="https://www.youtube.com/user/reuter" target="_blank"></a>
        </div>
        <div class="c-item-group__item">
            <a class="o-social-media-sprite o-social-media-sprite--pinterest" href="https://www.pinterest.co.uk/reuteronlineshop/" target="_blank"></a>
        </div>
        <div class="c-item-group__item">
            <a class="o-social-media-sprite o-social-media-sprite--xing" href="https://www.xing.com/companies/reutergruppe" target="_blank"></a>
        </div>
        <div class="c-item-group__item">
            <a class="o-social-media-sprite o-social-media-sprite--kununu" href="https://www.kununu.com/de/reuter-onlineshop1" target="_blank"></a>
        </div>
    </div>
</div>