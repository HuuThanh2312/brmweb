﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var _webUser = WebLogin.CurrentUser;
    var model = ViewBag.Model as CStaticModel ?? new CStaticModel();
    int _uid = VSW.Core.Web.HttpQueryString.GetValue("uid").ToInt();
    string _uid2 = VSW.Core.Web.HttpQueryString.GetValue("returnpath").ToString();

%>
<div class="header_top">
    <div class="container">
        <ul class="nav-left-top">
            <li>
                <a href="<%=ViewPage.BuninesskUrl %>">kênh người bán</a>
            </li>
            <li>
                <a href="javascrpit:void(0)">Kết nối</a>
                &nbsp;<span><a href="https://www.facebook.com/"><i class="fa fa-facebook-square"></i></a></span>
                &nbsp;<span><a href="https://www.facebook.com/"><i class="fa fa-instagram"></a></i></span>
            </li>
        </ul>
        <ul class="nav-left-top nav-right-top">
            <li>
                <a href=""><i class="fa fa-question-circle"></i>Trợ giúp</a>
            </li>
            <%if (_webUser != null)
                { %>
            <li>
                <a href=""><i class="fa fa-bell"></i>Thông báo</a>
            </li>
            <li>
                <a href="<%=ViewPage.WebUserCPUrl %>"><%=_webUser.LoginName %></a>
            </li>
            <li>
                <a href="<%=ViewPage.LogoutUrl %>">Đăng Xuất</a>
            </li>
            <%}
                else
                {%>

            <li>
                <a class="onclick" data-toggle="modal" data-target="#Modal_Resgister">Đăng ký</a>
                <div id="Modal_Resgister" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center"><b>ĐĂNG KÝ TÀI KHOẢN</b></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-singup row" id="register_form" method="post" name="register_form" accept-charset="utf-8">
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Tên tài khoản <sup id="messenger">(*)</sup></label>
                                        <input name="LoginName" value="<%=model.LoginName %>" type="text" onchange="check_acc(this.value,'LoginName'); return false;" placeholder="Nhập tên dăng nhập" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Email <sup id="email">(*)</sup></label>
                                        <input name="Email" value="<%=model.Email %>" type="text" placeholder="Nhập địa chỉ Email" onchange="check_acc(this.value,'email'); return false;" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Số điện thoại <sup>(*)</sup></label>
                                        <input name="Phone" value="<%=model.Phone %>" type="text" placeholder="Nhập số điện thoại" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Họ và tên <sup>(*)</sup></label>
                                        <input name="Name" value="<%=model.Name %>" type="text" placeholder="Nhập họ và tên" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Ngày sinh <sup>(*)</sup></label>
                                        <input name="BirthDay" onfocus="(this.type='date')" id="StartDate" value="<%=model.BirthDay> DateTime.MinValue? string.Format("{0:yyyy-MM-dd}", model.BirthDay)  :""%>" type="text" placeholder="" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Địa chỉ <sup>(*)</sup></label>
                                        <input name="Address" value="<%=model.Address %>" type="text" placeholder="" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Mật khẩu <sup>(*)</sup></label>
                                        <input name="Password" value="<%=model.Password %>" type="password" placeholder="" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Nhập lại mật khẩu <sup>(*)</sup></label>
                                        <input name="Password2" value="<%=model.Password2 %>" type="password" placeholder="" class="form-control input-md" required="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Nhập mã bảo mật <sup>(*)</sup></label>
                                        <input type="text" class="form-control w30p security fl" name="ValidCode" id="ValidCode" required="required" autocomplete="off" value="" size="40" placeholder="Mã bảo mật" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label" for="textinput">Mã bảo mật <sup>(*)</sup></label>
                                        <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                        <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                            <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                        </a>
                                    </div>
                                    <div class="form-group text-center mt20 col-md-12">
                                        <button type="submit" name="_vsw_action[CStatic-RegisterPOST-HeaderLogin]" class="btn button-resuser">Đăng ký tài khoản</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <li>
                <a href="" class="login" data-toggle="modal" data-target="#Modal_login">Đăng Nhập</a>
                <div id="Modal_login" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center"><b>ĐĂNG NHẬP</b></h4>
                            </div>
                            <div class="modal-body">
                                <div class="main">
                                    <div class="sticker-box full">
                                        <div>
                                            <img class="sticker" alt="https://i.imgur.com/YHs5f6H.png" src="https://i.imgur.com/YHs5f6H.png"><img class="sticker" alt="https://i.imgur.com/zFNuZTA.png" src="https://i.imgur.com/zFNuZTA.png"><img class="sticker" alt="https://i.imgur.com/ME1tJB0.png" src="https://i.imgur.com/ME1tJB0.png"><img class="sticker" alt="https://i.imgur.com/u3720e4.png" src="https://i.imgur.com/u3720e4.png"><img class="sticker" alt="https://i.imgur.com/KAUdgHo.png" src="https://i.imgur.com/KAUdgHo.png"><img class="sticker" alt="https://i.imgur.com/xjIzSG9.png" src="https://i.imgur.com/xjIzSG9.png"><img class="sticker" alt="https://i.imgur.com/320PQq9.png" src="https://i.imgur.com/320PQq9.png"><img class="sticker" alt="https://i.imgur.com/yBBewst.png" src="https://i.imgur.com/yBBewst.png"><img class="sticker" alt="https://i.imgur.com/FfsqRRV.png" src="https://i.imgur.com/FfsqRRV.png"><img class="sticker" alt="https://i.imgur.com/FY7e6U1.png" src="https://i.imgur.com/FY7e6U1.png"><img class="sticker" alt="https://i.imgur.com/BdgiW7R.png" src="https://i.imgur.com/BdgiW7R.png"><img class="sticker" alt="https://i.imgur.com/osCpCsi.png" src="https://i.imgur.com/osCpCsi.png"><img class="sticker" alt="https://i.imgur.com/uzQb2yt.png" src="https://i.imgur.com/uzQb2yt.png"><img class="sticker" alt="https://i.imgur.com/z8SmL8K.png" src="https://i.imgur.com/z8SmL8K.png"><img class="sticker" alt="https://i.imgur.com/1BW9Wj4.png" src="https://i.imgur.com/1BW9Wj4.png"><img class="sticker" alt="https://i.imgur.com/g8XXj8u.gif" src="https://i.imgur.com/g8XXj8u.gif"><img class="sticker" alt="https://i.imgur.com/K4Hcd5N.png" src="https://i.imgur.com/K4Hcd5N.png"><img class="sticker" alt="https://i.imgur.com/gvTwnV8.gif" src="https://i.imgur.com/gvTwnV8.gif"><img class="sticker" alt="https://i.imgur.com/V092S5K.gif" src="https://i.imgur.com/V092S5K.gif"><img class="sticker" alt="https://i.imgur.com/4gmOAMB.png" src="https://i.imgur.com/4gmOAMB.png"><img class="sticker" alt="https://i.imgur.com/kH9BFd2.gif" src="https://i.imgur.com/kH9BFd2.gif"><img class="sticker" alt="https://i.imgur.com/EB2RUU6.gif" src="https://i.imgur.com/EB2RUU6.gif"><img class="sticker" alt="https://i.imgur.com/ghXpJrI.png" src="https://i.imgur.com/ghXpJrI.png"><img class="sticker" alt="https://i.imgur.com/wlyO8eh.png" src="https://i.imgur.com/wlyO8eh.png"><img class="sticker" alt="https://i.imgur.com/uq1dgnk.png" src="https://i.imgur.com/uq1dgnk.png"><img class="sticker" alt="https://i.imgur.com/FqPSFPf.gif" src="https://i.imgur.com/FqPSFPf.gif"><img class="sticker" alt="https://i.imgur.com/Xv0BtTR.png" src="https://i.imgur.com/Xv0BtTR.png"><img class="sticker" alt="https://i.imgur.com/7lir9HU.png" src="https://i.imgur.com/7lir9HU.png"><img class="sticker" alt="https://i.imgur.com/8kNEyvT.png" src="https://i.imgur.com/8kNEyvT.png"><img class="sticker" alt="https://i.imgur.com/kElKEVl.gif" src="https://i.imgur.com/kElKEVl.gif"><img class="sticker" alt="https://i.imgur.com/RKp7zWn.png" src="https://i.imgur.com/RKp7zWn.png"><img class="sticker" alt="https://i.imgur.com/9jcUxnb.png" src="https://i.imgur.com/9jcUxnb.png"><img class="sticker" alt="https://i.imgur.com/JEWoIdl.png" src="https://i.imgur.com/JEWoIdl.png"><img class="sticker" alt="https://i.imgur.com/u40wsAh.png" src="https://i.imgur.com/u40wsAh.png"><img class="sticker" alt="https://i.imgur.com/KV0XGIA.gif" src="https://i.imgur.com/KV0XGIA.gif"><img class="sticker" alt="https://i.imgur.com/6l22n1x.png" src="https://i.imgur.com/6l22n1x.png"><img class="sticker" alt="https://i.imgur.com/KE5ti7l.png" src="https://i.imgur.com/KE5ti7l.png"><img class="sticker" alt="https://i.imgur.com/8LsIshX.png" src="https://i.imgur.com/8LsIshX.png"><img class="sticker" alt="https://i.imgur.com/EcV5PPL.png" src="https://i.imgur.com/EcV5PPL.png"><img class="sticker" alt="https://i.imgur.com/JCFtpJo.png" src="https://i.imgur.com/JCFtpJo.png"><img class="sticker" alt="https://i.imgur.com/KgmQHtR.png" src="https://i.imgur.com/KgmQHtR.png"><img class="sticker" alt="https://i.imgur.com/HR4W6DU.png" src="https://i.imgur.com/HR4W6DU.png"><img class="sticker" alt="https://i.imgur.com/UKiCiKh.png" src="https://i.imgur.com/UKiCiKh.png"><img class="sticker" alt="https://i.imgur.com/Q8sGcLO.png" src="https://i.imgur.com/Q8sGcLO.png"><img class="sticker" alt="https://i.imgur.com/yAW5d3s.gif" src="https://i.imgur.com/yAW5d3s.gif"><img class="sticker" alt="https://i.imgur.com/c6GVp0o.png" src="https://i.imgur.com/c6GVp0o.png"><img class="sticker" alt="https://i.imgur.com/1xEuo02.gif" src="https://i.imgur.com/1xEuo02.gif"><img class="sticker" alt="https://i.imgur.com/4RJD3gO.png" src="https://i.imgur.com/4RJD3gO.png"><img class="sticker" alt="https://i.imgur.com/GAI3Fr0.png" src="https://i.imgur.com/GAI3Fr0.png"><img class="sticker" alt="https://i.imgur.com/XEDbgyn.png" src="https://i.imgur.com/XEDbgyn.png"><img class="sticker" alt="https://i.imgur.com/6gCweAP.png" src="https://i.imgur.com/6gCweAP.png"><img class="sticker" alt="https://i.imgur.com/MjfezZB.png" src="https://i.imgur.com/MjfezZB.png"><img class="sticker" alt="https://i.imgur.com/run4nyW.png" src="https://i.imgur.com/run4nyW.png"><img class="sticker" alt="https://i.imgur.com/URoiprO.png" src="https://i.imgur.com/URoiprO.png"><img class="sticker" alt="https://i.imgur.com/Wf29Rhg.png" src="https://i.imgur.com/Wf29Rhg.png"><img class="sticker" alt="https://i.imgur.com/M1eVvfg.png" src="https://i.imgur.com/M1eVvfg.png"><img class="sticker" alt="https://i.imgur.com/wryvDSH.png" src="https://i.imgur.com/wryvDSH.png"><img class="sticker" alt="https://i.imgur.com/meoqQpA.png" src="https://i.imgur.com/meoqQpA.png"><img class="sticker" alt="https://i.imgur.com/Ad8fHwT.png" src="https://i.imgur.com/Ad8fHwT.png"><img class="sticker" alt="https://i.imgur.com/brK1fAe.png" src="https://i.imgur.com/brK1fAe.png"><img class="sticker" alt="https://i.imgur.com/7JO4RkJ.png" src="https://i.imgur.com/7JO4RkJ.png"><img class="sticker" alt="https://i.imgur.com/MeEmGOg.png" src="https://i.imgur.com/MeEmGOg.png"><img class="sticker" alt="https://i.imgur.com/at9JAlm.png" src="https://i.imgur.com/at9JAlm.png"><img class="sticker" alt="https://i.imgur.com/vKigGok.png" src="https://i.imgur.com/vKigGok.png"><img class="sticker" alt="https://i.imgur.com/Ba88Vk2.gif" src="https://i.imgur.com/Ba88Vk2.gif"><img class="sticker" alt="https://i.imgur.com/Z4LZsL9.png" src="https://i.imgur.com/Z4LZsL9.png"><img class="sticker" alt="https://i.imgur.com/t5IcdBZ.png" src="https://i.imgur.com/t5IcdBZ.png"><img class="sticker" alt="https://i.imgur.com/NMwYNcc.png" src="https://i.imgur.com/NMwYNcc.png"><img class="sticker" alt="https://i.imgur.com/RDHJP1e.png" src="https://i.imgur.com/RDHJP1e.png"><img class="sticker" alt="https://i.imgur.com/Tmv0WxW.png" src="https://i.imgur.com/Tmv0WxW.png"><img class="sticker" alt="https://i.imgur.com/Vm92EyD.png" src="https://i.imgur.com/Vm92EyD.png"><img class="sticker" alt="https://i.imgur.com/gFWxNt8.png" src="https://i.imgur.com/gFWxNt8.png"><img class="sticker" alt="https://i.imgur.com/jbJjmTi.png" src="https://i.imgur.com/jbJjmTi.png"><img class="sticker" alt="https://i.imgur.com/bcWKlJ8.png" src="https://i.imgur.com/bcWKlJ8.png"><img class="sticker" alt="https://i.imgur.com/v1fmMDd.gif" src="https://i.imgur.com/v1fmMDd.gif"><img class="sticker" alt="https://i.imgur.com/Rz6A7nL.png" src="https://i.imgur.com/Rz6A7nL.png"><img class="sticker" alt="https://i.imgur.com/7JplBGY.png" src="https://i.imgur.com/7JplBGY.png"><img class="sticker" alt="https://i.imgur.com/3p4UF0J.gif" src="https://i.imgur.com/3p4UF0J.gif"><img class="sticker" alt="https://i.imgur.com/bpUNvGy.png" src="https://i.imgur.com/bpUNvGy.png"><img class="sticker" alt="https://i.imgur.com/0Xs5kXN.png" src="https://i.imgur.com/0Xs5kXN.png"><img class="sticker" alt="https://i.imgur.com/egCHE2H.gif" src="https://i.imgur.com/egCHE2H.gif"><img class="sticker" alt="https://i.imgur.com/Nk1Lh3K.png" src="https://i.imgur.com/Nk1Lh3K.png"><img class="sticker" alt="https://i.imgur.com/wEqlboB.png" src="https://i.imgur.com/wEqlboB.png"><img class="sticker" alt="https://i.imgur.com/yAua8od.png" src="https://i.imgur.com/yAua8od.png"><img class="sticker" alt="https://i.imgur.com/VnVpDPf.png" src="https://i.imgur.com/VnVpDPf.png"><img class="sticker" alt="https://i.imgur.com/LTT2cUR.png" src="https://i.imgur.com/LTT2cUR.png"><img class="sticker" alt="https://i.imgur.com/KTbUhzb.png" src="https://i.imgur.com/KTbUhzb.png"><img class="sticker" alt="https://i.imgur.com/oyTfKgA.png" src="https://i.imgur.com/oyTfKgA.png"><img class="sticker" alt="https://i.imgur.com/gq7t32C.png" src="https://i.imgur.com/gq7t32C.png"><img class="sticker" alt="https://i.imgur.com/YhCyC2n.png" src="https://i.imgur.com/YhCyC2n.png"><img class="sticker" alt="https://i.imgur.com/5W2OAmY.png" src="https://i.imgur.com/5W2OAmY.png"><img class="sticker" alt="https://i.imgur.com/xCO9chd.png" src="https://i.imgur.com/xCO9chd.png"><img class="sticker" alt="https://i.imgur.com/JkpvuKo.png" src="https://i.imgur.com/JkpvuKo.png"><img class="sticker" alt="https://i.imgur.com/owRCxyU.png" src="https://i.imgur.com/owRCxyU.png"><img class="sticker" alt="https://i.imgur.com/gi9novF.png" src="https://i.imgur.com/gi9novF.png"><img class="sticker" alt="https://i.imgur.com/lUyeYzS.png" src="https://i.imgur.com/lUyeYzS.png"><img class="sticker" alt="https://i.imgur.com/MHoYqPz.png" src="https://i.imgur.com/MHoYqPz.png"><img class="sticker" alt="https://i.imgur.com/zQU2cJa.png" src="https://i.imgur.com/zQU2cJa.png"><img class="sticker" alt="https://i.imgur.com/SpGlmnL.png" src="https://i.imgur.com/SpGlmnL.png"><img class="sticker" alt="https://i.imgur.com/1hRttnD.png" src="https://i.imgur.com/1hRttnD.png"><img class="sticker" alt="https://i.imgur.com/qDotM7o.png" src="https://i.imgur.com/qDotM7o.png"><img class="sticker" alt="https://i.imgur.com/Qou44FN.gif" src="https://i.imgur.com/Qou44FN.gif"><img class="sticker" alt="https://i.imgur.com/vE2oAx6.png" src="https://i.imgur.com/vE2oAx6.png"><img class="sticker" alt="https://i.imgur.com/zCXONM1.gif" src="https://i.imgur.com/zCXONM1.gif"><img class="sticker" alt="https://i.imgur.com/OANgL56.png" src="https://i.imgur.com/OANgL56.png"><img class="sticker" alt="https://i.imgur.com/ruAbZ9q.png" src="https://i.imgur.com/ruAbZ9q.png"><img class="sticker" alt="https://i.imgur.com/dDcJCFN.png" src="https://i.imgur.com/dDcJCFN.png"><img class="sticker" alt="https://i.imgur.com/chudNpp.png" src="https://i.imgur.com/chudNpp.png"><img class="sticker" alt="https://i.imgur.com/ybZaBgZ.gif" src="https://i.imgur.com/ybZaBgZ.gif"><img class="sticker" alt="https://i.imgur.com/JBas8eI.png" src="https://i.imgur.com/JBas8eI.png"><img class="sticker" alt="https://i.imgur.com/ucheJZL.gif" src="https://i.imgur.com/ucheJZL.gif"><img class="sticker" alt="https://i.imgur.com/gXegt8j.png" src="https://i.imgur.com/gXegt8j.png"><img class="sticker" alt="https://i.imgur.com/ggumeUl.png" src="https://i.imgur.com/ggumeUl.png"><img class="sticker" alt="https://i.imgur.com/G3mmzaT.png" src="https://i.imgur.com/G3mmzaT.png"><img class="sticker" alt="https://i.imgur.com/HX6etkf.png" src="https://i.imgur.com/HX6etkf.png"><img class="sticker" alt="https://i.imgur.com/QL6XCEj.png" src="https://i.imgur.com/QL6XCEj.png"><img class="sticker" alt="https://i.imgur.com/PhqqlBV.png" src="https://i.imgur.com/PhqqlBV.png"><img class="sticker" alt="https://i.imgur.com/4h7xTdv.png" src="https://i.imgur.com/4h7xTdv.png"><img class="sticker" alt="https://i.imgur.com/VOAEsYN.png" src="https://i.imgur.com/VOAEsYN.png"><img class="sticker" alt="https://i.imgur.com/0KSdPUp.png" src="https://i.imgur.com/0KSdPUp.png"><img class="sticker" alt="https://i.imgur.com/CY6m3Lt.png" src="https://i.imgur.com/CY6m3Lt.png"><img class="sticker" alt="https://i.imgur.com/OAoEbal.gif" src="https://i.imgur.com/OAoEbal.gif"><img class="sticker" alt="https://i.imgur.com/lhuVlcm.png" src="https://i.imgur.com/lhuVlcm.png"><img class="sticker" alt="https://i.imgur.com/JSxuR2w.gif" src="https://i.imgur.com/JSxuR2w.gif"><img class="sticker" alt="https://i.imgur.com/vuH1gMF.png" src="https://i.imgur.com/vuH1gMF.png"><img class="sticker" alt="https://i.imgur.com/CeBgXls.png" src="https://i.imgur.com/CeBgXls.png"><img class="sticker" alt="https://i.imgur.com/SJ0m7xC.png" src="https://i.imgur.com/SJ0m7xC.png"><img class="sticker" alt="https://i.imgur.com/X7y7esE.png" src="https://i.imgur.com/X7y7esE.png"><img class="sticker" alt="https://i.imgur.com/RPOxGRd.png" src="https://i.imgur.com/RPOxGRd.png"><img class="sticker" alt="https://i.imgur.com/62tUZIe.gif" src="https://i.imgur.com/62tUZIe.gif"><img class="sticker" alt="https://i.imgur.com/G8TvFa4.gif" src="https://i.imgur.com/G8TvFa4.gif"><img class="sticker" alt="https://i.imgur.com/aVVa2xy.png" src="https://i.imgur.com/aVVa2xy.png"><img class="sticker" alt="https://i.imgur.com/qWXsrS0.png" src="https://i.imgur.com/qWXsrS0.png"><img class="sticker" alt="https://i.imgur.com/Hh3ek5F.png" src="https://i.imgur.com/Hh3ek5F.png"><img class="sticker" alt="https://i.imgur.com/r5QCk8d.png" src="https://i.imgur.com/r5QCk8d.png"><img class="sticker" alt="https://i.imgur.com/1Tk08rj.png" src="https://i.imgur.com/1Tk08rj.png"><img class="sticker" alt="https://i.imgur.com/PtlV4ku.png" src="https://i.imgur.com/PtlV4ku.png"><img class="sticker" alt="https://i.imgur.com/McmBTPc.png" src="https://i.imgur.com/McmBTPc.png"><img class="sticker" alt="https://i.imgur.com/811MoLZ.png" src="https://i.imgur.com/811MoLZ.png"><img class="sticker" alt="https://i.imgur.com/QwJ0V0V.png" src="https://i.imgur.com/QwJ0V0V.png"><img class="sticker" alt="https://i.imgur.com/Pa3C9kE.png" src="https://i.imgur.com/Pa3C9kE.png"><img class="sticker" alt="https://i.imgur.com/E6blzIh.gif" src="https://i.imgur.com/E6blzIh.gif"><img class="sticker" alt="https://i.imgur.com/irGoYrZ.gif" src="https://i.imgur.com/irGoYrZ.gif"><img class="sticker" alt="https://i.imgur.com/PUi3vHS.gif" src="https://i.imgur.com/PUi3vHS.gif"><img class="sticker" alt="https://i.imgur.com/Mv75GW3.gif" src="https://i.imgur.com/Mv75GW3.gif">
                                        </div>

                                        <div data-reactroot="">
                                            <div class="emo-header">
                                                <!-- react-text: 3 -->
                                                Smilies<!-- /react-text --><div class="emo-expand"><i class="fa fa-expand fa-lg" aria-hidden="true"></i></div>
                                            </div>
                                            <div class="emotion-box">
                                               
                                                     <img class="sticker"  alt=":sexy:" src="https://forums.voz.vn/images/smilies/Off/sexy_girl.gif">
                                               
                                                     <img class="sticker"  alt=":byebye:" src="https://forums.voz.vn/images/smilies/Off/byebye.gif">
                                               
                                                     <img class="sticker"  alt=":look_down:" src="https://forums.voz.vn/images/smilies/Off/look_down.gif">
                                               
                                                     <img class="sticker"  alt=":stick:" src="https://forums.voz.vn/images/smilies/Off/burn_joss_stick.gif">
                                               
                                                     <img class="sticker"  alt=":adore:" src="https://forums.voz.vn/images/smilies/Off/adore.gif">
                                               
                                                     <img class="sticker"  alt=":nosebleed:" src="https://forums.voz.vn/images/smilies/Off/nosebleed.gif">
                                               
                                                     <img class="sticker"  alt=":beauty:" src="https://forums.voz.vn/images/smilies/Off/beauty.gif">
                                               
                                                     <img class="sticker"  alt=":gach:" src="https://forums.voz.vn/images/smilies/brick.png">
                                               
                                                     <img class="sticker"  alt=":&quot;>" src="https://forums.voz.vn/images/smilies/Off/embarrassed.gif">
                                               
                                                     <img class="sticker"  alt=":surrender:" src="https://forums.voz.vn/images/smilies/Off/surrender.gif">
                                               
                                                     <img class="sticker"  alt=":pudency:" src="https://forums.voz.vn/images/smilies/Off/pudency.gif">
                                               
                                                     <img class="sticker"  alt=":sosad:" src="https://forums.voz.vn/images/smilies/Off/too_sad.gif">
                                               
                                                     <img class="sticker"  alt=":chaymau:" src="https://forums.voz.vn/images/smilies/Off/nosebleed.gif">
                                               
                                                     <img class="sticker"  alt=":go:" src="https://forums.voz.vn/images/smilies/Off/go.gif">
                                               
                                                     <img class="sticker"  alt=":sweat:" src="https://forums.voz.vn/images/smilies/Off/sweat.gif">
                                               
                                                     <img class="sticker"  alt=":canny:" src="https://forums.voz.vn/images/smilies/Off/canny.gif">
                                               
                                                     <img class="sticker"  alt=":sogood:" src="https://forums.voz.vn/images/smilies/Off/feel_good.gif">
                                               
                                                     <img class="sticker"  alt=":shame:" src="https://forums.voz.vn/images/smilies/Off/shame.gif">
                                               
                                                     <img class="sticker"  alt=":hungry:" src="https://forums.voz.vn/images/smilies/Off/hungry.gif">
                                               
                                                     <img class="sticker"  alt=":shot:" src="https://forums.voz.vn/images/smilies/Off/beat_shot.gif">
                                               
                                                     <img class="sticker"  alt=":rap:" src="https://forums.voz.vn/images/smilies/Off/rap.gif">
                                               
                                                     <img class="sticker"  alt=":hang:" src="https://forums.voz.vn/images/smilies/Off/hang.gif">
                                               
                                                     <img class="sticker"  alt=":*" src="https://forums.voz.vn/images/smilies/Off/sweet_kiss.gif">
                                               
                                                     <img class="sticker"  alt=":ops:" src="https://forums.voz.vn/images/smilies/Off/ops.gif">
                                               
                                                     <img class="sticker"  alt=":)" src="https://forums.voz.vn/images/smilies/Off/smile.gif">
                                               
                                                     <img class="sticker"  alt=":plaster:" src="https://forums.voz.vn/images/smilies/Off/beat_plaster.gif">
                                               
                                                     <img class="sticker"  alt=":tire:" src="https://forums.voz.vn/images/smilies/Off/tire.gif">
                                               
                                                     <img class="sticker"  alt=":brick:" src="https://forums.voz.vn/images/smilies/Off/beat_brick.gif">
                                               
                                                     <img class="sticker"  alt=":badsmell:" src="https://forums.voz.vn/images/smilies/Off/bad_smelly.gif">
                                               
                                                     <img class="sticker"  alt=":hell_boy:" src="https://forums.voz.vn/images/smilies/Off/hell_boy.gif">
                                               
                                                     <img class="sticker"  alt=":kool:" src="https://forums.voz.vn/images/smilies/Off/cool.gif">
                                               
                                                     <img class="sticker"  alt=":dribble:" src="https://forums.voz.vn/images/smilies/Off/dribble.gif">
                                               
                                                     <img class="sticker"  alt=":waaaht:" src="https://forums.voz.vn/images/smilies/Off/waaaht.gif">
                                               
                                                     <img class="sticker"  alt=":oh:" src="https://forums.voz.vn/images/smilies/Off/oh.gif">
                                               
                                                     <img class="sticker"  alt=":((" src="https://forums.voz.vn/images/smilies/Off/cry.gif">
                                               
                                                     <img class="sticker"  alt="^:)^" src="https://forums.voz.vn/images/smilies/Off/lay.gif">
                                               
                                                     <img class="sticker"  alt=":aboom:" src="https://forums.voz.vn/images/smilies/Off/after_boom.gif">
                                               
                                                     <img class="sticker"  alt=":sad:" src="https://forums.voz.vn/images/smilies/Off/sad.gif">
                                               
                                                     <img class="sticker"  alt=":hug:" src="https://forums.voz.vn/images/smilies/Off/hug.gif">
                                               
                                                     <img class="sticker"  alt=":fix:" src="https://forums.voz.vn/images/smilies/Off/fix.gif">
                                               
                                                     <img class="sticker"  alt=":amazed:" src="https://forums.voz.vn/images/smilies/Off/amazed.gif">
                                               
                                                     <img class="sticker"  alt=":shitty:" src="https://forums.voz.vn/images/smilies/Off/shit.gif">
                                               
                                                     <img class="sticker"  alt=":what:" src="https://forums.voz.vn/images/smilies/Off/what.gif">
                                               
                                                     <img class="sticker"  alt=":cheers:" src="https://forums.voz.vn/images/smilies/Off/cheers.gif">
                                               
                                                     <img class="sticker"  alt="-_-" src="https://forums.voz.vn/images/smilies/Off/sleep.gif">
                                               
                                                     <img class="sticker"  alt=":spam:" src="https://forums.voz.vn/images/smilies/Off/spam.gif">
                                               
                                                     <img class="sticker"  alt=":ah:" src="https://forums.voz.vn/images/smilies/Off/ah.gif">
                                               
                                                     <img class="sticker"  alt=":rofl:" src="https://forums.voz.vn/images/smilies/Off/rofl.gif">
                                               
                                                     <img class="sticker"  alt=":baffle:" src="https://forums.voz.vn/images/smilies/Off/baffle.gif">
                                               
                                                     <img class="sticker"  alt=":choler:" src="https://forums.voz.vn/images/smilies/Off/choler.gif">
                                               
                                                     <img class="sticker"  alt=":doubt:" src="https://forums.voz.vn/images/smilies/Off/doubt.gif">
                                               
                                                     <img class="sticker"  alt=":capture:" src="https://forums.voz.vn/images/smilies/Off/capture.gif">
                                               
                                                     <img class="sticker"  alt=":confident:" src="https://forums.voz.vn/images/smilies/Off/confident.gif">
                                               
                                                     <img class="sticker"  alt=":theft:" src="https://forums.voz.vn/images/smilies/Off/theft.gif">
                                               
                                                     <img class="sticker"  alt=":matrix:" src="https://forums.voz.vn/images/smilies/Off/matrix.gif">
                                               
                                                     <img class="sticker"  alt=":haha:" src="https://forums.voz.vn/images/smilies/Off/haha.gif">
                                               
                                                     <img class="sticker"  alt=":hehe:" src="https://forums.voz.vn/images/smilies/Off/hehe.gif">
                                               
                                                     <img class="sticker"  alt=":smoke:" src="https://forums.voz.vn/images/smilies/Off/smoke.gif">
                                               
                                                     <img class="sticker"  alt=":D" src="https://forums.voz.vn/images/smilies/Off/big_smile.gif">
                                               
                                                     <img class="sticker"  alt=":angry:" src="https://forums.voz.vn/images/smilies/Off/angry.gif">
                                               
                                                     <img class="sticker"  alt=":sos:" src="https://forums.voz.vn/images/smilies/Off/sos.gif">
                                               
                                                     <img class="sticker"  alt=":spiderman:" src="https://forums.voz.vn/images/smilies/Off/spiderman.gif">
                                               
                                                     <img class="sticker"  alt=":boss:" src="https://forums.voz.vn/images/smilies/Off/boss.gif">
                                               
                                                     <img class="sticker"  alt=":dreaming:" src="https://forums.voz.vn/images/smilies/Off/still_dreaming.gif">
                                               
                                                     <img class="sticker"  alt=":-s" src="https://forums.voz.vn/images/smilies/Off/confuse.gif">
                                               
                                                     <img class="sticker"  alt=":bike:" src="https://forums.voz.vn/images/smilies/Off/bike.gif">
                                               
                                                     <img class="sticker"  alt=":misdoubt:" src="https://forums.voz.vn/images/smilies/Off/misdoubt.gif">
                                               
                                                     <img class="sticker"  alt=":mage:" src="https://forums.voz.vn/images/smilies/Off/mage.gif">
                                               
                                                     <img class="sticker"  alt=":bye:" src="https://forums.voz.vn/images/smilies/Off/bye.gif">
                                               
                                                     <img class="sticker"  alt=":phone:" src="https://forums.voz.vn/images/smilies/Off/phone.gif">
                                               
                                                     <img class="sticker"  alt=":lmao:" src="https://forums.voz.vn/images/smilies/Off/lmao.gif">
                                               
                                                     <img class="sticker"  alt=":ot:" src="https://forums.voz.vn/images/smilies/Off/ot.gif">
                                               
                                                     <img class="sticker"  alt=":flame:" src="https://forums.voz.vn/images/smilies/Off/flame.gif">
                                               
                                                     <img class="sticker"  alt=":bang:" src="https://forums.voz.vn/images/smilies/Off/bang.gif">
                                               
                                                     <img class="sticker"  alt=":sure:" src="https://forums.voz.vn/images/smilies/Off/sure.gif">
                                               
                                                     <img class="sticker"  alt=":stupid:" src="https://forums.voz.vn/images/smilies/emos/stupid.gif">
                                               
                                                     <img class="sticker"  alt=":ban:" src="https://forums.voz.vn/images/smilies/Off/bann.gif">
                                               
                                                     <img class="sticker"  alt=":doublegun:" src="https://forums.voz.vn/images/smilies/emos/doublegun.gif">
                                               
                                                     <img class="sticker"  alt=":boom:" src="https://forums.voz.vn/images/smilies/emos/boom.gif">
                                               
                                                     <img class="sticker"  alt=":lol:" src="https://forums.voz.vn/images/smilies/emos/lol.gif">
                                               
                                                     <img class="sticker"  alt=":welcome:" src="https://forums.voz.vn/images/smilies/Off/welcome.gif">
                                               
                                                     <img class="sticker"  alt=":please:" src="https://forums.voz.vn/images/smilies/Off/please.gif">
                                               
                                                     <img class="sticker"  alt=":puke:" src="https://forums.voz.vn/images/smilies/emos/puke.gif">
                                               
                                                     <img class="sticker"  alt=":shit:" src="https://forums.voz.vn/images/smilies/emos/shit.gif">
                                               
                                                     <img class="sticker"  alt=":lovemachine:" src="https://forums.voz.vn/images/smilies/emos/lovemachine.gif">
                                               
                                                     <img class="sticker"  alt=":runrun:" src="https://forums.voz.vn/images/smilies/Off/runrun.gif">
                                               
                                                     <img class="sticker"  alt=":loveyou:" src="https://forums.voz.vn/images/smilies/emos/loveyou.gif">
                                               
                                                     <img class="sticker"  alt=":Birthday:" src="https://forums.voz.vn/images/smilies/emos/Birthday.gif">
                                               
                                                     <img class="sticker"  alt=":no:" src="https://forums.voz.vn/images/smilies/emos/no.gif">
                                               
                                                     <img class="sticker"  alt=":yes:" src="https://forums.voz.vn/images/smilies/emos/yes.gif">
                                               
                                                     <img class="sticker"  alt=":shoot1:" src="https://forums.voz.vn/images/smilies/emos/shoot1.gif">
                                               
                                                     <img class="sticker"  alt=":winner:" src="https://forums.voz.vn/images/smilies/emos/winner.gif">
                                               
                                                     <img class="sticker"  alt=":band:" src="https://forums.voz.vn/images/smilies/emos/band.gif">
                                               
                                                     <img class="sticker"  alt=":grin:" src="https://forums.voz.vn/images/smilies/biggrin.gif">
                                               
                                                     <img class="sticker"  alt=":frown:" src="https://forums.voz.vn/images/smilies/frown.gif">
                                               
                                                     <img class="sticker"  alt=":mad:" src="https://forums.voz.vn/images/smilies/mad.gif">
                                               
                                                     <img class="sticker"  alt=":p" src="https://forums.voz.vn/images/smilies/tongue.gif">
                                               
                                                     <img class="sticker"  alt=":embrass:" src="https://forums.voz.vn/images/smilies/redface.gif">
                                               
                                                     <img class="sticker"  alt=":confused:" src="https://forums.voz.vn/images/smilies/confused.gif">
                                               
                                                     <img class="sticker"  alt=";)" src="https://forums.voz.vn/images/smilies/wink.gif">
                                               
                                                     <img class="sticker"  alt=":rolleyes:" src="https://forums.voz.vn/images/smilies/rolleyes.gif">
                                               
                                                     <img class="sticker"  alt=":cool:" src="https://forums.voz.vn/images/smilies/cool.gif">
                                               
                                                     <img class="sticker"  alt=":eek:" src="https://forums.voz.vn/images/smilies/eek.gif">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <%} %>
        </ul>
    </div>
</div>
<%if (_uid > 0)
    { %>
<script type="text/javascript">
    $('.onclick').click();
</script>
<%} %>


<%if (!string.IsNullOrEmpty(_uid2))
    { %>
<script type="text/javascript">
    $('.login').click();
</script>
<%} %>

