﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    List<SysPageEntity> listItem = null;
    if (!ViewPage.CurrentPage.IsBike)
        listItem = SysPageService.Instance.GetByParent_Cache(9);
    else
        listItem = SysPageService.Instance.GetByParent_Cache(10);

    var model = ViewPage.ViewBag.BSCarModel as MBSCarModel ?? new MBSCarModel();

    var city = ViewPage.ViewBag.City as WebMenuEntity;
    var listCity = WebMenuService.Instance.GetCity();
%>

<div class="box-search-side all mb20">
    <h2 class="title-search-side">
        <i class="fa fa-search"></i>Lọc tìm kiếm nhanh
    </h2>
    <form method="post" class="form-search-side all" name="searchBSCar_form" onsubmit="searchBSCar(); return false;">
        <div class="form-group all">
            <select class="form-control form-search-now" id="menu" onchange="getChildPage(this.value, '<%=ViewPage.CurrentPage.ID %>'); $('#menuCode').val($(this).find(':selected').data('code')); $('#menuCode2').val(''); return false;">
                <option value="0">Hãng sản xuất</option>
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                <option value="<%=listItem[i].ID %>" data-code="<%=listItem[i].Code %>" <%if (ViewPage.IsPageActived(listItem[i])){%>selected="selected"<%} %>><%=listItem[i].Name %></option>
                <%} %>
            </select>
            <input type="hidden" id="menuCode" value="<%=ViewPage.CurrentPage.Parent.Code %>" />
        </div>
        <div class="form-group all">
            <label>Tên xe</label>
            <div class="ajax-box">
                <select class="form-control form-search-now" id="menu2" onchange="$('#menuCode2').val($(this).find(':selected').data('code')); return false;">
                    <option value="0">Tên xe</option>
                </select>
                <input type="hidden" id="menuCode2" value="<%=ViewPage.CurrentPage.Code %>" />
                <div class="loading"><i class="fa fa-spinner fa-2x"></i></div>
            </div>
        </div>

        <%if (!ViewPage.CurrentPage.IsBike){%>
        <div class="form-group all">
            <label>Kiểu xe</label>
            <select id="design" class="form-control form-search-now">
                <option value="0">Kiểu xe</option>
                <%=Utils.ShowDdlMenuByType2("Design", ViewPage.CurrentLang.ID, model.design) %>
            </select>
        </div>
        <div class="form-group all">
            <label>Kiểu dẫn động</label>
            <select id="driving" class="form-control form-search-now">
                <option value="0">Kiểu dẫn động</option>
                <%=Utils.ShowDdlMenuByType2("Driving", ViewPage.CurrentLang.ID, model.driving) %>
            </select>
        </div>
        <div class="form-group all">
            <select id="fuel" class="form-control form-search-now fl w49">
                <option value="0">Nhiên liệu</option>
                <%=Utils.ShowDdlMenuByType2("Fuel", ViewPage.CurrentLang.ID, model.fuel) %>
            </select>
            <select id="gear" class="form-control form-search-now fr w49">
                <option value="0">Hộp số</option>
                <%=Utils.ShowDdlMenuByType2("Gear", ViewPage.CurrentLang.ID, model.gear) %>
            </select>
        </div>
        <%} %>

        <%if (ViewPage.CurrentPage.IsBike){%>
        <div class="form-group all">
            <label>Dung tích</label>
            <select id="capacity" class="form-control form-search-now">
                <option value="0">Dung tích</option>
                <%=Utils.ShowDdlMenuByType2("Capacity", ViewPage.CurrentLang.ID, model.capacity) %>
            </select>
        </div>
        <%} %>

        <div class="form-group all">
            <label>Năm sản xuất</label>
            <select id="from" class="form-control form-search-now fl w49">
                <option value="0">Từ</option>
                <%for (int i = DateTime.Now.Year - 100; i <= DateTime.Now.Year; i++){%>
                <option value="<%=i %>" <%if (model.from == i){%>selected="selected"<%} %>><%=i %></option>
                <%} %>
            </select>
            <select id="to" class="form-control form-search-now fr w49">
                <option value="0">Đến</option>
                <%for (int i = DateTime.Now.Year - 100; i <= DateTime.Now.Year; i++){%>
                <option value="<%=i %>" <%if (model.to == i){%>selected="selected"<%} %>><%=i %></option>
                <%} %>
            </select>
        </div>
        <div class="form-group all">
            <label>Xuất xứ xe</label>
            <select id="madein" class="form-control form-search-now">
                <option value="0">Xuất xứ</option>
                <%=Utils.ShowDdlMenuByType2("MadeIn", ViewPage.CurrentLang.ID, model.madein) %>
            </select>
        </div>

        <%if (!ViewPage.CurrentPage.IsBike){%>
        <div class="form-group all">
            <label>Số chỗ ngồi</label>
            <select id="seat" class="form-control form-search-now">
                <option value="0">Số chỗ ngồi</option>
                <%=Utils.ShowDdlMenuByType2("Seat", ViewPage.CurrentLang.ID, model.seat) %>
            </select>
        </div>
        <%} %>

        <div class="form-group all">
            <label>Khoảng giá</label>
            <select id="price" class="form-control form-search-now">
                <option value="0">Khoảng giá</option>
                <%=Utils.ShowDdlMenuByType2("Price", ViewPage.CurrentLang.ID, model.price) %>
            </select>
        </div>
        <div class="form-group all">
            <div class="w49 fl all">
                <p class="txt-map"><i class="fa fa-map-marker "></i>Bạn muốn tìm mua xe ở khu vực nào?</p>

            </div>
            <select class="form-control form-search-now fr w49" onchange="$('#cityCode').val($(this).find(':selected').data('code')); return false;">
                <option value="0">Địa điểm</option>
                <%for (int i = 0; listCity != null && i < listCity.Count; i++){%>
                <option value="<%=listCity[i].ID %>" data-code="<%=listCity[i].Code %>" <%if(city != null && city.ID == listCity[i].ID) {%>selected="selected"<%} %>><%=listCity[i].Name %></option>
                <%} %>
            </select>
            <input type="hidden" id="cityCode" value="<%=city != null ? city.Code : string.Empty %>" />
        </div>
        <div class="form-group all check-find">
            <div class="w49 fl">
                <label class="itemCheckBox">
                    <input type="radio" name="status" <%if (model.status){%> checked="checked"<%} %> value="1"><i class="check-box"></i>
                    <span>Xe mới</span>
                </label>
            </div>
            <div class="w49 fr">
                <label class="itemCheckBox">
                    <input type="radio" name="status" <%if (!model.status){%> checked="checked"<%} %> value="0"><i class="check-box"></i>
                    <span>Xe cũ</span>
                </label>
            </div>
        </div>
        <div class="form-group all">
            <div class=" all">
                <%if (model.TotalRecord > 0){%>
                <p>Kết quả tìm kiếm được</p>
                <div class="w49 fl">
                    <b class="red"><%=string.Format("{0:#,##0}", model.TotalRecord) %> Xe</b>
                </div>
                <%} %>

                <%if (city != null){%>
                <div class="w49 fr">
                    <i class="fa fa-map-marker"></i>Tại <%=city.Name %>
                </div>
                <%} %>
            </div>
        </div>
        <div class="form-group all">
            <button type="button" onclick="searchBSCar(); return false;" class="btn btn-search btn-block"><i class="fa fa-search"></i>Xem ngay</button>
        </div>
        <p class="note-search">
            <span>*Lưu ý:</span> Không bắt buộc chọn hết các mục trên.<br>
            Nếu kết quả = 0 xin mời xem lại các mục đã chọn
        </p>
    </form>
</div>

<script type="text/javascript">
    $(function () {
        getChildPage($('#menu').val(), '<%=ViewPage.CurrentPage.ID%>');
    });

    function searchBSCar() {
        var url = '';

        var menuCode = $('#menuCode').val();
        var menuCode2 = $('#menuCode2').val();
        var driving = $('#driving').val();
        var fuel = $('#fuel').val();
        var gear = $('#gear').val();
        var from = $('#from').val();
        var to = $('#to').val();
        var madein = $('#madein').val();
        var seat = $('#seat').val();
        var price = $('#price').val();
        var cityCode = $('#cityCode').val();
        var status = $('input[type="radio"][name="status"]:checked').val();

        var keyword = $('#keyword').val();

        if (menuCode2 != '' && menuCode2 != 'default') url = menuCode2;
        else if (menuCode != '' && menuCode2 != 'default') url = menuCode;
        else url = 'cho-mua-ban-xe';

        if (cityCode != '') url = url + '--' + cityCode;

        url = '/' + url + '.html';

        if (driving > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'driving=' + driving;
        if (fuel > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'fuel=' + fuel;
        if (gear > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'gear=' + gear;
        if (from > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'from=' + from;
        if (to > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'to=' + to;
        if (madein > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'madein=' + madein;
        if (seat > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'seat=' + seat;
        if (price > 0) url += (url.indexOf('?') < 0 ? '?' : '&') + 'price=' + price;
        url += (url.indexOf('?') < 0 ? '?' : '&') + 'status=' + status;

        if (keyword != '') url += (url.indexOf('?') < 0 ? '?' : '&') + 'keyword=' + keyword;

        location.href = url;
    }
</script>