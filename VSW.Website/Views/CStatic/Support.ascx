﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<div class="sidebar-box mt10 ">
    <div class="ttl">
        <div class="txt">
            <a href="javascript:void(0)" rel="nofollow">Hỗ trợ online</a>
        </div>
    </div>
    <div class="sidebar-content list-support">
        <p>Hotline: <a href="call:0966888228" rel="nofollow">0966888228</a></p>
        <p>Email: <a href="mailto:support@webviet24h.com" rel="nofollow">support@webviet24h.com</a></p>
        <p class="social">
            <a href="">
                <img src="/Content/skins/images/skype_logo.png" alt="skype" />
            </a>
            <a href="">
                <img src="/Content/skins/images/fb_logo.png" width="100" alt="facebook" />
            </a>
        </p>
    </div>
</div>