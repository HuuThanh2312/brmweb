﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var _Cart = new Cart();
%>

<div id="header__links">
    <div class="container">
        <div class="row">
        </div>
    </div>
    <div id="header__segment_my_list_container">
        <div id="header__segment_my_list">
            <div class="container">
                <div class="row">
                    <div id="header__segment_my_list__left">
                        <div class="header">
                            <div class="title">
                                Danh sách của tôi
                            </div>
                        </div>
                        <div class="body">
                            <div class="list">
                            </div>
                        </div>
                    </div>

                    <div id="header__segment_my_list__right">
                        <div class="body">
                            <div class="list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="header__segment_my_list_overlay">
        </div>
    </div>
</div>
