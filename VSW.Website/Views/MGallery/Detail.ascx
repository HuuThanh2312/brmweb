﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModNewsEntity;
    var listItem = ViewBag.Other as List<ModNewsEntity>;
%>

<h1><%=item.Name %></h1>

<%=Utils.GetHtmlForSeo(item.Content) %>

<h2>Tin cùng chuyên mục</h2>

<ul class="news_list">
    <%for (var i = 0; listItem != null && i < listItem.Count; i++){
            string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
    %>
    <li>
        <p class="col1-4">
            <a href="<%=url %>">
                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 500, 500) %>" alt="<%=listItem[i].Name %>" />
            </a>
        </p>
        <div class="col3-4">
            <a href="<%=url %>"><%=listItem[i].Name %></a>
            <p class="news_summary"><%=listItem[i].Summary %></p>
        </div>
    </li>
    <%} %>
</ul>