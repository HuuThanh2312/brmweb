﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;
    if (item == null) { ViewPage.AlertThenRedirect("Bạn chưa được xác nhận là đối tác của Berich", "/"); return; }
    string _avatar = !string.IsNullOrEmpty(item.Logo) ? item.Logo.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<ModBerichProductEntity>;
    var model = ViewBag.Model as MBusinessProductModel;
    var listCategory = ViewBag.Category as List<ModBerichCategoryEntity>;
    string _url = VSW.Core.Global.Convert.ToString(VSW.Core.Web.HttpQueryString.GetValue("CategoryID"));
    _url = !string.IsNullOrEmpty(_url) ? "?CategoryID=" + _url : string.Empty;
%>


<!-- END PAGE BAR -->
<div class="row">
    <div class="col-md-12">
        <div class="header-tabs-doitac">
            <div class="tabs-find-doitac">
                <ul class="nav-justified">
                    <li class="active">
                        <a href="">Tất cả</a>
                    </li>
                    <%--<li>
                        <a href="">Còn hàng</a>
                    </li>
                    <li>
                        <a href="">Hết hàng</a>
                    </li>
                    <li>
                        <a href="">Đã bị khóa</a>
                    </li>--%>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark positionR">
                    <i class="fa fa-history"></i>
                    <span class="caption-subject bold uppercase">Sản phẩm từ nhà cung cấp</span>
                    <div class="div-export-import-oder">
                        <form name="AddProductPOST" method="post">
                            <div class="dropdown">
                                <input hidden="hidden" type="hidden" id="Count" value="0" />
                                <button class="dropbtn" type="button" onclick="AddProduct($('#Count').val())"><i class="fa fa-file-text"></i>&nbsp;Tải danh sách và nhập hàng loạt</button>
                                <input type="hidden" name="_vsw_action[AddProductPOST]"/>
                                <input type="submit" name="_vsw_action[AddProductPOST]" style="display: none">
                                <%--<div class="dropdown-content">
                                <a href=""><i class="fa fa-upload"></i>&nbsp;tải danh sách sản phẩm</a>
                            </div>--%>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tools"></div>
            </div>
            <div class="portlet-body">
                <input type="text" id="keyword" value="" onchange="search_dataname(this.value)" style="display: none" />
                <select name="CategoryID" class="form-control" onchange="redriect(this.value)" id="CategoryID" style="width: 35%">
                    <option value="0">Chọn Danh mục</option>
                    <%for (int i = 1; listCategory != null && i < listCategory.Count; i++)
                        {%>
                    <option class="excel" <%=!string.IsNullOrEmpty(model.CategoryID) && listCategory[i].CategoryID==model.CategoryID?"selected=\"selected\"":string.Empty %> data-name="<%=listCategory[i].CategoryName %>" value="<%=listCategory[i].CategoryID %>"><%=listCategory[i].CategoryName %></option>
                    <%} %>
                </select>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="center">#</th>
                            <th class="center">
                                <input type="checkbox" id="SelectAll" name="SelectAll" /></th>
                            <th class="center">Sản phẩm</th>
                            <th class="center" style="width: 15%;">Danh mục
                               
                            </th>
                            <th class="center">Giá sản phẩm</th>
                            <th class="center">Kho hàng</th>
                        </tr>
                    </thead>

                    <tbody class="center">
                        <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                            {
                                var _productFile = listItem[i].GetFileProduct();
                                %>
                        <tr>

                            <td><%=i+1 %></td>
                            <td>
                                <input type="checkbox" name="ProductID" onclick="doAddAirPort(this.value)" value="<%=listItem[i].ProductID %>" /></td>
                            <td>
                                <!-- Left-aligned media object -->
                                <div class="media">
                                    <div class="media-left">
                                        <img src="<%=Utils.GetMedia(_productFile.File, 60, 60) %>" class="media-object" style="width: 60px">
                                    </div>
                                    <div class="media-body text-left">
                                        <h5>
                                            <a href="javascript:void(0)"><%=listItem[i].ProductName %></a>
                                        </h5>
                                        <p>
                                            <%=listItem[i].ProductCode %>
                                        </p>
                                    </div>
                                </div>
                            </td>
                            <td><%=listItem[i].GetCategory().CategoryName %></td>
                            <td>
                                <b><%=string.Format("{0:#,##0}", listItem[i].GetCostPrice().SalePrice) %><sup>đ</sup></b>
                            </td>
                            <td>Số lượng: <%=listItem[i].InventoryAccount %></td>
                        </tr>
                        <%} %>
                    </tbody>

                </table>
                <div class="text-center mt40 mb40">
                    <ul class="pagination">
                        <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<script type="text/javascript">
    function AddProduct(Count) {
        if (Count < 1) {
            alert('Bạn phải chọn ít nhất 1 sản phẩm');
            return;
        }
        else {
            document.AddProductPOST.submit();
        }
    }
    //function show() {$('#keyword').slideToggle();}


    //function search_dataname(KeywordEmail) {
    //    var arrVal = document.getElementsByClassName('excel');

    //        var _showLi = '';

    //        for (var i = 0; i < arrVal.length; i++) {
    //            var name = arrVal[i].getAttribute('data-name');

    //             if (name.includes(KeywordEmail)) {
    //                _showLi += '<option class="excel" data-name="' + name + '" value="' + arrVal[i].value + '">' + name + '</option>';
    //            }
    //            else {
    //                _showLi += '<option class="excel" data-name="' + name + '" value="' + arrVal[i].value + '">' + name + '</option>';
    //            }
    //        }
    //        //alert(_showLi);
    //        $("#CategoryID").html(_showLi);
    //        $('#keyword').val('');

    //    }
    function redriect(url) {
        location.href = ' <%=ViewPage.CurrentURL +"?CategoryID="%>' + url;
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }
    var _cookie = readCookie('save_product_selected');

    function doAddAirPort() {

        var ArrAirPort = new Array();

        var AllAirPort = document.getElementsByName('ProductID');
        for (var i = 0; i < AllAirPort.length; i++) {
            if (AllAirPort[i].checked && ArrAirPort.indexOf(AllAirPort[i].value) < 0)
                ArrAirPort.push(AllAirPort[i].value);
        }
        var value = ''; var _count = 0;
        for (var i = 0; i < ArrAirPort.length; i++) {
            value += ArrAirPort[i] + ',';
        }
        if (value != '' && _cookie == null) { createCookie("template_save_product_selected", value, 90); _count += 1; $('#Count').val(_count); }
        else if (value != '' && _cookie != null && _cookie != '') { createCookie("template_save_product_selected", _cookie + value, 90); _count += 1; $('#Count').val(_count); }
        else { eraseCookie("template_save_product_selected"); }
        //$('#Emails').val(value);
    }


    var ArrProductID = new Array();

     <% for (var i = 0; listItem != null && i < listItem.Count; i++)
    { %>
    ArrProductID.push("<%=listItem[i].ProductID%>");
        <%} %>


    if (_cookie != null && _cookie != '') {
        var _subString = _cookie.split(',');
        var AllAirPort = document.getElementsByName('ProductID');
        for (var i = 0; i < _subString.length - 1; i++) {
            if (ArrProductID.indexOf(_subString[i]) > -1) {
                AllAirPort[ArrProductID.indexOf(_subString[i])].setAttribute('checked', 'checked');
            }
        }
    }
</script>
