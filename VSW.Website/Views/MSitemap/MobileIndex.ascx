﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModFeedbackEntity ?? new ModFeedbackEntity();
%>
<form method="post" name="feedback_form">
    <div class="box-pro all">
        <p class="title-cart all"><%=ViewPage.CurrentPage.Name %></p>
        <div class="modal-body">
            <ul class="login-form">
                <li>
                    <label>Họ và tên</label>
                    <input type="text" class="textform" name="Name" value="<%=item.Name %>">
                </li>
                <li>
                    <label>Email</label>
                    <input type="text" class="textform" name="Email" value="<%=item.Email %>">
                </li>
                <li>
                    <label>Điện thoại</label>
                    <input type="text" class="textform" name="Phone" value="<%=item.Phone %>">
                </li>
                <li>
                    <label>Địa chỉ </label>
                    <input type="text" class="textform" name="Address" value="<%=item.Address %>">
                </li>
                <li>
                    <label>Nội dung</label>
                    <textarea name="Content"><%=item.Content %></textarea>
                </li>
                <li>
                    <p class="btn-thanhtoan all"><a href="javascript:void(0)" onclick="document.feedback_form.submit()">Gửi Liên Hệ</a> </p>
                </li>
            </ul>
        </div>
        <input type="submit" name="_vsw_action[AddPOST]" style="display: none" />
        <input type="hidden" name="_vsw_action[AddPOST]" />
    </div>
</form>


