﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var listItem = ViewBag.Data as List<SysPageEntity>;
%>

<div class="content">
    <div class="wrapper">
        <ul itemprop="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#" class="breadcrumb">
            <%= Utils.GetMapPage(ViewPage.CurrentPage) %>
        </ul>

        <h1><%=ViewPage.CurrentPage.Name %></h1>
        <ul>
            <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                    var listChildItem = SysPageService.Instance.GetByParent_Cache(listItem[i].ID);
            %>
            <li>
                <a href="<%=ViewPage.GetPageURL(listItem[i]) %>"><%=listItem[i].Name %></a>

                <%if (listChildItem != null){%>
                <ul>
                    <%for (int j = 0; j < listChildItem.Count; j++){%>
                    <li><a href="<%=ViewPage.GetPageURL(listChildItem[j]) %>"><%=listChildItem[j].Name %></a></li>
                    <%} %>
                </ul>
                <%} %>
            </li>
            <%} %>
        </ul>
    </div>
</div>