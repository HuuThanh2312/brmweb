﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listItem = ViewBag.Data as List<Member>;
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>

<section class="manager-login">
    <div class="container">
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="">
                <!-- BEGIN PAGE TITLE-->

                <div class="row mt20">
                    <div class="col-md-12">
                        <!-- BEGIN PROFILE SIDEBAR -->
                        <div class="profile-sidebar">
                            <!-- PORTLET MAIN -->
                            <div class="portlet light profile-sidebar-portlet ">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name"><%=item.Name %></div>
                                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                                </div>
                                <div class="profile-usermenu">
                                    <ul class="nav">
                                        <li class="nav-item start">
                                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                                <i class="fa fa-home"></i>
                                                <span class="title">Trình quản lý</span>
                                            </a>
                                        </li>
                                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                                            { %>
                                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                                        </li>
                                        <%} %>
                                    </ul>
                                </div>
                            </div>

                            <!-- END PORTLET MAIN -->
                        </div>
                        <!-- END BEGIN PROFILE SIDEBAR -->
                        <!-- BEGIN PROFILE CONTENT -->
                        <div class="profile-content">
                            <div class="row mt20">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <i class="fa fa-list-ul font-dark"></i>
                                                <span class="caption-subject bold uppercase">Danh sách thành viên giới thiệu trực tiếp</span>
                                            </div>
                                            <div class="tools"></div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Số STT </th>
                                                        <th class="text-center">Họ và Tên </th>
                                                        <th class="text-center">Cấp </th>
                                                        <th class="text-center">Địa chỉ </th>
                                                        <th class="text-center">Ngày tham gia </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                                                        {
                                                    %>
                                                    <tr>
                                                        <td><%=i+1 %></td>
                                                        <td><%=listItem[i].Name %></td>
                                                        <td><%=listItem[i].Level %></td>
                                                        <td><%=listItem[i].Address %></td>
                                                        <td style="text-align: center;"><%=string.Format("{0:dd/MM/yyyy}", listItem[i].Created) %></td>
                                                    </tr>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>

                        </div>
                        <!-- END PROFILE CONTENT -->
                    </div>

                </div>
                <!-- END CONTENT BODY -->
            </div>
        </div>
        <!-- END CONTAINER -->
    </div>
</section>
