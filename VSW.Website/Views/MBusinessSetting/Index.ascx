﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;
    string _avatar = !string.IsNullOrEmpty(item.Logo) ? item.Logo.Replace("~/", "/") : string.Empty;
    var listPage = ViewBag.Page as List<SysPageEntity>;

%>

<div class="row">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %> </div>
                    <div class="profile-usertitle-job">Đã tham gia: <%=string.Format("{0:dd/MM/yyyy}", item.Published) %> </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            {
                                var listChildPage = SysPageService.Instance.GetByParent_Cache(listPage[i].ID);
                        %>
                        <h3 class="heading-menu"><%=listPage[i].Name %></h3>
                        <%if (listChildPage != null)
                            { %>
                        <%for (int j = 0; j < listChildPage.Count; j++)
                            {%>
                        <li class="<%=ViewPage.IsPageActived(listChildPage[j]) ? "active":string.Empty%>">
                            <a href="<%=ViewPage.GetPageURL(listChildPage[j]) %>"><i class="<%=listChildPage[j].Faicon %>"></i>&nbsp;<%=listChildPage[j].Name %></a>
                        </li>
                        <%} %>
                        <%} %>
                        <%} %>
                        <h3 class="heading-menu">Tài khoản</h3>
                        <li class="">
                            <a href="<%=ViewPage.WebUserCPUrl %>"><i class="fa fa-user"></i>&nbsp;Thông tin tài khoản</a>
                        </li>
                        <li class="">
                            <a href="<%=ViewPage.WebUserCPUpdateUrl %>"><i class="fa fa-key"></i>&nbsp;Thay đổi mật khẩu</a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <div class="profile-content">
            <div class="row">
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Hồ sơ tình trạng shop</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb10">
                                    <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void()">
                                        <div class="visual">
                                            <i class="fa fa-shopping-cart"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value=""><%=item.CountProduct %></span>
                                            </div>
                                            <div class="desc">Sản phẩm</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb10">
                                    <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void()">
                                        <div class="visual">
                                            <i class="fa fa-commenting-o"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="">57%</span>
                                            </div>
                                            <div class="desc">Tỷ lệ phản hồi</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb10">
                                    <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void()">
                                        <div class="visual">
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="">0</span>
                                            </div>
                                            <div class="desc">Đánh giá shop</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb10">
                                    <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void()">
                                        <div class="visual">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="">0</span>
                                            </div>
                                            <div class="desc">Đơn hàng chuyển thành công</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
                <div class="col-md-6">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin shop</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form  accept-charset="utf-8">
                                <div class="form-group">
                                    <label for="">Tên shop:</label>
                                    <input type="text" class="form-control" disabled="" value="<%=item.Name %>">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input type="text" class="form-control" disabled="" value="<%=item.Email %>">
                                </div>
                                <div class="form-group">
                                    <label for="">Số điện thoại:</label>
                                    <input type="text" class="form-control" disabled="" value="<%=item.Phone %>">
                                </div>
                                <div class="form-group">
                                    <label for="">Địa chỉ:</label>
                                    <input type="text" class="form-control" disabled="" value="<%=item.Address %>">
                                </div>

                                <div class="form-group">
                                    <label for="">Mô tả:</label>
                                    <textarea disabled="" class="form-control" rows="5" placeholder="Chưa có mô tả cho shop của bạn"><%=item.Summary %></textarea>
                                </div>
                                <div class="form-group text-center">
                                    <button type="button" onclick="location.href='<%=ViewPage.BuninessSettingCpUrl %>'" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Vào trang cập nhật</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
        </div>

    </div>
</div>
