﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;

    var listItem = ViewBag.Data as List<ModNewsEntity>;
%>

<div class="newsright">
    <label><%=page.Name %></label>
    <ul class="news-list">
        <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
        %>
        <li>
            <h3><a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></h3>
            <a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a><span><%=i + 1 %></span>
        </li>
        <%} %>
    </ul>
</div>