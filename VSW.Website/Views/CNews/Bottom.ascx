﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;

    var listItem = ViewBag.Data as List<ModNewsEntity>;
%>

<div class="news-top-right">
    <h3><a href="<%=ViewPage.GetPageURL(page) %>" title="<%=page.Name %>" target="_blank"><%=page.Name %></a></h3>
</div>
<div class="news-content-right">
    <ul>
        <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
        %>
        <li>
            <a href="<%=url %>" target="_blank">
                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 100, 100) %>" alt="<%=listItem[i].Name %>" />
                <h3><%=listItem[i].Name %></h3>
            </a>
        </li>
        <%} %>
    </ul>
</div>