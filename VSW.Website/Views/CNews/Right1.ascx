﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var page = ViewBag.Page as SysPageEntity;
    if (page == null) return;

    var listItem = ViewBag.Data as List<ModNewsEntity>;
%>

<div class="newsright">
    <label><%=page.Name %></label>
</div>
<div class="newsright bgghi">
    <ul class="news-right2">
        <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                string url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
        %>
        <li>
            <a href="<%=url %>" class="img-news" title="<%=listItem[i].Name %>">
                <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 260, 260) %>" alt="<%=listItem[i].Name %>" />
            </a>
            <h3><a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></h3>
        </li>
        <%} %>
    </ul>
</div>