﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEntity>;
%>

<div class="list-voucher">
    <ul class="owl-voucher positionR">
        <%for (var i = 0; listItem != null && i < listItem.Count; i++)
            {
                string _Url = ViewPage.GetURL(listItem[i].MenuID, listItem[i].Code);
        %>
        <li class="item">
            <div class="responsive-img">
                <a href="<%=_Url %>">
                    <img src="<%=listItem[i].File.Replace("~/","/") %>" alt="<%=listItem[i].Name %>">
                </a>
            </div>
        </li>
        <%} %>
    </ul>
</div>
