﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModVGPCntrDetailsEntity>;
    var model = ViewBag.Model as MVGPCntrDetailsModel;
%>

<div class="page-title">
    <h2 class="txt">Giao container Cho cảng</h2>
</div>
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Thông tin lô hàng</h3>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group clear">
                    <label class="col-md-4 col-sm-4 control-label pd0 blue">Mã Lô</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text" value="348525" disabled="">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group clear">
                    <label class="col-md-4 col-sm-4 control-label pd0">Mã số thuế</label>
                    <div class="col-md-8 col-sm-8">
                        <input class="form-control" type="text">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group clear">
                    <label class="col-md-4 col-sm-4 control-label pd0">Số lượng container</label>
                    <div class="col-md-8 col-sm-8">
                        <select name="" id="" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- /row -->
        <div class="row row_3_3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group clear">
                    <label class="col-md-2  col-sm-2 control-label pd0">Ghi chú</label>
                    <div class="col-md-10 col-sm-10">
                        <input class="form-control" type="text" value="">
                    </div>
                </div>
            </div>
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group clear ">
                    <p class="text-default">
                        Chủ hàng: CÔNG TY TNHH MỘT THÀNH VIÊN VICONSHIP HỒ CHÍ MINH - Địa chỉ: 2F, Đường 4F Cư xá Ngân hàng, P. Tân Thuận Tây, Quận 7, Tp.Hồ Chí Minh, Việt Nam
                    </p>
                </div>
            </div>
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 divpd-type">
                <div class="form-group clear">
                    <button class="btn btn-green mb5" type="button" data-toggle="modal" data-target="#popup-mk">Chọn lô hàng...</button>
                    <button class="btn btn-green mb5" type="button">Tạo lô mới</button>
                    <button class="btn btn-green mb5" type="button">Lưu Thông tin</button>
                    <button class="btn btn-green mb5" type="button" data-toggle="modal" data-target="#popup-hoadon">Hóa đơn điện tử</button>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group clear ">
                    <p class="text-default">(*) Mã số thuế dùng để xuất hóa đơn VAT </p>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /panel-body -->
</div>
<!-- /panel -->
<form method="post" id="save_vgp_cntr_details_form" name="save_vgp_cntr_details_form">
<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">Chi tiết lô hàng</h3>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="dataTables_length form-inline">
                    <label>
                        <select name="" id="" class="form-control">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                        </select>
                        Số hiển thị
                    </label>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 text-right">
                <button class="btn btn-default btn-add" type="button" id="click_toggle"><i class="fa fa-plus"></i>Thêm</button>
            </div>
        </div>
        <div id="toggle_block" class="pt15 pb15 toggle_add">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-3 col-sm-3 control-label pd0">Số Đ.Ký</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="text" class="form-control" name="CntrNo" value="" disabled="" />
                        </div>
                        <label class="col-md-2 col-sm-2 control-label pd0">Ngày</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" disabled="" value="03/07/2017 11:13">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Số Cont <span class="required">(*)</span></label>
                        <div class="col-md-8 col-sm-8">
                            <input type="text" class="form-control" name="CntrNo" value="" disabled="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Nhiệt độ <span class="required">(*)</span></label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control" type="text">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <p class="text-default">(Độ C)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Phương án</label>
                        <div class="col-md-8 col-sm-8  ">
                            <div class="input-group select-form" data-toggle="dropdown" role="button" aria-expanded="true">
                                <input class="form-control" type="text" value="HBCX - HẠ BÃI CHỜ XUẤT TÀU">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-form-control ">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Mã</th>
                                            <th>Mô tả</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>HBCX</td>
                                            <td>HẠ BÃI CHỜ XUẤT TÀU</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Số seal</label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" name="SealNo">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Thông gió</label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control" type="text" disabled="">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <p class="text-default">(M3/h)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Hãng tàu/Đại lý</label>
                        <div class="col-md-8 col-sm-8 select-form">
                            <div class="input-group select-form" data-toggle="dropdown" role="button" aria-expanded="true">
                                <input class="form-control" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-form-control ">
                                <div class="table-scroll">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Mã đại Lý</th>
                                                <th>Tên đại lý</th>
                                                <th>Mã chủ khai thác</th>
                                                <th>Tên chủ khai thác</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>CMA</td>
                                                <td>(PKG)High Peak Shipping</td>
                                                <td>HPK</td>
                                                <td>High Peak Shipping</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Kích cỡ <span class="required">(*)</span></label>
                        <div class="col-md-8 col-sm-8 select-form">
                            <div class="input-group select-form" data-toggle="dropdown" role="button" aria-expanded="true">
                                <input class="form-control" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-form-control ">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Kích cỡ</th>
                                            <th>Mô tả</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2200</td>
                                            <td>20, cao 8.6 Feet, GP</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Độ ẩm</label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control" type="text" disabled="">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <p class="text-default">(%)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Số Book <span class="required">(*)</span></label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Trọng lượng</label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control" type="text" disabled="">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <p class="text-default">(Tấn)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">IMO</label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Tàu/chuyến <span class="required">(*)</span></label>
                        <div class="col-md-8 col-sm-8 select-form">
                            <div class="input-group select-form" data-toggle="dropdown" role="button" aria-expanded="true">
                                <input class="form-control" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-form-control ">
                                <div class="table-scroll">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Mã tàu</th>
                                                <th>Tên tàu</th>
                                                <th>Chuyến</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>CMA</td>
                                                <td>(PKG)High Peak Shipping</td>
                                                <td>HPK</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 control-label pd0">IMO</label>
                        <div class="col-md-8 col-sm-8">
                            <label class="itemCheckBox">
                                <input type="checkbox" id="checkbox1">
                                <i for="checkbox1" class="check-box"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">UNNO</label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Cảng ch.tải <span class="required">(*)</span></label>
                        <div class="col-md-8 col-sm-8 select-form">
                            <div class="input-group select-form" data-toggle="dropdown" role="button" aria-expanded="true">
                                <input class="form-control" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Đơn vị kiểm định</label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Số ĐTDĐ</label>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Cảng đích</label>
                        <div class="col-md-8 col-sm-8 select-form">
                            <div class="input-group select-form" data-toggle="dropdown" role="button" aria-expanded="true">
                                <input class="form-control" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-form-control ">
                                <div class="table-scroll">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Mã Cảng</th>
                                                <th>Tên Cảng</th>
                                                <th>Quốc gia</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Noway</td>
                                                <td>.Noway</td>
                                                <td>Noway</td>
                                            </tr>
                                            <tr>
                                                <td>TW042</td>
                                                <td>042-KAOSHUNG</td>
                                                <td>KAOSHUNG OFF CHINA</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-4 col-sm-4 control-label pd0">Max gross</label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control" type="text" disabled="">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <p class="text-default">(%)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="itemCheckBox ">
                            <input type="checkbox" id="checkbox1">
                            <i for="checkbox1" class="check-box"></i>
                            <span>Kiểm hóa</span>
                        </label>
                        <label class="itemCheckBox">
                            <input type="checkbox" id="checkbox1">
                            <i for="checkbox1" class="check-box"></i>
                            <span>Kiểm hóa 100%</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-2  col-sm-2 control-label pd0">Ghi chú</label>
                        <div class="col-md-10 col-sm-10">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group clear">
                        <label class="itemCheckBox ">
                            <input type="checkbox" id="checkbox1">
                            <i for="checkbox1" class="check-box"></i>
                            <span>Quá khổ</span>
                        </label>
                        <label class="itemCheckBox">
                            <input type="checkbox" id="checkbox1">
                            <i for="checkbox1" class="check-box"></i>
                            <span>Không cài nhiệt độ</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row row_3_3">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group clear">
                        <label class="col-md-2  col-sm-2 control-label pd0"></label>
                        <div class="col-md-10 col-sm-10">
                            <p class="text-default">Ghi chú: Trường hợp cảng đích không có trong danh mục thì nhập "Cangdich:..." vào ghi chú</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12 divpd-type">
                    <div class="form-group clear">
                        <button class="btn btn-green mb5" type="button" onclick="save_vgp_cntr_details()">Lưu</button>
                        <button class="btn btn-green mb5" type="button" id="close_toggle">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-scroll">
            <table class="table table-striped  table-bordered dataTable">
                <thead>
                    <tr>
                        <th class="sorting">No.</th>
                        <th class="sorting">Số ĐK</th>
                        <th class="sorting">Container</th>
                        <th class="sorting">K.Thước</th>
                        <th class="sorting">K.Hóa</th>
                        <th class="sorting">Đại Lý</th>
                        <th class="sorting">Số Book</th>
                        <th class="sorting">Tàu</th>
                        <th class="sorting">Chuyến</th>
                        <th class="sorting">Cảng c.tải</th>
                        <th class="sorting">C.Đích</th>
                        <th class="sorting">T.Toán</th>
                        <th class="sorting">EIR Cổng</th>
                        <th class="sorting">Số ĐTDĐ</th>
                        <th class="sorting">In Phiếu</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td>52ZVK2</td>
                        <td>FCIU2726780</td>
                        <td>2200</td>
                        <td></td>
                        <td>MSC </td>
                        <td>339LN1722422</td>
                        <td>MSC SIERRA II</td>
                        <td>HL727R</td>
                        <td>SGSIN (SINGAPORE)</td>
                        <td>GRSKG (THESSALONIKI)</td>
                        <td>Y</td>
                        <td>R7799955</td>
                        <td></td>
                        <td><a href="" data-toggle="modal" data-target="#popup-phieuin">Chưa in</a></td>
                    </tr>
                    <tr style="display: none;">
                        <td colspan="16" class="text-center">No data to display</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="dataTables_info form-inline">
                    <label>
                        Đang hiển thi 1 đến 10 của 50 mục
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="dataTables_paginate  form-inline fr">
                    <ul class="pagination">
                        <li class="previous disabled"><a href="#">Previous</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="next"><a href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /panel-body -->
</div>
</form>
<!-- /panel -->
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 divpd-type">
                <div class=" clear">
                    <button class="btn btn-green mb5" type="button">Import Excel...</button>
                    <button class="btn btn-green mb5" type="button">Tạo hóa đơn...</button>
                    <button class="btn btn-green mb5" type="button">In Biên Nhận...</button>
                    <button class="btn btn-green mb5" type="button">In Phiếu...</button>
                    <button class="btn btn-green mb5" type="button">Xóa Phiếu</button>
                    <button class="btn btn-green mb5" type="button">Xuất Excel</button>
                    <button class="btn btn-green mb5" type="button">Refresh</button>
                    <button class="btn btn-green mb5" type="button">Gửi SMS</button>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /panel-body -->
</div>



<div class="next-page all">
    <ul>
        <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
    </ul>
</div>
