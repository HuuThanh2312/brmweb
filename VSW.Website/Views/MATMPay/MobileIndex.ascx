﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl"%>

<%
    var item = ViewBag.Data as ModWebUserEntity;
%>

<div class="giohang">
    <h1><a href="<%=ViewPage.CurrentURL %>"><%=ViewPage.CurrentPage.Name %></a></h1>

    <form method="post" name="login_form">
        <div class="customer_info">Đăng nhập</div>
        <ul class="customer_form">
            <li>
                <label>Email đăng nhập<span>*</span></label>
                <p><input type="text" name="Email" placeholder="Email đăng nhập" value="" /></p>
            </li>
            <li>
                <label>Mật khẩu</label>
                <p><input type="password" name="Password" placeholder="Mật khẩu" value="" /></p>
            </li>
            <li>
                <a href="javascript:void(0)" onclick="document.login_form.submit()" class="quick_button">Đăng nhập</a>
                <input type="hidden" name="_vsw_action[LoginPOST]" />
                <input type="submit" name="_vsw_action[LoginPOST]" style="display: none" />
            </li>
        </ul>
    </form>

</div>