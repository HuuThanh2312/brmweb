﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModATMPayEntity;
    var model = ViewBag.Model as MATMPayModel;
    var Order =  ViewBag.Order as ModOrderEntity;
    if (Order == null) return;
%>

<form method="post" name="nganluong_form">
    <section class="box-cate">
        <div class="container">
            <div class="row">
                <article class="thumbnail-news-view">
                    <h2>Chọn phương thức thanh toán</h2>
                    <div class="post_content">
                        <ul class="list-content">
                            <li class="active">

                                <div class="boxContent">
                                    <p>
                                        Thanh toán trực tuyến AN TOÀN và ĐƯỢC BẢO VỆ, sử dụng thẻ ngân hàng trong và ngoài nước hoặc nhiều hình thức tiện lợi khác.
				            Được bảo hộ & cấp phép bởi NGÂN HÀNG NHÀ NƯỚC, ví điện tử duy nhất được cộng đồng ƯA THÍCH NHẤT 2 năm liên tiếp, Bộ Thông tin Truyền thông trao giải thưởng Sao Khuê
				            <br />
                                        Giao dịch. Đăng ký ví NgânLượng.vn miễn phí <a href="https://www.nganluong.vn/?portal=nganluong&amp;page=user_register" target="_blank">tại đây</a>
                                    </p>
                                </div>
                            </li>
                            <li class="active">
                                <label>
                                    <input type="radio" name="option_payment" checked="checked" value="ATM_ONLINE" />
                                    Thanh toán online bằng thẻ ngân hàng nội địa
                                </label>
                                <div class="boxContent">
                                    <ul class="cardList clearfix">

                                        <li class="bank-online-methods ">
                                            <label for="vcb_ck_on">
                                                <i class="VCB" title="Ngân hàng TMCP Ngoại Thương Việt Nam"></i>
                                                <input type="radio" name="bankcode" value="VCB" />Ngân hàng TMCP Ngoại Thương Việt Nam
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="vnbc_ck_on">
                                                <i class="DAB" title="Ngân hàng Đông Á"></i>
                                                <input type="radio" name="bankcode" value="DAB" />Ngân hàng Đông Á
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="tcb_ck_on">
                                                <i class="TCB" title="Ngân hàng Kỹ Thương"></i>
                                                <input type="radio" name="bankcode" value="TCB" />Ngân hàng Kỹ Thương
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_mb_ck_on">
                                                <i class="MB" title="Ngân hàng Quân Đội"></i>
                                                <input type="radio" name="bankcode" value="MB" />Ngân hàng Quân Đội
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="shb_ck_on">
                                                <i class="SHB" title="Ngân hàng Sài Gòn - Hà Nội"></i>
                                                <input type="radio" name="bankcode" value="SHB" />Ngân hàng Sài Gòn - Hà Nội
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_vib_ck_on">
                                                <i class="VIB" title="Ngân hàng Quốc tế"></i>
                                                <input type="radio" name="bankcode" value="VIB" />Ngân hàng Quốc tế
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_vtb_ck_on">
                                                <i class="ICB" title="Ngân hàng Công Thương Việt Nam"></i>
                                                <input type="radio" name="bankcode" value="ICB" />Ngân hàng Công Thương Việt Nam
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_exb_ck_on">
                                                <i class="EXB" title="Ngân hàng Xuất Nhập Khẩu"></i>
                                                <input type="radio" name="bankcode" value="EXB" />Ngân hàng Xuất Nhập Khẩu
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_acb_ck_on">
                                                <i class="ACB" title="Ngân hàng Á Châu"></i>
                                                <input type="radio" name="bankcode" value="ACB" />Ngân hàng Á Châu
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_hdb_ck_on">
                                                <i class="HDB" title="Ngân hàng Phát triển Nhà TPHCM"></i>
                                                <input type="radio" name="bankcode" value="HDB" />Ngân hàng Phát triển Nhà TPHCM
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_msb_ck_on">
                                                <i class="MSB" title="Ngân hàng Hàng Hải"></i>
                                                <input type="radio" name="bankcode" value="MSB" />Ngân hàng Hàng Hải
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_nvb_ck_on">
                                                <i class="NVB" title="Ngân hàng Nam Việt"></i>
                                                <input type="radio" name="bankcode" value="NVB" />Ngân hàng Nam Việt
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_vab_ck_on">
                                                <i class="VAB" title="Ngân hàng Việt Á"></i>
                                                <input type="radio" name="bankcode" value="VAB" />Ngân hàng Việt Á
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_vpb_ck_on">
                                                <i class="VPB" title="Ngân Hàng Việt Nam Thịnh Vượng"></i>
                                                <input type="radio" name="bankcode" value="VPB" />Ngân Hàng Việt Nam Thịnh Vượng
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_scb_ck_on">
                                                <i class="SCB" title="Ngân hàng Sài Gòn Thương tín"></i>
                                                <input type="radio" name="bankcode" value="SCB" />Ngân hàng Sài Gòn Thương tín
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="ojb_ck_on">
                                                <i class="OJB" title="Ngân hàng Đại Dương"></i>
                                                <input type="radio" name="bankcode" value="OJB" />Ngân hàng Đại Dương
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="bnt_atm_pgb_ck_on">
                                                <i class="PGB" title="Ngân hàng Xăng dầu Petrolimex"></i>Ngân hàng Xăng dầu Petrolimex
                                        <input type="radio" name="bankcode" value="PGB" />
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="bnt_atm_gpb_ck_on">
                                                <i class="GPB" title="Ngân hàng TMCP Dầu khí Toàn Cầu"></i>Ngân hàng TMCP Dầu khí Toàn Cầu
                                        <input type="radio" name="bankcode" value="GPB" />
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="bnt_atm_agb_ck_on">
                                                <i class="AGB" title="Ngân hàng Nông nghiệp &amp; Phát triển nông thôn"></i>
                                                <input type="radio" name="bankcode" value="AGB" />Ngân hàng Nông nghiệp &amp; Phát triển nông thôn
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="bnt_atm_sgb_ck_on">
                                                <i class="SGB" title="Ngân hàng Sài Gòn Công Thương"></i>
                                                <input type="radio" name="bankcode" value="SGB" />Ngân hàng Sài Gòn Công Thương
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="bnt_atm_nab_ck_on">
                                                <i class="NAB" title="Ngân hàng Nam Á"></i>
                                                <input type="radio" name="bankcode" value="NAB" />Ngân hàng Nam Á
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_bab_ck_on">
                                                <i class="BAB" title="Ngân hàng Bắc Á"></i>
                                                <input type="radio" name="bankcode" value="BAB" />Ngân hàng Bắc Á
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_tpb_ck_on">
                                                <i class="TPB" title="Ngân hàng TMCP Tiên Phong"></i>
                                                <input type="radio" name="bankcode" value="TPB" />Ngân hàng TMCP Tiên Phong 
                                            </label>
                                        </li>

                                        <li class="bank-online-methods ">
                                            <label for="sml_atm_bidv_ck_on">
                                                <i class="BIDV" title="Ngân hàng Bắc Á"></i>
                                                <input type="radio" name="bankcode" value="BIDV" />Ngân hàng TMCP Tiên Phong 
                                            </label>
                                        </li>

                                    </ul>

                                </div>
                            </li>

                        </ul>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="box-cate mt10 all mb10">
        <div class="col-md-4 col-md-offset-4">
            <div class="Loadcard ">
                <p class="txt text-center">Nhập thông tin chuyển khoản</p>
                <div class="form-group form-group-select col-md-12 col-lg-12 sa-lichek">
                    <input type="text" class="form-control" name="TotalAmount" value="<%=Order!=null? Order.Total:0 %>" placeholder="Số tiền cần chuyển" />
                </div>
                <div class="form-group form-group-select col-md-12 col-lg-12 sa-lichek">
                    <input type="text" class="form-control" name="Name" value="<%=Order!=null?Order.Name:item.Name %>" placeholder="Họ và tên chủ thẻ" />
                </div>
                <div class="form-group form-group-select col-md-12 col-lg-12 sa-lichek">
                    <input type="text" class="form-control" name="Email" value="<%=Order!=null?Order.Email:item.Email %>" placeholder="Địa chỉ email chủ thẻ" />
                </div>
                <div class="form-group form-group-select col-md-12 col-lg-12 sa-lichek">
                    <input type="text" class="form-control" name="Phone" value="<%=Order!=null?Order.Phone:item.Phone %>" placeholder="Số điện thoại liên hệ" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <button type="submit" name="_vsw_action[AddPOST]" class=" btn btn-search">Xác nhận</button>
                </div>
            </div>
        </div>
    </section>


</form>

<script type="text/javascript">
    $('input[name="option_payment"]').bind('click', function () {
        $('.list-content li').removeClass('active');
        $(this).parent().parent('li').addClass('active');
    });
</script>
