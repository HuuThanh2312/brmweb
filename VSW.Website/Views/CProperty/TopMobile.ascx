﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    public string SortMode
    {
        get
        {
            string sort = VSW.Core.Web.HttpQueryString.GetValue("sort").ToString().ToLower().Trim();

            if (sort == "new" || sort == "view" || sort == "price_asc" || sort == "price_desc")
                return sort;
            return "new";
        }
    }

    public string GetURL(string sKey, string sValue)
    {
        string strUrl = string.Empty;
        string strKey = string.Empty;
        string strValue = string.Empty;
        for (int i = 0; i < ViewPage.PageViewState.Count; i++)
        {
            strKey = ViewPage.PageViewState.AllKeys[i];
            strValue = ViewPage.PageViewState[strKey].ToString();
            if (strKey.ToLower() == sKey.ToLower() || strKey.ToLower() == "vsw" || strKey.ToLower() == "v" || strKey.ToLower() == "s" || strKey.ToLower() == "w" || strKey.ToLower().Contains("web."))
                continue;
            if (strUrl == string.Empty)
                strUrl = "?" + strKey + "=" + HttpContext.Current.Server.UrlEncode(strValue);
            else
                strUrl += "&" + strKey + "=" + HttpContext.Current.Server.UrlEncode(strValue);
        }
        strUrl += (strUrl == string.Empty ? "?" : "&") + sKey + "=" + sValue;

        return strUrl;
    }

    public string GetAddAtr(int[] ArrPropertyID, int propertyId)
    {
        if (ArrPropertyID == null)
            return ViewPage.CurrentUrl  + "?atr=" + propertyId;

        var listId = new List<int>();
        listId.AddRange(ArrPropertyID);

        if (!listId.Contains(propertyId))
            listId.Add(propertyId);

        return ViewPage.CurrentUrl  + "?atr=" + string.Join("-", listId.ToArray());
    }

    public string GetRemoveAtr(int[] ArrPropertyID, int propertyId)
    {
        var listId = new List<int>();
        listId.AddRange(ArrPropertyID);

        if (listId.Contains(propertyId))
            listId.Remove(propertyId);

        if (listId.Count == 0)
            return ViewPage.CurrentUrl;

        return ViewPage.CurrentUrl  + "?atr=" + string.Join("-", listId.ToArray());
    }
</script>

<% 
    if (ViewPage.CurrentModule.Code != "MProduct") return;

    var listItem = ViewBag.Data as Dictionary<WebPropertyEntity, List<WebPropertyEntity>>;
    if (listItem == null) return;

    var ArrPropertyID = ViewBag.ArrPropertyID;

    var listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);
    if (listPage == null)
        listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
%>

<section class="box-home all">
    <%if (ViewPage.ViewBag.Data == null){%>
    <h1 class="page-title"><%=ViewPage.CurrentPage.Name %></h1>
    <%}else{%>
    <h2 class="page-title"><%=ViewPage.CurrentPage.Name %></h2>
    <%} %>

    <div class="container-fluid">
        <ul class="filter clear row">
            <li>
                <div class="click-filter">
                    <p class="outer-icon"><i class="fa fa-list-alt"></i></p>Danh mục<b></b>
                </div>
                <div class="filter-bg">
                    <div class="filter-box">
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++){%>
                        <a href="<%=ViewPage.GetPageUrl(listPage[i]) %>"><%=listPage[i].Name %></a>
                        <%} %>
                    </div>
                </div>
            </li>
            <li>
                <div class="click-filter">
                    <p class="outer-icon"><i class="fa fa-filter"></i></p>
                    Sắp xếp theo <b></b>
                </div>
                <div class="filter-bg">
                    <div class="filter-box">
                        <a href="<%= GetURL("sort", "price_desc") %>" rel="nofollow">Giá: thấp -> cao</a>
                        <a href="<%= GetURL("sort", "price_asc") %>" rel="nofollow">Giá: cao -> thấp</a>
                        <a href="<%= GetURL("sort", "view") %>">Xem nhiều nhất</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="click-filter">
                    <p class="outer-icon"><i class="fa fa-search"></i></p>
                    Tìm kiếm nâng cao<b></b>
                </div>
                <div class="filter-bg">
                    <%
                        foreach (var item in listItem.Keys){
                            var listChildItem = listItem[item];
                    %>
                    <div class="title-filter"><%=item.Name %></div>
                    <ul class="pca-filter">
                        <%for (var k = 0; listChildItem != null && k < listChildItem.Count; k++){%>

                        <%if (ArrPropertyID != null && Array.IndexOf(ArrPropertyID, listChildItem[k].ID) > -1){%>
                        <li><a href="<%=GetRemoveAtr(ArrPropertyID, listChildItem[k].ID) %>" class="active" title="<%=listChildItem[k].Name %>"><%=listChildItem[k].Name %></a></li>
                        <%}
                            else
                            {%>
                        <li><a href="<%=GetAddAtr(ArrPropertyID, listChildItem[k].ID) %>" title="<%=listChildItem[k].Name %>"><%=listChildItem[k].Name %></a></li>
                        <%} %>

                        <%} %>
                    </ul>
                    <%} %>
                </div>
            </li>
        </ul>
    </div>
</section>