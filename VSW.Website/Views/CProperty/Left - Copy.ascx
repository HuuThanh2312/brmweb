﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    string SortMode
    {
        get
        {
            string sort = VSW.Core.Web.HttpQueryString.GetValue("sort").ToString().ToLower().Trim();

            if (sort == "new_asc" || sort == "new_desc" || sort == "price_asc" || sort == "price_desc" || sort == "like_asc" || sort == "like_desc")
                return sort;

            return "new_desc";
        }
    }

    string GetURL(string key, string value)
    {
        string strUrl = string.Empty;
        string strKey = string.Empty;
        string strValue = string.Empty;
        for (int i = 0; i < ViewPage.PageViewState.Count; i++)
        {
            strKey = ViewPage.PageViewState.AllKeys[i];
            strValue = ViewPage.PageViewState[strKey].ToString();

            if (strKey.ToLower() == key.ToLower() || strKey.ToLower() == "vsw" || strKey.ToLower() == "v" || strKey.ToLower() == "s" || strKey.ToLower() == "w" || strKey.ToLower().Contains("web."))
                continue;
            if (strUrl == string.Empty)
                strUrl = "?" + strKey + "=" + ViewPage.Server.UrlEncode(strValue);
            else
                strUrl += "&" + strKey + "=" + ViewPage.Server.UrlEncode(strValue);
        }

        strUrl += (strUrl == string.Empty ? "?" : "&") + key + "=" + value;

        return strUrl;
    }

    public string GetAddAtr(int[] ArrPropertyID, int propertyId)
    {
        if (ArrPropertyID == null)
            return ViewPage.CurrentURL  + "?atr=" + propertyId;

        var listId = new List<int>();
        listId.AddRange(ArrPropertyID);

        if (!listId.Contains(propertyId))
            listId.Add(propertyId);

        return ViewPage.CurrentURL  + "?atr=" + string.Join("-", listId.ToArray());
    }

    public string GetRemoveAtr(int[] ArrPropertyID, int propertyId)
    {
        var listId = new List<int>();
        listId.AddRange(ArrPropertyID);

        if (listId.Contains(propertyId))
            listId.Remove(propertyId);

        if (listId.Count == 0)
            return ViewPage.CurrentURL;

        return ViewPage.CurrentURL  + "?atr=" + string.Join("-", listId.ToArray());
    }
</script>

<% 
    var listItem = ViewBag.Data as Dictionary<WebPropertyEntity, List<WebPropertyEntity>>;
    if (listItem == null) return;

    var ArrPropertyID = ViewBag.ArrPropertyID;

    var model = ViewPage.ViewBag.Model as MProductModel;
%>

<div class="form-find-check">
    <ul class="leftm" id="PP">
        <li>
            <label class="title-find">Hãng sản xuất<i id="ist12">-</i>
            </label>
            <div class="find-2">

                <p>
                    <input type="radio" id="find2-radio-1" name="radio-check_2">
                    <label for="find2-radio-1">
                        <i class="bgh ibosch"></i>
                    </label>
                </p>

            </div>
        </li>

        <%foreach (var item in listItem.Keys){
                var listChildItem = listItem[item];
        %>
        <li>
            <label class="title-find"><%=item.Name %><i>-</i></label>
            <div class="find-1">
                
                <%for (var k = 0; listChildItem != null && k < listChildItem.Count; k++){%>

                <%if (ArrPropertyID != null && Array.IndexOf(ArrPropertyID, listChildItem[k].ID) > -1){%>
                <p>
                    <input type="radio" name="<%=item.Code %>" checked="checked" onclick="location.href='<%=GetRemoveAtr(ArrPropertyID, listChildItem[k].ID) %>'" />
                    <label><%=listChildItem[k].Name %></label>
                </p>
                <%}else{%>
                <p>
                    <input type="radio" name="<%=item.Code %>" onclick="location.href='<%=GetAddAtr(ArrPropertyID, listChildItem[k].ID) %>'" />
                    <label><%=listChildItem[k].Name %></label>
                </p>
                <%} %>

                <%} %>

            </div>
        </li>
        <%} %>
    </ul>
</div>











<div class="box-filter-wrapper">
    <div class="container">
        <div class="row">
            <div class="filter-head">
                <h1 class="title-h"><%=ViewPage.CurrentPage.Name %></h1>
                <div class="txt-amount">
                    (<strong><%=string.Format("{0:#,##0}", model.TotalRecord) %></strong> sản phẩm)
                </div>
            </div>
            <div class="filter-content">
                <div class="wrap-filter">

                    <%foreach (var item in listItem.Keys){
                            var listChildItem = listItem[item];
                            string cssClass = "check";
                    %>
                    <div class="filter-row">
                        <div class="ttl-left "><%=item.Name %></div>
                        <div class="cont-filter-attr">
                            <ul>
                                <%for (var k = 0; listChildItem != null && k < listChildItem.Count; k++){%>

                                <%if (ArrPropertyID != null && Array.IndexOf(ArrPropertyID, listChildItem[k].ID) > -1){%>
                                <li class="<%=cssClass %> active">
                                    <a href="<%=GetRemoveAtr(ArrPropertyID, listChildItem[k].ID) %>" rel="nofollow"><i class="icon-checkbox"></i><%=listChildItem[k].Name %></a>
                                </li>
                                <%}else{%>
                                <li>
                                    <a href="<%=GetAddAtr(ArrPropertyID, listChildItem[k].ID) %>" rel="nofollow"><i class="icon-checkbox"></i><%=listChildItem[k].Name %></a>
                                </li>
                                <%} %>

                                <%} %>
                            </ul>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
            <div class="pagination-filter">
                <ul class="other-filters">
                    <li><a href="<%=GetURL("state", "1") %>">Shop đề cử</a></li>
                    <li class="active"><a href="<%=GetURL("state", "2") %>">Bán chạy</a></li>

                    <%if (SortMode == "price_asc"){%>
                    <li><a href="<%=GetURL("sort", "price_desc") %>">Giá<i class="fa fa-long-arrow-down"></i></a></li>
                    <%}else {%>
                    <li><a href="<%=GetURL("sort", "price_asc") %>">Giá<i class="fa fa-long-arrow-up"></i></a></li>
                    <%} %>

                    <%if (SortMode == "new_asc"){%>
                    <li><a href="<%=GetURL("sort", "new_desc") %>">Mới<i class="fa fa-long-arrow-down"></i></a></li>
                    <%}else {%>
                    <li><a href="<%=GetURL("sort", "new_asc") %>">Mới<i class="fa fa-long-arrow-up"></i></a></li>
                    <%} %>

                    <%if (SortMode == "like_asc"){%>
                    <li><a href="<%=GetURL("sort", "like_desc") %>">Lượt yêu thích<i class="fa fa-long-arrow-down"></i></a></li>
                    <%}else {%>
                    <li><a href="<%=GetURL("sort", "like_asc") %>">Lượt yêu thích<i class="fa fa-long-arrow-up"></i></a></li>
                    <%} %>

                </ul>
                <ul class="optionsFilterForShop">
                    <li class="lb-promotion"><a href="<%=GetURL("state", "8") %>"><i class="fa fa-gift"></i>Khuyến mại</a></li>
                    <li class="shop_free_shipping"><a href=""><i class="fa fa-truck"></i>Miễn phí</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>