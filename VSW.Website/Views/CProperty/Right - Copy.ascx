﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    string GetAddAtr(string url, int propertyID, WebPropertyEntity select)
    {
        var _ArrPropertyID = _ArrID;

        if (_ArrPropertyID == null)
            return url + "?atr=" + propertyID;

        var listID = new List<int>();
        listID.AddRange(_ArrPropertyID);

        if (!listID.Contains(propertyID)) listID.Add(propertyID);

        if (select != null && listID.Contains(select.ID)) listID.Remove(select.ID);

        return url + "?atr=" + string.Join("-", listID.ToArray());
    }

    string GetRemoveAtr(string url, int propertyID)
    {
        var _ArrPropertyID = _ArrID;
        
        var listID = new List<int>();
        listID.AddRange(_ArrPropertyID);

        if (listID.Contains(propertyID)) listID.Remove(propertyID);

        if (listID.Count == 0)
            return url;

        return url + "?atr=" + string.Join("-", listID.ToArray());
    }

    List<SysPageEntity> listPage = null;
    List<WebPropertyEntity> listItem = null;
    int[] _ArrID;
    protected void Page_Load(object sender, EventArgs e)
    {
        listItem = ViewBag.Data as List<WebPropertyEntity>;
        
        var _Atr = VSW.Core.Web.HttpQueryString.GetValue("atr").ToString();
        if (!string.IsNullOrEmpty(_Atr)) _ArrID = VSW.Core.Global.Array.ToInts(_Atr.Split('-'));

        listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);
        if (listPage == null) listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
    }
</script>

<div class="boloc">
    <%for (int i = 0; listItem != null && i < listItem.Count; i++){
          var listChildItem = WebPropertyService.Instance.CreateQuery()
                                          .Where(o => o.ParentID == listItem[i].ID)
                                          .OrderByAsc(o => o.Order)
                                          .ToList_Cache();

          WebPropertyEntity select = null;
          for (int k = 0; listChildItem != null && k < listChildItem.Count; k++)
          {
              if (_ArrID != null && Array.IndexOf(_ArrID, listChildItem[k].ID) > -1)
              {
                  select = listChildItem[k];
                  break;
              }
          }
    %>
    <div class="loc">
        <div class="title-loc"><%=listItem[i].Name%></div>
        <div class="box-loc">
            <ul class="loc-ngan">
                <%for (var k = 0; listChildItem != null && k < listChildItem.Count; k++){
                        var _Manufacturer = listChildItem[k].GetManufacturer();
                %>

                <%if (_ArrID != null && Array.IndexOf(_ArrID, listChildItem[k].ID) > -1){%>
                <li class="active">
                    <a href="<%=GetRemoveAtr(ViewPage.CurrentURL, listChildItem[k].ID) %>" class="hang">
                        <span class="bcheck">
                            <i class="icon checkboxed"></i>
                            <i class="icon unchecked"></i>
                        </span>
                        
                        <%if(listItem[i].IsManufacturer && !string.IsNullOrEmpty(_Manufacturer.File)) {%>
                        <img src="<%=_Manufacturer.File.Replace("~/", "/") %>" alt="<%=_Manufacturer.Name %>" />
                        <%}else {%>
                        <span><%=select.Name %></span>
                        <%} %>
                    </a>
                </li>
                <%}else {%>
                <li>
                    <a href="<%=GetAddAtr(ViewPage.CurrentURL, listChildItem[k].ID, select) %>" class="hang">
                        <span class="bcheck">
                            <i class="icon checkboxed"></i>
                            <i class="icon unchecked"></i>
                        </span>
                        
                        <%if(listItem[i].IsManufacturer && !string.IsNullOrEmpty(_Manufacturer.File)) {%>
                        <img src="<%=_Manufacturer.File.Replace("~/", "/") %>" alt="<%=_Manufacturer.Name %>" />
                        <%}else {%>
                        <span><%=listChildItem[k].Name %></span>
                        <%} %>
                    </a>
                </li>
                <%} %>

                <%} %>
            </ul>
        </div>
    </div>
    <%} %>
</div>