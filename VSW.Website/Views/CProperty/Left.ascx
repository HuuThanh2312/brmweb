﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    public string GetAddAtr(int[] ArrPropertyID, int propertyId, int pageIndex)
    {

        if (ArrPropertyID == null)
            return ViewPage.CurrentURL + "?atr=" + propertyId + (pageIndex > 0 ? "&page=" + pageIndex : string.Empty);

        var listId = new List<int>();
        listId.AddRange(ArrPropertyID);

        if (!listId.Contains(propertyId))
            listId.Add(propertyId);

        return ViewPage.CurrentURL + "?atr=" + string.Join("-", listId.ToArray()) + (pageIndex > 0 ? "&page=" + pageIndex : string.Empty);
    }

    public string GetRemoveAtr(int[] ArrPropertyID, int propertyId, int pageIndex)
    {


        var listId = new List<int>();
        listId.AddRange(ArrPropertyID);

        if (listId.Contains(propertyId))
            listId.Remove(propertyId);

        if (listId.Count == 0)
            return ViewPage.CurrentURL + (pageIndex > 0 ? "?page=" + pageIndex : string.Empty);

        return ViewPage.CurrentURL + "?atr=" + string.Join("-", listId.ToArray()) + (pageIndex > 0 ? "&page=" + pageIndex : string.Empty);
    }
</script>

<% 
    //  var listBrand = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage, true);
    var listChildPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);

    var listItem = ViewBag.Data as Dictionary<WebPropertyEntity, List<WebPropertyEntity>>;
    // if (listItem == null) return;

    var ArrPropertyID = ViewBag.ArrPropertyID;
    int _page = VSW.Core.Global.Convert.ToInt(VSW.Core.Web.HttpQueryString.GetValue("page"));
%>


<div class="col-md-3 col-left-find">
    <aside class="col-right-find">
        <div class="header-asir">
            <i class="fa fa-th-list"></i><%=ViewPage.CurrentPage.Name!="shop"? ViewPage.CurrentPage.Name: "Tất cả danh mục" %>
        </div>
        <ul class="list-menu-aside">
            <%for (int i = 0; listChildPage != null && i < listChildPage.Count; i++)
                { %>
            <li class="<%=ViewPage.IsPageActived(listChildPage[i])?"active":"" %>">
                <a href="<%=ViewPage.GetPageURL(listChildPage[i]) %>"><%=listChildPage[i].Name %>
                </a>
            </li>
            <%} %>
        </ul>
    </aside>
    <aside class="col-right-find">
        <div class="header-asir">
            <i class="fa fa-filter"></i>Bộ lọc tìm kiếm
        </div>
        <%if (listItem != null)
            { %>
        <form class="findter-form">
            <%foreach (var item in listItem.Keys)
                {
                    var listChildItem = listItem[item];
            %>
            <div class="box-find all">
                <p><%=item.Name %></p>

                <%for (var k = 0; listChildItem != null && k < listChildItem.Count; k++)
                    {%>

                <%if (ArrPropertyID != null && Array.IndexOf(ArrPropertyID, listChildItem[k].ID) > -1)
                    {%>
                <div class="form-group positionR">
                    <a onclick="location.href = '<%=GetRemoveAtr(ArrPropertyID, listChildItem[k].ID,_page) %>'" class="click-all"></a>
                    <div class="checkbox">
                        <input type="checkbox" name="prop-<%=item.ID %>" id="prop-<%=item.ID %>" checked="checked" onclick="location.href = '<%=GetRemoveAtr(ArrPropertyID, listChildItem[k].ID,_page) %>'">
                        <label for="prop-<%=item.ID %>"><%=listChildItem[k].Name %> (<%=listChildItem[k].Count %>)</label>
                    </div>
                </div>
                <%}
                    else
                    { %>
                <div class="form-group positionR">
                    <a onclick="location.href = '<%=GetAddAtr(ArrPropertyID, listChildItem[k].ID,_page) %>'" class="click-all"></a>
                    <div class="checkbox">
                        <input type="checkbox" name="prop-<%=item.ID %>" id="prop-<%=item.ID %>" onclick="location.href = '<%=GetAddAtr(ArrPropertyID, listChildItem[k].ID,_page) %>'">
                        <label for="prop-<%=item.ID %>"><%=listChildItem[k].Name %> (<%=listChildItem[k].Count %>)</label>
                    </div>
                </div>
                <%} %>
                <%} %>
            </div>

            <%} %>
        </form>
        <%} %>
    </aside>
</div>
