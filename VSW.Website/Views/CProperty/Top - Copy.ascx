﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">
    string GetAddAtr(string url, int propertyID, WebPropertyEntity select)
    {
        var _ArrPropertyID = _ArrID;

        if (_ArrPropertyID == null)
            return url + "?atr=" + propertyID;

        var listID = new List<int>();
        listID.AddRange(_ArrPropertyID);

        if (!listID.Contains(propertyID)) listID.Add(propertyID);

        if (select != null && listID.Contains(select.ID)) listID.Remove(select.ID);

        return url + "?atr=" + string.Join("-", listID.ToArray());
    }

    string GetRemoveAtr(string url, int propertyID)
    {
        var _ArrPropertyID = _ArrID;

        var listID = new List<int>();
        listID.AddRange(_ArrPropertyID);

        if (listID.Contains(propertyID)) listID.Remove(propertyID);

        if (listID.Count == 0)
            return url;

        return url + "?atr=" + string.Join("-", listID.ToArray());
    }

    List<SysPageEntity> listPage = null;
    Dictionary<WebPropertyEntity, List<WebPropertyEntity>> listItem = null;
    int[] _ArrID;
    protected void Page_Load(object sender, EventArgs e)
    {
        listItem = ViewBag.Data as  Dictionary<WebPropertyEntity, List<WebPropertyEntity>>;

        var _Atr = VSW.Core.Web.HttpQueryString.GetValue("atr").ToString();
        if (!string.IsNullOrEmpty(_Atr)) _ArrID = VSW.Core.Global.Array.ToInts(_Atr.Split('-'));

        listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);
        if (listPage == null) listPage = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
    }
</script>

<div id="category-filter-header" class="hide-for-large">
    <div class="c-title-breadcrumb has-back">
        <a href="javascript:history.back()" class="c-title-breadcrumb__back">
            <i class="o-linear-icon">arrowLeft</i>
            Back
        </a>
        <strong class="c-title-breadcrumb__title o-pseudo-h2"><%=ViewPage.CurrentPage.Name %></strong>
    </div>
    <div class="expanded gutter button-group margin-bottom-2 hide-for-medium">
        <button class="secondary button" type="button" data-toggle="mainNavOffcanvas" aria-label="Menu" aria-expanded="false" aria-controls="mainNavOffcanvas">
            Danh mục
        </button>
        <button class="secondary button" type="button" data-toggle="catFilterOffcanvas" aria-label="Filters" aria-expanded="false" aria-controls="catFilterOffcanvas">
            Lọc
        </button>
    </div>
</div>

<div class="off-canvas position-right is-light in-canvas-for-medium is-transition-overlap is-closed" id="catFilterOffcanvas" data-off-canvas="" aria-hidden="true">
    <section class="c-oc-fixed-for-small-only" data-oc-fixed="">
        <div class="hide-for-medium c-oc-fixed-for-small-only__item" data-oc-fixed-item="">
            <div class="c-oc-content c-oc-content--bordered c-oc-content--narrow">
                <button type="button" class="c-tiny-breadcrumb-close" data-close="">
                    <i class="o-linear-icon">cross2</i>
                    <span>Close</span>
                </button>
            </div>
            <div class="js-category-count c-oc-content-for-small-only c-oc-content-for-small-only--bordered">
                <div class="c-product-count">
                    <strong class="c-product-count__products">
                        <span id="product_count"><%=string.Format("{0:#,##0}", ViewPage.CurrentPage.Count) %></span> sản phẩm
                    </strong>
                </div>
            </div>
        </div>
        <div class="c-oc-fixed-for-small-only__body" data-oc-fixed-body="" data-off-canvas-scrollbox="">
            <div id="category-filter">
                <div class="c-oc-content-for-small-only c-oc-content-for-small-only--bordered">
                    <div id="category-filter-show-item-group" class="row small-up-1 medium-up-3 xmedium-up-4 gutter-decrease c-show-items-group-for-medium" data-show-items-group="">
                        <%
                            foreach (var item in listItem.Keys){
                                var listChildItem = listItem[item];

                                WebPropertyEntity select = null;
                                for (int k = 0; listChildItem != null && k < listChildItem.Count; k++)
                                {
                                    if (_ArrID != null && Array.IndexOf(_ArrID, listChildItem[k].ID) > -1)
                                    {
                                        select = listChildItem[k];
                                        break;
                                    }
                                }
                        %>
                        <div class="column column-block">
                            <div class="c-select-dropdown c-select-dropdown--tiny-for-medium">
                                <button type="button" class="c-select-dropdown__info-i" disabled="" data-toggle="filterInfoDropdown<%=item.ID %>"></button>
                                <button type="button" class="c-select-dropdown__toggle has-info-i" data-toggle="filterDropdown<%=item.ID %>" aria-controls="filterDropdown<%=item.ID %>" data-is-focus="false" data-yeti-box="filterDropdown<%=item.ID %>" aria-haspopup="true" aria-expanded="false">
                                    <span class="c-select-dropdown__text"><%=item.Name %></span>
                                </button>
                                <div class="c-select-dropdown__pane narrow dropdown-pane js-opener has-position-bottom has-alignment-left" id="filterDropdown<%=item.ID %>" data-dropdown="" data-options="closeOnClick:true; trapFocus:true;" data-dropdown-accordion="small only" aria-hidden="true" data-yeti-box="filterDropdown<%=item.ID %>" data-resize="filterDropdown<%=item.ID %>" aria-labelledby="voaohf-dd-anchor" data-t="7isn3o-t" data-events="resize">
                                    <div class="cb-list">
                                        <ul class="no-bullet faux-cb-filter-list">
                                            <%for (var k = 0; listChildItem != null && k < listChildItem.Count; k++){
                                                    var _Manufacturer = listChildItem[k].GetManufacturer();
                                            %>

                                            <%if (_ArrID != null && Array.IndexOf(_ArrID, listChildItem[k].ID) > -1){%>
                                            <li class="checked">
                                                <span class="float-right cb-list-item-count">44</span>
                                                <a href="<%=GetRemoveAtr(ViewPage.CurrentURL, listChildItem[k].ID) %>" class="cb-list-link"><%=select.Name %></a>
                                            </li>
                                            <%}else {%>
                                            <li>
                                                <span class="float-right cb-list-item-count">44</span>
                                                <a href="<%=GetAddAtr(ViewPage.CurrentURL, listChildItem[k].ID, select) %>" class="cb-list-link"><%=listChildItem[k].Name %></a>
                                            </li>
                                            <%} %>

                                            <%} %>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%} %>

                        <div class="column column-block is-toggle show-for-medium">
                            <button class="expanded tiny-select button c-plus-minus-button" type="button" data-show-items-group-toggle="">
                                <span class="c-plus-minus-button__plus">Mở rộng
                                    <i class="o-linear-icon">plus</i>
                                </span>
                                <span class="c-plus-minus-button__minus">Thu gọn
                                    <i class="o-linear-icon">minus</i>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <hr class="show-for-medium">
                <hr class="invisible show-for-small-only">
            </div>
        </div>
    </section>
</div>