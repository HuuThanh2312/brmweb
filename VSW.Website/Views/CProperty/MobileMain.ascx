﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<script runat="server">

    List<WebPropertyEntity> listItem = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        listItem = ViewBag.Data as List<WebPropertyEntity>;
    }
</script>

<% 
    if (listItem == null) return;
    if (ViewPage.CurrentModule.Code != "MProduct") return;
%>
<div class="filter_attributes all">
    <div class="title_filter">
        <h2><%=ViewPage.CurrentPage.Name %></h2>
        <p>(<strong><%=ModProductService.Instance.GetByPage(ViewPage.CurrentPage) %></strong> sản phẩm.)</p>
    </div>
    <div class="row_filter">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++)
          {
              var listChildItem = WebPropertyService.Instance.CreateQuery()
                                              .Where(o => o.ParentID == listItem[i].ID)
                                              .OrderByAsc(o => o.Order)
                                              .ToList();
        %>
        <p><%=listItem[i].Name%>:</p>
        <div class="cont-filter-attr">
            <ul class="other-role all">
                <%for (int j = 0; listChildItem != null && j < listChildItem.Count; j++)
                  {%>
                <li><a href="javascript:void(0)" id="<%=listChildItem[j].ID %>" rel="nofollow"><%=listChildItem[j].Name%></a></li>
                <%} %>
            </ul>
        </div>
        <%} %>
    </div>
</div>
