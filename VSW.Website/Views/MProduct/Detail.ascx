﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModProductEntity;
    var listItem = ViewBag.Other as List<ModProductEntity>;

    var listFile = item.GetFile();
    var model = ViewBag.Model as MProductModel;

    var listComment = ViewBag.Comment as List<ModCommentEntity>;
    // var listReview = ViewBag.Review as List<ModReviewEntity>;

    //var listShowroom = WebMenuService.Instance.CreateQuery()
    //                            .Where(o => o.Activity == true && o.Type == "Address" && o.ParentID > 0)
    //                            .OrderByAsc(o => o.Order)
    //                            .ToList_Cache();
%>



<div class="row">
    <div class="col-md-4 col-dt-01">
        <div class="box-img-thumb-pruduct all mb20">
            <div class="picture pd-picture">
                <div class="pdl-image zoom">
                    <a class="fancybox" data-fancybox="preview" href="<%=item.File.Replace("~/","/") %>">
                        <img src="<%=item.File.Replace("~/","/") %>" alt="<%=item.Name %>" data-bigimg="<%=item.File.Replace("~/","/") %>">
                    </a>
                </div>
                <div class="pdl-small-images">
                    <%for (int i = 0; listFile != null && i < listFile.Count; i++)
                        {
                    %>
                    <div class="item">
                        <a data-fancybox="preview" class="fancybox" href="<%=listFile[i].File.Replace("~/","/") %>" data-img="<%=listFile[i].File.Replace("~/","/") %>" data-bigimg="<%=listFile[i].File.Replace("~/","/") %>>">
                            <span class="responsive-img">
                                <img src="<%=listFile[i].File.Replace("~/","/") %>" alt="<%=item.Name %>" />
                            </span>
                        </a>
                    </div>
                    <%} %>
                </div>
                <div class="clearfix"></div>

                <div class="box-share-social">
                    <span>Chia sẻ tới:
                    </span>
                    <a href="" class="btn btn-social btn-facebook-share">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="" class="btn btn-social btn-google">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="" class="btn btn-social btn-tt">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="" class="btn btn-social btn-love-shop">
                        <i class="fa fa-heart"></i>&nbsp;Yêu Thích&nbsp;(120)
                    </a>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-8 col-dt-02 top-info-product">
        <div class="col-box">
            <h1 class="title-product"><%=item.Name %></h1>
            <%if (item.SellOffPercent > 0)
                { %>
            <div class="shopee-badge shopee-badge--fixed-width shopee-badge--promotion">
                <div class="shopee-badge--promotion__label-wrapper shopee-badge--promotion__label-wrapper--vi-VN"><span class="percent"><%=item.SellOffPercent %>%</span><span class="shopee-badge--promotion__label-wrapper__off-label shopee-badge--promotion__label-wrapper__off-label--vi-VN">giảm</span></div>
            </div>
            <%} %>
            <div class="col-prce-product">
                <span class="pr-sale">₫ <%=string.Format("{0:#,##0}", item.Price2) %> </span>- <span class="pr-lg">₫ <%=string.Format("{0:#,##0}", item.Price) %> </span>
            </div>
            <div class="clearfix"></div>



            <div class="danhgia-info">
                <div class="rate-star">
                    <input id="rating-input-sp" type="number" class="stars" value="<%=item.Star %>" /><input type="hidden" id="Star" value="" />
                    &nbsp;
                                       <span><%=string.Format("{0:#,##0}", item.Star) %> trên 5&nbsp;</span>
                </div>
                <span class="gray">(<%=item.Vote <1 ?"Chưa có ai":item.Vote.ToString() %> đánh giá)</span>
                <p class="text-color mt10">
                    <img src="/Content/skins/images/icon/xu.png" alt="" style="width: 25px">&nbsp;Mua hàng và tích 500 Xu
                </p>
            </div>

            <div class="vanchuyen-info gray">
                <i class="fa fa-truck blues"></i>&nbsp;<%=item.Price > 200000 ? "Miễn phí vận chuyển":"Tính phí vận chuyển" %>
                <div class="select-vanchuyen">
                    <span>Vận chuyển tới:&nbsp;</span>
                    <%-- <select name="CityID" class="chosen" ID="CityiD" onchange=""> 
                                        <%=Utils.ShowDdlMenuByType_NotParent("City", ViewPage.CurrentLang.ID, (model==null ? 0: model.CityID)) %>
                                       </select>--%>
                    <div class="_2wGlTU">
                        <div class="-TChkK">
                            <span class="_17013z" onclick="getDistrict_ghn();showCity();" id="name_cty" data-id="">Chọn nơi vận chuyển</span><i onclick="getDistrict_ghn();showCity();" style="margin-left: 5px" class="fa fa-chevron-down"></i>
                        </div>
                        <div class="shopee-address-picker" style="display: none;">
                            <div class="shopee-address-picker__search-bar">
                                <input placeholder="Tìm" value="" id="keyword-search">
                            </div>
                            <div class="shopee-address-picker__list-wrapper">
                                <ul class="shopee-address-picker__current-level-list" id="list_city">
                                </ul>
                                <ul class="shopee-address-picker__current-level-list" style="display: none!important;" id="list_district">
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="select-phivanchuyen">
                    <input type="hidden" value="<%=item.FromCityID %>" id="FromCityID_2" />
                    <input type="hidden" value="" id="ShippingPrice" />
                    <span>Phí vận chuyển:</span><span class="price-vanchuyen-detail"><span id="price-shiping">₫0&nbsp;</span><i class="fa fa-angle-down"></i>
                        <div class="hover-vanchuyen" id="show-fee-ghn">
                        </div>
                    </span>
                </div>
            </div>

            <div class="col-phanloai">
                <span class="title">Phân Loại:</span>
                <div class="list-thuoctinh-loaihang">
                    <%=item.PropertyHtml %>
                </div>
            </div>
            <div class="row mt10">
                <div class="col-md-2 pr0" style="line-height: 35px;">
                    Số lượng:
                </div>
                <div class="col-md-4">
                    <div class="input-group number-spinner" style="margin-left: 7px;">
                        <span class="input-group-btn">
                            <button class="btn btn-number-gr" data-dir="dwn"><i class="fa fa-minus"></i></button>
                        </span>
                        <input style="border-color: #d4d4d4; color: #d7381b;" type="text" id="Quantity" class="form-control text-center" value="1">
                        <span class="input-group-btn">
                            <button class="btn btn-number-gr" data-dir="up"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row mt20">
                <div class="col-md-12 button-buy">
                    <%--<button type="button" class="btn btn-chat-shop"><i class="fa fa-comments"></i>Chat Ngay</button>--%>
                    <button type="button" onclick="" class="btn btn-add-cart"><i class="fa fa-cart-plus"></i>Thêm Vào Giỏ Hàng</button>
                    <button type="button" data-url="<%=ViewPage.AddCartUrl %>" data-sid="<%=item.GetShop().ID %>" data-pid="<%=item.ID %>" data-returnpart="<%=ViewPage.GetURL(item.Code) %>" class="btn btn-buy-cart akr-add-cart-shop">Mua Ngay</button>
                </div>
            </div>
        </div>
        <div class="col-find-shop">
            <p><strong class="mb10">Tìm shop còn hàng gần bạn</strong></p>
            <div class="form-group">
                <select name="CityID" id="CityID" onchange="getDistrict(this.value),setTimeout(getShopProduct('<%=item.Model %>',this.value),5000)" class="form-control">
                    <option value="">Tỉnh/Thành phố</option>
                    <%=Utils.ShowDdlMenuByType1("City", ViewPage.CurrentLang.ID, model!=null? model.CityID :0) %>
                </select>
            </div>
            <div class="form-group">
                <select name="DistrictID" id="DistrictID" onchange="getShopProduct('<%=item.Model %>',this.value)" class="form-control">
                    <option value="">Chọn Quận/Huyện</option>
                </select>
            </div>
            <p class="mt10"><strong>Danh sách shop</strong></p>
            <ul class="list-shop-ganday" id="list-shop-ganday">
                <%if (item.GetShop() != null)
                    { %>
                <li>
                    <a href="<%=ViewPage.GetURL(item.GetShop())%>" class="click-all"></a>
                    <p><%=item.GetShop().Address +" - " + item.GetShop().GetDistrict().Name +" - " + item.GetShop().GetCity().Name%></p>
                    <p><span class="green">Còn <%=item.Inventory %> Sản phẩm</span></p>
                </li>
                <%} %>
            </ul>
        </div>
    </div>
</div>
<div class="product-page-seller-info card v-center mt20">
    <div class="product-page-seller-info__section-1">
        <div class="product-page-seller-info__header-wrapper">
            <a class="product-page-seller-info__header-portrait" href="<%=ViewPage.GetURL(item.GetShop()) %>">
                <div class="shopee-avatar">
                    <img class="shopee-avatar__img" src="<%=!string.IsNullOrEmpty(item.GetShop().Logo)? item.GetShop().Logo.Replace("~/","/"):string.Empty %>">
                </div>
            </a>
            <div class="product-page-seller-info__header-info">
                <a class="product-page-seller-info__name-status" href="<%=ViewPage.GetURL(item.GetShop()) %>">
                    <div class="product-page-seller-info__user-status">
                        <div class="product-page-seller-info__shop-name"><%=item.GetShop().Name %></div>
                        <div class="product-page-seller-info__active-time">Online <%=item.GetShop().OnlineTime %></div>
                    </div>
                </a>
                <div class="product-page-seller-info__buttons">
                    <a class="product-page-seller-info__view-shop" href="<%=ViewPage.GetURL(item.GetShop()) %>">
                        <button class="btn btn-light btn--s btn--inline">xem shop</button>
                    </a>
                    <button class="btn btn-light btn--s btn--inline" onclick="flowShop('<%=item.GetShop().ID %>')">theo dõi</button>
                </div>
            </div>
        </div>
    </div>
    <div class="product-page-seller-info__section-2">
        <div class="item-click-shop-info positionR">
            <a href="javascript:void(0)" class="click-all"></a>
            <div class="text-center">
                <i class="fa fa-dropbox"></i>&nbsp;<span><%=item.GetShop().CountProduct %></span>
                <br>
                Sản phẩm
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
        <div class="item-click-shop-info positionR">
            <a href="javascript:void(0)" class="click-all"></a>
            <div class="text-center">
                <i class="fa fa-star-o"></i>&nbsp;<span><%=item.GetShop().Vote %></span>
                <br>
                Đánh giá
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
        <div class="item-click-shop-info positionR">
            <a href="javascript:void(0)" class="click-all"></a>
            <div class="text-center">
                <i class="fa fa-comment"></i>&nbsp;<span>95%</span>
                <br>
                Tỷ lệ phản hồi
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
        <div class="item-click-shop-info positionR">
            <a href="javascript:void(0)" class="click-all"></a>
            <div class="text-center">
                <i class="fa fa-clock-o"></i>&nbsp;<span>Trong vài giờ</span>
                <br>
                Thời gian phản hồi
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
        <div class="item-click-shop-info positionR">
            <a href="javascript:void(0)" class="click-all"></a>
            <div class="text-center">
                <i class="fa fa-user-circle"></i>&nbsp;<span><%=item.GetShop().PublishedTime %></span><br>
                Tham gia
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
    </div>
</div>

<div class="content-detail-prd all">
    <div class="bx-content-detail">
        <h2>CHI TIẾT SẢN PHẨM</h2>
        <div class="body-post-detail">
            <div class="post-entry">
                <%=Utils.GetHtmlForSeo(item.Content)%>
            </div>
            <!-- /post entry- -->
        </div>
    </div>
    <%--<div class="bx-content-detail">
        <h2>Đánh giá</h2>
        <div class="body-post-detail">
            Update
        </div>
        <!-- /post entry- -->
    </div>--%>
    <div class="bx-content-detail">
        <h2>Bình luận</h2>
        <div class="body-post-detail">
            <div class="coment-detail">
                <ul id="list_comment">
                    <%for (int i = 0; listComment != null && i < listComment.Count; i++)
                        {
                            if (string.IsNullOrEmpty(listComment[i].Content)) continue;

                            string _avata = !string.IsNullOrEmpty(listComment[i].GetWebUser().File) ? Utils.GetResizeFile(listComment[i].GetWebUser().File, 4, 200, 200) : "";
                    %>
                    <li>
                        <div class="media">
                            <div class="media-left" style="width: 6%">
                                <img src="<%=_avata %>" class="media-object" style="width: 60px; border-radius: 100%; overflow: hidden;">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><%=listComment[i].Name %></h4>
                                <p><%=listComment[i].Content %></p>
                                <small class="gray"><%=listComment[i].PublishedTime %></small>
                            </div>
                        </div>
                        <hr>
                    </li>
                    <%} %>
                </ul>
                <div class="coment-enter">
                    <textarea name="Content" id="Content" class="form-control" rows="5" placeholder="nhập nội dung..."></textarea>
                    <div class="text-right mt10">
                        <a href="javascript:void(0)" onclick="<%=WebLogin.CurrentUser!=null ?"add_comment('0','"+item.ID +"','"+WebLogin.CurrentUser.LoginName+"',$('#Content').val());":"zebra_alert('Thông báo !', 'Bạn phải đăng nhập.');" %>" class="btn button-submit">Hoàn Thành</a>
                    </div>
                </div>
            </div>

        </div>
        <!-- /post entry- -->
    </div>
</div>
<script type="text/javascript" src="<%=Static.Tag("/Content/utils/fancybox/jquery.fancybox.js") %>"></script>
<script type="text/javascript">

    $("#keyword-search").on('keydown', function (e) {
        if (e.which == 13) {
            search_city($('#keyword-search').val());
        }
    });
    function search_city(keyword) {
        var list_district = $('.districtIDghn');
        var hshs = "";
        for (var i = 0; list_district != null && i < list_district.length; i++) {
            var haah = $(list_district[i]).text().toUpperCase();

            if (haah.includes(keyword.toUpperCase())) {
                hshs += '<li class="shopee-address-picker__current-level-list-item getFee_ghn"  data-districtid="' + $(list_district[i]).data('districtid') + '">' + $(list_district[i]).text() + '</li>';
            }
        }

        $('#list_city').html(hshs);
        $('.getFee_ghn').on('click', function () {
            var _id = $(this).data('districtid');
            var _text = $(this).text();

            document.getElementById("name_cty").textContent = _text;
            getFee_ghn(_id, '<%=item.FromCityID%>');
            $("#keyword-search").val("");
            showCity();

        });

    }
    function showCity() {

        $('.shopee-address-picker').slideToggle();
    }

    $(".fancybox").fancybox({
        animationEffect: "zoom-in-out",
        transitionEffect: "tube"
    });
</script>
