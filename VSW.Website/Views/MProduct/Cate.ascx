﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listPage = ViewBag.Cate as List<SysPageEntity>;
%>

<%for (int p = 0; listPage != null && p < listPage.Count; p++){%>
<div class="block-categories block-category-trend">
    <div class="categories-left">
        <div class="cate-slide">
            <div class="title-cate">
                <a href="<%=ViewPage.GetPageURL(listPage[p]) %>"><%=listPage[p].Name %><i class="fa fa-caret-right"></i></a>
            </div>
            <div class="img-big">
                <a class="hm-reponsive" href="">
                    <img src="uploads/pro02.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
            <div class="cate-tags">
                <a href="">Đầm ren</a>
                <a href="">Sơ mi nữ</a>
                <a href="">Áo thun</a>
                <a href="">Áo và chân váy</a>
            </div>
        </div>
        <div class="cate-big-img">
            <a class="hm-reponsive" href="">
                <img src="uploads/slide/cate1.1.png" alt="">
                <span>Quần tây công sở</span>
            </a>
        </div>
    </div>
    <div class="categories-right">
        <div class="box-collection">
            <div class="item">
                <a class="reponsive-img" href="">
                    <img src="uploads/pro05.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
            <div class="item">
                <a class="reponsive-img" href="">
                    <img src="uploads/pro06.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
            <div class="item">
                <a class="reponsive-img" href="">
                    <img src="uploads/pro01.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
            <div class="item">
                <a class="reponsive-img" href="">
                    <img src="uploads/pro04.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
            <div class="item">
                <a class="reponsive-img" href="">
                    <img src="uploads/pro07.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
            <div class="item">
                <a class="reponsive-img" href="">
                    <img src="uploads/pro02.jpg" alt="">
                    <span>Quần tây công sở</span>
                </a>
            </div>
        </div>
    </div>
</div>
<%} %>