﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var listPage = ViewBag.Page as List<SysPageEntity>;

%>

<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Trạng thái: <%=item.Activity?"Chính thức":"Chưa chính thức" %> </div>
                </div>
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle red btn-sm">Chi tiết hoa hồng</button>
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Trình quản lý</span>
                                <%-- <span class="selected"></span>--%>
                            </a>
                        </li>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
        </div>



        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thông tin tài khoản của tôi</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Thay đổi thông tin</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">Thay đổi thông tin chứng thực</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">Thay đổi mật khẩu</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form role="form" method="post" name="form_info" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Họ và tên</label>
                                                    <input type="text" placeholder="" id="Name" name="Name" value="<%=item.Name %>" class="form-control name_changer" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Chọn giới tính</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="Gender" id="gioitinh-prf1" value="1" <%if (item.Gender)
                                                                { %>
                                                                checked="checked" <%} %> />
                                                            Nam
                                                                                   
                                                                                <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="Gender" id="gioitinh-prf2" value="0" <%if (!item.Gender)
                                                                { %>
                                                                checked="checked" <%} %>>
                                                            Nữ
                                                                                   
                                                                                <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Tỉnh/Thành phố</label>
                                                    <select class="form-control" name="CityID" onchange="get_child(this.value,'<%=item.DistrictID %>','')">
                                                        <option value="0">Tỉnh/Thành Phố</option>
                                                        <%=Utils.ShowDdlMenuByType1("City", 1,item.CityID) %>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Quận huyện</label>
                                                    <select class="form-control" id="DistrictID" name="DistrictID" onchange="get_child(this.value,'<%=item.PhuongID %>','PhuongID')">
                                                        <option value="">Quận/Huyện</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Xã/Phường</label>
                                                    <select class="form-control" id="PhuongID" name="PhuongID" >
                                                        <option value="">-Xã/Phường-</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Địa chỉ hiện tại</label>
                                                    <input type="text" placeholder="" name="Address" value="<%=item.Address %>" class="form-control" />
                                                </div>

                                                <div class="col-md-6">
                                                    <label class="control-label">Ngày tháng năm sinh</label>
                                                    <input class="form-control form-control-inline date-picker" size="16" onfocus="this.type='date'" type="text" value="<%=string.Format("{0:dd/MM/yyyy}", item.BirthDay )%>" />
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label class="control-label">Avatar/Logo</label>
                                                    <input type="file" name="upload" onchange="previewFile()" class="form-control">
                                                    <p class="luu-y-avt">(Ảnh không được quá <span>5mb</span> và có phần mở rộng:<span>.gif, .jpg, .png</span>)</p>
                                                </div>
                                            </div>
                                        </div>

                                        <%--   <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Mã bảo mật <sup>(*)</sup></label>
                                                <div>
                                                    <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                                    <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                                        <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Nhập mã bảo mật</label>
                                                <input type="text" placeholder="Mã bảo mật" name="ValidCode" id="ValidCode" required="required" class="form-control" />
                                            </div>
                                        </div>--%>

                                        <div class="margiv-top-10 text-center">
                                            <button type="submit" style="margin-top: 10px" name="_vsw_action[InfoPost]" class="btn green">Cập nhật thông tin </button>
                                        </div>

                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_2">
                                    <form method="post" name="confirm_form" role="form">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Số CMND</label>
                                                    <input type="number" name="CodeCMND" value="<%=item.CodeCMND %>" class="form-control" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Tỉnh/thành Phố</label>
                                                    <select class=" form-control" name="CityCMND" onchange="get_child(this.value,'<%=item.DistrictCMND %>','CMND')">
                                                        <option value="0">Tỉnh/thành Phố</option>
                                                        <%=Utils.ShowDdlMenuByTypeCity("City",ViewPage.CurrentLang.ID, item.CityCMND) %>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Ngày cấp</label>
                                                    <input class="form-control form-control-inline date-picker" size="16" type="text" onfocus="this.type='date'" name="DateCMND" value="<%=string.Format("{0:dd/MM/yyyy}",item.DateCMND) %>" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Quận/Huyện</label>
                                                    <select class=" form-control" name="DistrictCMND" id="DistrictCMND">
                                                        <option value="0">Quận/Huyện</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">

                                                    <label class="control-label">Nơi cấp</label>
                                                    <select class=" form-control" name="CityCACMND">
                                                        <option value="0">Chọn nơi cấp</option>
                                                        <%=Utils.ShowDdlMenuByTypeCityCMND("City",ViewPage.CurrentLang.ID, item.CityCACMND) %>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Địa chỉ theo CMND</label>
                                                    <input class="form-control " type="text" name="AddressNo" value="<%=item.AddressNo %>" />

                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Email</label>
                                                    <input type="text" placeholder="" name="Email" value="<%=item.Email %>" onchange="check_acc(this.value,'email'); return false;" class="form-control" />
                                                    <p id="email" class="error"></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Số điện thoại</label>
                                                    <input type="text" placeholder="" value="<%=item.Phone %>" name="Phone" onchange="check_acc(this.value,'Phone'); return false;" class="form-control phone" />
                                                    <p id="Phone" class="error"></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label lbl-antoan">Mã bảo mật</label>
                                                    <input class="ma-an-toann " type="text" name="ValidCode" id="ValidCode" required="required" placeholder="Nhập mã bảo mật">
                                                    <span>
                                                        <img src="/ajax/Security.html" class="vAlignMiddle pl10" id="imgValidCode" alt="security code" />
                                                        <a href="javascript:void(0)" onclick="change_captcha()" title="Tạo mã khác" rel="nofollow">
                                                            <img src="/Content/skins/images/icon-refreh.png" alt="refresh security code" />
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="margin-top-10 text-center">
                                            <button name="_vsw_action[ConfirmPOST]" class="btn green">Cập nhật thông tin chứng thực</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_3">
                                    <form method="post">
                                        <div class="form-group">
                                            <label class="control-label">Mật khẩu hiện tại</label>
                                            <input type="password" class="form-control" name="Password" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Mật khẩu mới</label>
                                            <input type="password" class="form-control" name="Password1" required="required" id="Password1" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Nhập lại mật khẩu</label>
                                            <input type="password" class="form-control" name="Password2" onchange="check_pass(this.value,$('#Password1').val()); return false;" required="required" value="" />
                                        </div>
                                        <div class="margin-top-10 text-center">
                                            <button name="_vsw_action[PasswordPOST]" class="btn green">Thay đổi mật khẩu </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    function get_child2(ParentID, SelectedID, type) {
        var dataString = 'ParentID=' + ParentID + '&SelectedID=' + SelectedID;
        $.ajax({
            type: "get",
            url: "/ajax/GetChild.html",
            data: dataString,
            dataType: 'json',
            success: function (data) {
                var content = data.Html;

                if (content !== '' && type === '') {
                    $('#DistrictID').html(content);
                }
                if (content !== '' && type === 'CMND') {
                    $('#DistrictCMND').html(content);
                }
                if (content !== '' && type === 'PhuongID') {
                    $('#PhuongID').html(content);
                }
            },
            error: function (req) { }
        });
    }


    <%if (item.CityID > 0)
    {%>
    get_child2('<%=item.CityID%>','<%=item.DistrictID %>', '')
    <%}%>

     <%if (item.CityCMND > 0)
    {%>
    get_child2('<%=item.CityCMND%>','<%=item.DistrictCMND %>', 'CMND')
    <%}%>

     <%if (item.PhuongID > 0)
    {%>
    get_child2('<%=item.DistrictID%>','<%=item.PhuongID %>', 'PhuongID')
    <%}%>



    function previewFile() {
        var img = $('.profile-userpic img');
        var file = document.querySelector('input[type=file]').files[0];
        var reader = new FileReader();

        if (file) {
            reader.readAsDataURL(file);
        } else {
            img.attr('src', '');
        }

        reader.onloadend = function () {
            img.attr('src', reader.result);
        }
    }

</script>

