﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModShopEntity;
    var listOrder = ViewBag.Data as List<ModBusinessRevenueEntity>;

    var model = ViewBag.Model as MBusinessRevenueModel;
    double tongdoanhthu = ViewBag.Total;
%>

<%--<div class="row">
    <div class="col-md-12">
        <div class="header-tabs-doitac">
            <div class="tabs-find-doitac" style="width: 40%;">
                <ul class="nav-justified">
                     <li class="<%=(model.StartDate==DateTime.MinValue && model.EndDate==DateTime.MinValue)?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>">Tất cả</a>
                    </li>
                    <li class="<%=(model.StartDate==DateTime.Now.AddDays(1))?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>?StartDate=<%=string.Format("{0:MM-dd-yyyy}", DateTime.Now.AddDays(1)) %>">Sắp diễn ra</a>
                    </li>
                    <li class="<%=(model.StartDate==DateTime.Now.AddDays(-5))?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>?StartDate=<%=string.Format("{0:MM-dd-yyyy}", DateTime.Now.AddDays(-5)) %>">Đang diễn ra</a>
                    </li>
                    <li class="<%=model.EndDate>DateTime.MinValue&&model.StartDate==DateTime.MinValue ?"active":"" %>">
                        <a href="<%=ViewPage.BuninessMarketingCpUrl %>?EndDate=<%=string.Format("{0:MM-dd-yyyy}", DateTime.Now)%>">Đã kết thúc</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>--%>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark positionR">
                    <i class="fa fa-history"></i>
                    <span class="caption-subject bold uppercase">Lịch sử hoa hồng</span>
                    <%-- <div class="div-export-import-oder">
                        <div class="dropdown">
                            <button class="dropbtn"><i class="fa fa-file-text"></i>&nbsp;Tải danh sách và nhập hàng loạt</button>
                            <div class="dropdown-content">
                                <a href="#"><i class="fa fa-upload"></i>&nbsp;tải danh sách sản phẩm</a>
                            </div>
                        </div>
                    </div>--%>
                </div>
                <div class="tools"></div>
            </div>
            <div class="portlet-body">

                <div class="form-group input-find-hoahong-date">
                    <div class="input-group input-large <%--date-picker--%> input-daterange pull-left" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                        <input type="text" onfocus="(this.type='date')" class="form-control" name="from" id="start_date" value="<%=model.StartDate> DateTime.MinValue? string.Format("{0:yyyy-MM-dd}", model.StartDate)  :""%>" placeholder="Từ ngày">
                        <span class="input-group-addon">đến </span>
                        <input type="text" onfocus="(this.type='date')" class="form-control" name="to" id="end_date" value="<%=model.EndDate> DateTime.MinValue? string.Format("{0:yyyy-MM-dd}", model.EndDate)  :""%>" placeholder="Đến ngày">
                    </div>
                    <a href="javascript:;" onclick="time_order()" class="btn blue pull-left ml5"><i class="fa fa-filter"></i>&nbsp;Xem</a>
                </div>

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th class="center">#</th>
                            <th class="center" style="width: 40%;">Sản phẩm</th>
                            <th class="center">Mã đơn hàng</th>
                            <th class="center">Doanh thu</th>
                            <th class="center">Thao tác</th>
                        </tr>
                    </thead>

                    <tbody class="center">
                        <%for (int i = 0; listOrder != null && i < listOrder.Count; i++)
                            {
                                var _Product = ModProductService.Instance.GetByID(listOrder[i].ProductID);

                                string _File = _Product != null && !string.IsNullOrEmpty(_Product.File) ? Utils.GetResizeFile(_Product.File, 4, 500, 500) : "";
                                var _Order = ModOrderService.Instance.GetByID(listOrder[i].OrderID);
                                if (_Order == null) continue;
                        %>

                        <tr>
                            <td><%=i + 1 %></td>
                            <td>
                                <!-- Left-aligned media object -->
                                <div class="media">
                                    <div class="media-left">
                                        <img src="<%=_File %>" class="media-object" style="width: 60px">
                                    </div>
                                    <div class="media-body text-left">
                                        <h5><a href="<%=ViewPage.GetURL(_Product.MenuID, _Product.Code) %>"><%=_Product.Name %></a></h5>
                                        <p>Mã SP: <%=_Product.Model %></p>
                                    </div>
                                </div>
                            </td>
                            <td style="color: red; font-weight: 600;"><%=_Order.Code %></td>
                            <td><%=string.Format("{0:#,##0}", listOrder[i].Revenue) %></td>
                            <td><a href="<%=ViewPage.BuninessOrderCpUrl %>?id=<%=listOrder[i].OrderDetailID %>" title="Xem chi tiết đơn hàng" class="btn-edit-prd text-red">Chi tiết đơn hàng</a></td>
                        </tr>

                        <%} %>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th style="color: red;">Tổng doanh thu</th>
                        <th style="color: red; font-weight: 700; text-align: center; width: 50%"><%=string.Format("{0:#,##0}",tongdoanhthu) %> VND</th>
                    </tr>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<script type="text/javascript">
    function time_order() {
        var start_time = $('#start_date').val();
        var end_time = $('#end_date').val();

        location.href = '<%=ViewPage.CurrentURL%>' + '?start=' + start_time + '&end=' + end_time;
    }
</script>
