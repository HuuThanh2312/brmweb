﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var model = ViewBag.Model as MLoginModel;
%>

<h1 class="title-manage">Đăng nhập tài khoản</h1>
<div class="bgmanage-content">
    <form method="post" class="form-list login-form">
        <div class="account-profile col-lg-8 col-md-12 col-sm-12 col-12 fl">
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Email đăng nhập</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Email" placeholder="Nhập địa chỉ email của bạn" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Mật khẩu</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="password" class="form-control" name="Password" placeholder="Nhập mật khẩu từ 6 - 12 ký tự" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label></label>
                </div>
                <div class="text-left col-md-9">
                    <button type="submit" class="btn btn-blue" name="_vsw_action[AddPOST]">Đăng nhập</button>
                </div>
            </div>
        </div>
    </form>
</div>