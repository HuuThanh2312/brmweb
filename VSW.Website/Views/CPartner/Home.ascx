﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>




<section class="logo-doitac mb20">
    <div class="container">
        <h2 class="title-main">Đối tác <span><a href="">Xem thêm&nbsp;<i class="fa fa-angle-double-right"></i></a></span></h2>
        <div class="thuonghieu">
            <ul class="list-brank-prd list-owl-home">
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/276da74ade50ef58f48d741de3ace156" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/16546d77b4b66719b81c586c4ecc942f" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/ce318a54a4e470fbd4c307091ccb4332" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/4c3818c4f643afad94b6d4ec837a39dc" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/8185aa0de054cbfd722912d17b0fd0f2" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/eea73ea53596414eba41561b7bf6b50a" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/83051cf413992819a0a5465b5a6c5f9a" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/d52764c3f7ba424a795bea509a9cd8ac" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/738c7884db6c7e6d71588654ce1d2031" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/85361a32a0a6f26749fd5ad10ffe1a1e" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/dd9f864282f2954176dc475f2c8bfb97" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/5cb1dc7b4027a789519f118eed7bbd7a" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/eb96f0a6ec0d35f9339641e70b4d4872" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/185a940b15578e99d6f2519a5fbcbc89" alt="">
                        </a>
                    </div>
                </li>
                <li class="item">
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/f0bf7c637200d57c8d13f22bd1c2d106" alt="">
                        </a>
                    </div>
                    <div class="img-brank">
                        <a href="">
                            <img src="https://cf.shopee.vn/file/401c9d06096e080aa8e2596d0fcb5d1b" alt="">
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
