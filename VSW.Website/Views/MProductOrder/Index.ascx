﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModBSCarEntity>;
    var model = ViewBag.Model as MBSCarCPModel;

    var webUser = ViewBag.WebUser as ModWebUserEntity;
%>

<section class="main-cate main-manage clear">
    <div class="container">
        <div class="row">
            <div class="manage-left">
                <div class="profiles">
                    <p class="image">
                        <img src="<%=Utils.GetResizeFile(webUser.File, 1, 0, 0) %>" alt="<%=webUser.Name %>" />
                    </p>
                    <p class="name">Tài khoản của</p>
                    <h6><%=webUser.Name %></h6>
                </div>
                <ul class="menu-list">
                    <li class="item"><a href="<%=ViewPage.BSCarCPUrl %>" rel="nofollow"><i class="fa fa-car"></i>Quản lý tin bán xe</a></li>
                    <li class="item"><a href="<%=ViewPage.BSMarketCPUrl %>" rel="nofollow"><i class="fa fa-cogs"></i>Quản lý tin bán đồ</a></li>
                    <li class="item"><a href="<%=ViewPage.BSServiceCPUrl %>" rel="nofollow"><i class="fa fa-life-ring"></i>Quản lý tin dịch vụ</a></li>
                    <li class="item"><a href="<%=ViewPage.BSNewsCPUrl %>" rel="nofollow"><i class="fa fa-user-plus"></i>Quản lý tin Tuyển dụng</a></li>

                    <li class="item"><a href="<%=ViewPage.WebUserCPUrl %>" rel="nofollow"><i class="fa fa-user"></i>Chi tiết tài khoản</a></li>
                    <li class="item"><a href="" rel="nofollow"><i class="fa fa-money"></i>Quản lý Coin</a></li>
                    <li class="item"><a href="" rel="nofollow"><i class="fa fa-credit-card-alt"></i>Nâng cấp tài khoản</a></li>
                    <li class="item"><a href="<%=ViewPage.LogoutUrl %>" rel="nofollow"><i class="fa fa-sign-out"></i>Đăng xuất</a></li>
                </ul>
            </div>

            <div class="manage-right">
                <h1 class="title-manage">Danh sách tin đã đăng</h1>
                <div class="bgmanage-content">
                    <ul class="nav-tabs-shop all">
                        <li <%if (!VSW.Core.Web.HttpQueryString.Exists("activity") && !VSW.Core.Web.HttpQueryString.Exists("selled")) {%>class="active"<%} %>><a href="<%=ViewPage.CurrentURL %>">Tất cả</a></li>
                        <li <%if(VSW.Core.Web.HttpQueryString.Exists("activity") && model.activity) {%>class="active"<%} %>><a href="<%=ViewPage.BSCarCPUrl %>?activity=1">Đã duyệt<span class="txt-num">(<%=string.Format("{0:#,##0}", ViewBag.Activity) %>)</span></a></li>
                        <li <%if(VSW.Core.Web.HttpQueryString.Exists("activity") && !model.activity) {%>class="active"<%} %>><a href="<%=ViewPage.BSCarCPUrl %>?activity=0">Chờ duyệt<span class="txt-num">(<%=string.Format("{0:#,##0}", ViewBag.NotActivity) %>)</span></a></li>
                        <li <%if(VSW.Core.Web.HttpQueryString.Exists("selled") && model.selled) {%>class="active"<%} %>><a href="<%=ViewPage.BSCarCPUrl %>?selled=1">Đã bán<span class="txt-num">(<%=string.Format("{0:#,##0}", ViewBag.Selled) %>)</span></a></li>
                        <li <%if(VSW.Core.Web.HttpQueryString.Exists("selled") && !model.selled) {%>class="active"<%} %>><a href="<%=ViewPage.BSCarCPUrl %>?selled=0">Chưa bán<span class="txt-num">(<%=string.Format("{0:#,##0}", ViewBag.NotSelled) %>)</span></a></li>
                    </ul>
                    <div class="table-popcart all mb20 mt20">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Tin đăng</th>
                                    <th>Ngày đăng</th>
                                    <th>Lượt xem</th>
                                    <th>Số lượt up</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                                        string url = ViewPage.GetURL(listItem[i].Code);
                                %>
                                <tr>
                                    <td class="order-info">
                                        <div class=" all">
                                            <div class="img">
                                                <a href="<%=url %>">
                                                    <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 100, 100) %>" class="img-responsive" alt="<%=listItem[i].Name %>" />
                                                </a>
                                            </div>
                                            <div class="name">
                                                <a href="<%=url %>" class="name-product"><%=listItem[i].GetMenu().Parent.Name %> <%=listItem[i].GetMenu().Name %> - <%=listItem[i].Status ? "Xe mới" : "Xe cũ" %> - <%=listItem[i].YearID %></a>
                                                <div class="price ">
                                                    <strong><%=Utils.NumberToWordV3(listItem[i].Price.ToString()) %></strong>
                                                </div>
                                                <ul class="status">
                                                    <li class="status-pro"><%=listItem[i].Activity ? "Đã duyệt" : "Chưa duyệt" %></li>
                                                    <%--<li><a href="#" class="remove"><i class="fa fa-remove"></i>Xóa</a></li>--%>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <p><%=string.Format("{0:dd/MM/yyyy HH:mm}", listItem[i].Published) %></p>
                                    </td>
                                    <td class="text-center">
                                        <p><%=string.Format("{0:#,##0}", listItem[i].View) %></p>
                                    </td>
                                    <td class="text-center">
                                        <p><%=string.Format("{0:#,##0}", listItem[i].Count) %></p>
                                    </td>
                                    <td class="text-center akr-btn">
                                        <a href="javascript:void(0)" class="btn btn-primary akr-up" data-code="<%=ViewPage.CurrentPage.Code %>" data-id="<%=listItem[i].ID %>"><i class="fa fa-thumbs-up"></i>UP tin</a>
                                        
                                        <%if (listItem[i].Selled){%>
                                        <a href="javascript:void(0)" title="Click để chuyển trạng thái chưa bán" class="btn btn-primary akr-selled" data-code="<%=ViewPage.CurrentPage.Code %>" data-id="<%=listItem[i].ID %>"><i class="fa fa-check"></i>Xe đã bán</a>
                                        <%}else{%>
                                        <a href="javascript:void(0)" title="Click để chuyển trạng thái đã bán" class="btn btn-primary blue akr-selled" data-code="<%=ViewPage.CurrentPage.Code %>" data-id="<%=listItem[i].ID %>"><i class="fa fa-check"></i>Chưa bán</a>
                                        <%} %>

                                        <a href="<%=ViewPage.BSCarUPUrl %>?step=info&id=<%=listItem[i].ID %>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i>Sửa tin</a>
                                    </td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>