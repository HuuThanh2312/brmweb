﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    var _accBank = ModAccBcoinCardService.Instance.GetByUserID_Default(item.ID);
    var listBank = ViewBag.Data as ModAccBcoinCardEntity ?? new ModAccBcoinCardEntity();
    // var listTranctions = item.GetListTransaction();
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>


<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                </div>

                <%--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle red btn-sm">Chi tiết hoa hồng</button>
                </div>--%>

                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Trình quản lý</span>
                                <%-- <span class="selected"></span>--%>
                            </a>
                        </li>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>

            </div>

        </div>

        <div class="profile-content">
            <div class="row">
                <div class="col-md-6">

                    <div class="portlet light" style="min-height: 388px;">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Danh sách thẻ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <table class="table">
                                    <thead>
                                        <tr class="active">
                                            <th>Tên thẻ</th>
                                            <th>Số thẻ</th>
                                            <th>Số tài khoản thẻ</th>

                                        </tr>
                                    </thead>
                                    <tr>
                                        <td><%=listBank.Name %></td>
                                        <td><%=listBank.AccountBcoin %></td>
                                        <td><%=listBank.CardCode %></td>
                                    </tr>
                                </table>
                             <%--   <%if (listBank.ID < 1)
                                    { %>
                                <table class="table">
                                    <tr>
                                        <td>
                                            <div class="form-group text-center mt10">
                                                <button type="button" onclick="getHtml('Add')" class="btn blue btn-outline">Thêm</button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <%} %>--%>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Thay đổi mật khẩu thẻ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form method="post" accept-charset="utf-8">
                                <div class="form-group">
                                    <input type="text" name="AccountBcoin" class="form-control" value="<%=listBank.AccountBcoin %>" placeholder="" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="Password" id="Password" class="form-control" value="" placeholder="Mật khẩu hiện tại">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="Password1" id="Password1" class="form-control" value="" placeholder="Mật khẩu mới">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="Password2" id="Password2" class="form-control" value="" placeholder="Nhập lại mật khẩu mới">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="ValidCode" id="ValidCode" required="required" class="form-control" placeholder="Mã bảo mật(OTP)">
                                </div>
                                <div class="form-group text-center mt10">
                                    <button type="submit" name="_vsw_action[PasswordPost]" class="btn blue btn-outline">Cập nhật</button>
                                    <button type="button" onclick="clearText()" class="btn dark btn-outline">Nhập lại</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Quên mật khẩu thẻ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form method="post" accept-charset="utf-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <input type="text" name="Confirm" class="form-control" placeholder="Mã bảo mật">
                                        </div>
                                        <div class="col-xs-6">
                                            <img src="/ajax/Security.html" alt="" style="height: 35px;" id="imgValidCode" alt="security code" />
                                            <a href="javascript:void(0)" onclick="change_captcha()" class="btn btn-icon-only default">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <button type="submit" class="btn blue btn-outline">Gửi</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="note note-info">
                                    <p>
                                        <b>Bước 1: Nhập mã bảo mật website và ấn nút gửi<br>
                                            <br>
                                            Bước 2: Hệ thống sẽ gửi một tin nhắn mã số bảo mật tới điện thoại của bạn. Nhập chính xác mã số bảo mật điện thoại.<br>
                                            <br>
                                            Bước 3: Hệ thông kiểm tra mã số bảo mật điện thoại bạn nhập vào. Nếu đúng một mật khẩu thẻ mới sẽ được gửi vào số điện thoại trong gian hàng của bạn
                                        </b>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

