﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.User as ModWebUserEntity;
    string _avatar = !string.IsNullOrEmpty(item.File) ? item.File.Replace("~/", "/") : string.Empty;
    // var _accBank = item.GetBank();
    //var listBank = ViewBag.Data as List<ModBankingUserEntity>;
    // var listTranctions = item.GetListTransaction();
    var listPage = ViewBag.Page as List<SysPageEntity>;
%>


<div class="row mt20">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<%=_avatar %>" class="img-responsive" alt="<%=item.Name %>">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"><%=item.Name %></div>
                    <div class="profile-usertitle-job">Cấp độ: <%=item.Activity?"Chính thức":"" %> </div>
                </div>

                <%--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle red btn-sm">Chi tiết hoa hồng</button>
                </div>--%>

                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="nav-item start  <%=ViewPage.IsPageActived(ViewPage.CurrentPage) ? "active" :"" %>">
                            <a href="<%=ViewPage.WebUserCPUrl %>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Trình quản lý</span>
                                <%-- <span class="selected"></span>--%>
                            </a>
                        </li>
                        <%for (int i = 0; listPage != null && i < listPage.Count; i++)
                            { %>
                        <li class="<%=ViewPage.IsPageActived(listPage[i]) ? "active" :"" %>">
                            <a href="<%=ViewPage.GetPageURL(listPage[i]) %>"><i class="fa <%=listPage[i].Faicon %>"></i>&nbsp;<%=listPage[i].Name %></a>
                        </li>
                        <%} %>
                    </ul>
                </div>

            </div>

        </div>

        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PORTLET -->
                    <div class="portlet light" style="min-height: 388px;">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Mua thẻ</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable table-scrollable-borderless">
                                <form method="post" class="form-horizontal form-thanhtoan" accept-charset="utf-8">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Chọn loại dịch vụ:</label>
                                        <div class="col-md-8">
                                            <select name="TypeID" class="form-control" onchange="getCardOnline(this.value,'BrandID')">
                                                <option value="0">-- chọn dịch vụ --</option>
                                                <%=Utils.ShowDdlMenuByType2("CardOnline", ViewPage.CurrentLang.ID, 0)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">
                                            Chọn nhà cung cấp:</label>
                                        <div class="col-md-8">
                                            <select name="BrandID" id="BrandID" class="form-control" onchange="getCardOnline(this.value,'PriceID')">
                                                <option value="0">Nhà cung cấp</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Chọn mệnh giá:</label>
                                        <div class="col-md-8">
                                            <select name="PriceID" id="PriceID" class="form-control">
                                                <option value="0">Mệnh giá thẻ</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Số lượng thẻ:</label>
                                        <div class="col-md-8">
                                            <input type="number" name="Quantity" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Chọn hình thức thanh toán:</label>
                                        <div class="col-md-8">
                                            <select name="PaymentID" class="form-control">
                                                <option value="0">-- Hình thức thanh toán --</option>
                                                <%=Utils.ShowDdlMenuByType2("Payment", ViewPage.CurrentLang.ID, 0)%>
                                            </select>
                                        </div>
                                    </div>
                                    <%--   <div class="form-group">
                                        <label class="col-md-4 control-label">Mật khẩu tài khoản thẻ:</label>
                                        <div class="col-md-8">
                                            <input type="password" name="" class="form-control">
                                        </div>
                                    </div>--%>
                                    <div class="form-group text-center mt10">
                                        <button type="submit" class="btn blue btn-outline">Mua thẻ</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET -->
                </div>
            </div>
        </div>

    </div>

</div>
