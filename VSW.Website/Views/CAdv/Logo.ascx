﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModAdvEntity;
    if (item == null || string.IsNullOrEmpty(item.File)) return;
%>


<div class="logo">
    <%if (ViewPage.Request.RawUrl == "/")
        {%>
    <h1>
        <a href="/">
            <img src="<%=item.File.Replace("~/", "/") %>" alt="<%=item.Name %>">
        </a>
    </h1>
    <%}
        else
        {%>
    <a href="/">
        <img src="<%=item.File.Replace("~/", "/") %>" alt="<%=item.Name %>">
    </a>
    <%} %>
</div>
