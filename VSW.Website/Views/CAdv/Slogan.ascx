﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModAdvEntity;
    if (item == null || string.IsNullOrEmpty(item.File)) return;
%>

<div class="tagline cen">
    <%= Utils.GetCodeAdv(item)%>
</div>