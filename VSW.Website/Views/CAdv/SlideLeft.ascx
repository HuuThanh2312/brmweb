﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% var listItem = ViewBag.Data as List<ModAdvEntity>;%>



<div class="full-home-banners-lager">
    <div class="image-carousel positionR">
        <div id="akr_home">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++)
                {%>
            <div class="item">
                <%= Utils.GetCodeAdv(listItem[i])%>
            </div>
            <%} %>
        </div>
        <a href="javascript:void(0)" id="akr_home_prev" class="left carousel-control" rel="nofollow"><span class="icon-prev"><i class="fa fa-angle-left"></i></span></a>
        <a href="javascript:void(0)" id="akr_home_next" class="right carousel-control" rel="nofollow"><span class="icon-next"><i class="fa fa-angle-right"></i></span></a>
    </div>
</div>
