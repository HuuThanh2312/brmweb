﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% var listItem = ViewBag.Data as List<ModAdvEntity>;%>

<div class="block-footer clearfix">
    <h6 class="text-uppercase font-family-bold">Liên kết</h6>
    <ul class="list-unstyled">
        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
        <li><a href="<%=listItem[i].URL %>" title="<%=listItem[i].Name %>" rel="nofollow"><%=listItem[i].Name %></a></li>
        <%} %>
    </ul>
</div>