﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    if (ViewPage.CurrentModule.Code != "MProduct" || ViewPage.ViewBag.Data != null) return;
    var listItem = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ID);
    if (listItem != null) listItem.Insert(0, ViewPage.CurrentPage);

    if (listItem == null)
    {
        listItem = SysPageService.Instance.GetByParent_Cache(ViewPage.CurrentPage.ParentID);
        if (listItem != null) listItem.Insert(0, ViewPage.CurrentPage.Parent);
    }
%>

<div class="carousel slide">
    <div class="carousel slide">
        <div class="carousel-inner" id="akr_home">
            <%for (int i = 0; listItem != null && i < listItem.Count; i++){
                    if (string.IsNullOrEmpty(listItem[i].BannerBottom)) continue;
            %>
            <div class="item active">
                <a href="<%=ViewPage.GetPageUrl(listItem[i]) %>" rel="nofollow">
                    <%=Utils.GetResizeFile(listItem[i].BannerBottom, 1, 0, 0, listItem[i].Name) %>
                </a>
            </div>
            <%} %>
        </div>
        <a href="javascript:void(0)" id="akr_home_prev" class="left carousel-control" rel="nofollow"><span class="icon-prev"></span></a>
        <a href="javascript:void(0)" id="akr_home_next" class="right carousel-control" rel="nofollow"><span class="icon-next"></span></a>
    </div>
</div>