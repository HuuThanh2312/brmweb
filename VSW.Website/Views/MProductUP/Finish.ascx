﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var model = ViewBag.Model as MBSCarUPModel;

    var webUser = ViewBag.WebUser as ModWebUserEntity;

    var cookieBSCar = VSW.Lib.Global.Cookies.GetValue("BSCar", true);
    bool isCar = string.IsNullOrEmpty(cookieBSCar) || cookieBSCar == "car";
%>

<div class="banner-post">
    <div class="container">
        <div class="tittle-baner text-center ">
            Đăng tin bán <%=isCar ? "ô tô" : "xe máy" %>
        </div>
        <div class="row content-banner">
            <div class="col-md-4">
                <i class="iconltd-sell"></i>
                <p class="txt">Đăng tin qua 4 bước đơn giản</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-timkiem"></i>
                <p class="txt">Hàng triệu lượt tìm kiếm mỗi tháng</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-giaodich"></i>
                <p class="txt">Hàng ngàn giao dịch mỗi tháng</p>
            </div>
        </div>
    </div>
</div>
<div class="info-post">
    <div class="container">
        <div class="row">
            <ul class="list-info-post">
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=info<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">1. Thông tin xe</a></li>
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=price<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">2. Giá bán</a></li>
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=image<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">3. Ảnh chi tiết</a></li>
                <li class="active"><a href="<%=ViewPage.BSCarUPUrl %>?step=finish<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">4. Lưu ý</a></li>
            </ul>
            <div class="item-contentbanxe bg-note">
                <div class="col-md-12 text-center Expectedprice mb10">
                    <div class="bold mb10">Chúc mừng bạn đã hoàn thành</div>
                    <div class="lGray mb10">Mời bạn bắt đầu đăng bài, tham gia các hoạt động hội nhóm</div>
                </div>
                <div class="col_10_3">
                    <div class="user-post-list all">
                        <a href="#" rel="nofollow">
                            <div class="img-user-sm">
                                <img src="<%=Utils.GetResizeFile(webUser.File, 1, 0, 0) %>" class="lazyImg" alt="<%=webUser.Name %>" />
                            </div>
                            <span class="name"><%=webUser.Name %></span>
                            <span class="phone">Tel: <%=webUser.Phone %></span>
                            <span class="add">Add: <%=webUser.Address %>, <%=webUser.GetCity().Name %></span>
                        </a>
                    </div>
                </div>
                <div class="col_10_7 textbox-note">
                    <p><i class="fa fa-info-circle"></i>Lưu ý:</p>
                    <p class="pl15">Kiểu thành viên <b>Cá nhân</b> chỉ được đăng tải 1 tin/ngày.</p>
                    <p class="pl15">Nếu bạn muốn đăng tải nhiều tin / Ngày thì phải chuyển đổi </p>
                    <p class="pl15">Kiểu thành viên: Cá nhân --> Kiểu thành viên VIP</p>
                    <p><i class="fa fa-star-o"></i>Nâng cấp tài khoản VIP ngay tại đây! <a href="#" class="btn btn-danger btn-sm" rel="nofollow">Nâng Cấp</a></p>
                    <p class="hotline">
                        <i class="fa fa-phone-square"></i>Hỗ trợ viên: 0904.064.064
                    </p>
                </div>
                <div class="clear"></div>
                <div class="text-right"><a href="<%=ViewPage.BSCarCPUrl %>" class="btn btn-danger" rel="nofollow">Vào trang quản lý</a></div>
            </div>
        </div>
    </div>
</div>
