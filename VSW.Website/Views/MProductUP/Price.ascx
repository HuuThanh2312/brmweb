﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModBSCarEntity;
    var model = ViewBag.Model as MBSCarUPModel;
    
    var cookieBSCar = VSW.Lib.Global.Cookies.GetValue("BSCar", true);
    bool isCar = string.IsNullOrEmpty(cookieBSCar) || cookieBSCar == "car";
%>

<div class="banner-post">
    <div class="container">
        <div class="tittle-baner text-center ">
            Đăng tin bán <%=isCar ? "ô tô" : "xe máy" %>
        </div>
        <div class="row content-banner">
            <div class="col-md-4">
                <i class="iconltd-sell"></i>
                <p class="txt">Đăng tin qua 4 bước đơn giản</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-timkiem"></i>
                <p class="txt">Hàng triệu lượt tìm kiếm mỗi tháng</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-giaodich"></i>
                <p class="txt">Hàng ngàn giao dịch mỗi tháng</p>
            </div>
        </div>
    </div>
</div>
<div class="info-post">
    <div class="container">
        <div class="row">
            <ul class="list-info-post">
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=info<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">1. Thông tin xe</a></li>
                <li class="active"><a href="<%=ViewPage.BSCarUPUrl %>?step=price<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">2. Giá bán</a></li>
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=image<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">3. Ảnh chi tiết</a></li>
                <li><a href="#" rel="nofollow">4. Lưu ý</a></li>
            </ul>
            <div class="item-contentbanxe">
                <form method="post" name="price_form">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-center Setprice">
                                <input type="text" class="form-control" name="Price" value="<%=item.Price %>" onkeyup="$(this).parent().find('.akr-over').html(formatDollar(this.value))" placeholder="Nhập giá bán bằng số, viết liền. VD: 1000000000" />
                                <div class="akr-over right30"><%=string.Format("{0:#,##0}", item.Price) %></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center Expectedprice">
                            <p class="bold">Giá đề xuất <span class="attention"><%=string.Format("{0:#,##0}", item.AveragePrice) %></span></p>
                            <p>(Dựa trên phân tích trên 11 chiếc xe <%=item.GetMenu().Name %> tương tự được bày bán trên <%=VSW.Core.Web.HttpRequest.Host %>)</p>
                            <ul class="list-price ">
                                <li></li>
                                <li></li>
                                <li style="left: <%=item.Percent%>%"></li>
                                <li></li>
                            </ul>
                            <div class="content-price">
                                <span class="minpr">Giá bán thấp nhất<br /><%=string.Format("{0:#,##0}", item.MinPrice) %></span>
                                <span class="mudiumpr">Giá bán trung bình<br /><%=string.Format("{0:#,##0}", item.AveragePrice) %></span>
                                <span class="maxpr">Giá bán cao nhất<br /><%=string.Format("{0:#,##0}", item.MaxPrice) %></span>
                            </div>
                            <div class="mb20">* Giá bán đề xuất chỉ mang tính tham khảo. Giá trị có thể thay đổi theo chất lượng và tình trạng của chiếc xe</div>
                        </div>
                    </div>
                    <div class="text-right"><a href="javascript:void(0)" onclick="document.price_form.submit()" class="btn btn-danger">Tiếp tục</a></div>
                    <input type="hidden" name="_vsw_action[PricePOST]" />
                    <input type="submit" name="_vsw_action[PricePOST]" style="display: none" />

                </form>
            </div>
        </div>
    </div>
</div>
