﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModBSCarEntity;
    var model = ViewBag.Model as MBSCarUPModel;

    var listItem = SysPageService.Instance.GetByParent_Cache(9);

    var cookieBSCar = VSW.Lib.Global.Cookies.GetValue("BSCar", true);
    bool isCar = string.IsNullOrEmpty(cookieBSCar) || cookieBSCar == "car";

    //image
    var arrFileOverView = !string.IsNullOrEmpty(model.FileOverView) ? model.FileOverView.Split('|') : new string[] { };
    var arrFileFurniture = !string.IsNullOrEmpty(model.FileFurniture) ? model.FileFurniture.Split('|') : new string[] { };
    var arrFileEngine = !string.IsNullOrEmpty(model.FileEngine) ? model.FileEngine.Split('|') : new string[] { };
    var arrFileExhibit = !string.IsNullOrEmpty(model.FileExhibit) ? model.FileExhibit.Split('|') : new string[] { };
    var arrFileKey = !string.IsNullOrEmpty(model.FileKey) ? model.FileKey.Split('|') : new string[] { };
%>

<div class="banner-post">
    <div class="container">
        <div class="tittle-baner text-center ">
            Đăng tin bán <%=isCar ? "ô tô" : "xe máy" %>
        </div>
        <div class="row content-banner">
            <div class="col-md-4">
                <i class="iconltd-sell"></i>
                <p class="txt">Đăng tin qua 4 bước đơn giản</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-timkiem"></i>
                <p class="txt">Hàng triệu lượt tìm kiếm mỗi tháng</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-giaodich"></i>
                <p class="txt">Hàng ngàn giao dịch mỗi tháng</p>
            </div>
        </div>
    </div>
</div>
<div class="info-post">
    <div class="container">
        <div class="row">
            <ul class="list-info-post">
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=info<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">1. Thông tin xe</a></li>
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=price<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">2. Giá bán</a></li>
                <li class="active"><a href="<%=ViewPage.BSCarUPUrl %>?step=image<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">3. Ảnh chi tiết</a></li>
                <li><a href="#" rel="nofollow">4. Lưu ý</a></li>
            </ul>
            <div class="item-contentbanxe post-img clear">
                <form method="post" name="image_form" enctype="multipart/form-data">

                    <input type="hidden" name="FileOverView" id="FileOverView" value="<%=model.FileOverView %>" />
                    <input type="hidden" name="FileFurniture" id="FileFurniture" value="<%=model.FileFurniture %>" />
                    <input type="hidden" name="FileEngine" id="FileEngine" value="<%=model.FileEngine %>" />
                    <input type="hidden" name="FileExhibit" id="FileExhibit" value="<%=model.FileExhibit %>" />
                    <input type="hidden" name="FileKey" id="FileKey" value="<%=model.FileKey %>" />

                    <div class="col-md-6">
                        <div class="summary">
                            <p>
                                
                            </p>
                        </div>
                        <div class="txt-pic-big">* Thêm hình ảnh vào các phần dưới <span class="required">(Yêu cầu ảnh)</span></div>
                        <div class="row-pic clear">
                            <div class="txt-pdl-picture">
                                Chi tiết bên ngoài xe <span>(Đầu xe, đuôi, đèn, cánh cửa..)</span>
                            </div>
                            <div class="all list_uploadimg">
                                <%for (int i = 0; i < arrFileOverView.Length; i++){%>
                                <div class="item-uploadimg">
                                    <input type="file" class="upload-file" data-id="FileOverView_<%=i %>" name="FileOverView" title="<%=File.GetFileName(arrFileOverView[i]) %>" accept="image/*" />
                                    <div class="upload-item-single">
                                        <img src="<%=Utils.GetResizeFile(arrFileOverView[i], 2, 83, 60) %>" class="FileOverView_Preview_<%=i %>" />
                                    </div>
                                </div>
                                <%} %>

                                <%for (int i = arrFileOverView.Length; i < 12; i++) {%>
                                <div class="item-uploadimg">
                                    <input type="file" class="upload-file" data-id="FileOverView_<%=i %>" name="FileOverView" accept="image/*" />
                                    <div class="upload-item-single">
                                        <img src="/Content/skins/images/1-upload.png" class="FileOverView_Preview_<%=i %>" />
                                    </div>
                                </div>
                                <%} %>
                            </div>
                            <p class="note clear">Tối thiếu 4 ảnh</p>
                        </div>
                        <div class="row-pic clear">
                            <div class="txt-pdl-picture">
                                Chi tiết nội thất bên trong xe<span>(Taplo, ghế, Option...)</span>
                            </div>
                            <div class="all list_uploadimg">
                                <%for (int i = 0; i < arrFileFurniture.Length; i++){%>
                                <div class="item-uploadimg">
                                    <input type="file" class="upload-file" data-id="FileFurniture_<%=i %>" name="FileFurniture" title="<%=File.GetFileName(arrFileFurniture[i]) %>" accept="image/*" />
                                    <div class="upload-item-single">
                                        <img src="<%=Utils.GetResizeFile(arrFileFurniture[i], 2, 83, 60) %>" class="FileFurniture_Preview_<%=i %>" />
                                    </div>
                                </div>
                                <%} %>

                                <%for (int i = arrFileFurniture.Length; i < 12; i++) {%>
                                <div class="item-uploadimg">
                                    <input type="file" class="upload-file" data-id="FileFurniture_<%=i %>" name="FileFurniture" accept="image/*" />
                                    <div class="upload-item-single">
                                        <img src="/Content/skins/images/1-upload.png" class="FileFurniture_Preview_<%=i %>" />
                                    </div>
                                </div>
                                <%} %>
                            </div>
                            <p class="note clear">Tối thiếu 4 ảnh</p>
                        </div>
                        <div class="row-pic clear">
                            <div class="col_6_2">
                                <div class="txt-pdl-picture">Máy &amp; Hộp số</div>
                                <div class="list_uploadimg list_uploadimg_col2 all">
                                    <%for (int i = 0; i < arrFileEngine.Length; i++){%>
                                    <div class="item-uploadimg">
                                        <input type="file" class="upload-file" data-id="FileEngine_<%=i %>" name="FileEngine" title="<%=File.GetFileName(arrFileEngine[i]) %>" accept="image/*" />
                                        <div class="upload-item-single">
                                            <img src="<%=Utils.GetResizeFile(arrFileEngine[i], 2, 83, 60) %>" class="FileEngine_Preview_<%=i %>" />
                                        </div>
                                    </div>
                                    <%} %>

                                    <%for (int i = arrFileEngine.Length; i < 4; i++) {%>
                                    <div class="item-uploadimg">
                                        <input type="file" class="upload-file" data-id="FileEngine_<%=i %>" name="FileEngine" accept="image/*" />
                                        <div class="upload-item-single">
                                            <img src="/Content/skins/images/1-upload.png" class="FileEngine_Preview_<%=i %>" />
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                                <p class="note clear">Tối thiếu 2 ảnh</p>
                            </div>
                            <div class="col_6_3">
                                <div class="txt-pdl-picture">Giấy tờ pháp lý</div>
                                <div class="list_uploadimg list_uploadimg_col3 all">
                                    <%for (int i = 0; i < arrFileExhibit.Length; i++){%>
                                    <div class="item-uploadimg">
                                        <input type="file" class="upload-file" data-id="FileExhibit_<%=i %>" name="FileExhibit" title="<%=File.GetFileName(arrFileExhibit[i]) %>" accept="image/*" />
                                        <div class="upload-item-single">
                                            <img src="<%=Utils.GetResizeFile(arrFileExhibit[i], 2, 83, 60) %>" class="FileExhibit_Preview_<%=i %>" />
                                        </div>
                                    </div>
                                    <%} %>

                                    <%for (int i = arrFileExhibit.Length; i < 6; i++) {%>
                                    <div class="item-uploadimg">
                                        <input type="file" class="upload-file" data-id="FileExhibit_<%=i %>" name="FileExhibit" accept="image/*" />
                                        <div class="upload-item-single">
                                            <img src="/Content/skins/images/1-upload.png" class="FileExhibit_Preview_<%=i %>" />
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                                <p class="note clear">Tối thiếu 2 ảnh</p>
                            </div>
                            <div class="col_6_1">
                                <div class="txt-pdl-picture">Chìa khóa</div>
                                <div class="list_uploadimg list_uploadimg_col1 all">
                                    <%for (int i = 0; i < arrFileKey.Length; i++){%>
                                    <div class="item-uploadimg">
                                        <input type="file" class="upload-file" data-id="FileKey_<%=i %>" name="FileKey" title="<%=File.GetFileName(arrFileKey[i]) %>" accept="image/*" />
                                        <div class="upload-item-single">
                                            <img src="<%=Utils.GetResizeFile(arrFileKey[i], 2, 83, 60) %>" class="FileKey_Preview_<%=i %>" />
                                        </div>
                                    </div>
                                    <%} %>

                                    <%for (int i = arrFileKey.Length; i < 2; i++) {%>
                                    <div class="item-uploadimg">
                                        <input type="file" class="upload-file" id="FileKey_<%=i %>" name="FileKey" accept="image/*" />
                                        <div class="upload-item-single">
                                            <img src="/Content/skins/images/1-upload.png" class="FileKey_Preview_<%=i %>" />
                                        </div>
                                    </div>
                                    <%} %>
                                </div>
                                <p class="note clear"></p>
                            </div>
                        </div>
                        <div class="clear">
                            <img src="/Content/skins/images/vvhotline.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pd-video all mb20">
                            <div class="title-video">
                                Video hướng dẫn
                            </div>
                            <div class="content-video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/ZSf2SOOU-1A?controls=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                            </div>
                        </div>
                        <p class="note clear">(*) Các thủ thuật cực hay khi chụp hình và quay video clip sản phẩm</p>
                        <div class="text-center">
                            <img src="skins/images/YYi2Rli.gif" alt=""></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><i class="fa fa-youtube-square"></i>Đăng video sản phẩm</label>
                                    <div class="control-div ">
                                        <input type="text" class="form-control" name="Video" value="<%=item.Video %>" placeholder="Bạn hãy nhập link video youtube vào đây." />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="help-post all mb30">
                            <div class="title">Bạn đang loay hoay cách đăng video ???</div>
                            <p>1. Upload Video của bạn trong điện thoại lên YouTube</p>
                            <p>2. Copy đường link của video vừa upload xong và dán vào trang là ok</p>
                        </div>
                        <div class="pd-video all mb20">
                            <div class="title-video">
                                Video hướng dẫn
                            </div>
                            <div class="content-video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/ShzyJjafecw" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <p class="note clear">(*) Video hướng dẫn cách đưa video lên youtube và copy link video</p>
                    </div>
                    <div class="text-right"><a href="javascript:void(0)" onclick="document.image_form.submit()" class="btn btn-danger">Tiếp tục</a></div>
                    <input type="hidden" name="_vsw_action[ImagePOST]" />
                    <input type="submit" name="_vsw_action[ImagePOST]" style="display: none" />

                </form>
            </div>
        </div>
    </div>
</div>
