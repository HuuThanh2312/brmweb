﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModBSCarEntity;
    var model = ViewBag.Model as MBSCarUPModel;

    var cookieBSCar = VSW.Lib.Global.Cookies.GetValue("BSCar", true);
    bool isCar = string.IsNullOrEmpty(cookieBSCar) || cookieBSCar == "car";

    List<WebMenuEntity> listItem = null;
    if (isCar)
        listItem = WebMenuService.Instance.GetByParentID_Cache(9);
    else
        listItem = WebMenuService.Instance.GetByParentID_Cache(10);
%>

<div class="banner-post">
    <div class="container">
        <div class="tittle-baner text-center ">
            Đăng tin bán <%=isCar ? "ô tô" : "xe máy" %>
        </div>
        <div class="row content-banner">
            <div class="col-md-4">
                <i class="iconltd-sell"></i>
                <p class="txt">Đăng tin qua 4 bước đơn giản</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-timkiem"></i>
                <p class="txt">Hàng triệu lượt tìm kiếm mỗi tháng</p>
            </div>
            <div class="col-md-4">
                <i class="iconltd-giaodich"></i>
                <p class="txt">Hàng ngàn giao dịch mỗi tháng</p>
            </div>
        </div>
    </div>
</div>
<div class="info-post">
    <div class="container">
        <div class="row">
            <ul class="list-info-post">
                <li class="active"><a href="<%=ViewPage.BSCarUPUrl %>?step=info<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">1. Thông tin xe</a></li>
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=price<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">2. Giá bán</a></li>
                <li><a href="<%=ViewPage.BSCarUPUrl %>?step=image<%=model.id > 0 ? "&id=" + model.id : "" %>" rel="nofollow">3. Ảnh chi tiết</a></li>
                <li><a href="#" rel="nofollow">4. Lưu ý</a></li>
            </ul>
            <div class="item-contentbanxe">
                <form method="post" name="info_form">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tình trạng xe</label>
                                <div class="control-div ">
                                    <label class="radioPure">
                                        <input type="radio" name="status" <%if (item.Status){%> checked="checked"<%} %> value="1">
                                        <span class="outer"><span class="inner"></span></span><i>Xe mới</i>
                                    </label>
                                    <label class="radioPure">
                                        <input type="radio" name="status" <%if (!item.Status){%> checked="checked"<%} %> value="0">
                                        <span class="outer"><span class="inner"></span></span><i>Xe cũ</i>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Hãng sản xuất</label>
                                <div class="control-div ">
                                    <select class="form-control chosen-select" id="menu" onchange="getChildPage(this.value, '<%=item.MenuID %>');">
                                        <option value="0">Hãng sản xuất</option>
                                        <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                                        <option value="<%=listItem[i].ID %>" data-code="<%=listItem[i].Code %>" <%if (item.GetMenu().ParentID == listItem[i].ID){%>selected="selected"<%} %>><%=listItem[i].Name %></option>
                                        <%} %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tên xe</label>
                                <div class="control-div ">
                                    <select class="form-control chosen-select" name="MenuID" id="menu2">
                                        <option value="0">Tên xe</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Muốn chào bán tại</label>
                                <div class="control-div ">
                                    <select name="CityID" class="form-control chosen-select">
                                        <option value="0">Chọn tỉnh / thành phố</option>
                                        <%=Utils.ShowDdlMenuByType2("City", ViewPage.CurrentLang.ID, item.CityID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Năm sản xuất</label>
                                <div class="control-div ">
                                    <select name="YearID" class="form-control chosen-select">
                                        <option value="0">Năm sản xuất</option>
                                        <%for (int i = DateTime.Now.Year - 100; i <= DateTime.Now.Year; i++){%>
                                        <option value="<%=i %>" <%if (item.YearID == i){%>selected="selected"<%} %>><%=i %></option>
                                        <%} %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Xuất xứ</label>
                                <div class="control-div ">
                                    <select name="MadeInID" class="form-control chosen-select">
                                        <option value="0">Xuất xứ xe</option>
                                        <%=Utils.ShowDdlMenuByType2("MadeIn", ViewPage.CurrentLang.ID, item.MadeInID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Biển số xe(BKS)</label>
                                <div class="control-div ">
                                    <input type="text" class="form-control" name="License" value="<%=item.License %>" placeholder="Ví dụ: 29A-6666(Gõ liền không dấu)" />
                                </div>
                            </div>
                        </div>

                        <%if (isCar){%>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Hộp số</label>
                                <div class="control-div ">
                                    <select name="GearID" class="form-control chosen-select">
                                        <option value="0">Hộp số</option>
                                        <%=Utils.ShowDdlMenuByType2("Gear", ViewPage.CurrentLang.ID, item.GearID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Số chỗ ngồi</label>
                                <div class="control-div ">
                                    <select name="SeatID" class="form-control chosen-select">
                                        <option value="0">Số chỗ ngồi</option>
                                        <%=Utils.ShowDdlMenuByType2("Seat", ViewPage.CurrentLang.ID, item.SeatID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <%}else{%>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Dung tích</label>
                                <div class="control-div ">
                                    <select name="CapacityID" class="form-control chosen-select">
                                        <option value="0">Dung tích</option>
                                        <%=Utils.ShowDdlMenuByType2("Capacity", ViewPage.CurrentLang.ID, item.CapacityID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <%} %>
                    </div>

                    <%if (isCar){%>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Kiểu xe</label>
                                <div class="control-div ">
                                    <select name="DesignID" class="form-control chosen-select">
                                        <option value="0">Kiểu xe</option>
                                        <%=Utils.ShowDdlMenuByType2("Design", ViewPage.CurrentLang.ID, item.DesignID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Nhiên liệu</label>
                                <div class="control-div ">
                                    <select name="FuelID" class="form-control chosen-select">
                                        <option value="0">Nhiên liệu</option>
                                        <%=Utils.ShowDdlMenuByType2("Fuel", ViewPage.CurrentLang.ID, item.FuelID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Kiểu dẫn động</label>
                                <div class="control-div ">
                                    <select name="DrivingID" class="form-control chosen-select">
                                        <option value="0">Kiểu dẫn động</option>
                                        <%=Utils.ShowDdlMenuByType2("Driving", ViewPage.CurrentLang.ID, item.DrivingID) %>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tiêu đề tin đăng</label>
                                <div class="control-div ">
                                    <input type="text" class="form-control" name="Name" value="<%=item.Name %>" placeholder="Ví dụ: Cần bán xe Camry 2.5. Máy xăng, số tự động, đã đi 8,230 km" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nội dung bán xe</label>
                                <div class="control-div ">
                                    <textarea class="ckeditor form-control" name="Content" rows="" cols=""><%=item.Content %></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right"><a href="javascript:void(0)" onclick="document.info_form.submit()" class="btn btn-danger">Tiếp tục</a></div>
                    <input type="hidden" name="_vsw_action[InfoPOST]" />
                    <input type="submit" name="_vsw_action[InfoPOST]" style="display: none" />

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<%=Static.Tag("/Content/utils/ckeditor/ckeditor.js") %>"></script>
<script type="text/javascript" src="<%=Static.Tag("/Content/utils/ckfinder/ckfinder.js") %>"></script>
<script type="text/javascript">
    CKFinder.setupCKEditor(null, { basePath: '/Content/utils/ckfinder/', rememberLastFolder: true });
    var path = '<%=WebLogin.CurrentUser.Username%>';

    $(function () {
        getChildPage($('#menu').val(), '<%=item.MenuID%>');
    });
</script>