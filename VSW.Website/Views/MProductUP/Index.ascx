﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var item = ViewBag.Data as ModProductEntity;
    var model = ViewBag.Model as MProductModel;

    var listProductFile = item.GetFile();
    if (listProductFile.Count < 5)
    {
        for (int i = 0; i < 5 - listProductFile.Count; i++)
        {
            listProductFile.Add(new ModProductFileEntity()
            {
                ProductID = item.ID,
                File = "~/Content/skins/images/img.png"
            });
        }
    }
%>

<h1 class="title-manage">Thông tin tài khoản</h1>
<div class="bgmanage-content">
    <form method="post" class="form-list" name="product_up" enctype="multipart/form-data">
        <div class="account-profile col-lg-12 col-md-12 col-sm-12 col-12 fl">
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Tên sản phẩm</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Name" value="<%=item.Name %>" placeholder="Nhập tên sản phẩm" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Giá bán</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Price" value="<%=item.Price %>" placeholder="Nhập giá sản phẩm" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Giá niêm yết (gạch ngang)</label>
                </div>
                <div class="text-left col-md-9">
                    <input type="text" class="form-control" name="Price2" value="<%=item.Price2 %>" placeholder="Nhập giá sản phẩm" />
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Hình ảnh</label>
                </div>
                <div class="text-left col-md-9">
                    <ul class="post-img">
                        <%for (int i = 0; listProductFile != null && i < listProductFile.Count; i++){%>
                        <li>
                            <input type="file" class="upload-file" name="File" accept="image/*" />
                            <a href="#"><img src="<%=Utils.GetResizeFile(listProductFile[i].File, 2, 116, 116) %>" /></a>
                        </li>
                        <%} %>
                    </ul>
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Chuyên mục</label>
                </div>
                <div class="text-left col-md-9">
                    <select class="form-control fl" name="MenuID" onchange="getProperties(this.value, '<%=ViewPage.CurrentLang.ID %>', '<%=item.ID %>')">
                        <option value="0" selected="selected">- Chọn chuyên mục -</option>
                        <%=Utils.ShowDdlMenuByType("Product", ViewPage.CurrentLang.ID, item.MenuID) %>
                    </select>
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Thuộc tính</label>
                </div>
                <div class="text-left col-md-9">
                    <fieldset id="listProperty"></fieldset>
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3">
                    <label>Thông tin sản phẩm</label>
                </div>
                <div class="text-left col-md-9">
                    <textarea class="form-control ckeditor" name="Content" rows="" cols=""><%=item.Content %></textarea>
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label>Mã bảo mật</label>
                </div>
                <div class="text-left col-md-9 security">
                    <div class="row">
                        <div class="text-left col-md-5">
                            <input type="text" class="form-control" name="ValidCode" id="ValidCode" value="" />
                        </div>
                        <div class="text-left col-md-5">
                            <img src="/ajax/Security.html" class="form-control" id="imgValidCode" alt="ValidCode" />
                        </div>
                        <div class="text-left col-md-2">
                            <a href="javascript:void(0)" class="form-control" onclick="change_captcha()" title="Đổi mã khác"><i class="fa fa-refresh fa-lg"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group clear">
                <div class="text-left col-md-3 ">
                    <label></label>
                </div>
                <div class="text-left col-md-9">
                    <button type="submit" class="btn btn-blue" name="_vsw_action[AddPOST]">Đăng tin</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function () {
        if ('<%=item.MenuID%>' > 0)
            getProperties('<%=item.MenuID%>', '<%=ViewPage.CurrentLang.ID%>', '<%=item.ID%>');
    })
</script>