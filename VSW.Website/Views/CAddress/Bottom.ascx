﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listRoot = ViewBag.Data as List<WebMenuEntity>;
%>

<div class="showroom-ft">
    <div class="diachishowroom all">
        <%for (int m = 0; listRoot != null && m < listRoot.Count; m++){
                int MenuID = listRoot[m].ID;
                var listItem = ModAddressService.Instance.CreateQuery()
                                            .Where(o => o.Activity == true && o.MenuID == MenuID)
                                            .OrderByAsc(o => o.Order)
                                            .ToList_Cache();
        %>
        <div class="bx-adr-showroom">
            <p class="title-store"><%=listRoot[m].Name.ToUpper() %></p>
            <ul class="diachoshdk all">
                <%for (int i = 0; listItem != null && i < listItem.Count; i++){%>
                <li>
                    <div class="store-info">
                        <p class="name-showroom"><i class="fa fa-fire"></i><%=listItem[i].Name.ToUpper() %></p>
                        <p class="diachi">
                            Showroom: <%=listItem[i].Address %>

                            <%if (!string.IsNullOrEmpty(listItem[i].Note)){%>
                            <br />(<%=listItem[i].Note %>)
                            <%} %>
                        </p>
                        <p>Email: <%=listItem[i].Email %></p>
                        <p>Điện thoại: <b><%=listItem[i].Phone %></b></p>
                    </div>
                </li>
                <%} %>
            </ul>
        </div>
        <%} %>
    </div>
</div>