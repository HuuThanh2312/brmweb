﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<% 
    var item = ViewBag.Data as ModNewsEntity;
    var listItem = ViewBag.Other as List<ModNewsEntity>;
%>

<div class="content-news">
    <h1><%=item.Name %></h1>
    <div class="dtop">
        <div class="clock">Đăng ngày <%=string.Format("{0:f}", item.Published) %></div>
        <div class="views"><%=string.Format("{0:#,##0}", item.View) %> lượt xem</div>
        <img src="Content/img/share.JPG" alt="">
    </div>
    <div class="post-entry"><%=Utils.GetHtmlForSeo(item.Content) %></div>
    <div class="clear"></div>

    <%if (listItem != null){%>
    <div class="other">
        <label class="dtit">Tin cùng chuyên mục</label>
        <ul>
            <%for (var i = 0; listItem != null && i < listItem.Count; i++){
                    string url = ViewPage.GetURL(listItem[i].Code);
            %>
            <li>
                <h3><a href="<%=url %>"><%=listItem[i].Name %></a></h3>
                <div><span><%=string.Format("{0:f}", listItem[i].Published) %></span> | <span><%=string.Format("{0:#,##0}", listItem[i].View) %> lượt xem</span></div>
            </li>
            <%} %>
        </ul>
    </div>
    <div class="clear"></div>
    <%} %>
</div>