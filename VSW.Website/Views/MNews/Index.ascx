﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.ViewControl" %>

<%
    var listItem = ViewBag.Data as List<ModNewsEntity>;
    var model = ViewBag.Model as MNewsModel;
%>

<ul class="tt">
    <%for (var i = 0; listItem != null && i < listItem.Count; i++){
            string url = ViewPage.GetURL(listItem[i].Code);
    %>
    <li>
        <a href="<%=url %>" class="ttimg" title="<%=listItem[i].Name %>">
            <img src="<%=Utils.GetResizeFile(listItem[i].File, 4, 190, 190) %>" alt="<%=listItem[i].Name %>" />
        </a>
        <h3><a href="<%=url %>" title="<%=listItem[i].Name %>"><%=listItem[i].Name %></a></h3>
        <p><%=listItem[i].Summary %></p>
        <div><span><%=string.Format("{0:f}", listItem[i].Published) %></span> | <span><%=string.Format("{0:#,##0}", listItem[i].View) %> lượt xem</span></div>
    </li>
    <%} %>
</ul>
<ul class="phantrang">
    <%= GetPagination(model.page, model.PageSize, model.TotalRecord)%>
</ul>