﻿function ShowFile() {
    var finder = new CKFinder();
    finder.basePath = '../';
    finder.selectActionFunction = refreshFile;
    finder.popup();

    return false;
}

function ShowGiftForm(sValue) {
    window.open('/' + window.CPPath + '/FormGift/Index.aspx?Value=' + sValue, '', 'width=1024, height=800, top=80, left=200,scrollbars=yes');
    return false;
}

function refreshFile(arg) {
    addFile(arg)
}

function refreshGift(arg) {
    addGift(arg)
}

function CloseGift(arg) {
    if (window.opener)
        window.opener.refreshGift(arg);
    else
        window.parent.refreshGift(arg);

    window.close();
}

//file
function addFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/AddFile.aspx',
        data: {
            Name: $('#Name').val(),
            MenuID: $('#MenuID').val(),
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var js = data.Js;
            var params = data.Params;
            var content = data.Html;

            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }

            if (js != '') {
                location.href = '/' + window.CPPath + '/ModProduct/Add.aspx/RecordID/' + js;
                return;
            }            

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function deleteFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DeleteFile.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function upFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/UpFile.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

function downFile(file) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DownFile.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            File: file
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-file').html(content);
        },
        error: function (status) { }
    });
}

//gift
function addGift(giftID) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/AddGift.aspx',
        data: {
            Name: $('#Name').val(),
            MenuID: $('#MenuID').val(),
            ProductID: $('#RecordID').val(),
            GiftID: giftID
        },
        dataType: 'json',
        success: function (data) {
            var js = data.Js;
            var params = data.Params;
            var content = data.Html;

            if (params != '') {
                zebra_alert('Thông báo !', params);
                return;
            }

            if (js != '') {
                location.href = '/' + window.CPPath + '/ModProduct/Add.aspx/RecordID/' + js;
                return;
            }

            $('#list-gift').html(content);
        },
        error: function (status) { }
    });
}

function deleteGift(giftID) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DeleteGift.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            GiftID: giftID
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-gift').html(content);
        },
        error: function (status) { }
    });
}

function upGift(giftID) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/UpGift.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            GiftID: giftID
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-gift').html(content);
        },
        error: function (status) { }
    });
}

function downGift(giftID) {
    $.ajax({
        type: 'post',
        url: '/' + window.CPPath + '/Ajax/DownGift.aspx',
        data: {
            ProductID: $('#RecordID').val(),
            GiftID: giftID
        },
        dataType: 'json',
        success: function (data) {
            var content = data.Html;

            $('#list-gift').html(content);
        },
        error: function (status) { }
    });
}