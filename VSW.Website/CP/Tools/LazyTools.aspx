﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!VSW.Lib.Global.CPLogin.IsLogin() || !VSW.Lib.Global.CPLogin.CurrentUser.IsAdministrator)
        {
            Response.Redirect("Login.aspx?ReturnPath=" + Server.UrlEncode(Request.RawUrl));
            return;
        }


    }
    public static string ShowDdlPageParent(int parent)
    {
        var list = SysPageService.Instance.CreateQuery()
                                    .Where(o => o.Activity == true && o.ParentID == 0)
                                    .OrderByAsc(o => new { o.Order, o.ID })
                                    .ToList_Cache();

        var s = string.Empty;

        for (var i = 0; list != null && i < list.Count; i++)
        {
            var listChidPage = SysPageService.Instance.CreateQuery().Where(o => o.Activity == true && o.ParentID == list[i].ID)
                                    .OrderByAsc(o => new { o.Order, o.ID }).ToList_Cache();
            s += "<option value=\"" + list[i].ID + "\">&nbsp; " + list[i].Name + "</option>";
            for (int j = 0; listChidPage != null && j < listChidPage.Count; j++)
            {
                var listGrandPage = SysPageService.Instance.CreateQuery().Where(o => o.Activity == true && o.ParentID == listChidPage[j].ID)
                                    .OrderByAsc(o => new { o.Order, o.ID }).ToList_Cache();
                s += "<option value=\"" + listChidPage[j].ID + "\">&nbsp;--------" + listChidPage[j].Name + "</option>";
                for (int z = 0; listGrandPage != null && z < listGrandPage.Count; z++)
                {
                    s += "<option value=\"" + listGrandPage[z].ID + "\">&nbsp;-----------" + listGrandPage[z].Name + "</option>";
                }
            }
        }

        return s;
    }

    public static string ShowModule()
    {
        var listModule = VSW.Lib.Web.Application.Modules.Where(o => o.IsControl == false).OrderBy(o => o.Order).ToList();
        string s = string.Empty;
        for (var i = 0; listModule != null && i < listModule.Count; i++)
        {
            s += "<option value=\"" + listModule[i].Code + "\">&nbsp; " + listModule[i].Name + "</option>";

        }
        return s;
    }

    public static string ShowDdlTemplate(int device)
    {
        var list = SysTemplateService.Instance.CreateQuery()
                                    .Where(o => o.Device == device)
                                    .OrderByAsc(o => new { o.Order, o.ID })
                                    .ToList_Cache();

        var s = string.Empty;

        for (var i = 0; list != null && i < list.Count; i++)
        {
            s += "<option value=\"" + list[i].ID + "\">&nbsp; " + list[i].Name + "</option>";
        }

        return s;
    }
    protected void btnRun9_Click(object sender, EventArgs e)
    {
        var listCartegory = ModBerichCategoryService.Instance.CreateQuery()
                                                      .Where(o => o.CategoryID != "1").ToList();

        for (int i = 0; listCartegory != null && i < listCartegory.Count; i++)
        {
            var _menu = new WebMenuEntity();
            _menu.ParentID = 882;
            _menu.Type = "Product";
            _menu.LangID = 1;
            _menu.Order = i + 1;
            _menu.Name = listCartegory[i].CategoryName;
            _menu.Activity = true;
            _menu.Code = Data.GetCode(_menu.Name);
            _menu.CategoryBerichID = listCartegory[i].CategoryID;
            WebMenuService.Instance.Save(_menu);

            var listCon = ModBerichCategoryService.Instance.CreateQuery()
                                                      .Where(o => o.ParentID == listCartegory[i].CategoryID).ToList();
            for (int j = 0; listCon != null && j < listCon.Count; j++)
            {
                var _menu2 = new WebMenuEntity();
                _menu2.ParentID = _menu.ID;
                _menu2.Type = "Product";
                _menu2.LangID = 1;
                _menu2.Order = j + 1;
                _menu2.Name = listCon[j].CategoryName;
                _menu2.Activity = true;
                _menu2.Code = Data.GetCode(_menu2.Name);
                _menu2.CategoryBerichID = listCon[j].CategoryID;
                WebMenuService.Instance.Save(_menu2);
            }
        }
        VSW.Core.Web.Cache.Clear(WebMenuService.Instance);
    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        string[] list = Request.Form["Page"].ToString().Split(',');
        string _ModuleCode = Request.Form["ModuleCode"].ToString();
        int _ParentID = VSW.Core.Global.Convert.ToInt(Request.Form["ParentID"]);
        int _Template = VSW.Core.Global.Convert.ToInt(Request.Form["TemplateID"]);
        int _TemplateMobile = VSW.Core.Global.Convert.ToInt(Request.Form["TemplateMobileID"]);

        if (list.Length == 0) { ltError.Text += "Chưa nhập tên menu.<br />"; return; }
        else
        {
            SysPageEntity _syspage = null;
            for (int i = 0; i < list.Length; i++)
            {


                _syspage = new SysPageEntity();
                _syspage.Activity = true;
                _syspage.Name = list[i];
                _syspage.Code = Data.GetCode(list[i]);
                _syspage.ModuleCode = _ModuleCode;
                _syspage.TemplateID = _Template;
                _syspage.TemplateMobileID = _TemplateMobile;
                _syspage.ParentID = _ParentID;
                _syspage.LangID = 1;
                _syspage.Created = DateTime.Now;
                _syspage.Order = i + 1;

                var _menu = WebMenuService.Instance.CreateQuery()
                                   .Where(o => o.Code == _syspage.Code)
                                   .ToSingle();
                if (_menu != null) _syspage.MenuID = _menu.ID;


                SysPageService.Instance.Save(_syspage);

                ModCleanURLService.Instance.InsertOrUpdate(_syspage.Code, "Page", _syspage.ID, _syspage.MenuID, 1);
            }
            VSW.Core.Web.Cache.Clear(SysPageService.Instance);
        }
    }
    protected void btnRun4_Click(object sender, EventArgs e)
    {
        //string[] list = Request.Form["Page"].ToString().Split(',');
        string _ModuleCode = Request.Form["ModuleCode"].ToString();
        int _ParentID = VSW.Core.Global.Convert.ToInt(Request.Form["ParentID"]);
        int _Template = VSW.Core.Global.Convert.ToInt(Request.Form["TemplateID"]);
        int _TemplateMobile = VSW.Core.Global.Convert.ToInt(Request.Form["TemplateMobileID"]);

        var list = SysPageService.Instance.CreateQuery().Where(o => o.Activity == true && o.ParentID == _ParentID).ToList();
        for (int i = 0; list != null && i < list.Count; i++)
        {

            list[i].ModuleCode = _ModuleCode;
            list[i].TemplateID = _Template;
            list[i].TemplateMobileID = _TemplateMobile;
            SysPageService.Instance.Save(list[i], o => new { o.ModuleCode, o.TemplateID, o.TemplateMobileID });
        }

    }

    protected void btnRun1_Click(object sender, EventArgs e)
    {
        string _title = Request.Form["NameNews"].ToString();
        string _summary = Request.Form["Summary"].ToString();
        string _Content = Request.Form["Content"].ToString();
        string _File = Request.Form["File"].ToString();
        int _MenuID = VSW.Core.Global.Convert.ToInt(Request.Form["MenuID2"]);
        int _Copy = VSW.Core.Global.Convert.ToInt(Request.Form["Copy"]);


        if (string.IsNullOrEmpty(_title)) { ltError.Text += "Chưa nhập tên tin tức.<br />"; return; }
        else
        {
            ModNewsEntity _news = null;
            for (int i = 0; i < _Copy; i++)
            {
                _news = new ModNewsEntity();
                _news.Activity = true;
                _news.Name = _title + " " + (i + 1);
                _news.Code = Data.GetCode(_news.Name);
                _news.Summary = _summary;
                _news.Content = _Content;
                _news.MenuID = _MenuID;
                _news.Updated = DateTime.Now;
                _news.Published = DateTime.Now;
                _news.Order = i + 1;
                _news.File = _File;

                ModNewsService.Instance.Save(_news);

                ModCleanURLService.Instance.InsertOrUpdate(_news.Code, "News", _news.ID, _news.MenuID, 1);
            }
        }
    }

    protected void btnRun2_Click(object sender, EventArgs e)
    {
        string _title = Request.Form["NameProduct"].ToString();
        string _summary = Request.Form["Summary2"].ToString();
        string _Content = Request.Form["Content2"].ToString();
        string _File = Request.Form["File2"].ToString();
        int _MenuID = VSW.Core.Global.Convert.ToInt(Request.Form["MenuID3"]);
        int _Price = VSW.Core.Global.Convert.ToInt(Request.Form["Price"]);
        int _Price2 = VSW.Core.Global.Convert.ToInt(Request.Form["Price2"]);
        int _Copy = VSW.Core.Global.Convert.ToInt(Request.Form["Copy2"]);


        if (string.IsNullOrEmpty(_title)) { ltError.Text += "Chưa nhập tên tin tức.<br />"; return; }
        else
        {
            ModProductEntity _product = null;
            for (int i = 0; i < _Copy; i++)
            {
                _product = new ModProductEntity();
                _product.Activity = true;
                _product.Name = _title + " " + (i + 1);
                _product.Code = Data.GetCode(_product.Name);
                _product.Summary = _summary;
                _product.Content = _Content;
                _product.MenuID = _MenuID;
                _product.Price = _Price;
                _product.Price2 = _Price2;
                _product.Published = DateTime.Now;
                _product.Updated = DateTime.Now;
                _product.Order = i + 1;
                _product.File = _File;

                ModProductService.Instance.Save(_product);

                ModCleanURLService.Instance.InsertOrUpdate(_product.Code, "Product", _product.ID, _product.MenuID, 1);
            }
        }
    }
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Tool dành cho thánh lười - ANGKORICH</title>
</head>
<body>
    <form id="form1" runat="server" style="background-color: #64c0f999;">

        <center>
            <h1 style="color: blue;">Tool dành cho thánh lười - WEBVIET24H.COM</h1>
        </center>

        <center>
            <h1 style="color: blue;">Tạo menu - WEBVIET24H.COM</h1>
        </center>

        <div style="text-align: center; padding-top: 30px; color: #FF0000; font-weight: bold;">
            <asp:Button ID="Button9" runat="server" OnClick="btnRun9_Click" Text="Thực hiện" Width="111px" />
            <br />
            <asp:Literal ID="Literal9" runat="server"></asp:Literal>
        </div>



        <p style="font-size: 20px; font-style: italic; font-weight: bold; margin-top: 50px; text-align: center">Ahiihi Đăng 1 đống MENU</p>
        <table width="100%" cellpadding="10" cellspacing="10" style="border: 1px dotted #CCC; border-collapse: collapse;">
            <tr>
                <td width="575">
                    <span style="margin: 10px 0;">Tên menu muốn tạo</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">Thêm vào menu</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">Chọn Chức Năng</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">Chọn mẫu giao diện PC</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">Chọn mẫu giao diện Mobile</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="Page" id="Page" value="" placeholder="Menu1,menu2,menu3" style="width: 400px;" />
                </td>
                <td>
                    <select name="ParentID" id="ParentID">
                        <option value="0">- Chọn Menu Cha -</option>
                        <%=ShowDdlPageParent(0)%>
                    </select>
                </td>
                <td>
                    <select class="form-control" name="ModuleCode" id="ModuleCode">
                        <option value="">- Chọn Chức Năng -</option>
                        <%=ShowModule() %>
                    </select>
                </td>
                <td>
                    <select name="TemplateID" id="TemplateID">
                        <option value="0">- Chọn Mẫu Giao Diện PC -</option>
                        <%=ShowDdlTemplate(0)%>
                    </select>
                </td>
                <td>
                    <select name="TemplateMobileID" id="TemplateMobileID">
                        <option value="0">- Chọn Mẫu Giao Diện Mobile -</option>
                        <%=ShowDdlTemplate(1)%>
                    </select>
                </td>
            </tr>
        </table>

        <div style="text-align: center; padding-top: 30px; color: #FF0000; font-weight: bold;">
            <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Thực hiện" Width="111px" />
            <br />
            <asp:Literal ID="ltError" runat="server"></asp:Literal>
        </div>

        <div style="text-align: center; padding-top: 30px; color: #FF0000; font-weight: bold;">
            <asp:Button ID="btnRun4" runat="server" OnClick="btnRun4_Click" Text="Cập nhật" Width="111px" />
            <br />

        </div>


        <center>
            <h1 style="color: blue;">Đăng sản phẩm - WEBVIET24H.COM</h1>
        </center>


        <p style="font-size: 20px; font-style: italic; font-weight: bold; margin-top: 50px; text-align: center">Ahiihi Đăng 1 đống SP</p>
        <table width="100%" cellpadding="10" cellspacing="10" style="border: 1px dotted #CCC; border-collapse: collapse;">
            <tr>
                <td>
                    <span style="margin: 10px 0;">Đường dẫn ảnh</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">LẤY VÀO CHUYÊN MỤC</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">Chọn số lượng nhân bản</span>
                </td>
            </tr>
            <tr>
                <td>
                    <img id="img_view_product" width="100" height="80" />
                    <br />
                    <input class="text_input" type="text" name="File2" id="File2" style="width: 65%" value="" />
                    <input class="text_input" style="width: 75px;" type="button" onclick="ShowFileForm('File2'); return false;" value="Chọn ảnh" />
                </td>
                <td>
                    <select name="MenuID3" id="MenuID3">
                        <option value="0">- Chọn chuyên mục -</option>
                        <%=Utils.ShowDdlMenuByType("Product", 1, 0)%>
                    </select>
                </td>
                <td>
                    <select name="Copy2" id="Copy2">
                        <option value="0">- Chọn số lượng bản ghi -</option>
                        <%for (int i = 1; i <= 10; i++)
                            {%>
                        <option value="<%=i %>">- <%=i %> -</option>
                        <%} %>
                    </select>
                </td>
            </tr>
        </table>
        <div class="width-100 fltlft">
            <fieldset class="adminform">
                <legend>THÔNG TIN CHUNG</legend>
                <table class="admintable" style="width: 100%">
                    <tr>
                        <td class="key">
                            <label>Tiêu đề :</label>
                        </td>
                        <td>
                            <input class="text_input" type="text" name="NameProduct" id="NameProduct" value="" style="width: 400px;" maxlength="255" />
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Giá NY:</label>
                        </td>
                        <td>
                            <input class="text_input text_input_price" type="text" name="Price2" id="Price2" onkeyup="$(this).parent().find('span').html(formatDollar(this.value))" value="" maxlength="255" />

                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Giá bán:</label>
                        </td>
                        <td>
                            <input class="text_input text_input_price" type="text" name="Price" id="Price" onkeyup="$(this).parent().find('span').html(formatDollar(this.value))" value="" maxlength="255" />
                        </td>
                    </tr>
                    <tr>
                        <td class="key">
                            <label>Tóm tắt :</label>
                        </td>
                        <td>
                            <textarea class="text_input" style="height: 100px; width: 98%" name="Summary2" id="Summary2"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="text-align: center" class="key">NỘI DUNG
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="content">
                            <textarea class="ckeditor" style="width: 100%; height: 500px" name="Content2" id="Content2"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div style="text-align: center; padding-top: 30px; color: #FF0000; font-weight: bold;">
            <asp:Button ID="Button1" runat="server" OnClick="btnRun2_Click" Text="Thực hiện" Width="111px" />
            <br />
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </div>



        <center>
            <h1 style="color: blue;">Đăng tin - WEBVIET24H.COM</h1>
        </center>

        <p style="font-size: 20px; font-style: italic; font-weight: bold; margin-top: 50px; text-align: center">Ahiihi Đăng 1 đống tin</p>
        <table width="100%" cellpadding="10" cellspacing="10" style="border: 1px dotted #CCC; border-collapse: collapse;">
            <tr>
                <td>
                    <span style="margin: 10px 0;">Đường dẫn ảnh</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">LẤY VÀO CHUYÊN MỤC</span>
                </td>
                <td>
                    <span style="margin: 10px 0;">Chọn số lượng nhân bản</span>
                </td>
            </tr>
            <tr>
                <td>
                    <img id="img_view" width="100" height="80" />
                    <br />
                    <input class="text_input" type="text" name="File" id="File" style="width: 65%" value="" />
                    <input class="text_input" style="width: 75px;" type="button" onclick="ShowFileForm('File'); return false;" value="Chọn ảnh" />
                </td>
                <td>
                    <select name="MenuID2" id="MenuID2">
                        <option value="0">- Chọn chuyên mục -</option>
                        <%=Utils.ShowDdlMenuByType("News", 1, 0)%>
                    </select>
                </td>
                <td>
                    <select name="Copy" id="Copy">
                        <option value="0">- Chọn số lượng bản ghi -</option>
                        <%for (int i = 1; i <= 10; i++)
                            {%>
                        <option value="<%=i %>">- <%=i %> -</option>
                        <%} %>
                    </select>
                </td>
            </tr>
        </table>
        <div class="width-100 fltlft">
            <fieldset class="adminform">
                <legend>THÔNG TIN CHUNG</legend>
                <table class="admintable" style="width: 100%">
                    <tr>
                        <td class="key">
                            <label>Tiêu đề :</label>
                        </td>
                        <td>
                            <input class="text_input" type="text" name="NameNews" id="NameNews" value="" style="width: 400px;" maxlength="255" />
                        </td>
                    </tr>

                    <tr>
                        <td class="key">
                            <label>Tóm tắt :</label>
                        </td>
                        <td>
                            <textarea class="text_input" style="height: 100px; width: 98%" name="Summary" id="Summary"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="text-align: center" class="key">NỘI DUNG
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="content">
                            <textarea class="ckeditor" style="width: 100%; height: 500px" name="Content" id="Content"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div style="text-align: center; padding-top: 30px; color: #FF0000; font-weight: bold;">
            <asp:Button ID="Button2" runat="server" OnClick="btnRun1_Click" Text="Thực hiện" Width="111px" />
            <br />
            <asp:Literal ID="ltError1" runat="server"></asp:Literal>
        </div>




        <center style="margin-top: 50px;">Công ty cổ phần <a href="http://angkorich.com/" target="_blank">ANGKORICH</a></center>
        <script type="text/javascript" src="/CP/Content/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/CP/Content/ckfinder/ckfinder.js"></script>
        <script type="text/javascript">
            CKFinder.setupCKEditor(null, { basePath: "/CP/Content/ckfinder/", rememberLastFolder: true });
            var name_control = '';

            function refreshPage(arg) {
                arg = "~" + arg;
                document.getElementById(name_control).value = arg;
                if (document.getElementById("img_view"))
                    document.getElementById("img_view").src = arg.replace("~/", "/");
            }

            function refreshPage(arg) {
                arg = "~" + arg;
                document.getElementById(name_control).value = arg;
                if (document.getElementById("img_view_product"))
                    document.getElementById("img_view_product").src = arg.replace("~/", "/");
            }

            function ShowFileForm(cID, sValue) {
                name_control = cID;

                var finder = new CKFinder();
                finder.basePath = '../';
                finder.selectActionFunction = refreshPage;
                finder.popup();

                return false;
            }
        </script>
    </form>
</body>
</html>
