﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<script runat="server">

    object GetValue(ModProductEntity item, string proName)
    {
        System.Reflection.PropertyInfo prop = item.GetType().GetProperty(proName);

        var x = item.GetType().GetMethod(proName);
        x.
        if (prop != null)
            return prop.GetValue(item, null);

        return null;
    }

    void Import()
    {
        List<ModProductEntity> listItem = new List<ModProductEntity>();

        listItem.Add(new ModProductEntity()
        {
            ID = 1,
            Name = "name 1",
            Price = 1000,
            Summary = "summary 1"
        });
        listItem.Add(new ModProductEntity()
        {
            ID = 2,
            Name = "name 2",
            Price = 2000,
            Summary = "summary 2"
        });
        listItem.Add(new ModProductEntity()
        {
            ID = 3,
            Name = "name 3",
            Price = 3000,
            Summary = "summary 3"
        });

        string[] ArrCol = { "ID", "Name", "Price", "Summary" };

        List<List<object>> listRow = new List<List<object>>();

        List<object> listCol = new List<object>();
        for (int i=0;ArrCol!=null && i<ArrCol.Length;i++)
            listCol.Add(ArrCol[i]);

        listRow.Add(listCol);

        for (int i = 0; listItem != null && i < listItem.Count; i++)
        {
            listCol = new List<object>();

            for (int j = 0; ArrCol != null && j < ArrCol.Length; j++)
                listCol.Add(GetValue(listItem[i], ArrCol[j]));

            listRow.Add(listCol);
        }

        //string excelFile = CPViewPage.Server.MapPath("~/Data/upload/files/Product_" + string.Format("{0:ddMMyyyy}", DateTime.Now) + "_" + DateTime.Now.Ticks + ".xls");

        //Excel.Export(listRow, 0, CPViewPage.Server.MapPath("~/CP/Template/Template.xls"), excelFile);

        //CPViewPage.Response.Clear();
        //CPViewPage.Response.ContentType = "application/excel";
        //CPViewPage.Response.AppendHeader("Content-Disposition", "attachment; filename=" + System.IO.Path.GetFileName(excelFile));
        //CPViewPage.Response.WriteFile(excelFile);
        //CPViewPage.Response.End();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CPLogin.IsLogin() && CPLogin.CurrentUser.IsAdministrator) return;

        Response.Redirect("Login.aspx?ReturnPath=" + Server.UrlEncode(Request.RawUrl));
    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        Import();
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SQL</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center;" >
                <br/>
                <asp:TextBox ID="txtSQL" TextMode="MultiLine" runat="server" Height="200px" Width="574px"></asp:TextBox>
                <br/>
                <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click" Text="Run" Width="111px" />
                <br/>
                <br/>
                <asp:GridView ID="gvSQL" runat="server"></asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>