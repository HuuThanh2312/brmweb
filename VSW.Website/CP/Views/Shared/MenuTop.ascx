﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<% 
    var listModule = VSW.Lib.Web.Application.CPModules.Where(o => o.ShowInMenu == true).OrderBy(o => o.Order).ToList();
    var listModule2 = VSW.Lib.Web.Application.CPModules.Where(o => o.ShowInMenu == false).OrderBy(o => o.Order).ToList();
%>

<div class="hor-menu nav-desktop">
    <ul class="navmenu">
        <li class="item active">
            <a href="/{CPPath}/">Home</a>
        </li>
        <li class="item">
            <a class="a-open-down"></a>
            <a href="javascript:;">{RS:MenuTop_Management} <i class="fa fa-angle-down"></i></a>
            <ul class="sub-menu">
                <%for (var i = 0; i < listModule.Count; i++){
                        string icon = "folder";
                        if (listModule[i].Code == "ModAdv") icon = "money";
                        else if (listModule[i].Code == "ModNews") icon = "list-alt";
                        else if (listModule[i].Code == "ModFile") icon = "upload";
                        else if (listModule[i].Code == "ModComment") icon = "comments";
                        else if (listModule[i].Code == "ModOrder") icon = "shopping-cart";
                        else if (listModule[i].Code == "ModProduct") icon = "th";
                        else if (listModule[i].Code == "ModVideo") icon = "film";
                %>
                <li><a href="/{CPPath}/<%=listModule[i].Code%>/Index.aspx"><i class="fa fa-<%=icon %>"></i><%=listModule[i].Name%></a></li>
                <%} %>
            </ul>
        </li>
         <li class="item">
            <a class="a-open-down"></a>
            <a href="javascript:;">Đối tác<i class="fa fa-angle-down"></i></a>
            <ul class="sub-menu">
                <%for (var i = 0; i < listModule2.Count; i++){
                        string icon = "folder";
                        if (listModule2[i].Code == "ModAdv") icon = "money";
                        else if (listModule2[i].Code == "ModNews") icon = "list-alt";
                        else if (listModule2[i].Code == "ModFile") icon = "upload";
                        else if (listModule2[i].Code == "ModShop") icon = "slideshare";
                        else if (listModule2[i].Code == "ModMarketingNews") icon = "list-alt";
                        else if (listModule2[i].Code == "ModProduct") icon = "th";
                        else if (listModule2[i].Code == "ModWebUser") icon = "users";
                %>
                <li><a href="/{CPPath}/<%=listModule2[i].Code%>/Index.aspx"><i class="fa fa-<%=icon %>"></i><%=listModule2[i].Name%></a></li>
                <%} %>
            </ul>
        </li>
        <li class="item">
            <a class="a-open-down"></a>
            <a href="javascript:void(0)">{RS:MenuTop_Design} <i class="fa fa-angle-down"></i></a>
            <ul class="sub-menu">
                <li><a href="/{CPPath}/SysPage/Index.aspx"><i class="fa fa-th"></i>{RS:MenuTop_Page}</a></li>
                <li><a href="/{CPPath}/SysTemplate/Index.aspx"><i class="fa fa-columns"></i>{RS:MenuTop_Template}</a></li>
                <li><a href="/{CPPath}/SysSite/Index.aspx"><i class="fa fa-sitemap"></i>Site</a></li>
            </ul>
        </li>
        <li class="item">
            <a class="a-open-down"></a>
            <a href="javascript:;">{RS:MenuTop_System} <i class="fa fa-angle-down"></i></a>
            <ul class="sub-menu">
                <li><a href="/{CPPath}/SysMenu/Index.aspx"><i class="fa fa-folder"></i>Chuyên mục</a></li>
                <%--<li><a href="/{CPPath}/SysRedirection/Index.aspx"><i class="fa fa-folder"></i>Chuyển hướng</a></li>--%>
                <li><a href="/{CPPath}/SysProperty/Index.aspx"><i class="fa fa-folder"></i>Thuộc tính</a></li>
                <li><a href="/{CPPath}/SysRole/Index.aspx"><i class="fa fa-users"></i>Nhóm người sử dụng</a></li>
                <li><a href="/{CPPath}/SysUser/Index.aspx"><i class="fa fa-user"></i>Người sử dụng</a></li>
                <li><a href="/{CPPath}/SysResource/Index.aspx"><i class="fa fa-globe"></i>Tài nguyên</a></li>
                <li><a href="/{CPPath}/SysUserLog/Index.aspx"><i class="fa fa-calendar"></i>Nhật ký đăng nhập</a></li>
                <%--<li><a href="/{CPPath}/ModSearch/Index.aspx"><i class="fa fa-folder"></i>Đánh chỉ mục</a></li>--%>
            </ul>
        </li>
    </ul>
</div>