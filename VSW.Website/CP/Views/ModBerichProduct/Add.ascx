﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModBerichProductModel;
    var item = ViewBag.Data as ModBerichProductEntity;
    var _productFile = ViewBag.DataFile as ModProductBerichFileEntity;
    var listFile = _productFile != null ? _productFile.GetFiles() : new List<string>();


%>

<form id="vswForm" name="vswForm" method="post">
    <input type="hidden" id="_vsw_action" name="_vsw_action" />
    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />
    <input type="hidden" name="Files" id="Files" value="<%=_productFile!=null? _productFile.Files:"" %>" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Sản phẩm <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Sản phẩm</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên sản phẩm:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control title" name="ProductName" disabled="" id="Name" value="<%=item.ProductName %>" />
                                                        <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá gốc:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control price" disabled="" value="<%=string.Format("{0:#,##0}", item.GetCostPrice().SalePrice )%>" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-12 col-form-label text-right">Nội dung</label>
                                                    <div class="col-md-12">
                                                        <textarea class="form-control ckeditor" name="Description" id="Description" rows="" cols=""><%=_productFile.Content %></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">HÌNH ẢNH</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <%if (!string.IsNullOrEmpty(_productFile.File))
                                                        { %>
                                                    <p class="preview "><%= Utils.GetMedia(_productFile.File, 80, 80)%></p>
                                                    <%}
                                                        else
                                                        { %>
                                                    <p class="preview">
                                                        <img src="" width="80" height="80" />
                                                    </p>
                                                    <%} %>

                                                    <label class="portlet-title-sub">Hình minh họa:</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="File" id="File" value="<%=_productFile.File %>" />
                                                        <button type="button" class="btn btn-primary" onclick="ShowFileForm('File'); return false">Chọn ảnh</button>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Ảnh phụ:</label>
                                                    <ul class="cmd-custom" id="list_image">
                                                        <% for (var i = 0; listFile != null && i < listFile.Count; i++)
                                                            { %>
                                                        <li>
                                                            <img src="<%=listFile[i] %>" class="small" />
                                                            <span>
                                                                <a href="javascript:void(0)" onclick="image_delete('<%=i %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                            </span>
                                                        </li>
                                                        <%} %>
                                                    </ul>
                                                    <div class="form-inline">
                                                        <button type="button" class="btn btn-primary" onclick="call_ckfinder('#Temp'); return false;">Chọn ảnh</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<script type="text/javascript" src="/{CPPath}/Content/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKFinder.setupCKEditor(null, { basePath: "/{CPPath}/Content/ckfinder/", rememberLastFolder: true });

    function refreshPage(arg) {
        arg = "~" + arg;
        document.getElementById(name_control).value = arg;
        if (document.getElementById("img_view"))
            document.getElementById("img_view").src = arg.replace("~/", "/{ApplicationPath}");
    }
    //hinh anh phu
    var _ArrImages = new Array();

        <% for (var i = 0; listFile != null && i < listFile.Count; i++)
    { %>
    _ArrImages.push('<%=listFile[i]%>');
        <%} %>

    function image_add(file) {
        _ArrImages.push(file);
        image_display();
    }
    function call_ckfinder(control) {
        var finder = new CKFinder();

        name_control = control;

        finder.selectActionFunction = get_selected_image;
        finder.popup();
    }
    function get_selected_image(value) {
        if (name_control === '#File') {
            $(name_control).val("~" + value);
            $('.img-upload img').attr('src', value);
        }
        else if (name_control === '#Temp') {
            image_add(value);
        }
    }

    function image_display() {
        var s = '';
        var v = '';

        for (var i = 0; i < _ArrImages.length; i++) {
            v += (v === '' ? "" : '\n') + _ArrImages[i];
            s += '  <li>\
                            <img src="' + _ArrImages[i] + '" class="small" />\
                            <span><a href="javascript:void(0)" onclick="image_delete(' + i + ')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a></span>\
                        </li>';
        }

        $('#list_image').html(s);
        $('#Files').val(v);
    }

    function image_delete(index) {
        if (confirm('Bạn chắc muốn xóa không ?')) {
            _ArrImages.splice(index, 1);
            image_display();
        }
    }
</script>
