﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModBerichSAPriceModel;
    var listItem = ViewBag.Data as List<ModBerichSAPriceEntity>;
    string _valueDefault = VSW.Lib.Global.Setting.BerichSAPrice;
%>
<form id="vswForm" name="vswForm" method="post">

    <input type="hidden" id="_vsw_action" name="_vsw_action" />
    <input type="hidden" id="boxchecked" name="boxchecked" value="0" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Sản phẩm</h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Giá theo bảng</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="portlet portlet">
                    <div class="portlet-title">
                        <div class="actions btn-set">
                           
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="table-scrollable">
                            <table class="table table-striped table-hover table-bordered dataTable">
                                <thead>
                                    <tr>
                                        <th class="sorting text-center w1p">#</th>
                                        <th class="sorting_disabled text-center w1p">
                                            <label class="itemCheckBox itemCheckBox-sm">
                                                <input type="checkbox" name="toggle" onclick="checkAll(<%= model.PageSize %>)">
                                                <i class="check-box"></i>
                                            </label>
                                        </th>
                                        <th class="sorting text-center  hidden-sm hidden-col"><%= GetSortLink("Giá trị", "RefID")%></th>
                                        <th class="sorting text-center"><%= GetSortLink("Tên", "Name")%></th>
                                        <th class="sorting text-center w5p"><%= GetSortLink("Mặc định", "ID")%></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (var i = 0; listItem != null && i < listItem.Count; i++)
                                        { %>
                                    <tr>
                                        <td class="text-center"><%= i + 1%></td>
                                        <td class="text-center">
                                            <%= GetCheckbox(listItem[i].ID, i)%>
                                        </td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].RefID %></td>
                                        <td class="text-center hidden-sm hidden-col"><%= listItem[i].ApplyBranch %></td>

                                        <td class="text-center">
                                            <a href="javascript:void(0)" onclick="set_default('<%=listItem[i].RefID %>','unpublish'); return false" data-toggle="tooltip" data-placement="bottom" data-original-title="Click để duyệt giá hoặc hủy duyệt">
                                                <span class="fa fa-dot-circle-o <%=_valueDefault==listItem[i].RefID.ToString()?"publish":"unpublish" %>"></span>
                                            </a>

                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 justify-content-center d-flex ">
                                <%= GetPagination(model.PageIndex, model.PageSize, model.TotalRecord)%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        var VSWController = "ModBerichSAPrice";

        var VSWArrVar = [
            "filter_menu", "MenuID",
            "filter_state", "State",
            "filter_lang", "LangID",
            "limit", "PageSize"
        ];

        var VSWArrVar_QS = [
            "filter_search", "SearchText"
        ];

        var VSWArrQT = [
            "<%= model.PageIndex + 1 %>", "PageIndex",
            "<%= model.Sort %>", "Sort"
        ];

        var VSWArrDefault = [
            "1", "PageIndex",
            "1", "LangID",
            "20", "PageSize"
        ];


        //thuoc tinh
        function set_default(Value) {
            var dataString = "Value=" + Value;

            $.ajax({
                url: "/{CPPath}/Ajax/SetDefault.aspx",
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    if (content !== '') location.reload();
                },
                error: function (status) { }
            });
        }
    </script>

</form>
