﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="VSW.Lib.MVC.CPViewControl" %>

<%
    var model = ViewBag.Model as ModProductModel;
    var item = ViewBag.Data as ModProductEntity;

    var listFile = item.GetFile();
    //var listGift = item.GetGift();
%>

<form id="vswForm" name="vswForm" method="post">
    <input type="hidden" id="_vsw_action" name="_vsw_action" />
    <input type="hidden" id="RecordID" value="<%=model.RecordID %>" />

    <div class="page-content-wrapper">
        <h3 class="page-title">Sản phẩm <small><%= model.RecordID > 0 ? "Chỉnh sửa": "Thêm mới"%></small></h3>
        <div class="page-bar justify-content-between">
            <ul class="breadcrumb">
                <li class="breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/{CPPath}/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/{CPPath}/<%=CPViewPage.CurrentModule.Code%>/Index.aspx">Sản phẩm</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group">
                    <a href="/" class="btn green" target="_blank"><i class="icon-screen-desktop"></i>Xem Website</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%= ShowMessage()%>

                <div class="form-horizontal form-row-seperated">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"></div>
                            <div class="actions btn-set">
                                <%= GetDefaultAddCommand()%>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin chung</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Tên sản phẩm:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control title" name="Name" id="Name" value="<%=item.Name %>" />
                                                        <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">URL trình duyệt:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="Code" value="<%=item.Code %>" placeholder="Nếu không nhập sẽ tự sinh theo Tiêu đề" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá gốc nhập vào:</label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control price" disabled="" value="<%=string.Format("{0:#,##0}", item.CostPrice )%>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.CostPrice.ToString())%></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá bán:</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control price" name="Price" value="<%=item.Price %>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.Price.ToString())%></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Giá cũ <span style="text-decoration: line-through; color: #7e7e7e">(giá gạch chân)</span>:</label>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control price" name="Price2" value="<%=item.Price2 %>" />
                                                        <span class="help-block text-primary"><%=Utils.NumberToWord(item.Price2.ToString())%></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Mô tả:</label>
                                                    <div class="col-md-9">
                                                        <textarea class="form-control description" rows="5" name="Summary" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.Summary%></textarea>
                                                        <span class="help-block text-primary">Ký tự đối ta: 400</span>
                                                    </div>
                                                </div>
                                                <%-- <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Thương hiệu:</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="BrandID" id="BrandID">
                                                            <option value="0">Root</option>
                                                            <%= Utils.ShowDdlMenuByType2("Brand", model.LangID, item.BrandID)%>
                                                        </select>
                                                    </div>
                                                </div>--%>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Chuyên mục:</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="MenuID" id="MenuID" onchange="GetProperties(this.value)">
                                                            <option value="0">Root</option>
                                                            <%= Utils.ShowDdlMenuByType_NotParent("Product", model.LangID, item.MenuID)%>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-right">Thuộc tính lọc:</label>
                                                    <div class="col-md-9" id="list-property">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-12 col-form-label text-right">Nội dung</label>
                                                    <div class="col-md-12">
                                                        <textarea class="form-control ckeditor" name="Content" id="Content" rows="" cols=""><%=item.Content %></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">Thông tin đối tác</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Thông tin:</label>
                                                    <ul class="cmd-custom" id="list-gift">
                                                        <li>
                                                            <label style="font-weight: bold">Tên đối tác bán: <a style="color: blue" href="http://localhost:12346/CP/ModShop/Add.aspx/RecordID/<%=item.GetShop().ID %>" target="_blank"><%=item.GetShop().Name %></a></label></li>
                                                        <li>
                                                            <label style="font-weight: bold">Số lượng nhập sản phẩm: <%=item.Inventory%></label></li>
                                                        <li>
                                                            <label style="font-weight: bold">Giá gốc: <%=string.Format("{0:#,##0}", item.CostPrice) %> vnđ</label></li>
                                                        <li>
                                                            <label style="font-weight: bold">Giá bán trên web: <%=string.Format("{0:#,##0}", item.Price) %> vnđ</label></li>
                                                        <li>
                                                            <label style="font-weight: bold">Ngày nhập hàng: <%=string.Format("{0:dd-MM-yyyy HH:mm}", item.Published) %></label></li>


                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">HÌNH ẢNH</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <%if (!string.IsNullOrEmpty(item.File))
                                                        { %>
                                                    <p class="preview "><%= Utils.GetMedia(item.File, 80, 80)%></p>
                                                    <%}
                                                        else
                                                        { %>
                                                    <p class="preview">
                                                        <img src="" width="80" height="80" />
                                                    </p>
                                                    <%} %>

                                                    <label class="portlet-title-sub">Hình minh họa:</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="File" id="File" value="<%=item.File %>" />
                                                        <button type="button" class="btn btn-primary" onclick="ShowFileForm('File'); return false">Chọn ảnh</button>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Ảnh phụ:</label>
                                                    <ul class="cmd-custom" id="list-file">
                                                        <%for (int i = 0; i < listFile.Count; i++)
                                                            {%>
                                                        <li>
                                                            <img src="<%=listFile[i].File.Replace("~/","/") %>" />
                                                            <a href="javascript:void(0)" onclick="deleteFile('<%=listFile[i].File %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Xóa"><i class="fa fa-ban"></i></a>
                                                            <a href="javascript:void(0)" onclick="upFile('<%=listFile[i].File %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển lên trên"><i class="fa fa-arrow-up"></i></a>
                                                            <a href="javascript:void(0)" onclick="downFile('<%=listFile[i].File %>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Chuyển xuống dưới"><i class="fa fa-arrow-down"></i></a>
                                                        </li>
                                                        <%} %>
                                                    </ul>
                                                    <div class="form-inline">
                                                        <button type="button" class="btn btn-primary" onclick="ShowFile(); return false">Chọn ảnh</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">THUỘC TÍNH</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                               <%-- <div class="form-group">
                                                    <label class="portlet-title-sub">Vị trí</label>
                                                    <div class="checkbox-list">
                                                        <%= Utils.ShowCheckBoxByConfigkey("Mod.ProductState", "ArrState", item.State)%>
                                                    </div>
                                                </div>--%>

                                                <%if (CPViewPage.UserPermissions.Approve)
                                                    {%>
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Duyệt giá bán</label>
                                                    <div class="radio-list">
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="ActivityPrice" <%= item.ActivityPrice ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                        </label>
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="ActivityPrice" <%= !item.ActivityPrice ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="portlet-title-sub">Duyệt</label>
                                                    <div class="radio-list">
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="Activity" <%= item.Activity ? "checked": "" %> value="1" />
                                                            <span class="outer"><span class="inner"></span></span><i>Có</i>
                                                        </label>
                                                        <label class="radioPure radio-inline">
                                                            <input type="radio" name="Activity" <%= !item.Activity ? "checked": "" %> value="0" />
                                                            <span class="outer"><span class="inner"></span></span><i>Không</i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <%} %>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="portlet box blue-steel">
                                        <div class="portlet-title">
                                            <div class="caption">SEO</div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="form-body">
                                                <div class="form-group ">
                                                    <label class="col-form-label">PageTitle:</label>
                                                    <input type="text" class="form-control title" name="PageTitle" value="<%=item.PageTitle %>" />
                                                    <span class="help-block text-primary">Ký tự đối ta: 200</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Description:</label>
                                                    <textarea class="form-control description" rows="5" name="PageDescription" placeholder="Nhập nội dung tóm tắt. Tối đa 400 ký tự"><%=item.PageDescription%></textarea>
                                                    <span class="help-block text-primary">Ký tự đối ta: 400</span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Keywords:</label>
                                                    <input type="text" class="form-control" name="PageKeywords" value="<%=item.PageKeywords %>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //setTimeout(function () { vsw_exec_cmd('[autosave][<%=model.RecordID%>]') }, 30000);

        //thuoc tinh
        function GetProperties(MenuID) {
            var ranNum = Math.floor(Math.random() * 999999);
            var dataString = "MenuID=" + MenuID + "&LangID=<%=model.LangID %>&ProductID=<%=item.ID%>&rnd=" + ranNum;

            $.ajax({
                url: "/{CPPath}/Ajax/GetProperties.aspx",
                type: "get",
                data: dataString,
                dataType: 'json',
                success: function (data) {
                    var content = data.Html;
                    $("#list-property").html(content);
                },
                error: function (status) { }
            });
        }

        if (<%=item.MenuID%> > 0) GetProperties('<%=item.MenuID%>');
        else GetProperties($('#MenuID').val());

    </script>
</form>
